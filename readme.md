/////////////////////Notification Service ( https://www.npmjs.com/package/angular-notifier )//////////////////

1>. IN RESPECTIVE COMPONENT

  import { NotifierService } from 'angular-notifier';
 
  @Component({
    // ...
  })
  export class MyAwesomeComponent {
 
    private readonly notifier: NotifierService;
 
    constructor( notifierService: NotifierService ) {
      this.notifier = notifierService;
    }
  }

2>. IN RESPECTIVE COMPONENT

   this.notifier.notify( 'success', 'You are awesome! I mean it!' );
   this.notifier.notify( 'error', 'You are awesome! I mean it!' );
 
   example notification types - success,error,default,info,warning

/////////////////////Spinner Service ( https://www.npmjs.com/package/ngx-spinner )///////////////////////////

1>. IN RESPECTIVE MODULE

  import { NgxSpinnerModule } from 'ngx-spinner';

  @NgModule({
    imports: [
        NgxSpinnerModule
    ]
  })

2>. IN RESPECTIVE COMPONENT

  import { NgxSpinnerService } from 'ngx-spinner';

  constructor(private spinner: NgxSpinnerService) {
     ......
  }

  this.spinner.show();
  this.spinner.hide();

  =====HTML=========

  <!-- <ngx-spinner bdColor="rgba(51,51,51,0.8)" size="medium" color="#fff" type="ball-scale-multiple">
    <p style="font-size: 20px; color: white">Loading...</p>
  </ngx-spinner> -->

  <!-- <ngx-spinner bdOpacity=0.9 bdColor="rgba(51,51,51,0.41)" size="medium" color="#ffffff" type="ball-8bits"  [fullScreen] = "false">
    <p style="color: white" > Loading... </p>
  </ngx-spinner> -->
  =============Making the Image Sprite==============
  We made make sprite image by combining 10 or more separate images in a single image
1> First of all, we will create the class .sprite that will load our sprite image. This is to avoid repetition, because all items share the same background-image.
    .sprite {
    background: url("images/mySprite.png") no-repeat;
  }
2> Now, we must define a class for each item we want to display.
  .className {
    width: 50px; /* Icon width */
    height: 50px; /* Icon height */
    display: inline-block; /* Display icon as inline block */
    background-position: 0 -200px; /* Icon background position in sprite */
}
3> Please find referance of  '''BasePatientInfoComponent''' css file.