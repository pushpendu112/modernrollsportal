import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    title = 'Modern Rolls';
    constructor(private titleService: Title,
        private meta: Meta) {
    }

    ngOnInit() {
        this.titleService.setTitle(this.title);
        this.meta.addTag({ name: 'keywords', content: 'Modern Rolls' });
        this.meta.addTag({ name: 'keywords', content: 'Modern Forgings And Alloys industries' });
        this.meta.addTag({ name: 'keywords', content: 'Rolls & Forging' });
        this.meta.addTag({ name: 'description', content: "Modern Forgings And Alloys industries is Gujarat's leading company" });
        this.meta.addTag({ name: 'author', content: 'Trident Software solutions' });
        this.meta.addTag({ name: 'robots', content: 'index, follow' });
    }
}
