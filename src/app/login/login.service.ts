import { Injectable } from '@angular/core';
import {HttpService} from '../base/http.service';
import {AppConstants } from '../base/appconstants';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpservice: HttpService) {

   }

   getlogin(loginformvalues: any, ) {
    const reqbody = {
      userName : loginformvalues.userName,
      password: loginformvalues.password
    };
     return  this.httpservice.httpPostObservable(AppConstants.post_get_passport_public_login, reqbody, '');
  }
}
