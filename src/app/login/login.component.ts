import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TestDataService } from 'src/app/shared/services/test-data.service';
import { Title, Meta } from '@angular/platform-browser';

import * as CryptoJS from 'crypto-js';

import { NgxSpinnerService } from 'ngx-spinner';
import { first } from 'rxjs/operators';

import { routerTransition } from '../router.animations';
import { AuthenticationService } from '../shared/services';
import { LoginService } from '../login/login.service';
import { jsonpCallbackContext } from '@angular/common/http/src/module';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  userObject: any;
  Secret = 'i_have_some_small_master_secret_live_pin';

  // loginFormvalues = {
  //     userName: 'super_admin',
  //     password: 'super_admin'
  // };
  title = 'Modern Rolls | Login';
  loginFormvalues = {
    userName: 'harish',
    password: '9177027432'
  };
  wrongEntry = false;
  type = 'password';
  showPass = false;

  constructor(
    public router: Router,
    private ngZone: NgZone,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private authenticationService: AuthenticationService,
    private loginservice: LoginService,
    private logInparent: TestDataService,
    private titleService: Title,
    private meta: Meta


  ) {
    // redirect to home if already logged in
    // if (this.authenticationService.currentUserValue) {
    //     this.ngZone.run(() => this.router.navigate(['/dashboard']));
    // }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.titleService.setTitle(this.title);
    this.meta.addTag({ name: 'keywords', content: 'Modern Rolls' });
    this.meta.addTag({ name: 'keywords', content: 'Modern Forgings And Alloys industries' });
    this.meta.addTag({ name: 'keywords', content: 'Rolls & Forging' });
    this.meta.addTag({ name: 'description', content: "Modern Forgings And Alloys industries is Gujarat's leading company" });
    this.meta.addTag({ name: 'author', content: 'Trident Software solutions' });
    this.meta.addTag({ name: 'robots', content: 'index, follow' });

  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onLoggedin() {
    this.submitted = true;
    this.spinner.show();
    // console.log(this.loginForm.value);

    if (this.loginForm.invalid) {
      this.spinner.hide();
      return;
    }
    // this.router.navigateByUrl('dashboard');
    const userObj = this.loginForm.value;
    const loginObj = {
      userName: userObj.userName.trim(),
      password: userObj.password
    }
    this.loginservice.getlogin(loginObj).subscribe(data => {
      this.userObject = data;
      // console.log('userobject', this.userObject);
      // console.log('this.userobject===' + JSON.stringify(this.userObject));
      if (this.userObject.jwtToken) {
        localStorage.setItem('jwttoken', this.encryptData(this.userObject.jwtToken));
      }

      // const centerrId = this.userObject.centerId;
      // localStorage.setItem('centerId', JSON.stringify(centerrId));
      //console.log(centerrId);


      const organisationId = this.userObject.orgId;
      localStorage.setItem('orgId', JSON.stringify(organisationId));
      const employeeId = this.userObject._id;
      localStorage.setItem('_id', JSON.stringify(employeeId));
      const assignRole = this.userObject.assignedRole;
      //console.log('assignRole====>',assignRole);

      localStorage.setItem('roleFeatureList', JSON.stringify(assignRole));

      this.router.navigate([`/dashboard/${assignRole[0].roleName}`]);
      const orgId = JSON.parse(localStorage.getItem('orgId'));
      this.logInparent.notifyOther({ option: 'call_loginData', value: data });
      //this.spinner.hide();
    },

      err => {
        this.spinner.hide();
        this.wrongEntry = true;
      }
    );
    // this.loading = true;
  }

  showPassword() {
    this.showPass = !this.showPass;

    if (this.showPass) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }


  encryptData(data) {
    try {
      return CryptoJS.AES.encrypt(JSON.stringify(data), this.Secret).toString();
    } catch (e) {
      console.log(e);
    }
  }

  decryptData(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, this.Secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }
}

