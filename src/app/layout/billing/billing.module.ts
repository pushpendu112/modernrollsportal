import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillingRoutingModule } from './billing-routing.module';
import { BillingComponent } from './billing.component';
import { BillingQueueComponent } from './components/billing-queue/billing-queue.component';
import { BillingDetailsComponent } from './components/billing-details/billing-details.component';
import {  DataTableModule } from 'primeng/datatable';

@NgModule({
  declarations: [BillingComponent, BillingQueueComponent, BillingDetailsComponent],
  imports: [
    CommonModule,
    BillingRoutingModule,
    DataTableModule
  ]
})
export class BillingModule { }
