import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TestDataService } from 'src/app/shared/services/test-data.service';
import { TabledataService } from 'src/app/shared/services';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-billing-queue',
  templateUrl: './billing-queue.component.html',
  styleUrls: ['./billing-queue.component.scss']
})
export class BillingQueueComponent implements OnInit {
  title = 'Modern Rolls | Billing Queuue';
  public reciveTableData: any = [];
  cols: any[];
  sortF: any;
  statusChange: any;

  @Output() valueChange = new EventEmitter();
  counter = true;
  constructor(private passBoolean: TestDataService,
    private tableDatas: TabledataService,
    private router: Router,
    private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.cols = [
      { field: 'patientName', header: 'Patient Name' },
      { field: 'patientMobile', header: 'Mobile Numbe' },
      { field: 'gender', header: 'Gender' },
      { field: 'status', header: 'Status' }
    ];
    // Reciving Table Data Values.
    this.tableDatas.getTableData().subscribe((data) => this.reciveTableData = data);
  }
  // valueChanged() { // You can give any function name
  //       // this.valueChange.emit(this.Counter);
  //       // alert('');
  //       this.passBoolean.notifyOther({ option: 'patient-queue', value: this.counter });

  // }

  onRowSelect(event) {
    console.log(event);
  }
  changeSort(event) {
    if (!event.order) {
      this.sortF = 'year';
    } else {
      this.sortF = event.field;
    }
  }
  selectedRowData(data) {
    // console.log(data);

    this.statusChange = data.patientMobile;
  }

  navigateComponent() {
  this.router.navigate(['/dashboard/Billing/BillingDetails']);
  }

}
