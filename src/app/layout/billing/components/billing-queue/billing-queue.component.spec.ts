import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingQueueComponent } from './billing-queue.component';

describe('BillingQueueComponent', () => {
  let component: BillingQueueComponent;
  let fixture: ComponentFixture<BillingQueueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingQueueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
