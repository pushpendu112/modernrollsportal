import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-billing-details',
  templateUrl: './billing-details.component.html',
  styleUrls: ['./billing-details.component.scss']
})
export class BillingDetailsComponent implements OnInit {
  title = 'Modern Rolls | Details';

  constructor(private titleService: Title) { }

  ngOnInit() {
        this.titleService.setTitle(this.title);
  }

}
