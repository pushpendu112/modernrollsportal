import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.scss']
})
export class BillingComponent implements OnInit {
  title = 'Modern Rolls | Billing';
  constructor(private titleService: Title, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.titleService.setTitle(this.title);
  	this.spinner.hide();
  }

}
