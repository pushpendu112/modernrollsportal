import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillingDetailsComponent } from './components/billing-details/billing-details.component';
import { BillingComponent } from './billing.component';
import { BillingQueueComponent } from './components/billing-queue/billing-queue.component';

const routes: Routes = [
  {
    path: '', component: BillingComponent,
    children: [
      {
        path: 'Billing Queue',
        component: BillingQueueComponent
      },
      {
        path: 'BillingDetails',
        component: BillingDetailsComponent
      },
      {
        path: '',
        redirectTo: 'Billing Queue'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingRoutingModule { }
