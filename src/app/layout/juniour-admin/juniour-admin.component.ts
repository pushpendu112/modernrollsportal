import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-juniour-admin',
  templateUrl: './juniour-admin.component.html',
  styleUrls: ['./juniour-admin.component.scss']
})
export class JuniourAdminComponent implements OnInit {

  constructor(private spinner: NgxSpinnerService) { }

  ngOnInit() {
  	this.spinner.hide();
  }

}
