import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import  {JuniourAdminComponent } from './juniour-admin.component';
import  {EmployeeCreationComponent } from './components/employee-creation/employee-creation.component';


const routes: Routes = [
 {
    path: '', component: JuniourAdminComponent,
    children: [
      {
        path: 'Employee Creation',
        component: EmployeeCreationComponent
      },
      {
        path: '',
        redirectTo: 'Employee Creation'
      }
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JuniourAdminRoutingModule { }
