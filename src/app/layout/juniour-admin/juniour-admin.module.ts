import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DataTableModule} from 'primeng/datatable';
import { AccordionModule } from 'primeng/accordion';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { JuniourAdminRoutingModule } from './juniour-admin-routing.module';
import { JuniourAdminComponent } from './juniour-admin.component';
import { EmployeeCreationComponent } from './components/employee-creation/employee-creation.component';

@NgModule({
  declarations: [JuniourAdminComponent, EmployeeCreationComponent],
  imports: [
    CommonModule,
    JuniourAdminRoutingModule,
    DataTableModule,
    AccordionModule,
    NotifierModule,
    Ng2SearchPipeModule,
    FormsModule,
    ReactiveFormsModule



  ]
})
export class JuniourAdminModule { }
