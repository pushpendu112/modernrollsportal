import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JuniourAdminComponent } from './juniour-admin.component';

describe('JuniourAdminComponent', () => {
  let component: JuniourAdminComponent;
  let fixture: ComponentFixture<JuniourAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JuniourAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JuniourAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
