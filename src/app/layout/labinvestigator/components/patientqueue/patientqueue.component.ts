import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { TestDataService } from 'src/app/shared/services/test-data.service';
import { TabledataService } from 'src/app/shared/services';
declare var $: any;   // not required

import * as CryptoJS from 'crypto-js';
import { LabinvestigatorService } from '../../labinvestigator.service';
import { JuniorDoctorService } from 'src/app/layout/junior-doctor/junior-doctor.service';

@Component({
  selector: 'app-patientqueue',
  templateUrl: './patientqueue.component.html',
  styleUrls: ['./patientqueue.component.scss']
})
export class PatientqueueComponent implements OnInit {

  public reciveTableData: any = [];
  cols: any[];
  sortF: any;
  statusChange: any;

  ptName;
  ptTime;
  ptDate;

  secret = 'i_have_some_small_master_secret_live_pin';
  isDisabled = false;
  pocenterId: any;
  userToken: any;
  getjwttoken: any;
  existPatientObjectQueue: any;

  @Output() valueChange = new EventEmitter();
  counter = true;
  constructor(private passBoolean: TestDataService,
    private tableDatas: TabledataService,
    private router: Router,
    private labinvestigatorService: LabinvestigatorService,
    private juniorDoctorService: JuniorDoctorService ) { }

  ngOnInit() {
    this.cols = [
      { field: 'firstName', header: 'Patient Name' },
      { field: 'mobileNumber', header: 'Mobile Numbe' },
      { field: 'gender', header: 'Gender' },
      { field: 'presentStatus', header: 'Status' }
    ];
    // Reciving Table Data Values.
    // this.tableDatas.getTableData().subscribe((data) => this.reciveTableData = data);
    this.getExistingPatientQueuees();
    this.getjwttoken = localStorage.getItem('jwttoken');
    this.userToken = this.decryptData(this.getjwttoken);
  }
  // valueChanged() { // You can give any function name
  //       // this.valueChange.emit(this.Counter);
  //       // alert('');
  //       this.passBoolean.notifyOther({ option: 'patient-queue', value: this.counter });

  // }

  onRowSelect(event) {
    console.log(event);
  }
  changeSort(event) {
    if (!event.order) {
      this.sortF = 'year';
    } else {
      this.sortF = event.field;
    }
  }
  selectedRowData(data) {
    // console.log(data);

    this.statusChange = data.patientMobile;
  }

  navigateComponent() {
  this.router.navigate(['/dashboard/Lab Investigator/labReports']);
  }
  getExistingPatientQueuees() {

    this.pocenterId = JSON.parse(localStorage.getItem('centerId'));

    const reqbody = this.pocenterId;
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });

    this.juniorDoctorService.getPatientQueueForPoc(reqbody, headers).subscribe(data => {

      this.existPatientObjectQueue = data.patientQueueData;
      this.existPatientObjectQueue.forEach(status => {
        if (status.presentQueueType === 0) {
          this.isDisabled = false;
          status.presentStatus = 'Registration Queue';
        } else if (status.presentQueueType === 1) {
          this.isDisabled = true;
          status.presentStatus = 'Investigation Queue';
        } else if (status.presentQueueType === 2) {
          status.presentStatus = 'Doctor Queue';
        } else if (status.presentQueueType === 3) {
          status.presentStatus = 'Manager Queue';
        } else if (status.presentQueueType === 4) {
          status.presentStatus = 'Diagnostics Queue';
        } else if (status.presentQueueType === 5) {
          status.presentStatus = 'Pharmacy Queue';
        } else if (status.presentQueueType === 6) {
          status.presentStatus = 'Medicine Issued';
        }
      });
      console.log(this.existPatientObjectQueue);
    });
  }
  decryptData(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, this.secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }
  // goToDoctor(data) {
  //   console.log(data);
  //   const statusBody = {
  //     queueId: data.queueId,
  //     queueEntryDateAndTime: data.queueEntryDateAndTime,
  //     arrivalDateAndTime: data.arrivalDateAndTime,
  //     presentQueueType: data.presentQueueType,
  //     nextQueueType: 1,
  //   };
  //   // data.patientQueueData.forEach(status => {
  //   //   if (status.presentQueueType !== 0) {
  //   //     this.buttonDisabled = true;
  //   //   }
  //   // });

  //   const headers = new Headers(
  //     {
  //       'accept': 'application/json',
  //       'x-access-token': this.userToken
  //     });

  //   this.labinvestigatorService.updatePatientQueue(statusBody, headers).subscribe(updateStatus => {
  //     // alert('');
  //     console.log(updateStatus);
  //     this.getExistingPatientQueuees();
  //   });

  // }



 valueChanged(data) { // You can give any function name
    console.log(data);
    this.ptName = data.firstName + ' ' + data.lastName;
    this.ptTime = data.arrivalDateAndTime;
    this.ptDate = data.queueEntryDateAndTime;
    const queueId = data.queueId;
    const queueIdEncrypt = this.encryptData(queueId);
    localStorage.setItem('queId', queueIdEncrypt);
    // this.valueChange.emit(this.Counter);
    const patinetId = data.patientId;
    const patinetID = this.encryptData(patinetId);
    localStorage.setItem('patientId', patinetID);
    // alert('');
    this.passBoolean.notifyOther({ option: 'patient-queue', value: data });
    // this.passBoolean.notifyOther({ option: 'pt-data', value: data });

}

onPayment() {
  this.router.navigate(['/dashboard/Lab Investigator/billingReciept']);
  // this.router.navigate(['dashboard/Lab Investigator/billing-receipt']);
  // $('#exampleModal').modal('hide');
}
encryptData(data) {
  try {
    return CryptoJS.AES.encrypt(JSON.stringify(data), this.secret).toString();
  } catch (e) {
    console.log(e);
  }
}
}


// /dashboard/billingreceipt/billingReceipt
