import { TestBed } from '@angular/core/testing';

import { LabinvestigatorService } from './labinvestigator.service';

describe('LabinvestigatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LabinvestigatorService = TestBed.get(LabinvestigatorService);
    expect(service).toBeTruthy();
  });
});
