import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PatientqueueComponent } from './components/patientqueue/patientqueue.component';
import { LabinvestigatorComponent } from './labinvestigator.component';
import { LabReportsComponent } from './components/lab-reports/lab-reports.component';
// import { BillingReceiptComponent } from './components/billing-receipt/billing-receipt.component';
import { BillingReceiptComponent } from './components/billing-receipt/billing-receipt.component';

const routes: Routes = [
  {
    path: '', component: LabinvestigatorComponent,
    children: [
      {
        path: 'Diagnostics Queue',
        component: PatientqueueComponent
      },
      {
        path: 'labReports',
        component: LabReportsComponent
      },
      {
        path: 'billingReciept',
        component: BillingReceiptComponent
      },
      {
        path: '',
        redirectTo: 'Diagnostics Queue'
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LabinvestigatorRoutingModule { }
