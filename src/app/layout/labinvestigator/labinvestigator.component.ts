import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-labinvestigator',
  templateUrl: './labinvestigator.component.html',
  styleUrls: ['./labinvestigator.component.scss']
})
export class LabinvestigatorComponent implements OnInit {

  constructor(private spinner: NgxSpinnerService) { }

  ngOnInit() {
  	this.spinner.hide();
  }

}
