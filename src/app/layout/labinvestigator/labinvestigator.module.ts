import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DataTableModule} from 'primeng/datatable';
import { FormsModule } from '@angular/forms';

import { LabinvestigatorRoutingModule } from './labinvestigator-routing.module';
import { LabinvestigatorComponent } from './labinvestigator.component';
import { PatientqueueComponent } from './components/patientqueue/patientqueue.component';
import { LabReportsComponent } from './components/lab-reports/lab-reports.component';
import { BillingReceiptComponent } from './components/billing-receipt/billing-receipt.component';

@NgModule({
  declarations: [LabinvestigatorComponent, PatientqueueComponent, LabReportsComponent, BillingReceiptComponent],
  imports: [
    CommonModule,
    LabinvestigatorRoutingModule,
    DataTableModule,
    FormsModule
  ]
  ,
  exports: [LabinvestigatorComponent]
})
export class LabinvestigatorModule { }
