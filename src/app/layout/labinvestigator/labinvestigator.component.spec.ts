import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabinvestigatorComponent } from './labinvestigator.component';

describe('LabinvestigatorComponent', () => {
  let component: LabinvestigatorComponent;
  let fixture: ComponentFixture<LabinvestigatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabinvestigatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabinvestigatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
