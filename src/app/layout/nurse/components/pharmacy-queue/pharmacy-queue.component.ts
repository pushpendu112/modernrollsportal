import { Component, OnInit } from '@angular/core';
import { NurseService } from '../../nurse.service';
import { typeWithParameters } from '@angular/compiler/src/render3/util';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import { NgIf } from '@angular/common';
import * as CryptoJS from 'crypto-js';
declare var $: any;   // not required
import { Router } from '@angular/router';
// import { TabledataService } from 'src/app/shared/services';


@Component({
  selector: 'app-pharmacy-queue',
  templateUrl: './pharmacy-queue.component.html',
  styleUrls: ['./pharmacy-queue.component.scss']
})
export class PharmacyQueueComponent implements OnInit {
    private readonly notifier: NotifierService;

  public reciveTableData: any = [];

  cols: any[];
  sortF: any;
  statusChange: any;
  patientQueueObject: any;


  Secret = 'i_have_some_small_master_secret_live_pin';

  pocenterId: any;
  existPatientObjectQueue: any;

  isDisabled = false;




  lorgId: any;
  lempId: any;
  patientIdd: any;
  lcenterId: any;
  getjwttoken: any;
  abcd: any;
  userToken: string;

  constructor(private nurseService: NurseService,
    private router: Router,
     private spinner: NgxSpinnerService,
    private notifierService: NotifierService,) {
    this.notifier = notifierService; 
  }

  ngOnInit() {

    this.cols = [
      { field: 'firstName', header: 'Patient Name' },
      { field: 'mobileNumber', header: 'Mobile Numbe' },
      { field: 'gender', header: 'Gender' },
      { field: 'presentStatus', header: 'Status' },
    ];
    // Reciving Table Data Values.
    // this.tableDatas.getTableData().subscribe((data) => this.reciveTableData = data);
    // this.CreatePatientDetails();

    this.getExistingPatientQueuees();
    this.getjwttoken = localStorage.getItem('jwttoken');
    this.userToken = this.decryptData(this.getjwttoken);

  }



  onRowSelect(event) {
    console.log(event);
  }
  changeSort(event) {
    if (!event.order) {
      this.sortF = 'year';
    } else {
      this.sortF = event.field;
    }
  }
  selectedRowData(data) {
    // // console.log(data);

    // this.statusChange = data.patientMobile;
  }



  // CreatePatientDetails() {

  //    const PatientQueue = {
  //     patientId: '5d0b26f6599ed2479a6e1646',
  //     orgId: JSON.parse(localStorage.getItem('orgId')),
  //     centerId: JSON.parse(localStorage.getItem('centerId')),
  //     presentQueueType: 0,
  //     durationList: []

  //    };
  //   // this.abcd =  JSON.parse(localStorage.getItem(\'orgId\'))'\,
  //   //  console.log('zzzzzzzzzzzzzz', this.abcd );
  //   //  const headers = new Headers(
  //   //   {
  //   //     'accept': 'application/json',
  //   //     'x-access-token': this.userToken
  //   //   });

  //   this.nurseService.createPatientQueue(PatientQueue).subscribe(data => {
  //     this.patientQueueObject = data;
  //     console.log('aaaaaaaaaaaaaaaaaaaaaa', data);
  //     // alert('iam in creating position');
  //     // console.log('sssssssssssssssssss' + JSON.stringify(this.patientQueueObject));
  //   });
  // }

  getExistingPatientQueuees() {

    this.pocenterId = JSON.parse(localStorage.getItem('centerId'));

    const reqbody = this.pocenterId;
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });
    this.spinner.show();
    this.nurseService.getPatientQueueForPoc(reqbody, headers).subscribe(data => {
      this.spinner.hide();
      this.existPatientObjectQueue = data.patientQueueData;
      // console.log( 'this.existPatientObjectQueue',  this.existPatientObjectQueue);

      this.existPatientObjectQueue.forEach(status => {
        if (status.presentQueueType === 0) {
          status.presentStatus = 'Registration Queue';

        } else if (status.presentQueueType === 1) {
          status.presentStatus = 'Investigation Queue';
          //  alert('iam in Investigation Queue');
        } else if (status.presentQueueType === 2) {
          status.presentStatus = 'Doctor Queue';
        } else if (status.presentQueueType === 3) {
          status.presentStatus = 'Manager Queue';
        } else if (status.presentQueueType === 4) {
          status.presentStatus = 'Diagnostics Queue';
        } else if (status.presentQueueType === 5) {
          status.presentStatus = 'Pharmacy Queue';
        } else if (status.presentQueueType === 6) {
          status.presentStatus = 'Medicine Issued';
        }
      });
      console.log(this.existPatientObjectQueue);
   },err=>{
              this.spinner.hide();
              this.notifier.notify('error', 'Something went wrong please try again! ')
              console.log(err);
    });
  }
  decryptData(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, this.Secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }


  //   onSubmit() {
  //     this.lorgId = JSON.parse(localStorage.getItem('orgId'));
  //     this.lempId = JSON.parse(localStorage.getItem('_id'));
  //     this.lcenterId = JSON.parse(localStorage.getItem('centerId'));
  //     this.patientIdd = '5d04fa04273d7114cced5f00';

  //     this.nurseService.get(bidy, header).
  //   // this.patientForm.get('orgId').setValue(this.lorgId);
  //   // this.patientForm.get('employeeId').setValue(lempId);
  // }

  goToDoctor(data) {
    console.log(data);
    const statusBody = {
      queueId: data.queueId,
      queueEntryDateAndTime: data.queueEntryDateAndTime,
      arrivalDateAndTime: data.arrivalDateAndTime,
      presentQueueType: data.presentQueueType,
      nextQueueType: 1,
    };
    // data.patientQueueData.forEach(status => {
    //   if (status.presentQueueType !== 0) {
    //     this.buttonDisabled = true;
    //   }
    // });

    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });
    this.spinner.show();
    this.nurseService.updatePatientQueue(statusBody, headers).subscribe(updateStatus => {
      // alert('');
      //console.log(updateStatus);
      this.spinner.hide();
      this.getExistingPatientQueuees();
    },err=>{
              this.spinner.hide();
              this.notifier.notify('error', 'Something went wrong please try again! ')
              console.log(err);
    });

  }

  onPayment() {
    this.router.navigate(['/dashboard/billingreceipt/billingReceipt']);
    $('#exampleModal').modal('hide');

  }

}

