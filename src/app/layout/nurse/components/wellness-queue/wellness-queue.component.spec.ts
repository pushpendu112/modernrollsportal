import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WellnessQueueComponent } from './wellness-queue.component';

describe('WellnessQueueComponent', () => {
  let component: WellnessQueueComponent;
  let fixture: ComponentFixture<WellnessQueueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WellnessQueueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WellnessQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
