import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnboardingQueueComponent } from './onboarding-queue.component';

describe('OnboardingQueueComponent', () => {
  let component: OnboardingQueueComponent;
  let fixture: ComponentFixture<OnboardingQueueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnboardingQueueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnboardingQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
