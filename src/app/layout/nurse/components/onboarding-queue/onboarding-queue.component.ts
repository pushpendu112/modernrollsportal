import { Component, OnInit, Input } from '@angular/core';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-onboarding-queue',
  templateUrl: './onboarding-queue.component.html',
  styleUrls: ['./onboarding-queue.component.scss']
})
export class OnboardingQueueComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
    private calendar: NgbCalendar,
    private router: Router) { }
    @Input() PData: any;

  bmiFeet = ['Select Feet', 1, 2, 3, 4, 5, 6, 7, 8, 9];
  bmiInch = ['Select Inch', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
  bmiHeightOption = ['feet-Inch', 'cms'];
  bmiWeightOption = ['Kgs', 'lbs'];
  maritalStatusOption = ['Married', 'Un Married'];
  children = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
  dietOption = ['Select', 'Vegetarian', 'Mixed'];
  homeopathicMedicineOption = ['Select', 'Used', 'Not Used'];
  bmiValue: any;
  selectedFeet = true;
  selectedCms = false;
  feetValue: Number;
  inchValue: Number;
  testHight: any;

  selected = 'dob';
  dobSelected = true;
  ageSelected = false;

  maleGenderActive = false;
  feMaleGenderActive = false;
  otherGenderActive = false;
  model: NgbDateStruct;

  private selectedLink = 'DOB';
  selectedLink2 = false;
  selectedLink3 = false;
  selectedLink4 = false;

  isDisabled1 = true;



  myForm: FormGroup;
  bmiWeight = '';
  bmiHeight = '';
  bmiCalcuation = '';
  ngOnInit() {
    this.myForm = this.formBuilder.group({
      bmiWeight: [this.bmiWeight],
      bmiHeight: [this.bmiHeight],
      bmiCalcuation: [this.bmiCalcuation]
    });




    this.myForm.controls['bmiHeight'].valueChanges.subscribe(value => {
        this.testHight = value;
     //  this.myForm.controls['bmiCalcuation'].setValue(value);
    });
    this.myForm.controls['bmiWeight'].valueChanges.subscribe(weight => {
      console.log(  this.testHight);
      const heightCal = this.testHight / 100 * this.testHight / 100;
         // heightCal = heightCal * 0.025;
         const newValue = weight / heightCal;
          console.log('newValue');
          console.log(newValue);

            this.myForm.controls['bmiCalcuation'].setValue(Math.round(newValue));
      // this.myForm.controls['bmiCalcuation'].setValue(weight / (this.myForm.value.bmiWeight));
    });
  }

  onChangeHeightOption(heightOption) {
    if (heightOption === 'feet-Inch') {
      this.selectedFeet = true;
      this.selectedCms = false;
    } else if (heightOption === 'cms') {
      this.selectedCms = true;
      this.selectedFeet = false;
    }
  }

  onChangeFeetOption(feetOption) {
    this.feetValue = feetOption;
  }
  onChangeInchOption(inchOption) {
    this.inchValue = inchOption;
  }

  onHeightChange(e) {
    //  const heigh = e.currentTarget.value;
    // this.myForm.controls['bmiCalcuation'].setValue(heigh * this.myForm.value.amount);
  }
  onWeightChange(e) {
    // const Weight = e.currentTarget.value;
    // this.myForm.controls['bmiCalcuation'].setValue(Weight / (this.myForm.value.bmiHeight * 12) * 0.25);

  }

}
