import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NurseComponent } from './nurse.component';
import { PharmacyQueueComponent } from './components/pharmacy-queue/pharmacy-queue.component';
import { WellnessQueueComponent } from './components/wellness-queue/wellness-queue.component';
import { ReportsComponent } from './components/reports/reports.component';
import { MedicinesComponent } from './components/medicines/medicines.component';
import { OnboardingQueueComponent } from './components/onboarding-queue/onboarding-queue.component';

const routes: Routes = [
  {
    path: '', component: NurseComponent,
    children: [
      {
        path: 'Registration Queue',
        component: PharmacyQueueComponent
      },
      {
        path: 'Wellness Queue',
        component: WellnessQueueComponent
      },
      {
        path: 'Reports',
        component: ReportsComponent
      },
      {
        path: 'medicines',
        component: MedicinesComponent
      },
      {
        path: 'Onboarding Queue',
        component: OnboardingQueueComponent
      },
      {
        path: '',
        redirectTo: 'Registration Queue'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NurseRoutingModule { }
