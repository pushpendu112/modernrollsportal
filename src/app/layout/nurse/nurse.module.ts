import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NurseRoutingModule } from './nurse-routing.module';
import { PharmacyQueueComponent } from './components/pharmacy-queue/pharmacy-queue.component';
import { WellnessQueueComponent } from './components/wellness-queue/wellness-queue.component';
import { ReportsComponent } from './components/reports/reports.component';
import { MedicinesComponent } from './components/medicines/medicines.component';
import { NurseComponent } from './nurse.component';
import { OnboardingQueueComponent } from './components/onboarding-queue/onboarding-queue.component';
import { BasePatientInfoModule } from 'src/app/base-patient-info/base-patient-info.module';
import { DataTableModule } from 'primeng/datatable';

@NgModule({
  declarations: [PharmacyQueueComponent, WellnessQueueComponent, ReportsComponent,
    MedicinesComponent, NurseComponent, OnboardingQueueComponent],
  imports: [
    CommonModule,
    NurseRoutingModule,
    BasePatientInfoModule,
    DataTableModule,
  ],
  exports: [NurseComponent, OnboardingQueueComponent, PharmacyQueueComponent]
})
export class NurseModule { }
