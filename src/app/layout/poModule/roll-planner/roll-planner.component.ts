import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { PartyService } from 'src/app/shared/services/party.service';
import * as CryptoJS from 'crypto-js';
import { AppConstants } from '../../../base/appconstants';
import { NgxSpinnerService } from 'ngx-spinner';
import { QueueStatus } from '../../../models/queueStatus';
import { Ng2SearchPipe } from 'ng2-search-filter';
import { Observable } from 'rxjs';
import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
declare var jquery: any;   // not required
declare var $: any;   // not required
@Component({
    selector: 'app-roll-planner',
    templateUrl: './roll-planner.component.html',
    styleUrls: ['./roll-planner.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class RollPlannerComponent implements OnInit {

    dispatchedTime: any;
    isDispacthed: boolean = false;
    fromDate: any;
    toDate: any;
    poProductList: any = [];
    statusQueue: any = [];
    listCount: any;
    userToken: any;
    pocols: any[];
    dcols: any[];
    duplicatepoProductList: any = [];
    tempduplicatepoProductList: any = [];
    history: boolean = false;
    image: any;
    filteredList: any = [];
    moveCast: boolean;
    search: any;
    filter: Ng2SearchPipe;
    colorMap = {
        "ADAMITS": "red",
        "CHILLS": "blue",
        "SG-IRON": "green",
        "SG-ACC": "#c4c233"
    }
    searchtype: any = "0";
    basicSearchTerm: any = "";

    constructor(private partyService: PartyService, private spinner: NgxSpinnerService) {
        this.filter = new Ng2SearchPipe();
        this.search = '';
        this.fromDate = '';
        this.toDate = '';
    }

    ngOnInit() {
        const getjwt = localStorage.getItem('jwttoken');
        this.userToken = this.conversionDecrypt(getjwt);
        // this.fetchAllPoProduct(false);
        this.pocols = [
            { field: 'idList.displayId', header: 'Roll Id', width: '5%' },
            // { field: 'grade', header: 'Grade', width: '8%' },
            { field: 'producttype', header: 'Prod. Type', width: '8%', color: 'yellow' },
            { field: 'itemtype', header: 'Item Type', width: '8%' },
            { field: 'partyData.partyname', header: 'PartyName', width: '8%' },
            { field: 'weightperpiece', header: 'Weight/Piece', width: '8%' },
            { field: 'partyData.poid', header: 'POID', width: '7%' },
            // { field: 'idList.currentQueue', header: 'Status', width: '7%' },
            { field: 'reqhardness', header: 'Req. Hard', width: '8%' },
            { field: 'dimensionFormString', header: 'Dimension', width: '7%' },
            { field: 'remark', header: 'Remark', width: '7%' },
        ];

        this.dcols = [
            { field: 'queue', header: 'Status', width: '10%' },
            { field: 'startTime', header: 'StartTime', width: '10%' },
            { field: 'endTime', header: 'EndTime', width: '10%' },
            { field: 'durationString', header: 'Duration', width: '10%' },
        ];

    }
    conversionDecrypt(decrypt) {
        try {
            const bytes = CryptoJS.AES.decrypt(decrypt, AppConstants.secret);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return decrypt;
        } catch (e) {
            console.log(e);
        }
    }

    searchProduct(isCheck) {
        let filteredList = [];
        let id;
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        if (this.history == false) {
            id = 1;
        }
        else {
            id = 0;
        }

        this.duplicatepoProductList = [];
        this.tempduplicatepoProductList = [];
        this.poProductList = [];
        this.spinner.show();
        if (id == 1) {
            let x = {
                currentQueue: "1",
                searchTerm: this.basicSearchTerm,
                searchtype: this.searchtype
            };
            // console.log("x ---" + JSON.stringify(x));
            if (this.basicSearchTerm == "") {
                this.spinner.hide();
                return;
            }
            this.partyService.searchPOProductByQueueStatus(x, headers).subscribe(data => {
                this.poProductList = data.pOProductData;
                if (this.poProductList.length < 1) {
                    this.spinner.hide();
                }
                // if (this.history == false) {
                // this.poProductList.forEach(element => {
                // if (element.idList.currentQueue == 1) {
                this.duplicatepoProductList = JSON.parse(JSON.stringify(this.poProductList));
                if (this.search.length == 0) {
                    this.listCount = this.duplicatepoProductList.length;
                }
                else {
                    filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                    this.listCount = filteredList.length;
                }

                this.duplicatepoProductList.forEach(element => {
                    if (element.processtype == 0) {
                        element.processtype = 'FORGING';
                    }
                    else {
                        element.processtype = 'ROLL CASTING';
                    }
                    if (element.itemtype == 0) {
                        element.itemtype = 'ROLLS';
                    }
                    else if (element.itemtype == 1) {
                        element.itemtype = 'STEP ROLLS';
                    }
                    else if (element.itemtype == 2) {
                        element.itemtype = 'REEL';
                    }
                    else {
                        element.itemtype = 'OTHER';
                    }
                    if (element.uom == 0) {
                        element.uom = 'Per-KG';
                    }
                    else if (element.uom == 1) {
                        element.uom = 'MT';
                    }
                    else {
                        element.uom = 'Per-Piece';
                    }
                    if (element.idList.currentQueue == '1') {
                        element.idList.currentQueue = 'Planning';
                    }
                    else if (element.idList.currentQueue == '2') {
                        element.idList.currentQueue = 'Roll Casting';
                    }
                    else if (element.idList.currentQueue == '0') {
                        element.idList.currentQueue = 'Rejected';
                    }
                    else if (element.idList.currentQueue == '3') {
                        element.idList.currentQueue = 'QualityCheck';
                    }
                    else if (element.idList.currentQueue == '4') {
                        element.idList.currentQueue = 'Machining';
                    }
                    else if (element.idList.currentQueue == '5') {
                        element.idList.currentQueue = 'Inspection';
                    }
                    else if (element.idList.currentQueue == '6') {
                        element.idList.currentQueue = 'GrovingCentreFinishing';
                    }
                    else if (element.idList.currentQueue == '7') {
                        element.idList.currentQueue = 'BearingShapperFinishing';
                    }
                    else if (element.idList.currentQueue == '8') {
                        element.idList.currentQueue = 'GrovingCentreFinal';
                    }
                    else if (element.idList.currentQueue == '9') {
                        element.idList.currentQueue = 'BearingShapperFinal';
                    }
                    else if (element.idList.currentQueue == '10' && element.idList.isDispacthed == true) {
                        element.idList.currentQueue = 'Dispatched';
                    }
                    else if (element.idList.currentQueue == '10' && element.idList.isDispacthed == false) {
                        element.idList.currentQueue = 'FinalInspection';
                    }
                    // else if (element.idList.currentQueue == '13') {
                    //     element.idList.currentQueue = 'PackagingFinish';
                    // }
                    // else if (element.idList.currentQueue == '14') {
                    //     element.idList.currentQueue = 'FinalReport';
                    // }
                    let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                    element['dimensionFormString'] = toString(element.dimensionForm);

                });
                if (isCheck == true) {
                    this.fromDate = '';
                    this.toDate = '';
                }
                if (this.fromDate != '' && this.toDate != '') {
                    this.onDateSelection();
                }
                this.spinner.hide();
            })
        } else {
            let x = {
                currentQueue: "1",
                searchTerm: this.basicSearchTerm,
                searchtype: this.searchtype
            };
            this.partyService.fetchAllPoProductsForHistory(x, headers).subscribe(data => {
                this.poProductList = data.pOProductData;
                if (this.poProductList.length < 1) {
                    this.spinner.hide();
                }
                // console.log('enetr' + JSON.stringify(this.poProductList));
                // if (this.poProductList != undefined)
                this.poProductList.forEach(element => {

                    this.duplicatepoProductList.push(element);
                    // console.log('enetr' + JSON.stringify(this.duplicatepoProductList));
                    if (this.search.length == 0) {
                        this.listCount = this.duplicatepoProductList.length;
                    }
                    else {
                        filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                        this.listCount = filteredList.length;
                    }
                    this.duplicatepoProductList.forEach(element => {
                        if (element.processtype == 0) {
                            element.processtype = 'FORGING';
                        }
                        else {
                            element.processtype = 'ROLL CASTING';
                        }
                        if (element.itemtype == 0) {
                            element.itemtype = 'ROLLS';
                        }
                        else if (element.itemtype == 1) {
                            element.itemtype = 'STEP ROLLS';
                        }
                        else if (element.itemtype == 2) {
                            element.itemtype = 'REEL';
                        }
                        else {
                            element.itemtype = 'OTHER';
                        }
                        if (element.uom == 0) {
                            element.uom = 'Per-KG';
                        }
                        else if (element.uom == 1) {
                            element.uom = 'MT';
                        }
                        else {
                            element.uom = 'Per-Piece';
                        }
                        if (element.idList.currentQueue == '1') {
                            element.idList.currentQueue = 'Planning';
                        }
                        else if (element.idList.currentQueue == '2') {
                            element.idList.currentQueue = 'Roll Casting';
                        }
                        else if (element.idList.currentQueue == '0') {
                            element.idList.currentQueue = 'Rejected';
                        }
                        else if (element.idList.currentQueue == '3') {
                            element.idList.currentQueue = 'QualityCheck';
                        }
                        else if (element.idList.currentQueue == '4') {
                            element.idList.currentQueue = 'Machining';
                        }
                        else if (element.idList.currentQueue == '5') {
                            element.idList.currentQueue = 'Inspection';
                        }
                        else if (element.idList.currentQueue == '6') {
                            element.idList.currentQueue = 'GrovingCentreFinishing';
                        }
                        else if (element.idList.currentQueue == '7') {
                            element.idList.currentQueue = 'BearingShapperFinishing';
                        }
                        else if (element.idList.currentQueue == '8') {
                            element.idList.currentQueue = 'GrovingCentreFinal';
                        }
                        else if (element.idList.currentQueue == '9') {
                            element.idList.currentQueue = 'BearingShapperFinal';
                        }
                        else if (element.idList.currentQueue == '10' && element.idList.isDispacthed == true) {
                            element.idList.currentQueue = 'Dispatched';
                        }
                        else if (element.idList.currentQueue == '10' && element.idList.isDispacthed == false) {
                            element.idList.currentQueue = 'FinalInspection';
                        }
                        // else if (element.idList.currentQueue == '13') {
                        //     element.idList.currentQueue = 'PackagingFinish';
                        // }
                        // else if (element.idList.currentQueue == '14') {
                        //     element.idList.currentQueue = 'FinalReport';
                        // }
                        let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                        element['dimensionFormString'] = toString(element.dimensionForm);

                    });
                    if (isCheck == true) {
                        this.fromDate = '';
                        this.toDate = '';
                    }
                    if (this.fromDate != '' && this.toDate != '') {
                        this.onDateSelection();
                    }

                });
                this.spinner.hide();
            });
        }
    }

    fetchAllPoProduct(isCheck) {
        let filteredList = [];
        this.spinner.show();
        let id;
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        if (this.history == false) {
            id = 1;
        }
        else {
            id = 0;
        }

        this.duplicatepoProductList = [];
        this.tempduplicatepoProductList = [];
        this.poProductList = [];
        if (id == 1) {
            this.partyService.getAllPOProductByQueueStatus(headers, id).subscribe(data => {
                this.poProductList = data.pOProductData;
                if (this.poProductList.length < 1) {
                    this.spinner.hide();
                }
                // if (this.history == false) {
                // this.poProductList.forEach(element => {
                // if (element.idList.currentQueue == 1) {
                this.duplicatepoProductList = JSON.parse(JSON.stringify(this.poProductList));
                if (this.search.length == 0) {
                    this.listCount = this.duplicatepoProductList.length;
                }
                else {
                    filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                    this.listCount = filteredList.length;
                }
                // this.tempduplicatepoProductList = this.duplicatepoProductList;
                // }
                // });
                // }
                // else {
                // this.poProductList.forEach(element => {
                //     if (element.idList.currentQueue > 1 || element.idList.currentQueue == 0) {
                //         this.duplicatepoProductList.push(element);
                //         if (this.search.length == 0) {
                //             this.listCount = this.duplicatepoProductList.length;
                //         }
                //         else {
                //             filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                //             this.listCount = filteredList.length;
                //         }
                //         this.tempduplicatepoProductList = this.duplicatepoProductList;
                //     }
                // });
                // }

                this.duplicatepoProductList.forEach(element => {
                    if (element.processtype == 0) {
                        element.processtype = 'FORGING';
                    }
                    else {
                        element.processtype = 'ROLL CASTING';
                    }
                    if (element.itemtype == 0) {
                        element.itemtype = 'ROLLS';
                    }
                    else if (element.itemtype == 1) {
                        element.itemtype = 'STEP ROLLS';
                    }
                    else if (element.itemtype == 2) {
                        element.itemtype = 'REEL';
                    }
                    else {
                        element.itemtype = 'OTHER';
                    }
                    if (element.uom == 0) {
                        element.uom = 'Per-KG';
                    }
                    else if (element.uom == 1) {
                        element.uom = 'MT';
                    }
                    else {
                        element.uom = 'Per-Piece';
                    }
                    if (element.idList.currentQueue == '1') {
                        element.idList.currentQueue = 'Planning';
                    }
                    else if (element.idList.currentQueue == '2') {
                        element.idList.currentQueue = 'Roll Casting';
                    }
                    else if (element.idList.currentQueue == '0') {
                        element.idList.currentQueue = 'Rejected';
                    }
                    else if (element.idList.currentQueue == '3') {
                        element.idList.currentQueue = 'QualityCheck';
                    }
                    else if (element.idList.currentQueue == '4') {
                        element.idList.currentQueue = 'Machining';
                    }
                    else if (element.idList.currentQueue == '5') {
                        element.idList.currentQueue = 'Inspection';
                    }
                    else if (element.idList.currentQueue == '6') {
                        element.idList.currentQueue = 'GrovingCentreFinishing';
                    }
                    else if (element.idList.currentQueue == '7') {
                        element.idList.currentQueue = 'BearingShapperFinishing';
                    }
                    else if (element.idList.currentQueue == '8') {
                        element.idList.currentQueue = 'GrovingCentreFinal';
                    }
                    else if (element.idList.currentQueue == '9') {
                        element.idList.currentQueue = 'BearingShapperFinal';
                    }
                    else if (element.idList.currentQueue == '10' && element.idList.isDispacthed == true) {
                        element.idList.currentQueue = 'Dispatched';
                    }
                    else if (element.idList.currentQueue == '10' && element.idList.isDispacthed == false) {
                        element.idList.currentQueue = 'FinalInspection';
                    }
                    // else if (element.idList.currentQueue == '13') {
                    //     element.idList.currentQueue = 'PackagingFinish';
                    // }
                    // else if (element.idList.currentQueue == '14') {
                    //     element.idList.currentQueue = 'FinalReport';
                    // }
                    let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                    element['dimensionFormString'] = toString(element.dimensionForm);

                });
                if (isCheck == true) {
                    this.fromDate = '';
                    this.toDate = '';
                }
                if (this.fromDate != '' && this.toDate != '') {
                    this.onDateSelection();
                }
                this.spinner.hide();
            })
        } else {
            this.partyService.fetchAllPoProducts(headers).subscribe(data => {
                this.poProductList = data.pOProductData;
                if (this.poProductList.length < 1) {
                    this.spinner.hide();
                }
                // console.log('enetr' + JSON.stringify(this.poProductList));
                // if (this.poProductList != undefined)
                this.poProductList.forEach(element => {
                    if (element.idList.currentQueue > 1 || element.idList.currentQueue == 0) {
                        this.duplicatepoProductList.push(element);
                        // console.log('enetr' + JSON.stringify(this.duplicatepoProductList));
                        if (this.search.length == 0) {
                            this.listCount = this.duplicatepoProductList.length;
                        }
                        else {
                            filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                            this.listCount = filteredList.length;
                        }
                        this.duplicatepoProductList.forEach(element => {
                            if (element.processtype == 0) {
                                element.processtype = 'FORGING';
                            }
                            else {
                                element.processtype = 'ROLL CASTING';
                            }
                            if (element.itemtype == 0) {
                                element.itemtype = 'ROLLS';
                            }
                            else if (element.itemtype == 1) {
                                element.itemtype = 'STEP ROLLS';
                            }
                            else if (element.itemtype == 2) {
                                element.itemtype = 'REEL';
                            }
                            else {
                                element.itemtype = 'OTHER';
                            }
                            if (element.uom == 0) {
                                element.uom = 'Per-KG';
                            }
                            else if (element.uom == 1) {
                                element.uom = 'MT';
                            }
                            else {
                                element.uom = 'Per-Piece';
                            }
                            if (element.idList.currentQueue == '1') {
                                element.idList.currentQueue = 'Planning';
                            }
                            else if (element.idList.currentQueue == '2') {
                                element.idList.currentQueue = 'Roll Casting';
                            }
                            else if (element.idList.currentQueue == '0') {
                                element.idList.currentQueue = 'Rejected';
                            }
                            else if (element.idList.currentQueue == '3') {
                                element.idList.currentQueue = 'QualityCheck';
                            }
                            else if (element.idList.currentQueue == '4') {
                                element.idList.currentQueue = 'Machining';
                            }
                            else if (element.idList.currentQueue == '5') {
                                element.idList.currentQueue = 'Inspection';
                            }
                            else if (element.idList.currentQueue == '6') {
                                element.idList.currentQueue = 'GrovingCentreFinishing';
                            }
                            else if (element.idList.currentQueue == '7') {
                                element.idList.currentQueue = 'BearingShapperFinishing';
                            }
                            else if (element.idList.currentQueue == '8') {
                                element.idList.currentQueue = 'GrovingCentreFinal';
                            }
                            else if (element.idList.currentQueue == '9') {
                                element.idList.currentQueue = 'BearingShapperFinal';
                            }
                            else if (element.idList.currentQueue == '10' && element.idList.isDispacthed == true) {
                                element.idList.currentQueue = 'Dispatched';
                            }
                            else if (element.idList.currentQueue == '10' && element.idList.isDispacthed == false) {
                                element.idList.currentQueue = 'FinalInspection';
                            }
                            // else if (element.idList.currentQueue == '13') {
                            //     element.idList.currentQueue = 'PackagingFinish';
                            // }
                            // else if (element.idList.currentQueue == '14') {
                            //     element.idList.currentQueue = 'FinalReport';
                            // }
                            let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                            element['dimensionFormString'] = toString(element.dimensionForm);

                        });
                        if (isCheck == true) {
                            this.fromDate = '';
                            this.toDate = '';
                        }
                        if (this.fromDate != '' && this.toDate != '') {
                            this.onDateSelection();
                        }
                        this.spinner.hide();
                    }
                });
            });
        }
    }

    moveToRollCasting(event) {
        let data = event;
        // this.search = '';
        let x = {
            poDetailsId: data.poDetailsId,
            productId: data._id,
            displayId: data.idList.displayId,
            queueUpdateTimeStamp: data.idList.queueUpdateTimeStamp,
            upcomingQueue: QueueStatus.RollCasting,
            currentQueue: QueueStatus.Planning,
        };
        this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.updateQueueStatus(x, headers).subscribe(data => {
            // console.log(data);
            // this.fetchAllPoProduct(false);
            this.searchProduct(false);
            this.spinner.hide();
        });
    }

    getProductDetails(event) {
        // console.log('event' + JSON.stringify(event.data.idList.queueDurationList));
        this.statusQueue = [];
        // this.search = '';
        this.isDispacthed = event.data.idList.isDispacthed;
        this.dispatchedTime = event.data.idList.queueUpdateTimeStamp;
        this.dispatchedTime = this.dispatchedTime.substring(0, 4) + '-' + this.dispatchedTime.substring(5, 7) + '-' + this.dispatchedTime.substring(8, 10);
        this.statusQueue = event.data.idList.queueDurationList;
        this.statusQueue.forEach(element => {
            if (element.queue == '1') {
                element.queue = 'Planning';
            }
            else if (element.queue == '2') {
                element.queue = 'Roll Casting';
            }
            else if (element.queue == '0') {
                element.queue = 'Rejected';
            }
            else if (element.queue == '3') {
                element.queue = 'QualityCheck';
            }
            else if (element.queue == '4') {
                element.queue = 'Machining';
            }
            else if (element.queue == '5') {
                element.queue = 'Inspection';
            }
            else if (element.queue == '6') {
                element.queue = 'GrovingCentreFinishing';
            }
            else if (element.queue == '7') {
                element.queue = 'BearingShapperFinishing';
            }
            else if (element.queue == '8') {
                element.queue = 'GrovingCentreFinal';
            }
            else if (element.queue == '9') {
                element.queue = 'BearingShapperFinal';
            }
            else if (element.queue == '10') {
                element.queue = 'FinalInspection';
            }
            else if (element.queue == '11') {
                element.queue = 'TransportFinish';
            }
            else if (element.queue == '12') {
                element.queue = 'TestCertificateFinish';
            }
            else if (element.queue == '13') {
                element.queue = 'PackagingFinish';
            }
            else if (element.queue == '14') {
                element.queue = 'FinalReport';
            }
            element.startTime = element.startTime.substring(0, 4) + '-' + element.startTime.substring(5, 7) + '-' + element.startTime.substring(8, 10) + '  ' + element.startTime.substring(12, 16);
            element.endTime = element.endTime.substring(0, 4) + '-' + element.endTime.substring(5, 7) + '-' + element.endTime.substring(8, 10) + '  ' + element.endTime.substring(12, 16);
            element.durationString = this.secondsToHms(element.duration);
            // element.duration = parseInt(element.duration);
        });
        $('#duration').modal('show');
    }

    previewImage(event) {
        this.image = this.poProductList[event].productimg;
        // console.log('dataaiMage' + JSON.stringify(this.image));

    }

    secondsToHms(d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);

        var hDisplay = h > 0 ? h + (h == 1 ? " hr, " : " hrs, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? " min, " : " mins, ") : "";
        var sDisplay = s > 0 ? s + (s == 1 ? " sec" : " secs") : "";
        return hDisplay + mDisplay + sDisplay;
    }

    castingHistory() {
        this.history = !this.history;
        // this.fetchAllPoProduct(false);
        this.listCount = 0;
        this.duplicatepoProductList = [];
        this.basicSearchTerm = "";
        this.searchtype = "0"
    }

    onDateSelection() {
        let fromdateutc: any;
        let todateutc: any;
        if (this.fromDate != '' && this.toDate != '') {
            if (JSON.stringify(this.fromDate) == JSON.stringify(this.toDate)) {
                this.toDate.day = this.toDate.day + 1;
            }
        }
        this.duplicatepoProductList = this.tempduplicatepoProductList;
        fromdateutc = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
        fromdateutc = fromdateutc.getTime();
        todateutc = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
        todateutc = todateutc.getTime();
        let duplicatepoProductList = [];
        duplicatepoProductList = this.duplicatepoProductList;
        this.duplicatepoProductList = [];
        // duplicatepoProductList.forEach(element => {
        //     for (let index = element.idList.queueDurationList.length - 1; index >= 0; index--) {
        //         const durationObject = element.idList.queueDurationList[index];
        //         if (durationObject.queue == "1") {
        //             element.idList.tempFilteringDate = durationObject.endTime;
        //         }
        //     }
        // });

        duplicatepoProductList.forEach(element => {
            element.filterDate = new Date(element.idList.queueUpdateTimeStamp).getTime();
        });
        duplicatepoProductList.forEach(element => {
            if (element.filterDate >= fromdateutc && element.filterDate <= todateutc) {
                this.duplicatepoProductList.push(element);
            }
        });
        this.listCount = this.duplicatepoProductList.length;
        // return  this.spinner.hide();
    }


    onKeyup() {
        let filteredList = [];
        if (this.search.length == 0) {
            this.listCount = this.duplicatepoProductList.length;
        }
        else {
            // console.log('dataata' + JSON.stringify(this.duplicatepoProductList));
            filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
            this.listCount = filteredList.length;
        }
    }

    convertPoDetailsExcel() {
        let value = [];

        this.filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
        // console.log('dataata' + JSON.stringify(this.filteredList));

        this.filteredList.forEach(fuel => {
            // console.log('fuel' + JSON.stringify(fuel));
            let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
            fuel.dimensionForm = toString(fuel.dimensionForm);
            let subValueArray = [];
            subValueArray.push(fuel.idList.displayId);
            subValueArray.push(fuel.producttype);
            subValueArray.push(fuel.itemtype);
            subValueArray.push(fuel.partyData.partyname);
            subValueArray.push(fuel.weightperpiece);
            subValueArray.push(fuel.idList.currentQueue);
            subValueArray.push(fuel.reqhardness);
            subValueArray.push(fuel.dimensionForm);
            subValueArray.push(fuel.grade);
            subValueArray.push(fuel.productimg);
            value.push(subValueArray);
        });

        // let headerName = "Fuel Summary Report (" + this.fromDate.formatted + " to " + this.toDate.formatted + ")";
        let headerName = "RollPlanner Report";
        let body = {
            "headerName": headerName,
            "excelName": "RollPlanner",
            "header": [
                "ROLLID",
                "PRODUCTTYPE",
                "ITEMTYPE",
                "PARTYNAME",
                "WEIGHT/PIECE",
                "STATUS",
                "REQ.HARDNESS",
                "DIMENSION",
                "GRADE",
                "PROD.IMG"
            ],
            "value": value
        }
        this.downloadExcelSheet(body).subscribe(blob => {
            // Doing it this way allows you to name the file
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = "RollPlanner.xlsx";
            link.click();
        }, error => console.log("Error downloading the file."),
            () => console.log('Completed file download.'));
    }

    downloadExcelSheet(reqBody): Observable<Object[]> {
        //this.loading = true;
        return Observable.create(observer => {
            let xhr = new XMLHttpRequest();
            xhr.open('POST', AppConstants.EXCEL_DOWNLOAD_URL, true);
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.responseType = 'blob';

            xhr.onreadystatechange = () => {
                //this.loading = false;
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {

                        var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        var blob = new Blob([xhr.response], { type: contentType });
                        observer.next(blob);
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            xhr.send(JSON.stringify(reqBody));

        });
    }
}