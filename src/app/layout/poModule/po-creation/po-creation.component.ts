import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { POService } from '../po.service';
import { AppConstants } from '../../../base/appconstants'
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';

import { FormBuilder, FormGroup, Validators, AbstractControl, FormArray, FormControl } from '@angular/forms';
import { NotifierService } from 'angular-notifier';
import * as $AB from 'jquery';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
declare var jquery: any;   // not required
declare var $: any;   // not required
import * as CryptoJS from 'crypto-js';
import { PartyService } from 'src/app/shared/services/party.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Ng2SearchPipe } from 'ng2-search-filter';
declare var require: any;
require('aws-sdk/dist/aws-sdk');

@Component({
    selector: 'app-po-creation',
    templateUrl: './po-creation.component.html',
    styleUrls: ['./po-creation.component.scss'],
    encapsulation: ViewEncapsulation.Emulated
})
export class PoCreationComponent implements OnInit {
    private readonly notifier: NotifierService;
    private readonly spinnerCall: NgxSpinnerService;
    createPocForm: FormGroup;
    porderForm: FormGroup;
    dform: FormGroup;
    setOrgId: any;
    searchHistory: any;
    placement = 'top';
    dimensionForm = [];
    allPocList: any;
    userToken: any;
    newPoc = true;
    listCount: any;
    updatedPoc = false;
    pocValue: any;
    isNext: boolean = false;
    isUpdate: boolean = false;
    issubmitted = false;
    isAdd = false;
    counter: number = 0;
    search: any;
    cols: any[];
    pocols: any[];
    sortF: any;
    isWeight: boolean = false;
    index: number;
    poProductId: string;
    title = 'Modern Rolls | Add PO';
    searchPartyKeyWord: string;
    searchIdKeyWord: string;
    poadddiv = false;
    pobasictab = true;
    processtype = '';
    forgPOType = 0;
    poproducttab = false;
    pochemcomp = false;
    poProductTable = false;
    imageSrc: any;
    isQuantity: boolean = false;
    porderList = [];
    partyList = [];
    allSearchedParty = [];
    allSearchedId = [];
    chemcompList: any = [];
    allPoDetailList: any = [];
    poProductList: any = [];
    poDetailsId: any;
    showpolist = false;
    poProductDetail: any;
    base64textString: any;
    image: any;
    filteredList: any = [];
    weightPer: any;
    forgingGrades = ["EN9", "EN8", "EN42", "C45", "Other",];
    rollcastingGrades = ["ADAMITS", "CHILLS", "SG-IRON", "SG-ACC", "Other"];

    forgingPtype = ["As Forged", "As Machining"];

    rollcastingPtype = ["GROOVE", "CENTER", "BEARING", "SHAPER", "OTHERS"];
    unitOfMeasurement = ["Per-KG", "MT", "Per-Piece"];
    displayName: any;
    displayId: any;
    weightpercentage = ["0%", "1%", "2%", "3%", "4%", "5%"];
    toggleForm: boolean;
    imageDataList: any = new Array();
    filter: Ng2SearchPipe;
    colorMap = {
        "ADAMITS": "red",
        "CHILLS": "blue",
        "SG-IRON": "green",
        "SG-ACC": "#c4c233"
    }
    constructor(private _fb: FormBuilder,
        private partyService: PartyService,
        private adminService: POService,
        notifierService: NotifierService,
        private router: Router,
        private spinner: NgxSpinnerService,
        private spinner2: NgxSpinnerService,
        private titleService: Title,
        private cd: ChangeDetectorRef,
    ) {
        this.notifier = notifierService;
        this.spinnerCall = spinner;
        this.spinner.show();
        this.filter = new Ng2SearchPipe();


    }
    // tt(){
    //     console.log("FILTERED DATA:",
    //      this.filter.transform(this.allPoDetailList, this.search));
    // }
    addOrderToList() {
        this.isAdd = true;
        // console.log('check' + this.porderForm.controls['chemicalcomp'].value);
        // console.log('check' + this.porderForm.controls['producttype'].value);
        let processtype: number;
        let itemtype: number;
        let uom: number;
        // console.log(this.porderForm);
        // this.porderForm.controls['quantity'].setValue(1);
        if (this.porderForm.valid) {
            if (this.porderForm.controls['processtype'].value == 'forging') {
                processtype = 0;
            }
            else {
                processtype = 1;
            }
            if (this.porderForm.controls['itemtype'].value == 'rolls') {
                itemtype = 0;
            }
            else if (this.porderForm.controls['itemtype'].value == 'steproll') {
                itemtype = 1;
            }
            else if (this.porderForm.controls['itemtype'].value == 'reel') {
                itemtype = 2;
            }
            else {
                itemtype = 3;
            }
            if (this.porderForm.controls['uom'].value == 'Per-KG') {
                uom = 0;
            }
            else if (this.porderForm.controls['uom'].value == 'MT') {
                uom = 1;
            }
            else {
                uom = 2;
            }
            const headers = new Headers(
                {
                    'accept': 'application/json',
                    'x-access-token': this.userToken
                });
            let x = {
                poDetailsId: this.partyService.poDetailsId,
                processtype: processtype,
                grade: this.porderForm.controls['grade'].value,
                producttype: this.porderForm.controls['producttype'].value,
                itemtype: itemtype,
                dimensionForm: this.porderForm.controls['dimensionForm'].value,
                quantity: this.porderForm.controls['quantity'].value,
                uom: uom,
                priceperuom: this.porderForm.controls['priceperuom'].value,
                weightperpiece: this.porderForm.controls['weightperpiece'].value,
                reqhardness: this.porderForm.controls['reqhardness'].value,
                remark: this.porderForm.controls['remark'].value,
                chemicalcomp: this.porderForm.controls['chemicalcomp'].value,
                productimg: this.imageDataList[0].pic

            };
            // console.log('form value' + JSON.stringify(x));
            this.partyService.createPOProduct(x, headers).subscribe(data => {
                // console.log(data);

                this.fetchAllPoProduct();
                // this.porderForm.reset();
                // this.resetOrderForm('rollcasting');
                // this.OnItemTypeChange('rolls');
            },
                err => console.log(err),
            );
            this.isAdd = false;
        } else {
            this.counter = this.counter - 1;
        }
    }

    updateOrderToList() {
        this.isAdd = true;
        let processtype: number;
        let itemtype: number;
        let uom: number;
        if (this.porderForm.valid) {
            this.spinner.show();
            if (this.porderForm.controls['producttype'].value == 'forging') {
                processtype = 0;
            }
            else {
                processtype = 1;
            }
            if (this.porderForm.controls['itemtype'].value == 'rolls') {
                itemtype = 0;
            }
            else if (this.porderForm.controls['itemtype'].value == 'steproll') {
                itemtype = 1;
            }
            else if (this.porderForm.controls['itemtype'].value == 'reel') {
                itemtype = 2;
            }
            else {
                itemtype = 3;
            }
            if (this.porderForm.controls['uom'].value == 'Per-KG') {
                uom = 0;
            }
            else if (this.porderForm.controls['uom'].value == 'MT') {
                uom = 1;
            }
            else {
                uom = 2;
            }
            const headers = new Headers(
                {
                    'accept': 'application/json',
                    'x-access-token': this.userToken
                });
            let x = {
                _id: this.poDetailsId,
                poDetailsId: this.partyService.poDetailsId,
                processtype: processtype,
                grade: this.porderForm.controls['grade'].value,
                producttype: this.porderForm.controls['producttype'].value,
                itemtype: itemtype,
                dimensionForm: this.porderForm.controls['dimensionForm'].value,
                quantity: this.porderForm.controls['quantity'].value,
                uom: uom,
                priceperuom: this.porderForm.controls['priceperuom'].value,
                weightperpiece: this.porderForm.controls['weightperpiece'].value,
                reqhardness: this.porderForm.controls['reqhardness'].value,
                remark: this.porderForm.controls['remark'].value,
                chemicalcomp: this.porderForm.controls['chemicalcomp'].value,
                productimg: this.imageDataList[0].pic

            };
            // console.log('form value' + JSON.stringify(x));
            this.partyService.UpdatePOProduct(x, headers).subscribe(data => {
                // console.log(data);

                this.fetchAllPoProduct();
                this.isUpdate = false;
                this.porderForm.reset();
                this.resetOrderForm('rollcasting');
                this.OnItemTypeChange('rolls');
                this.spinner.hide();
            },
                err => console.log(err),
            );
            this.isAdd = false;

        }
        this.porderForm.controls['quantity'].enable();
    }
    // createDimensionForm() {




    //     this.OnItemTypeChange();



    // }

    OnItemTypeChange(value) {
        setTimeout(() => {
            this.toggleForm = true;
        }, 0);
        this.toggleForm = false;
        this.porderForm.controls['weightperpiece'].setValue('');
        this.porderForm.controls['weightpercent'].setValue('');
        this.porderForm.controls['weightWithPerc'].setValue('');
        this.porderForm.controls['itemtype'].setValue(value);
        const group = this.porderForm.get('dimensionForm') as FormGroup;

        Object.keys(group.controls).forEach(key => {
            group.removeControl(key);

        });

        if (this.porderForm.controls['itemtype'].value == 'rolls') {
            this.porderForm.controls['weightperpiece'].disable();
            group.addControl('BD', new FormControl('', Validators.required));
            group.addControl('BL', new FormControl('', Validators.required));
            group.addControl('JD', new FormControl('', Validators.required));
            group.addControl('JL1', new FormControl('', Validators.required));
            group.addControl('JL2', new FormControl('', Validators.required));
            group.addControl('TL', new FormControl('', Validators.required));

        }
        else if (this.porderForm.controls['itemtype'].value == 'steproll') {
            this.porderForm.controls['weightperpiece'].disable();
            group.addControl('BD1', new FormControl('', Validators.required));
            group.addControl('BL1', new FormControl('', Validators.required));
            group.addControl('BD2', new FormControl('', Validators.required));
            group.addControl('BL2', new FormControl('', Validators.required));
            group.addControl('JD', new FormControl('', Validators.required));
            group.addControl('JL', new FormControl('', Validators.required));
            group.addControl('TL', new FormControl('', Validators.required));

        }
        else if (this.porderForm.controls['itemtype'].value == 'reel') {
            this.porderForm.controls['weightperpiece'].disable();
            group.addControl('OD', new FormControl('', Validators.required));
            group.addControl('ID', new FormControl('', Validators.required));
            group.addControl('Face', new FormControl('', Validators.required));
        }
        else if (this.porderForm.controls['itemtype'].value == 'other') {
            this.isWeight = false;
            this.porderForm.controls['weightperpiece'].enable();
            group.addControl('OTHER', new FormControl('', Validators.required));
        }
        // console.log('jio' + JSON.stringify(this.porderForm.controls['dimensionForm'].value));

    }
    readURL(event: Event): void {
        const target = event.target as HTMLInputElement;
        if (target.files && target.files[0]) {
            const file = target.files[0];

            const reader = new FileReader();
            reader.onload = e => this.imageSrc = reader.result;

            reader.readAsDataURL(file);
        }
    }

    ShowPoDiv() {
        this.createPocForm.reset();
        this.searchIdKeyWord = '';
        this.searchPartyKeyWord = '';
        this.isNext = false;
        this.poadddiv = true;
    }
    HidePoDiv() {
        this.fetchAllPODetails();
        this.search = this.searchHistory;
        this.createPocForm.reset();
        // this.createPocFoms['partyid'].setValue('');
        // this.createPocFoms['partyname'].setValue('');
        this.poadddiv = false;
        this.pobasictab = true;
        this.pochemcomp = false;
        this.poProductTable = false;
        this.poproducttab = false;
        this.showpolist = false;
        this.porderForm.controls['quantity'].enable();

    }

    onSubmit() {
        this.issubmitted = true;
        if (this.createPocForm.valid) {
            this.pobasictab = false;
            this.pochemcomp = false;
            this.poProductTable = false;
            this.poproducttab = true;
            // console.log('hide');
            const headers = new Headers(
                {
                    'accept': 'application/json',
                    'x-access-token': this.userToken
                });
            let podate = this.createPocForm.controls['podate'].value;
            // console.log(podate);
            let date = podate.year + '-' + podate.month + '-' + podate.day;
            // console.log(date);
            let podateutc = new Date(date).toUTCString();
            // console.log(podateutc);
            let ordercnfdate = this.createPocForm.controls['ordercnfdate'].value;
            let date1 = ordercnfdate.year + '-' + ordercnfdate.month + '-' + ordercnfdate.day;
            let ordercnfdateutc = new Date(date1).toUTCString();
            let deliverydate = this.createPocForm.controls['deliverydate'].value;
            let date2 = deliverydate.year + '-' + deliverydate.month + '-' + deliverydate.day;
            let deliverydateutc = new Date(date2).toUTCString();
            let deliverybasis: string;
            if (this.createPocForm.controls['deliverybasis'].value == "OTHER") {
                deliverybasis = this.createPocForm.controls['deliverybasisText'].value;
            }
            else {
                deliverybasis = this.createPocForm.controls['deliverybasis'].value;
            }
            let x = {
                partyid: this.createPocForm.controls['partyid'].value,
                partyname: this.createPocForm.controls['partyname'].value,
                partyaddress: this.createPocForm.controls['partyaddress'].value,
                partypodate: podateutc,
                partyponame: this.createPocForm.controls['ponumber'].value,
                paymentterm: this.createPocForm.controls['paymentterm'].value,
                deliverybasis: deliverybasis,

                ordercnfdate: ordercnfdateutc,
                deliverydate: deliverydateutc,
            };
            this.partyService.createPODetails(x, headers).subscribe(data => {
                // console.log(data);
                this.fetchAllChemComp();
                this.partyService.poDetailsId = data.createdRecord._id;
                this.porderForm.reset();
                this.showpolist = true;
                this.resetOrderForm('rollcasting');
                this.OnItemTypeChange('rolls');
                // this.pobasictab = false;
                // this.pochemcomp = false;
                // this.poproducttab = true;
            },
                err => console.log(err),
            );
            this.issubmitted = false;
        }
    }

    ngOnInit() {
        this.setOrgId = JSON.parse(localStorage.getItem('orgId'));
        const getjwt = localStorage.getItem('jwttoken');
        this.userToken = this.conversionDecrypt(getjwt);
        // console.log('this.userToken',this.userToken);
        this.titleService.setTitle(this.title);
        this.createPocFoms();
        this.fetchAllParty();
        this.resetOrderForm('rollcasting');
        this.OnItemTypeChange('rolls');
        this.getAllPocsForOrg();
        this.fetchAllPODetails();
        this.outsideCaptureEvent(); this.spinner.show();
        this.cols = [
            { field: 'poid', header: 'POID', width: '10%' },
            { field: 'partyname', header: 'Party Name', width: '20%' },
            { field: 'partyponame', header: 'Party PO No.', width: '15%' },
            { field: 'partypodate', header: 'Party PO Date', width: '15%' },
            { field: 'ordercnfdate', header: 'Conf. Date', width: '15%' },
            { field: 'deliverydate', header: 'Del. Date', width: '15%' },
            { field: 'deliverybasis', header: 'Del. Basis', width: '10%' }
        ];
        this.pocols = [
            // { field: 'processtype', header: 'Process Type', width: '10%' },
            // { field: 'grade', header: 'Grade', width: '7%' },
            { field: 'producttype', header: 'Prod. Type', width: '8%' },
            { field: 'itemtype', header: 'Item Type', width: '8%' },
            { field: 'uom', header: 'UOM', width: '8%' },
            { field: 'priceperuom', header: 'Price/UOM', width: '8%' },
            { field: 'weightperpiece', header: 'Weight/Piece', width: '8%' },
            // { field: 'quantity', header: 'Quant', width: '7%' },
            { field: 'reqhardness', header: 'Req. Hard', width: '8%' },
            { field: 'dimensionFormString', header: 'Dimension', width: '15%' },
            { field: 'remark', header: 'Remark', width: '7%' },
        ];

    }

    outsideCaptureEvent() {
        $("#createPocForm").on("hidden.bs.modal", () => {
            this.newPoc = true;
            this.updatedPoc = false;
        });
    }

    createPocFoms() {
        this.createPocForm = this._fb.group({
            partyid: [null, Validators.required],
            partyname: [null, Validators.required],
            partyaddress: [null, Validators.required],
            ponumber: [null, Validators.required],
            podate: [null, Validators.required],
            paymentterm: [null, Validators.required],
            deliverybasis: [null, Validators.required],
            deliverybasisText: null,
            ordercnfdate: [null, Validators.required],
            deliverydate: [null, Validators.required]

        });

        this.porderForm = this._fb.group({
            processtype: [null, Validators.required],
            grade: [null, Validators.required],
            producttype: [null, Validators.required],
            itemtype: [null, Validators.required],
            dimensionForm: new FormGroup({
                // rollsDimensionBD:new FormControl('4',Validators.required),
                // rollsDimensionBL:new FormControl('4',Validators.required)
            }),
            quantity: [null, [Validators.required, Validators.max(25)]],
            uom: [null, Validators.required],
            priceperuom: [null, Validators.required],
            weightperpiece: [null, Validators.required],
            weightpercent: [null],
            weightWithPerc: [null],
            reqhardness: [null, Validators.required],
            remark: [null, Validators.required],
            chemicalcomp: [null, Validators.required],
            productimg: [null, Validators.required]

        });
        // this.createDimensionForm();
    }

    resetOrderForm(value: any) {

        this.porderForm.reset();
        this.porderForm.controls['processtype'].setValue(value);
        this.isUpdate = false;
    }

    OnChemComChange(value: any) {
        // console.log('chemcomp' + this.porderForm.controls['chemicalcomp'].value);

        this.porderForm.controls['chemicalcomp'].setValue(value);
    }

    get f() {
        return this.createPocForm.controls;
    }

    submitPocHandler() {
        this.spinner.show();
        this.issubmitted = true;
        if (this.createPocForm.invalid) {
            this.spinner.hide();
            this.notifier.notify('error', 'Something went wrong please try again! ')
            return;
        }
        this.createPocForm.get('orgId').setValue(this.setOrgId);
        $('#createPocForm').modal('hide');
        this.adminService.cteatePocService(this.createPocForm.value).subscribe(
            data => {
                this.spinner.hide();
                // console.log(data);
                if (data.statusCode === 200) {
                }
                this.getAllPocsForOrg();
            },
            err => {
                this.spinner.hide();
                this.notifier.notify('error', 'Please Check ' + err.statusText);
            },
            // () => this.notifier.notify('success', 'New POC Created!!! ')
        );
        // console.log(this.createPocForm.value);
        this.issubmitted = false;
        // window.location.reload();

    }

    getAllPocsForOrg() {
        this.spinnerCall.show();
        this.adminService.getAllPocsForOrg(this.setOrgId).subscribe(data => {
            this.spinnerCall.hide();
            if (data.statusCode === 200) {
                this.allPocList = data.pocData;
                // console.log(JSON.stringify(this.allPocList));
                data.pocData.forEach(poc => {
                    const location = poc.location;
                    // this.getLatLngAddressFormat(location, poc);
                    try {
                        if (location.toString().length) {
                            if (location.lat !== null && location.lng !== null) {
                                this.adminService.getLatLngAddressFormat(location).subscribe(data => {
                                    this.spinnerCall.hide();
                                    const results = data.results[0].formatted_address;
                                    poc.address = results;
                                    //console.log(results);
                                }, err => {
                                    console.log(err);
                                }
                                );
                            }
                        }
                    } catch (err) {
                        console.log('Error:' + err);
                    }
                });
            }
        },
            err => {
                this.spinnerCall.hide();
                console.log(err);
                this.notifier.notify('error', 'Please Check ' + err.statusText);
            },
            // () => this.notifier.notify('success', 'Getting All Poc List Is Done ')
        );
    }

    get keys(): Array<any> {
        return Object.keys(this.allPocList);
    }

    onclear() {
        this.porderForm.reset();
        this.counter = 0;
        this.resetOrderForm('rollcasting');
        this.OnItemTypeChange('rolls');
        this.porderForm.controls['quantity'].enable();
    }
    onPocView(pocValue) {

        this.createPocForm.patchValue(pocValue);
        this.newPoc = false;
        this.updatedPoc = true;
        this.pocValue = pocValue;
        this.getZipCode(pocValue.pinCode);
        $('#createPocForm').modal('show');
    }
    onPocUpdate(pocValue) {
        const body = {
            _id: pocValue.Id,
            orgId: pocValue.orgId,
            centerName: pocValue.orgId,
            doorNo: pocValue.orgId,
            streetName: pocValue.orgId,
            area: pocValue.orgId,
            city: pocValue.orgId,
            pinCode: pocValue.orgId,
            location: pocValue.orgId,
        };
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.createPocForm.patchValue(pocValue);
        // this.adminService.updatePocForOrg(body,headers).subscribe(updatePoc => {

        // })
        this.getZipCode(pocValue.pinCode);
        $('#createPocForm').modal('show');
    }
    getZipCode(code) {

        // this.createPocForm.get('pinCode').valueChanges.subscribe((result) => {
        //     // const size = result;
        //     if (!result) {
        //         const size = result.toString().length;
        //         if(size === 6) {
        //             console.log(size);
        //         }
        //     }
        // });
        const pincode = code;
        if (pincode.length === 6) {
            this.adminService.getAddress(pincode).subscribe(data => {
                const geoCodeObj = data.results;
                geoCodeObj.forEach(element => {
                    const city = element.address_components;
                    city.forEach(element => {
                        const pinAddress = element;
                        const types = element.types;
                        types.forEach(element => {
                            if (element === 'administrative_area_level_1') {
                                this.createPocForm.get('state').patchValue(pinAddress.long_name);
                            }
                            if (element === 'administrative_area_level_2') {
                                this.createPocForm.get('city').patchValue(pinAddress.long_name);
                            }
                        });
                    });
                });
            });
        }
    }
    onUpdatedPoc(createPocForm) {
        // console.log('createPocForm');
        // console.log(createPocForm);
        // console.log('this.pocValue');
        // console.log(this.pocValue);
        const body = {
            _id: this.pocValue._id,
            orgId: this.pocValue.orgId,
            centerName: createPocForm.centerName,
            doorNo: createPocForm.doorNo,
            streetName: createPocForm.streetName,
            area: createPocForm.area,
            city: createPocForm.city,
            pinCode: createPocForm.pinCode,
            location: createPocForm.location,
        };
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        this.spinner.show();

        this.adminService.updatePocForOrg(body, headers).subscribe(updatePoc => {
            this.spinner.hide();
            // console.log('updatePoc');
            // console.log(updatePoc);
            this.getAllPocsForOrg();

        },
            err => {
                this.spinner.hide();
                this.notifier.notify('error', 'Please Check ' + err.statusText);
            },
            //() => this.notifier.notify('success', 'New POC Created!!! ')
        );
        this.getZipCode(this.pocValue.pinCode);
        $('#createPocForm').modal('hide');
    }

    onPocDelete(poc) {
        // console.log(poc);
        this.pocValue = poc;
    }
    onPocDeleteConform() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        // console.log(this.pocValue._id);
        this.spinner.show();
        this.adminService.removePocForOrg(this.pocValue._id, headers).subscribe(data => {
            this.spinner.hide();
            // console.log(data);
            this.getAllPocsForOrg();
            $('#deleteModal').modal('hide');
        },
            err => {
                this.spinner.hide();
                this.notifier.notify('error', 'Please Check ' + err.statusText);
            },
            //() => this.notifier.notify('success', 'New POC Created!!! ')
        );
    }
    conversionDecrypt(decrypt) {
        try {
            const bytes = CryptoJS.AES.decrypt(decrypt, AppConstants.secret);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return decrypt;
        } catch (e) {
            console.log(e);
        }
    }

    onEdit(event) {
        setTimeout(() => {
            this.toggleForm = true;
        }, 0);
        this.toggleForm = false;
        this.resetOrderForm('rollcasting');
        // this.OnItemTypeChange('other');
        //this.porderForm.controls['itemtype'].setValue('OTHER');
        this.isUpdate = true;
        this.poDetailsId = event._id;
        // console.log('edit here' + JSON.stringify(event));
        // const group = this.porderForm.get('dimensionForm') as FormGroup;

        // Object.keys(group.controls).forEach(key => {
        //     group.removeControl(key);

        // });

        this.porderForm.reset();



        if (event.processtype == 0) {
            this.porderForm.controls['processtype'].setValue('forging');
        }
        else {
            this.porderForm.controls['processtype'].setValue('rollcasting');
        }
        this.porderForm.controls['grade'].setValue(event.grade);
        this.porderForm.controls['producttype'].setValue(event.producttype);
        if (event.itemtype == 'ROLLS') {
            this.porderForm.controls['itemtype'].setValue('rolls');
            this.OnItemTypeChange(this.porderForm.controls['itemtype'].value);
            this.porderForm.controls['dimensionForm'].setValue(event.dimensionForm);
            // console.log('form' + JSON.stringify(this.porderForm.controls['dimensionForm'].value));
            // this.porderForm.controls['dimensionForm'].value.controls['BD'].setValue(event.dimensionForm.TD);

            this.calculateWeight();
        }
        else if (event.itemtype == 'STEP ROLLS') {
            this.porderForm.controls['itemtype'].setValue('steproll');
            this.OnItemTypeChange(this.porderForm.controls['itemtype'].value);
            this.porderForm.controls['dimensionForm'].setValue(event.dimensionForm);
            (window as any).c = this.porderForm.controls;
            // console.log('form' + JSON.stringify(this.porderForm.controls['dimensionForm'].value));
            this.calculateWeight();
        }
        else if (event.itemtype == 'REEL') {
            this.porderForm.controls['itemtype'].setValue('reel');
            this.OnItemTypeChange(this.porderForm.controls['itemtype'].value);
            this.porderForm.controls['dimensionForm'].setValue(event.dimensionForm);
            // console.log('form' + JSON.stringify(this.porderForm.controls['dimensionForm'].value));
            this.calculateWeight();
        }
        else {
            this.porderForm.controls['itemtype'].setValue('other');
            this.OnItemTypeChange(this.porderForm.controls['itemtype'].value);
            this.porderForm.controls['dimensionForm'].setValue(event.dimensionForm);
            // console.log('form' + JSON.stringify(this.porderForm.controls['dimensionForm'].value));
            this.calculateWeight();
        }
        // console.log('item' + JSON.st/ringify(this.porderForm.controls['itemtype'].value));
        this.porderForm.controls['quantity'].setValue(event.quantity);
        if (event.uom == 0) {
            this.porderForm.controls['uom'].setValue('Per-KG');
        }
        else if (event.uom == 1) {
            this.porderForm.controls['uom'].setValue('MT');
        }
        else {
            this.porderForm.controls['uom'].setValue('Per-Piece');
        }
        this.porderForm.controls['priceperuom'].setValue(event.priceperuom);
        this.porderForm.controls['reqhardness'].setValue(event.reqhardness);
        this.porderForm.controls['remark'].setValue(event.remark);
        this.porderForm.controls['chemicalcomp'].setValue(event.chemicalcomp);
        this.isQuantity = true;
        // console.log(this.porderForm);
        this.porderForm.controls['quantity'].disable();
        this.cd.detectChanges();
        // this.porderForm.controls['productimg'].setValue(event.productimg);
    }

    onRemove(order: any) {
        // console.log('re/move' + JSON.stringify(order));
        this.poProductId = order._id;
        $('#delete').modal('show');
    }

    onDeletePoProduct() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.spinner.show();
        this.partyService.removePoProduct(this.poProductId, headers).subscribe(data => {
            this.spinner.hide();
            $('#delete').modal('hide');
            this.fetchAllPoProduct();
            this.porderForm.reset();
            this.resetOrderForm('rollcasting');
            this.OnItemTypeChange('rolls');
            // console.log(data);
        },
            err => {
                this.spinner.hide();
                $('#delete').modal('hide');
                this.notifier.notify('error', 'Something went wrong please try again! ');
            }
        );
    }

    fetchAllParty() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.getAllParty(headers).subscribe(data => {
            this.partyList = data.partyData;
            // console.log('list here' + JSON.stringify(this.partyList));

        })
    }

    partyNameSearch() {
        let partyNameList = [];
        this.partyList.forEach(element => {
            partyNameList.push(element.name);
        });
        this.allSearchedParty = [];
        if (this.searchPartyKeyWord == '') {
            this.allSearchedParty = partyNameList;
        }
        else {
            if (this.searchPartyKeyWord.length == 1) {
                // this.searchPartyKeyWord = this.searchPartyKeyWord.toLowerCase();
                let searchPartyKeyWord = this.searchPartyKeyWord.substring(0, 1);
                partyNameList.forEach(element => {
                    let temp = element.substring(0, 1);
                    temp = temp.toLowerCase();
                    if (temp == searchPartyKeyWord) {
                        this.allSearchedParty.push(element);
                        // this.searchPartyKeyWord.toUpperCase();
                    }
                });
            }
            else if (this.searchPartyKeyWord.length > 1) {
                // this.searchPartyKeyWord = this.searchPartyKeyWord.toLowerCase();
                let searchPartyKeyWord = this.searchPartyKeyWord.substring(0, 2);
                partyNameList.forEach(element => {
                    let temp = element.substring(0, 2);
                    temp = temp.toLowerCase();
                    if (temp == searchPartyKeyWord) {
                        this.allSearchedParty.push(element);
                        // this.searchPartyKeyWord.toUpperCase();
                    }
                });
            }
        }

    }

    partyIdSearch() {
        let partyIdList = [];
        this.partyList.forEach(element => {
            partyIdList.push(element.partyId);
        });
        this.allSearchedId = [];
        if (this.searchIdKeyWord == '') {
            this.allSearchedId = partyIdList;
        }
        else {
            if (this.searchIdKeyWord.length == 1) {
                // this.searchIdKeyWord = this.searchIdKeyWord.toLowerCase();
                let searchIdKeyWord = this.searchIdKeyWord.substring(0, 1);
                partyIdList.forEach(element => {
                    let temp = element.substring(0, 1);
                    temp = temp.toLowerCase();
                    if (temp == searchIdKeyWord) {
                        this.allSearchedId.push(element);
                        // this.searchIdKeyWord.toUpperCase();
                    }
                });
            }
            else if (this.searchIdKeyWord.length > 1) {
                // this.searchIdKeyWord = this.searchIdKeyWord.toLowerCase();
                let searchIdKeyWord = this.searchIdKeyWord.substring(0, 2);
                partyIdList.forEach(element => {
                    let temp = element.substring(0, 2);
                    temp = temp.toLowerCase();
                    if (temp == searchIdKeyWord) {
                        this.allSearchedId.push(element);
                        // this.searchIdKeyWord.toUpperCase();
                    }
                });
            }
        }
    }

    onIdDropDownClick(id) {
        // console.log('id here' + id);

        this.partyList.forEach(element => {
            if (element.partyId == id) {
                this.createPocForm.controls['partyid'].setValue(element.partyId);
                this.createPocForm.controls['partyname'].setValue(element.name);
                this.searchIdKeyWord = this.searchIdKeyWord.toUpperCase();
                this.createPocForm.controls['partyaddress'].setValue(element.address);
            }
        });
    }

    onNameDropDownClick(name) {
        // console.log('name here' + name);

        this.partyList.forEach(element => {
            if (element.name == name) {
                this.createPocForm.controls['partyid'].setValue(element.partyId);
                this.createPocForm.controls['partyname'].setValue(element.name);
                this.createPocForm.controls['partyaddress'].setValue(element.address);
            }
        });
    }

    fetchAllChemComp() {
        let list: any;
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.getAllChemicalComposition(headers).subscribe(data => {
            list = data.chemCompData;
            // console.log('hello' + JSON.stringify(list));
            list.forEach(element => {
                this.chemcompList.push(element.ccId);
            });

        })

        // console.log('hi' + JSON.stringify(this.chemcompList));

    }

    fetchAllPoProduct() {
        this.spinner.show();
        let id = this.partyService.poDetailsId;
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.getAllPoProducts(headers, id).subscribe(data => {
            this.poProductList = data.pOProductData;


            this.poProductList.forEach(element => {
                if (element.processtype == 0) {
                    element.processtype = 'FORGING';
                }
                else {
                    element.processtype = 'ROLL CASTING';
                }
                if (element.itemtype == 0) {
                    element.itemtype = 'ROLLS';
                }
                else if (element.itemtype == 1) {
                    element.itemtype = 'STEP ROLLS';
                }
                else if (element.itemtype == 2) {
                    element.itemtype = 'REEL';
                }
                else {
                    element.itemtype = 'OTHER';
                }
                if (element.uom == 0) {
                    element.uom = 'Per-KG';
                }
                else if (element.uom == 1) {
                    element.uom = 'MT';
                }
                else {
                    element.uom = 'Per-Piece';
                }
                let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                element['dimensionFormString'] = toString(element.dimensionForm);

            });
            // console.log('here' + JSON.stringify(this.poProductList));

            this.spinner.hide();
            if (this.poproducttab == true) {
                this.showpolist = true;
            }
            else {
                this.showpolist = false;
            }

            // console.log('list here' + JSON.stringify(this.chemcompList));

        })
    }

    calculateWeight() {
        let dimensionObj: any;
        let weightPer: any;
        dimensionObj = this.porderForm.controls['dimensionForm'].value;
        // console.log('dimensionObj' + JSON.stringify(dimensionObj));
        this.porderForm.controls['weightperpiece'].setValue('');
        // this.porderForm.controls['weightpercent'].setValue('');
        this.porderForm.controls['weightWithPerc'].setValue('');
        if (this.porderForm.controls['itemtype'].value == 'rolls') {
            if (dimensionObj != undefined && dimensionObj.BD != null && dimensionObj.BL != null && dimensionObj.JD != null && dimensionObj.JL1 != null && dimensionObj.JL2 != null) {
                weightPer = 0.00000617 * ((dimensionObj.BD * dimensionObj.BD * dimensionObj.BL) + (dimensionObj.JD * dimensionObj.JD * dimensionObj.JL1) + (dimensionObj.JD * dimensionObj.JD * dimensionObj.JL2));
                this.weightPer = weightPer;
                weightPer = weightPer.toFixed(2);
                // console.log('weightchnge' + weightPer);

                this.porderForm.controls['weightperpiece'].setValue(weightPer);
                this.OnPerChange();
            }

        }
        else if (this.porderForm.controls['itemtype'].value == 'steproll') {
            if (dimensionObj != undefined && dimensionObj.BD1 != null && dimensionObj.BL1 != null && dimensionObj.BD2 != null && dimensionObj.BL2 != null && dimensionObj.JD != null && dimensionObj.JL != null) {
                // console.log('change');

                weightPer = 0.00000617 * ((dimensionObj.BD1 * dimensionObj.BD1 * dimensionObj.BL1) + (dimensionObj.BD2 * dimensionObj.BD2 * dimensionObj.BL2) + 2 * (dimensionObj.JD * dimensionObj.JD * dimensionObj.JL));
                this.weightPer = weightPer;
                weightPer = weightPer.toFixed(2);
                this.porderForm.controls['weightperpiece'].setValue(weightPer);
                this.OnPerChange();
            }
        }
        else if (this.porderForm.controls['itemtype'].value == 'reel') {
            // console.log('enter' + dimensionObj.ID);

            if (dimensionObj != undefined && dimensionObj.OD != null && dimensionObj.ID != null && dimensionObj.Face != null) {
                // console.log('dimesionId' + JSON.stringify(dimensionObj));
                weightPer = 0.00000617 * (((dimensionObj.OD * dimensionObj.OD) - (dimensionObj.ID * dimensionObj.ID)) * dimensionObj.Face);
                // console.log('reel' + weightPer);
                this.weightPer = weightPer;
                weightPer = weightPer.toFixed(2);
                this.porderForm.controls['weightperpiece'].setValue(weightPer);
                this.OnPerChange();
            }
        }
    }

    OnPerChange() {
        let weightWithPer: any;
        let selectedPer: any;
        let totalWeight: any;
        let weight = this.weightPer;
        if (this.porderForm.controls['weightpercent'].value != '') {
            selectedPer = this.porderForm.controls['weightpercent'].value;
            selectedPer = parseInt(selectedPer);
            // console.log('percentage' + JSON.stringify(selectedPer));
            weightWithPer = (weight / 100) * selectedPer;
            // weightWithPer = weightWithPer.toFixed(2);
            totalWeight = weight + weightWithPer;
            // console.log('weightper' + totalWeight);
            totalWeight = parseFloat(totalWeight).toFixed(3);
            // parseFloat(this.value).toFixed(2);
            this.porderForm.controls['weightWithPerc'].setValue(totalWeight);

        }
    }

    fetchAllPODetails() {
        this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.getAllPoDetails(headers).subscribe(data => {
            this.allPoDetailList = data.pODetailsData;
            this.listCount = this.allPoDetailList.length;
            this.allPoDetailList.forEach(element => {
                element.ordercnfdate = element.ordercnfdate.substring(0, 4) + '-' + element.ordercnfdate.substring(5, 7) + '-' + element.ordercnfdate.substring(8, 10);
                element.partypodate = element.partypodate.substring(0, 4) + '-' + element.partypodate.substring(5, 7) + '-' + element.partypodate.substring(8, 10);
                element.deliverydate = element.deliverydate.substring(0, 4) + '-' + element.deliverydate.substring(5, 7) + '-' + element.deliverydate.substring(8, 10);
            });
            // console.log('list here' + JSON.stringify(this.chemcompList));
            if (this.allPoDetailList != undefined) {
                this.spinner.hide();
            }

        })
    }

    showProductTable(event) {
        this.searchHistory = this.search;
        this.search = '';
        this.poProductDetail = event;
        this.displayName = event.data.partyname;
        this.displayId = event.data.partyid;
        this.partyService.poDetailsId = event.data._id;
        this.showpolist = false;
        this.fetchAllPoProduct();
        this.poadddiv = true;
        this.poProductTable = true;
        this.pobasictab = false;
        this.pochemcomp = false;
        this.poproducttab = false;
    }

    getProductDetails() {
        let event = this.poProductDetail;
        this.showpolist = false;
        this.poProductTable = false;
        this.pobasictab = true;
        this.pochemcomp = false;
        this.poproducttab = false;
        this.isNext = true;
        // console.log('here' + JSON.stringify(event));
        this.poadddiv = true;
        let ordercndate: any;
        let poPartydate: any;
        let deliveryDate: any;
        let odate: any;
        let podate: any;
        let deldate: any;
        let odateModal: any;
        let poModal: any;
        let delModal: any;
        let deliverybasis: any;
        odate = event.data.ordercnfdate;
        podate = event.data.partypodate;
        deldate = event.data.deliverydate;
        ordercndate = odate.substring(0, 4) + '-' + odate.substring(5, 7) + '-' + odate.substring(8, 10);
        poPartydate = podate.substring(0, 4) + '-' + podate.substring(5, 7) + '-' + podate.substring(8, 10);
        deliveryDate = deldate.substring(0, 4) + '-' + deldate.substring(5, 7) + '-' + deldate.substring(8, 10);
        odateModal = {
            "year": parseInt(odate.substring(0, 4)),
            "month": parseInt(odate.substring(5, 7)),
            "day": parseInt(odate.substring(8, 10))
        };
        poModal = {
            "year": parseInt(podate.substring(0, 4)),
            "month": parseInt(podate.substring(5, 7)),
            "day": parseInt(podate.substring(8, 10))
        };
        delModal = {
            "year": parseInt(deldate.substring(0, 4)),
            "month": parseInt(deldate.substring(5, 7)),
            "day": parseInt(deldate.substring(8, 10))
        };
        this.partyService.poDetailsId = event.data._id;
        this.partyService.poId = event.data.poid;
        if (event.data.deliverybasis == "EXWORK") {
            deliverybasis = 'EXWORK';
            this.createPocForm.controls['deliverybasis'].setValue(deliverybasis);
        }
        else if (event.data.deliverybasis == "FOR") {
            deliverybasis = 'FOR';
            this.createPocForm.controls['deliverybasis'].setValue(deliverybasis);
        }
        else {
            deliverybasis = 'OTHER';
            this.createPocForm.controls['deliverybasis'].setValue(deliverybasis);
            this.createPocForm.controls['deliverybasisText'].setValue(event.data.deliverybasis);
        }
        // console.log('modal' + JSON.stringify(odateModal));
        this.createPocForm.controls['partyid'].setValue(event.data.partyid),
            this.searchIdKeyWord = event.data.partyid,
            this.createPocForm.controls['partyname'].setValue(event.data.partyname),
            this.searchPartyKeyWord = event.data.partyname,
            this.createPocForm.controls['partyaddress'].setValue(event.data.partyaddress),
            this.createPocForm.controls['ponumber'].setValue(event.data.partyponame),
            this.createPocForm.controls['paymentterm'].setValue(event.data.paymentterm),
            this.createPocForm.controls['ordercnfdate'].setValue(odateModal),
            this.createPocForm.controls['podate'].setValue(poModal),
            this.createPocForm.controls['deliverydate'].setValue(delModal)
    }

    onNext() {
        this.showpolist = true;
        this.pobasictab = false;
        this.pochemcomp = false;
        this.poProductTable = false;
        this.poproducttab = true;
        this.fetchAllChemComp();
        this.fetchAllPoProduct();
        this.resetOrderForm('rollcasting');
        this.OnItemTypeChange('rolls');
    }

    onUpdate() {
        this.issubmitted = true;
        if (this.createPocForm.valid) {
            this.spinner.show();
            this.pobasictab = false;
            this.pochemcomp = false;
            this.poProductTable = false;
            this.poproducttab = true;
            // console.log('hide');
            const headers = new Headers(
                {
                    'accept': 'application/json',
                    'x-access-token': this.userToken
                });
            let podate = this.createPocForm.controls['podate'].value;
            // console.log(podate);
            let date = podate.year + '-' + podate.month + '-' + podate.day;
            // console.log(date);
            let podateutc = new Date(date).toUTCString();
            // console.log(podateutc);
            let ordercnfdate = this.createPocForm.controls['ordercnfdate'].value;
            let date1 = ordercnfdate.year + '-' + ordercnfdate.month + '-' + ordercnfdate.day;
            let ordercnfdateutc = new Date(date1).toUTCString();
            let deliverydate = this.createPocForm.controls['deliverydate'].value;
            let date2 = deliverydate.year + '-' + deliverydate.month + '-' + deliverydate.day;
            let deliverydateutc = new Date(date2).toUTCString();
            let deliverybasis: string;
            if (this.createPocForm.controls['deliverybasis'].value == "OTHER") {
                deliverybasis = this.createPocForm.controls['deliverybasisText'].value;
            }
            else {
                deliverybasis = this.createPocForm.controls['deliverybasis'].value;
            }
            let x = {
                _id: this.partyService.poDetailsId,
                partyid: this.createPocForm.controls['partyid'].value,
                partyname: this.createPocForm.controls['partyname'].value,
                partyaddress: this.createPocForm.controls['partyaddress'].value,
                partypodate: podateutc,
                partyponame: this.createPocForm.controls['ponumber'].value,
                paymentterm: this.createPocForm.controls['paymentterm'].value,
                deliverybasis: deliverybasis,
                poid: this.partyService.poId,
                ordercnfdate: ordercnfdateutc,
                deliverydate: deliverydateutc,
            };
            this.partyService.UpdatePODetails(x, headers).subscribe(data => {
                // console.log(data);
                this.fetchAllChemComp();
                this.resetOrderForm('rollcasting');
                this.OnItemTypeChange('rolls');
                // this.partyService.poDetailsId = data.createdRecord._id;
                this.porderForm.reset();
                this.pobasictab = false;
                this.pochemcomp = false;
                this.poProductTable = false;
                this.poproducttab = true;
                this.fetchAllPoProduct();
                this.spinner.hide();
            },
                err => console.log(err),
            );
            this.issubmitted = false;
        }
    }

    onPlusClick(event: any) {
        event.preventDefault();
        // console.log("plusss");
        // product.isAddButtonVisible = false;
        this.counter = this.counter + 1;
        this.addOrderToList();
        // this.setCartItem(product);
    }
    onMinusClick(event: any) {
        event.preventDefault();
        if (this.counter > 1) {
            this.counter = this.counter - 1;
            // this.removeCartItem(product, false);
        } else {
            // product.isAddButtonVisible = true;
            // this.removeCartItem(product, true);
        }
    }

    convert() {

        let value = [];

        this.poProductList.forEach(fuel => {
            // console.log('fuel' + JSON.stringify(fuel));
            let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
            fuel.dimensionForm = toString(fuel.dimensionForm);
            let subValueArray = [];
            // subValueArray.push(fuel.productId);
            subValueArray.push(fuel.processtype);
            subValueArray.push(fuel.grade);
            subValueArray.push(fuel.producttype);
            subValueArray.push(fuel.itemtype);
            subValueArray.push(fuel.uom);
            subValueArray.push(fuel.priceperuom);
            subValueArray.push(fuel.weightperpiece);
            subValueArray.push(fuel.quantity + " / " + fuel.dispatchedQuantity);
            subValueArray.push(fuel.reqhardness);
            subValueArray.push(fuel.dimensionForm);
            subValueArray.push(fuel.remark);
            subValueArray.push(fuel.chemicalcomp);
            subValueArray.push(fuel.productimg);
            value.push(subValueArray);
        });

        // let headerName = "Fuel Summary Report (" + this.fromDate.formatted + " to " + this.toDate.formatted + ")";
        let headerName = this.displayName;
        let body = {
            "headerName": headerName,
            "excelName": "PoProductListReport",
            "header": [
                // "ROLLID",
                "PROCESSTYPE",
                "GRADE",
                "PROD.TYPE",
                "ITEMTYPE",
                "UOM",
                "PRICE/UOM",
                "WEIGHT/PIECE",
                "Q / D",
                "REQ.HARD",
                "DIMENSION",
                "REMARK",
                "CHEMICAL COMP.",
                "PROD. IMG"
            ],
            "value": value
        }
        this.downloadExcelSheet(body).subscribe(blob => {
            // Doing it this way allows you to name the file
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = "PoProductList.xlsx";
            link.click();
        }, error => console.log("Error downloading the file."),
            () => console.log('Completed file download.'));
    }


    downloadExcelSheet(reqBody): Observable<Object[]> {
        //this.loading = true;
        return Observable.create(observer => {
            let xhr = new XMLHttpRequest();
            xhr.open('POST', AppConstants.EXCEL_DOWNLOAD_URL, true);
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.responseType = 'blob';

            xhr.onreadystatechange = () => {
                //this.loading = false;
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {

                        var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        var blob = new Blob([xhr.response], { type: contentType });
                        observer.next(blob);
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            xhr.send(JSON.stringify(reqBody));

        });
    }
    onlyNumbers(evt) {
        // console.log('evt' + this.porderForm.controls['quantity'].value);
        // console.log('evt' + evt);
        if (evt > 25) {
            evt = 25;
            this.porderForm.controls['quantity'].setValue(25);
        }
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    // tt(){

    //      this.filteredList=this.filter.transform(this.allPoDetailList, this.search);
    //      console.log('filtered data'+JSON.stringify(this.filteredList));

    // }

    convertPoDetailsExcel() {
        let value = [];
        this.filteredList = this.filter.transform(this.allPoDetailList, this.search);
        this.filteredList.forEach(fuel => {
            // console.log('fuel' + JSON.stringify(fuel));
            fuel.ordercnfdate = fuel.ordercnfdate.substring(0, 4) + '-' + fuel.ordercnfdate.substring(5, 7) + '-' + fuel.ordercnfdate.substring(8, 10);
            fuel.partypodate = fuel.partypodate.substring(0, 4) + '-' + fuel.partypodate.substring(5, 7) + '-' + fuel.partypodate.substring(8, 10);
            fuel.deliverydate = fuel.deliverydate.substring(0, 4) + '-' + fuel.deliverydate.substring(5, 7) + '-' + fuel.deliverydate.substring(8, 10);
            let subValueArray = [];
            subValueArray.push(fuel.partyid);
            subValueArray.push(fuel.partyname);
            subValueArray.push(fuel.partyaddress);
            subValueArray.push(fuel.partypodate);
            subValueArray.push(fuel.partyponame);
            subValueArray.push(fuel.paymentterm);
            subValueArray.push(fuel.deliverybasis);
            subValueArray.push(fuel.poid);
            subValueArray.push(fuel.ordercnfdate);
            subValueArray.push(fuel.deliverydate);
            value.push(subValueArray);
        });

        // let headerName = "Fuel Summary Report (" + this.fromDate.formatted + " to " + this.toDate.formatted + ")";
        let headerName = "PoProductList Report";
        let body = {
            "headerName": headerName,
            "excelName": "PoDetailsReport",
            "header": [
                "PARTYID",
                "PARTYNAME",
                "PARTYADDRESS",
                "PARTYPODATE",
                "PARTYPONAME",
                "PAYMENTTERM",
                "DELIVERYBASIS",
                "POID",
                "ORDERDNFDATE",
                "DELIVERYDATE"
            ],
            "value": value
        }
        this.downloadExcelSheet(body).subscribe(blob => {
            // Doing it this way allows you to name the file
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = "PoProductList.xlsx";
            link.click();
        }, error => console.log("Error downloading the file."),
            () => console.log('Completed file download.'));
    }


    upload() {
        if (this.base64textString != undefined) {
            this.spinner.show();
            this.fileUploadToAwsS3(this.base64textString).then((awsS3FileResult: any) => {
                // console.log("awsS3FileResult kkkkk" + JSON.stringify(awsS3FileResult));
                this.imageDataList.push({ 'pic': awsS3FileResult.Location });
                this.addOrderToList();
                this.spinner.hide();
            });
        }
        else {
            this.addOrderToList();
        }
    }


    UpdateImage() {
        if (this.base64textString != undefined) {
            this.spinner.show();
            this.fileUploadToAwsS3(this.base64textString).then((awsS3FileResult: any) => {
                // console.log("awsS3FileResult kkkkk" + JSON.stringify(awsS3FileResult));
                this.imageDataList.push({ 'pic': awsS3FileResult.Location });
                this.updateOrderToList();
                this.spinner.hide();
            });
        }
        else {
            this.updateOrderToList();
        }
    }

    handleFileSelect(evt) {
        var files = evt.target.files;
        var file = files[0];

        if (files && file) {
            var reader = new FileReader();

            reader.onload = this._handleReaderLoaded.bind(this);

            reader.readAsBinaryString(file);
        }
    }

    _handleReaderLoaded(readerEvt) {
        var binaryString = readerEvt.target.result;
        this.base64textString = btoa(binaryString);
        this.base64textString = 'data:image/jpeg;base64,' + this.base64textString;
    }

    async fileUploadToAwsS3(uploadFile) {

        var dataURItoBlob = (dataURI) => {
            var binary = atob(dataURI.split(',')[1]);
            var array = [];
            for (var i = 0; i < binary.length; i++) {
                array.push(binary.charCodeAt(i));
            }
            return new Blob([new Uint8Array(array)], { type: "image/jpeg" });
        };

        //imgData returned from camera getPicture
        var body = await dataURItoBlob(uploadFile);
        // var body = imageData;
        return new Promise(function (resolve, reject) {
            let AWS = (<any>window).AWS;
            AWS.config.update({
                accessKeyId: 'AKIAISNSUQEMBYVBUIRQ',
                secretAccessKey: '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',
                region: 'ap-south-1',
                endpoint: 'https://s3.ap-south-1.amazonaws.com/',
                sslEnabled: false,
                s3ForcePathStyle: true,
                signatureVersion: 'v4'
            });

            let awsS3Options = {
                'params': { 'Bucket': 'modernro' },
                'accessKeyId': 'AKIAISNSUQEMBYVBUIRQ',
                'secretAccessKey': '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',
                'endpoint': 'https://s3.ap-south-1.amazonaws.com/',
                'region': 'ap-south-1'
            };

            let modifiedName: string = `${Date.now()}_attached`;
            let awsS3UploadParams = {
                'Bucket': 'modernro',
                'Key': `engimgs/${modifiedName}`,
                'ContentType': "image/jpeg",
                'Body': body
            };

            let bucket = new AWS.S3(awsS3Options);
            bucket.upload(awsS3UploadParams, (err, data) => {
                if (err) {
                    reject(err);
                }
                resolve(data);
            });
        });
    }

    previewImage(event) {
        this.image = this.poProductList[event].productimg;
        // console.log('dataaiMage' + JSON.stringify(this.image));

    }

    onKeyup() {
        let filteredList = [];
        if (this.search.length == 0) {
            this.listCount = this.allPoDetailList.length;
        }
        else {
            filteredList = this.filter.transform(this.allPoDetailList, this.search);
            this.listCount = filteredList.length;
        }
    }
}
