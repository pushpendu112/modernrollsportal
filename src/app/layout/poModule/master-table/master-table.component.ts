import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { PartyService } from 'src/app/shared/services/party.service';
import * as CryptoJS from 'crypto-js';
import { AppConstants } from '../../../base/appconstants';
import { NgxSpinnerService } from 'ngx-spinner';
import { QueueStatus } from '../../../models/queueStatus';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Ng2SearchPipe } from 'ng2-search-filter';
import { Observable } from 'rxjs';
declare var jquery: any;   // not required
declare var $: any;   // not required
@Component({
    selector: 'app-master-table',
    templateUrl: './master-table.component.html',
    styleUrls: ['./master-table.component.scss'],
    // encapsulation: ViewEncapsulation.None
})
export class MasterTableComponent implements OnInit {

    searchtype: any = "0";
    basicSearchTerm: any = "";
    isDisToggle: boolean = false;

    constructor(private partyService: PartyService, private spinner: NgxSpinnerService, private formBuilder: FormBuilder) {
        this.filter = new Ng2SearchPipe();
        this.search = '';
        this.fromDate = '';
        this.toDate = '';
    }
    filter: Ng2SearchPipe;
    filteredList: any = [];
    listCount: any;
    fromDate: any;
    toDate: any;
    statusQueue: any = [];
    dcols: any[];
    tempduplicatepoProductList: any = [];
    poProductList: any = [];
    duplicatepoProductList: any = [];
    userToken: any;
    remark: string = '';
    index: number;
    search: any;
    data: any;
    image: any;
    pocols: any[];
    moveCast: boolean;
    machineNo: string;
    OperatorName: string;
    actualDimension: any;
    actualHardness: any;
    inspection: any;
    inspectorName: any;
    finalremark: any;
    finalDimension: any;
    finalInspectorName: any;
    finalRemark: any;
    chemcompForm: FormGroup;
    chemcompForm1: FormGroup;
    dispatchedTime: any;
    isDispacthed: boolean = false;
    dia1: any;
    dia2: any;
    dia3: any;
    dia4: any;
    dia5: any;
    dia6: any;
    dia7: any;
    dia8: any;
    dia9: any;
    dia10: any;
    dia11: any;
    dia12: any;
    dia13: any;
    dia14: any;
    dia15: any;
    dia16: any;
    dia17: any;
    dispatchedProd: boolean = false;
    colorMap = {
        "ADAMITS": "red",
        "CHILLS": "blue",
        "SG-IRON": "green",
        "SG-ACC": "#c4c233"
    }
    ngOnInit() {
        const getjwt = localStorage.getItem('jwttoken');
        this.userToken = this.conversionDecrypt(getjwt);
        this.empRegisterForms();
        this.testCertificateForm();
        // this.fetchAllPoProduct(false);
        this.pocols = [
            { field: 'idList.displayId', header: 'Roll Id', width: '7%' },
            // { field: 'processtype', header: 'Process Type', width: '8%' },
            // { field: 'grade', header: 'Grade', width: '7%' },
            { field: 'producttype', header: 'Prod. Type', width: '7%' },
            { field: 'itemtype', header: 'Item Type', width: '7%' },
            { field: 'partyData.partyname', header: 'PartyName', width: '8%' },
            // { field: 'uom', header: 'UOM', width: '8%' },
            // { field: 'priceperuom', header: 'Price/UOM', width: '8%' },
            { field: 'weightperpiece', header: 'Weight/Piece', width: '9%' },
            // { field: 'idList.currentQueue', header: 'Status', width: '7%' },
            // { field: 'idList.queueUpdateTimeStamp', header: 'PlanningDate', width: '7%' },
            { field: 'reqhardness', header: 'Req. Hard', width: '8%' },
            { field: 'dimensionFormString', header: 'Dimension', width: '7%' },
            { field: 'remark', header: 'Remark', width: '7%' },
        ];

        this.dcols = [
            { field: 'queue', header: 'Status', width: '10%' },
            { field: 'startTime', header: 'StartTime', width: '10%' },
            { field: 'endTime', header: 'EndTime', width: '10%' },
            { field: 'durationString', header: 'Duration', width: '10%' },
        ];
    }
    conversionDecrypt(decrypt) {
        try {
            const bytes = CryptoJS.AES.decrypt(decrypt, AppConstants.secret);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return decrypt;
        } catch (e) {
            console.log(e);
        }
    }

    empRegisterForms() {
        this.chemcompForm = this.formBuilder.group({
            partyname: ['', Validators.required],
            clientgrade: ['', Validators.required],
            cpercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            cpercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mnpercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mnpercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            sipercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            sipercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            spercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            spercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            ppercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            ppercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            crpercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            crpercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            nipercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            nipercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mopercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mopercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            remark: ['', Validators.required]

        });
    }

    testCertificateForm() {
        this.chemcompForm1 = this.formBuilder.group({
            partyname: ['', Validators.required],
            clientgrade: ['', Validators.required],
            cpercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mnpercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            sipercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            spercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            ppercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            crpercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            nipercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mopercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            remark: [''],
            drgNumber: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            quality: [''],
            sizeofRoll: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            orderHardBarrel: [''],
            actualHardBarrel: [''],
            remarks: [''],
            rollFor: [''],
            itemNo: [''],
            dateOfOrder: [''],
            ourOrderNo: [''],
            dateOfTest: [''],
            dia1: [''],
            dia2: [''],
            dia3: [''],
            dia4: [''],
            dia5: [''],
            dia6: [''],
            dia7: [''],
            dia8: [''],
            dia9: [''],
            dia10: [''],
            dia11: [''],
            dia12: [''],
            dia13: [''],
            dia14: [''],
            dia15: [''],
            dia16: [''],
            dia17: [''],
            dia18: [''],
            long1: [''],
            long2: [''],
            long3: [''],
            long4: [''],
            long5: [''],
            long6: [''],
            long7: [''],
            long8: [''],
            long9: [''],
            long10: [''],
            long11: [''],
            long12: [''],
            long13: [''],
            long14: [''],
            long15: [''],
            long16: [''],
            long17: [''],
            long18: [''],
            act1: [''],
            act2: [''],
            act3: [''],
            act4: [''],
            act5: [''],
            act6: [''],
            act7: [''],
            act8: [''],
            act9: [''],
            act10: [''],
            act11: [''],
            act12: [''],
            act13: [''],
            act14: [''],
            act15: [''],
            act16: [''],
            act17: [''],
            act18: [''],
            lact1: [''],
            lact2: [''],
            lact3: [''],
            lact4: [''],
            lact5: [''],
            lact6: [''],
            lact7: [''],
            lact8: [''],
            lact9: [''],
            lact10: [''],
            lact11: [''],
            lact12: [''],
            lact13: [''],
            lact14: [''],
            lact15: [''],
            lact16: [''],
            lact17: [''],
            lact18: ['']
        });
    }


    searchProduct(isCheck) {
        let filteredList = [];
        this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.duplicatepoProductList = [];
        this.tempduplicatepoProductList = [];

        let x = {
            searchTerm: this.basicSearchTerm,
            searchtype: this.searchtype
        };
        if (this.basicSearchTerm == "") {
            this.spinner.hide();
            return;
        }
        this.partyService.searchPOProductByQueueStatusForMasterTable(x, headers).subscribe(data => {
            this.poProductList = data.pOProductData;
            this.poProductList.forEach(element => {
                if ((element.idList.currentQueue > 1 || element.idList.currentQueue == 0) && element.idList.isDispacthed == this.isDisToggle) {
                    this.duplicatepoProductList.push(element);
                }
            });
            if (this.search.length == 0) {
                this.listCount = this.duplicatepoProductList.length;
            }
            else {
                filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                this.listCount = filteredList.length;
            }
            this.duplicatepoProductList.forEach(element => {
                if (element.processtype == 0) {
                    element.processtype = 'FORGING';
                }
                else {
                    element.processtype = 'ROLL CASTING';
                }
                if (element.itemtype == 0) {
                    element.itemtype = 'ROLLS';
                }
                else if (element.itemtype == 1) {
                    element.itemtype = 'STEP ROLLS';
                }
                else if (element.itemtype == 2) {
                    element.itemtype = 'REEL';
                }
                else {
                    element.itemtype = 'OTHER';
                }
                if (element.uom == 0) {
                    element.uom = 'Per-KG';
                }
                else if (element.uom == 1) {
                    element.uom = 'MT';
                }
                else {
                    element.uom = 'Per-Piece';
                }
                if (element.idList.currentQueue == '1') {
                    element.idList.currentQueue = 'Planning';
                }
                else if (element.idList.currentQueue == '2') {
                    element.idList.currentQueue = 'Roll Casting';
                }
                else if (element.idList.currentQueue == '0') {
                    element.idList.currentQueue = 'Rejected';
                }
                else if (element.idList.currentQueue == '3') {
                    element.idList.currentQueue = 'QualityCheck';
                }
                else if (element.idList.currentQueue == '4') {
                    element.idList.currentQueue = 'Machining';
                }
                else if (element.idList.currentQueue == '5') {
                    element.idList.currentQueue = 'Inspection';
                }
                else if (element.idList.currentQueue == '6') {
                    element.idList.currentQueue = 'GrovingCentreFinishing';
                }
                else if (element.idList.currentQueue == '7') {
                    element.idList.currentQueue = 'BearingShapperFinishing';
                }
                else if (element.idList.currentQueue == '8') {
                    element.idList.currentQueue = 'GrovingCentreFinal';
                }
                else if (element.idList.currentQueue == '9') {
                    element.idList.currentQueue = 'BearingShapperFinal';
                }
                else if (element.idList.currentQueue == '10' && element.idList.isDispacthed == true) {
                    element.idList.currentQueue = 'Dispatched';
                }
                else if (element.idList.currentQueue == '10' && element.idList.isDispacthed == false) {
                    element.idList.currentQueue = 'FinalInspection';
                }
                // else if (element.idList.currentQueue == '11') {
                //     element.idList.currentQueue = 'TransportFinish';
                // }
                // else if (element.idList.currentQueue == '12') {
                //     element.idList.currentQueue = 'TestCertificateFinish';
                // }
                // else if (element.idList.currentQueue == '13') {
                //     element.idList.currentQueue = 'PackagingFinish';
                // }
                // else if (element.idList.currentQueue == '14') {
                //     element.idList.currentQueue = 'FinalReport';
                // }
                let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                element['dimensionFormString'] = toString(element.dimensionForm);

            });
            this.tempduplicatepoProductList = this.duplicatepoProductList;
            if (isCheck == true) {
                this.fromDate = '';
                this.toDate = '';
            }
            if (this.fromDate != '' && this.toDate != '') {
                this.onDateSelection();
            }
            // console.log('here' + JSON.stringify(this.duplicatepoProductList));
            this.spinner.hide();

        })
    }

    castingHistory() {
        this.dispatchedProd = !this.dispatchedProd;
        // console.log('boolean' + this.history);
        this.fetchAllPoProduct(true);
        this.listCount = 0;
        this.duplicatepoProductList = [];
        this.basicSearchTerm = "";
        this.searchtype = "0"
    }

    fetchAllPoProduct(isCheck) {
        let filteredList = [];
        this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.duplicatepoProductList = [];
        this.tempduplicatepoProductList = [];
        this.partyService.getDispatchPOProductForModernRolls(`basicNode/services/getDispatchPOProductForModernRolls/${this.dispatchedProd}`, headers).subscribe(data => {
            this.poProductList = data.pOProductData;
            this.poProductList.forEach(element => {
                if (element.idList.currentQueue > 1 || element.idList.currentQueue == 0) {
                    this.duplicatepoProductList.push(element);
                }
            });
            if (this.search.length == 0) {
                this.listCount = this.duplicatepoProductList.length;
            }
            else {
                filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                this.listCount = filteredList.length;
            }
            this.duplicatepoProductList.forEach(element => {
                if (element.processtype == 0) {
                    element.processtype = 'FORGING';
                }
                else {
                    element.processtype = 'ROLL CASTING';
                }
                if (element.itemtype == 0) {
                    element.itemtype = 'ROLLS';
                }
                else if (element.itemtype == 1) {
                    element.itemtype = 'STEP ROLLS';
                }
                else if (element.itemtype == 2) {
                    element.itemtype = 'REEL';
                }
                else {
                    element.itemtype = 'OTHER';
                }
                if (element.uom == 0) {
                    element.uom = 'Per-KG';
                }
                else if (element.uom == 1) {
                    element.uom = 'MT';
                }
                else {
                    element.uom = 'Per-Piece';
                }
                if (element.idList.currentQueue == '1') {
                    element.idList.currentQueue = 'Planning';
                }
                else if (element.idList.currentQueue == '2') {
                    element.idList.currentQueue = 'Roll Casting';
                }
                else if (element.idList.currentQueue == '0') {
                    element.idList.currentQueue = 'Rejected';
                }
                else if (element.idList.currentQueue == '3') {
                    element.idList.currentQueue = 'QualityCheck';
                }
                else if (element.idList.currentQueue == '4') {
                    element.idList.currentQueue = 'Machining';
                }
                else if (element.idList.currentQueue == '5') {
                    element.idList.currentQueue = 'Inspection';
                }
                else if (element.idList.currentQueue == '6') {
                    element.idList.currentQueue = 'GrovingCentreFinishing';
                }
                else if (element.idList.currentQueue == '7') {
                    element.idList.currentQueue = 'BearingShapperFinishing';
                }
                else if (element.idList.currentQueue == '8') {
                    element.idList.currentQueue = 'GrovingCentreFinal';
                }
                else if (element.idList.currentQueue == '9') {
                    element.idList.currentQueue = 'BearingShapperFinal';
                }
                else if (element.idList.currentQueue == '10' && element.idList.isDispacthed == true) {
                    element.idList.currentQueue = 'Dispatched';
                }
                else if (element.idList.currentQueue == '10' && element.idList.isDispacthed == false) {
                    element.idList.currentQueue = 'FinalInspection';
                }
                // else if (element.idList.currentQueue == '11') {
                //     element.idList.currentQueue = 'TransportFinish';
                // }
                // else if (element.idList.currentQueue == '12') {
                //     element.idList.currentQueue = 'TestCertificateFinish';
                // }
                // else if (element.idList.currentQueue == '13') {
                //     element.idList.currentQueue = 'PackagingFinish';
                // }
                // else if (element.idList.currentQueue == '14') {
                //     element.idList.currentQueue = 'FinalReport';
                // }
                let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                element['dimensionFormString'] = toString(element.dimensionForm);

            });
            this.tempduplicatepoProductList = this.duplicatepoProductList;
            if (isCheck == true) {
                this.fromDate = '';
                this.toDate = '';
            }
            if (this.fromDate != '' && this.toDate != '') {
                this.onDateSelection();
            }
            // console.log('here' + JSON.stringify(this.duplicatepoProductList));
            this.spinner.hide();
        })
    }

    remarkData(event) {
        this.remark = '';
        this.data = event;
    }


    previewImage(event) {
        this.image = this.poProductList[event].productimg;
        // console.log('dataaiMage' + JSON.stringify(this.image));

    }


    machineData(event) {

        this.data = event;
        this.machineNo = this.data.idList.mechiningData.mechineNo;
        this.OperatorName = this.data.idList.mechiningData.operatorName;
    }

    dimensionData(event) {

        this.data = event;
        this.actualDimension = this.data.idList.inspectionData.actualDimension;
        this.actualHardness = this.data.idList.inspectionData.actualHardness;

    }

    inspectionData(event) {
        this.inspection = event.idList.gACInspectionData.grooveInspection;
        this.inspectorName = event.idList.gACInspectionData.inspectorName;
        this.finalremark = event.idList.gACInspectionData.remark;
    }

    finalDimensionData(event) {
        this.finalDimension = event.idList.bASInspectionData.finalDimension;
        this.finalInspectorName = event.idList.bASInspectionData.finalInspectorName;
        this.finalRemark = event.idList.bASInspectionData.finalRemark;
    }

    bearingData(event) {
        this.dia1 = event.idList.bASInspectionData.actualBearingDia[0];
        this.dia2 = event.idList.bASInspectionData.actualBearingDia[1];
        this.dia3 = event.idList.bASInspectionData.actualBearingDia[2];
        this.dia4 = event.idList.bASInspectionData.actualBearingDia[3];
        this.dia5 = event.idList.bASInspectionData.actualBearingDia[4];
        this.dia6 = event.idList.bASInspectionData.actualBearingDia[5];
        this.dia7 = event.idList.bASInspectionData.actualBearingDia[6];
        this.dia8 = event.idList.bASInspectionData.actualBearingDia[7];
        this.dia9 = event.idList.bASInspectionData.actualBearingDia[8];
        this.dia10 = event.idList.bASInspectionData.actualBearingDia[9];
        this.dia11 = event.idList.bASInspectionData.actualBearingDia[10];
        this.dia12 = event.idList.bASInspectionData.actualBearingDia[11];
        this.dia13 = event.idList.bASInspectionData.actualBearingDia[12];
        this.dia14 = event.idList.bASInspectionData.actualBearingDia[13];
        this.dia15 = event.idList.bASInspectionData.actualBearingDia[14];
        this.dia16 = event.idList.bASInspectionData.actualBearingDia[15];
        this.dia17 = event.idList.bASInspectionData.actualBearingDia[16];
    }

    updateChemComp(event) {
        this.data = event;
        this.chemcompForm1.reset();
        this.chemcompForm.reset();
        let data = this.data.idList.testCertificateData;
        if (this.data.producttype == "GROOVE" || this.data.producttype == "CENTER" || this.data.producttype == "OTHERS") {
            // console.log('hello');

            $('#exampleModalCenter1').modal('show');
        }
        else {
            // console.log('BYE' + JSON.stringify(this.poProductList[index].idList.bAndSTestCertificateData));
            let dateoforder = this.data.partyData.ordercnfdate.substring(0, 4) + '-' + this.data.partyData.ordercnfdate.substring(5, 7) + '-' + this.data.partyData.ordercnfdate.substring(8, 10);
            this.chemcompForm1.controls['rollFor'].setValue(this.data.partyData.partyname);
            this.chemcompForm1.controls['itemNo'].setValue(this.data.idList.displayId);
            this.chemcompForm1.controls['dateOfOrder'].setValue(dateoforder);
            this.chemcompForm1.controls['ourOrderNo'].setValue(this.data.partyData.partyid);
            this.chemcompForm1.controls['cpercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.cpercentage);
            this.chemcompForm1.controls['spercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.spercentage);
            this.chemcompForm1.controls['sipercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.sipercentage);
            this.chemcompForm1.controls['remarks'].setValue(this.data.idList.bAndSTestCertificateData.ccData.remark);
            this.chemcompForm1.controls['ppercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.ppercentage);
            this.chemcompForm1.controls['nipercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.nipercentage);
            this.chemcompForm1.controls['mopercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.mopercentage);
            this.chemcompForm1.controls['mnpercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.mnpercentage);
            this.chemcompForm1.controls['crpercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.crpercentage);
            this.chemcompForm1.controls['drgNumber'].setValue(this.data.idList.bAndSTestCertificateData.drgNumber);
            this.chemcompForm1.controls['dateOfTest'].setValue(this.data.idList.bAndSTestCertificateData.dateOfTest);
            this.chemcompForm1.controls['quality'].setValue(this.data.idList.bAndSTestCertificateData.quality);
            this.chemcompForm1.controls['sizeofRoll'].setValue(this.data.idList.bAndSTestCertificateData.sizeofRoll);
            this.chemcompForm1.controls['orderHardBarrel'].setValue(this.data.idList.bAndSTestCertificateData.orderHardBarrel);
            this.chemcompForm1.controls['actualHardBarrel'].setValue(this.data.idList.bAndSTestCertificateData.actualHardBarrel);
            this.chemcompForm1.controls['remarks'].setValue(this.data.idList.bAndSTestCertificateData.remarks);
            if (this.data.idList.bAndSTestCertificateData.diaDrg.length > 0) {
                this.chemcompForm1.controls['dia1'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[0]);
                this.chemcompForm1.controls['dia2'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[1]);
                this.chemcompForm1.controls['dia3'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[2]);
                this.chemcompForm1.controls['dia4'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[3]);
                this.chemcompForm1.controls['dia5'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[4]);
                this.chemcompForm1.controls['dia6'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[5]);
                this.chemcompForm1.controls['dia7'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[6]);
                this.chemcompForm1.controls['dia8'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[7]);
                this.chemcompForm1.controls['dia9'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[8]);
                this.chemcompForm1.controls['dia10'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[9]);
                this.chemcompForm1.controls['dia11'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[10]);
                this.chemcompForm1.controls['dia12'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[11]);
                this.chemcompForm1.controls['dia13'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[12]);
                this.chemcompForm1.controls['dia14'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[13]);
                this.chemcompForm1.controls['dia15'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[14]);
                this.chemcompForm1.controls['dia16'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[15]);
                this.chemcompForm1.controls['dia17'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[16]);
                this.chemcompForm1.controls['dia18'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[17]);
            }
            if (this.data.idList.bAndSTestCertificateData.longDrg.length > 0) {
                this.chemcompForm1.controls['long1'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[0]);
                this.chemcompForm1.controls['long2'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[1]);
                this.chemcompForm1.controls['long3'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[2]);
                this.chemcompForm1.controls['long4'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[3]);
                this.chemcompForm1.controls['long5'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[4]);
                this.chemcompForm1.controls['long6'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[5]);
                this.chemcompForm1.controls['long7'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[6]);
                this.chemcompForm1.controls['long8'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[7]);
                this.chemcompForm1.controls['long9'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[8]);
                this.chemcompForm1.controls['long10'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[9]);
                this.chemcompForm1.controls['long11'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[10]);
                this.chemcompForm1.controls['long12'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[11]);
                this.chemcompForm1.controls['long13'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[12]);
                this.chemcompForm1.controls['long14'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[13]);
                this.chemcompForm1.controls['long15'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[14]);
                this.chemcompForm1.controls['long16'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[15]);
                this.chemcompForm1.controls['long17'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[16]);
                this.chemcompForm1.controls['long18'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[17]);
            }
            if (this.data.idList.bAndSTestCertificateData.diaActual.length > 0) {
                this.chemcompForm1.controls['act1'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[0]);
                this.chemcompForm1.controls['act2'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[1]);
                this.chemcompForm1.controls['act3'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[2]);
                this.chemcompForm1.controls['act4'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[3]);
                this.chemcompForm1.controls['act5'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[4]);
                this.chemcompForm1.controls['act6'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[5]);
                this.chemcompForm1.controls['act7'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[6]);
                this.chemcompForm1.controls['act8'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[7]);
                this.chemcompForm1.controls['act9'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[8]);
                this.chemcompForm1.controls['act10'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[9]);
                this.chemcompForm1.controls['act11'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[10]);
                this.chemcompForm1.controls['act12'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[11]);
                this.chemcompForm1.controls['act13'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[12]);
                this.chemcompForm1.controls['act14'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[13]);
                this.chemcompForm1.controls['act15'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[14]);
                this.chemcompForm1.controls['act16'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[15]);
                this.chemcompForm1.controls['act17'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[16]);
                this.chemcompForm1.controls['act18'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[17]);
            }
            if (this.data.idList.bAndSTestCertificateData.longActual.length > 0) {
                this.chemcompForm1.controls['lact1'].setValue(this.data.idList.bAndSTestCertificateData.longActual[0]);
                this.chemcompForm1.controls['lact2'].setValue(this.data.idList.bAndSTestCertificateData.longActual[1]);
                this.chemcompForm1.controls['lact3'].setValue(this.data.idList.bAndSTestCertificateData.longActual[2]);
                this.chemcompForm1.controls['lact4'].setValue(this.data.idList.bAndSTestCertificateData.longActual[3]);
                this.chemcompForm1.controls['lact5'].setValue(this.data.idList.bAndSTestCertificateData.longActual[4]);
                this.chemcompForm1.controls['lact6'].setValue(this.data.idList.bAndSTestCertificateData.longActual[5]);
                this.chemcompForm1.controls['lact7'].setValue(this.data.idList.bAndSTestCertificateData.longActual[6]);
                this.chemcompForm1.controls['lact8'].setValue(this.data.idList.bAndSTestCertificateData.longActual[7]);
                this.chemcompForm1.controls['lact9'].setValue(this.data.idList.bAndSTestCertificateData.longActual[8]);
                this.chemcompForm1.controls['lact10'].setValue(this.data.idList.bAndSTestCertificateData.longActual[9]);
                this.chemcompForm1.controls['lact11'].setValue(this.data.idList.bAndSTestCertificateData.longActual[10]);
                this.chemcompForm1.controls['lact12'].setValue(this.data.idList.bAndSTestCertificateData.longActual[11]);
                this.chemcompForm1.controls['lact13'].setValue(this.data.idList.bAndSTestCertificateData.longActual[12]);
                this.chemcompForm1.controls['lact14'].setValue(this.data.idList.bAndSTestCertificateData.longActual[13]);
                this.chemcompForm1.controls['lact15'].setValue(this.data.idList.bAndSTestCertificateData.longActual[14]);
                this.chemcompForm1.controls['lact16'].setValue(this.data.idList.bAndSTestCertificateData.longActual[15]);
                this.chemcompForm1.controls['lact17'].setValue(this.data.idList.bAndSTestCertificateData.longActual[16]);
                this.chemcompForm1.controls['lact18'].setValue(this.data.idList.bAndSTestCertificateData.longActual[17]);
            }
            $('#exampleModalCenter3').modal('show');
        }
        let cper = data.cpercentage;
        let cperArr = cper.split('-');
        // this.chemcompForm.controls['partyname'].setValue(data.partyName);
        // this.chemcompForm.controls['clientgrade'].setValue(this.chemCompData.clientGrade);
        this.chemcompForm.controls['remark'].setValue(data.remark);
        this.chemcompForm.controls['cpercentagefrom'].setValue(cperArr[0]);
        this.chemcompForm.controls['cpercentageto'].setValue(cperArr[1]);
        let mnper = data.mnpercentage;
        let mnperArr = mnper.split('-');
        this.chemcompForm.controls['mnpercentagefrom'].setValue(mnperArr[0]);
        this.chemcompForm.controls['mnpercentageto'].setValue(mnperArr[1]);
        let siper = data.sipercentage;
        let siperArr = siper.split('-');
        this.chemcompForm.controls['sipercentagefrom'].setValue(siperArr[0]);
        this.chemcompForm.controls['sipercentageto'].setValue(siperArr[1]);
        let sper = data.spercentage;
        let sperArr = sper.split('-');
        this.chemcompForm.controls['spercentagefrom'].setValue(sperArr[0]);
        this.chemcompForm.controls['spercentageto'].setValue(sperArr[1]);
        let pper = data.ppercentage;
        let pperArr = pper.split('-');
        this.chemcompForm.controls['ppercentagefrom'].setValue(pperArr[0]);
        this.chemcompForm.controls['ppercentageto'].setValue(pperArr[1]);
        let crper = data.crpercentage;
        let crperArr = crper.split('-');
        this.chemcompForm.controls['crpercentagefrom'].setValue(crperArr[0]);
        this.chemcompForm.controls['crpercentageto'].setValue(crperArr[1]);
        let niper = data.nipercentage;
        let niperArr = niper.split('-');
        this.chemcompForm.controls['nipercentagefrom'].setValue(niperArr[0]);
        this.chemcompForm.controls['nipercentageto'].setValue(niperArr[1]);
        let moper = data.mopercentage;
        let moperArr = moper.split('-');
        this.chemcompForm.controls['mopercentagefrom'].setValue(moperArr[0]);
        this.chemcompForm.controls['mopercentageto'].setValue(moperArr[1]);
    }

    onDateSelection() {
        let fromdateutc: any;
        let todateutc: any;
        // console.log("--1--", this.fromDate);
        // console.log("--2--", this.toDate);
        if (this.fromDate != '' && this.toDate != '') {
            if (JSON.stringify(this.fromDate) == JSON.stringify(this.toDate)) {
                this.toDate.day = this.toDate.day + 1;
            }
        }
        this.duplicatepoProductList = this.tempduplicatepoProductList;
        fromdateutc = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
        fromdateutc = fromdateutc.getTime();
        todateutc = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
        todateutc = todateutc.getTime();
        let duplicatepoProductList = [];
        duplicatepoProductList = this.duplicatepoProductList;
        this.duplicatepoProductList = [];
        duplicatepoProductList.forEach(element => {
            element.filterDate = new Date(element.idList.queueUpdateTimeStamp).getTime();
        });
        duplicatepoProductList.forEach(element => {
            if (element.filterDate >= fromdateutc && element.filterDate <= todateutc) {
                this.duplicatepoProductList.push(element);
            }
        });
        this.listCount = this.duplicatepoProductList.length;
    }

    getProductDetails(event) {

        this.statusQueue = [];
        // this.search = '';
        this.isDispacthed = event.data.idList.isDispacthed;
        this.dispatchedTime = event.data.idList.queueUpdateTimeStamp;
        this.dispatchedTime = this.dispatchedTime.substring(0, 4) + '-' + this.dispatchedTime.substring(5, 7) + '-' + this.dispatchedTime.substring(8, 10);

        this.statusQueue = event.data.idList.queueDurationList;
        this.statusQueue.forEach(element => {
            if (element.queue == '1') {
                element.queue = 'Planning';
            }
            else if (element.queue == '2') {
                element.queue = 'Roll Casting';
            }
            else if (element.queue == '0') {
                element.queue = 'Rejected';
            }
            else if (element.queue == '3') {
                element.queue = 'QualityCheck';
            }
            else if (element.queue == '4') {
                element.queue = 'Machining';
            }
            else if (element.queue == '5') {
                element.queue = 'Inspection';
            }
            else if (element.queue == '6') {
                element.queue = 'GrovingCentreFinishing';
            }
            else if (element.queue == '7') {
                element.queue = 'BearingShapperFinishing';
            }
            else if (element.queue == '8') {
                element.queue = 'GrovingCentreFinal';
            }
            else if (element.queue == '9') {
                element.queue = 'BearingShapperFinal';
            }
            else if (element.queue == '10') {
                element.queue = 'FinalInspection';
            }
            else if (element.queue == '11') {
                element.queue = 'TransportFinish';
            }
            else if (element.queue == '12') {
                element.queue = 'TestCertificateFinish';
            }
            else if (element.queue == '13') {
                element.queue = 'PackagingFinish';
            }
            else if (element.queue == '14') {
                element.queue = 'FinalReport';
            }
            element.startTime = element.startTime.substring(0, 4) + '-' + element.startTime.substring(5, 7) + '-' + element.startTime.substring(8, 10) + '  ' + element.startTime.substring(12, 16);
            element.endTime = element.endTime.substring(0, 4) + '-' + element.endTime.substring(5, 7) + '-' + element.endTime.substring(8, 10) + '  ' + element.endTime.substring(12, 16);
            element.durationString = this.secondsToHms(element.duration);
            // element.duration = parseInt(element.duration);
        });
        $('#duration').modal('show');
    }

    secondsToHms(d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);

        var hDisplay = h > 0 ? h + (h == 1 ? " hr, " : " hrs, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? " min, " : " mins, ") : "";
        var sDisplay = s > 0 ? s + (s == 1 ? " sec" : " secs") : "";
        return hDisplay + mDisplay + sDisplay;
    }

    onKeyup() {
        let filteredList = [];
        if (this.search.length == 0) {
            this.listCount = this.duplicatepoProductList.length;
        }
        else {
            // console.log('dataata' + JSON.stringify(this.duplicatepoProductList));
            filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
            this.listCount = filteredList.length;
            // console.log('count'+this.listCount);

        }
    }

    convertPoDetailsExcel() {
        let value = [];

        this.filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
        // console.log('dataata' + JSON.stringify(this.filteredList));

        this.filteredList.forEach(fuel => {
            // console.log('fuel' + JSON.stringify(fuel));
            let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
            fuel.dimensionForm = toString(fuel.dimensionForm);
            let subValueArray = [];
            subValueArray.push(fuel.idList.displayId);
            subValueArray.push(fuel.producttype);
            subValueArray.push(fuel.itemtype);
            subValueArray.push(fuel.partyData.partyname);
            subValueArray.push(fuel.weightperpiece);
            subValueArray.push(fuel.idList.currentQueue);
            subValueArray.push(fuel.reqhardness);
            subValueArray.push(fuel.dimensionForm);
            subValueArray.push(fuel.remark);
            subValueArray.push(fuel.grade);
            subValueArray.push(fuel.productimg);
            if (fuel.idList.qualityCheckData && fuel.idList.qualityCheckData.sizeInBlack) {
                subValueArray.push(fuel.idList.qualityCheckData.sizeInBlack);
            } else {
                subValueArray.push("");
            }
            subValueArray.push("MechineNo : " + fuel.idList.mechiningData.mechineNo +
                " OperatorName : " + fuel.idList.mechiningData.operatorName);
            subValueArray.push(fuel.idList.rejection.rejectionRemark);
            subValueArray.push(fuel.idList.inspectionData.actualDimension);
            subValueArray.push(fuel.idList.inspectionData.actualHardness);
            subValueArray.push("grooveInspection : " + fuel.idList.gACInspectionData.grooveInspection +
                " inspectorName : " + fuel.idList.gACInspectionData.inspectorName);

            subValueArray.push(fuel.idList.bASInspectionData.finalDimension);
            subValueArray.push(fuel.idList.bASInspectionData.inspectorName);
            subValueArray.push(fuel.idList.bASInspectionData.remark);
            subValueArray.push(toString(fuel.idList.bASInspectionData.actualBearingDia));
            subValueArray.push(fuel.idList.bASInspectionData.finalInspectorName);
            subValueArray.push(fuel.idList.bASInspectionData.finalRemark);
            subValueArray.push(fuel.idList.bASInspectionData.extraField);

            value.push(subValueArray);
        });

        // let headerName = "Fuel Summary Report (" + this.fromDate.formatted + " to " + this.toDate.formatted + ")";
        let headerName = "MasterTable Report";
        let body = {
            "headerName": headerName,
            "excelName": "MasterTable",
            "header": [
                "ROLLID",
                "PRODUCTTYPE",
                "ITEMTYPE",
                "PARTYNAME",
                "WEIGHT/PIECE",
                "STATUS",
                "REQ.HARDNESS",
                "DIMENSION",
                "REMARK",
                "GRADE",
                "PROD.IMG",
                "QUALITY CHECK DATA",
                "MECHINING DATA",
                "REJECTION DATA",
                "INSPECTION DATA (ACTUAL DIA)",
                "INSPECTION DATA (HARDNESS)",
                "GC INSPECTION DATA",
                "BS INSPECTION (FINAL DIMEN.)",
                "BS INSPECTION (INSPECTOR NAME)",
                "BS INSPECTION (REMARK)",
                "BS INSPECTION (ACTUAL BEARING DIA)",
                "BS INSPECTION (FINAL INSPECTOR NAME)",
                "BS INSPECTION (FINAL REMARK)",
                "BS INSPECTION (EXTRA)"
            ],
            "value": value
        }
        this.downloadExcelSheet(body).subscribe(blob => {
            // Doing it this way allows you to name the file
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = "MasterTable.xlsx";
            link.click();
        }, error => console.log("Error downloading the file."),
            () => console.log('Completed file download.'));
    }

    downloadExcelSheet(reqBody): Observable<Object[]> {
        //this.loading = true;
        return Observable.create(observer => {
            let xhr = new XMLHttpRequest();
            xhr.open('POST', AppConstants.EXCEL_DOWNLOAD_URL, true);
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.responseType = 'blob';

            xhr.onreadystatechange = () => {
                //this.loading = false;
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {

                        var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        var blob = new Blob([xhr.response], { type: contentType });
                        observer.next(blob);
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            xhr.send(JSON.stringify(reqBody));

        });
    }
}