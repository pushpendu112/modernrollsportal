
import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { POService } from '../po.service';
import * as $AB from 'jquery';
import * as CryptoJS from 'crypto-js';
import { Ng2SearchPipe } from 'ng2-search-filter';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import { PartyService } from 'src/app/shared/services/party.service';
import { Observable } from 'rxjs';
import { AppConstants } from 'src/app/base/appconstants';
declare var jquery: any;   // not required
declare var $: any;   // not required



@Component({
    selector: 'app-cc-creation',
    templateUrl: './cc-creation.component.html',
    styleUrls: ['./cc-creation.component.scss'],
    encapsulation: ViewEncapsulation.None

})
export class CCCreationComponent implements OnInit {
    private readonly notifier: NotifierService;
    chemcompForm: FormGroup;
    submitted = false;
    ismodelClose = false;
    newEmp = true;
    updatedEmp = false;
    getActiveGender;
    saveUsername = true;
    search: any;
    getGenderVaue: any;
    issubmitted = false;
    listCount: any;
    maleGenderActive = false;
    feMaleGenderActive = false;
    otherGenderActive = false;
    finalString: any;
    userToken: any;
    secret = 'i_have_some_small_master_secret_live_pin';
    setOrgId: any;
    centerId: any;
    getUpdatedRole: any;
    getcreatedRole: any;
    selectedFeature: any = [];
    getAllPocData: any;
    filteredList: any = [];
    empList = [];
    selectedCCObjId: any;
    title = 'Modern Rolls | Add CC';
    chemcompList: any = [];
    ccList = [];
    updatedRoleList = [];
    UpdatedCCObj: any;
    baseSelecter: any;
    ccid: string;
    ccId: string;
    showpolist: boolean = false;
    filter: Ng2SearchPipe;
    @ViewChild('closeModal') private closeModal: ElementRef;


    centerName: any = ['Select', 'Master Homeo 1', 'Master Homeo 2', 'Master Homeo 3', 'Master Homeo 4', 'Master Homeo 5'];
    roleName: any = ['Select', 'Super Admin', 'Nurse', 'Receptionist', 'Juniour Doctor', 'Senior Doctor', 'Pharmacy'];


    cols: any[];
    sortF: any;
    empCreation = [
        { empName: 'Kundhan', age: '25', gender: 'Male', roleName: 'Super Admin', userName: 'kundhan@gmail.com', password: '123456', centerName: 'Master Homeo 1' },
        { empName: 'Kumar', age: '26', gender: 'Male', roleName: 'Admin', userName: 'Kumar@gmail.com', password: '123456', centerName: 'Master Homeo 2' },
        { empName: 'Vinoth', age: '28', gender: 'Male', roleName: 'Admin', userName: 'Vinoth@gmail.com', password: '123456', centerName: 'Master Homeo 3' },
        { empName: 'Harish', age: '24', gender: 'Male', roleName: 'Admin', userName: 'Harish@gmail.com', password: '123456', centerName: 'Master Homeo 3' },

    ];

    constructor(private formBuilder: FormBuilder,
        private partyService: PartyService,
        private adminService: POService,
        private spinner: NgxSpinnerService,
        private notifierService: NotifierService,
        private titleService: Title) {
        this.notifier = notifierService;
        this.filter = new Ng2SearchPipe();
    }

    ngOnInit() {
        this.titleService.setTitle(this.title);
        this.cols = [
            { field: 'ccId', header: 'CC Id' },
            { field: 'partyName', header: 'Party Name' },
            // { field: 'width', header: 'Width' },
            // { field: 'heatno', header: 'Heat. No.' },
            // { field: 'quality', header: 'Quality' },
            // { field: 'quantity', header: 'Qty. (Nos. of pcs)', width: '10%' },
            { field: 'cpercentage', header: 'C %', width: '5%' },
            { field: 'mnpercentage', header: 'Mn %', width: '5%' },
            { field: 'sipercentage', header: 'Si %', width: '6%' },
            { field: 'spercentage', header: 'S %', width: '6%' },
            { field: 'ppercentage', header: 'P %', width: '5%' },
            { field: 'crpercentage', header: 'Cr %', width: '6%' },
            { field: 'nipercentage', header: 'Ni %', width: '5%' },
            { field: 'mopercentage', header: 'Mo %', width: '6%' },
            { field: 'remark', header: 'Remarks', width: '15%' }
        ];

        const getjwt = localStorage.getItem('jwttoken');
        this.userToken = this.conversionDecrypt(getjwt);
        this.setOrgId = JSON.parse(localStorage.getItem('orgId'));
        this.centerId = JSON.parse(localStorage.getItem('centerId'));
        this.getRloeList();
        this.fetchAllChemComp();
        this.empRegisterForms();
        this.getgetAllPocsForOrg();
        this.getAllEmployee();
        this.outsdiedCaptureEvent();
    }

    outsdiedCaptureEvent() {
        $("#exampleModalCenter").on("hidden.bs.modal", () => {
            this.getcreatedRole.forEach((dbRole) => {
                dbRole.forEach(role => {
                    this.onChange(role, false);
                })
            });
        });
        $("#updateempModel").on("hidden.bs.modal", () => {
            this.getcreatedRole.forEach((dbRole) => {
                dbRole.forEach(role => {
                    role.isSelected = false;
                    this.updatedRoleList.forEach((element2) => {
                        if (role.roleName === element2.roleName) {
                            role.isSelected = true;

                        }
                    })
                })
            });

        });
    }

    genderActive(event) {
        const target = event.target || event.srcElement || event.currentTarget;
        if (target.attributes) {
            const idAttr = target.attributes.id;
            const value = idAttr.nodeValue;
            this.chemcompForm.get('gender').setValue(value);

            if (value === 'Male') {
                this.maleGenderActive = true;
                this.feMaleGenderActive = false;
                this.otherGenderActive = false;
                this.getGenderVaue = value;
            } else if (value === 'FeMale') {
                this.feMaleGenderActive = true;
                this.maleGenderActive = false;
                this.otherGenderActive = false;
                this.getGenderVaue = value;
            } else if (value === 'Other') {
                this.otherGenderActive = true;
                this.maleGenderActive = false;
                this.feMaleGenderActive = false;
                this.otherGenderActive = true;
                this.getGenderVaue = value;
            } else {
                this.maleGenderActive = false;
                this.feMaleGenderActive = false;
                this.otherGenderActive = false;
                this.getGenderVaue = value;
            }
        }
    }
    genderSetActive(value) {
        if (value === 'Male') {
            this.maleGenderActive = true;
            this.feMaleGenderActive = false;
            this.otherGenderActive = false;
            this.getGenderVaue = value;
        } else if (value === 'FeMale') {
            this.feMaleGenderActive = true;
            this.maleGenderActive = false;
            this.otherGenderActive = false;
            this.getGenderVaue = value;
        } else if (value === 'Other') {
            this.otherGenderActive = true;
            this.maleGenderActive = false;
            this.feMaleGenderActive = false;
            this.otherGenderActive = true;
            this.getGenderVaue = value;
        } else {
            this.maleGenderActive = false;
            this.feMaleGenderActive = false;
            this.otherGenderActive = false;
            this.getGenderVaue = value;
        }
    }

    // convenience getter for easy access to form fields
    get empRegister() { return this.chemcompForm.controls; }

    empRegisterForms() {
        this.chemcompForm = this.formBuilder.group({
            partyname: ['', Validators.required],
            clientgrade: ['', Validators.required],
            cpercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            cpercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mnpercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mnpercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            sipercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            sipercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            spercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            spercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            ppercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            ppercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            crpercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            crpercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            nipercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            nipercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mopercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mopercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            remark: ['', Validators.required]
        });

    }
    getRloeList() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.adminService.getUpdatedRoleFromDB(this.setOrgId, headers).subscribe(data => {
            this.getUpdatedRole = data;
            const rolelist = [];
            this.getUpdatedRole.roleData.forEach(elemet => {
                rolelist.push(elemet.rolelist);
            });
            this.getcreatedRole = rolelist;

        },
            err => console.log(err),
            // () => console.log('Get Updated Role FromDB Is Done')
        );

    }

    getgetAllPocsForOrg() {
        this.adminService.getAllPocsForOrg(this.setOrgId).subscribe(data => {
            this.getAllPocData = data.pocData;
        },
            err => console.log(err),
            // () => console.log('Get All Pocs For Org is Done')
        );
    }

    onChange(value: string, checked: boolean) {
        // console.log('value, checked');
        // console.log(value, checked);
        if (checked) {
            this.selectedFeature.push(value);
            // console.log(this.selectedFeature);
            // console.log('this.selectedFeature');
        } else {
            const index = this.selectedFeature.indexOf(value);
            this.selectedFeature.splice(index, 1);
        }
        // console.log(this.selectedFeature);
    }

    isSelected(value: string): boolean {
        return this.selectedFeature.indexOf(value) >= 0;
    }

    get f() {
        return this.chemcompForm.controls;
    }



    onSubmit() {
        // console.log('form', this.chemcompForm);

        this.issubmitted = true;
        if (this.chemcompForm.valid) {
            var x = {
                // height: this.chemcompForm.get('height').value,
                // width: this.chemcompForm.get('width').value,
                // itemsp: this.chemcompForm.get('height').value + ' x ' + this.chemcompForm.get('width').value,
                // heatno: this.chemcompForm.get('heatno').value,
                // quality: this.chemcompForm.get('quality').value,
                partyName: this.chemcompForm.get('partyname').value,
                clientGrade: this.chemcompForm.get('clientgrade').value,
                cpercentage: this.covertnumberToString(this.chemcompForm.get('cpercentagefrom').value, this.chemcompForm.get('cpercentageto').value),
                mnpercentage: this.covertnumberToString(this.chemcompForm.get('mnpercentagefrom').value, this.chemcompForm.get('mnpercentageto').value),
                sipercentage: this.covertnumberToString(this.chemcompForm.get('sipercentagefrom').value, this.chemcompForm.get('sipercentageto').value),
                spercentage: this.covertnumberToString(this.chemcompForm.get('spercentagefrom').value, this.chemcompForm.get('spercentageto').value),
                ppercentage: this.covertnumberToString(this.chemcompForm.get('ppercentagefrom').value, this.chemcompForm.get('ppercentageto').value),
                crpercentage: this.covertnumberToString(this.chemcompForm.get('crpercentagefrom').value, this.chemcompForm.get('crpercentageto').value),
                nipercentage: this.covertnumberToString(this.chemcompForm.get('nipercentagefrom').value, this.chemcompForm.get('nipercentageto').value),
                mopercentage: this.covertnumberToString(this.chemcompForm.get('mopercentagefrom').value, this.chemcompForm.get('mopercentageto').value),
                remark: this.chemcompForm.get('remark').value,

            };
            const headers = new Headers(
                {
                    'accept': 'application/json',
                    'x-access-token': this.userToken
                });
            this.partyService.createChemicalComposition(x, headers).subscribe(data => {
                // console.log(data);
                $('#exampleModalCenter').modal('hide');
                this.chemcompForm.reset();
                this.fetchAllChemComp();

            },
                err => console.log(err),
            );
            this.issubmitted = false;
        }



        // this.spinner.show();
        // this.issubmitted = true;
        // if (this.chemcompForm.invalid) {
        //     this.spinner.hide();
        //     this.notifier.notify('error', 'Please Fill All Mandatory Fields! ')
        //     return;
        // }
        // const roleListId: any = [];

        // this.selectedFeature.forEach(element => {
        //     if (element._id) {
        //         roleListId.push(element._id);
        //     }
        // });
        // this.chemcompForm.get('roleList').setValue(roleListId);
        // this.chemcompForm.get('orgId').setValue(this.setOrgId);

        // console.log('this.chemcompForm.value');
        // console.log(this.chemcompForm.value);
        // this.adminService.cteateEmployeeService(this.chemcompForm.value).subscribe(data => {
        //     this.spinner.hide();
        //     console.log(data);
        //     this.getAllEmployee();
        // },
        //     err => {
        //         this.spinner.hide();
        //         this.notifier.notify('error', 'Something went wrong please try again! ')
        //         console.log(err);
        //     },
        //     //() => console.log('Get All Pocs For Org is Done')
        // );

        // console.log(this.chemcompForm.value);


    }

    covertnumberToString(a, b) {
        let string1: string;
        let string2: string;
        string1 = a.toString();
        string2 = b.toString();
        this.finalString = string1 + "-" + string2;
        return this.finalString;
    }


    updateSelection(centerNameObj) {
        // console.log(centerNameObj);
    }

    fetchAllChemComp() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.getAllChemicalComposition(headers).subscribe(data => {
            this.chemcompList = data.chemCompData;
            this.listCount = this.chemcompList.length;
            // console.log('list here' + JSON.stringify(this.chemcompList));

        })
    }

    getAllEmployee() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        const body = {
            orgId: this.setOrgId,
            centerId: this.centerId

        };
        this.spinner.show();
        this.adminService.getAllEmployee(body, headers).subscribe(element => {
            this.spinner.hide();
            this.empList = element.employeesData;
        });
    }


    isUpdated(value: string): boolean {
        return this.updatedRoleList.indexOf(value) >= 0;
    }

    onUpdateChange(value: string, checked) {
        if (checked) {
            this.updatedRoleList.push(value);
            // console.log('this.updatedRoleList');
            // console.log(this.updatedRoleList)
        } else {
            const index = this.updatedRoleList.indexOf(value);
            this.updatedRoleList.splice(index, 1);
        }
    }



    getChemCompValue(event) {
        // console.log(JSON.stringify(event));
        this.ccid = event.data._id;
        this.ccId = event.data.ccId;
        this.UpdatedCCObj = event.data;
        let cper = this.UpdatedCCObj.cpercentage;
        let cperArr = cper.split('-');
        this.chemcompForm.controls['partyname'].setValue(event.data.partyName);
        this.chemcompForm.controls['cpercentagefrom'].setValue(cperArr[0]);
        this.chemcompForm.controls['cpercentageto'].setValue(cperArr[1]);
        let mnper = this.UpdatedCCObj.mnpercentage;
        let mnperArr = mnper.split('-');
        this.chemcompForm.controls['mnpercentagefrom'].setValue(mnperArr[0]);
        this.chemcompForm.controls['mnpercentageto'].setValue(mnperArr[1]);
        let siper = this.UpdatedCCObj.sipercentage;
        let siperArr = siper.split('-');
        this.chemcompForm.controls['sipercentagefrom'].setValue(siperArr[0]);
        this.chemcompForm.controls['sipercentageto'].setValue(siperArr[1]);
        let sper = this.UpdatedCCObj.spercentage;
        let sperArr = sper.split('-');
        this.chemcompForm.controls['spercentagefrom'].setValue(sperArr[0]);
        this.chemcompForm.controls['spercentageto'].setValue(sperArr[1]);
        let pper = this.UpdatedCCObj.ppercentage;
        let pperArr = pper.split('-');
        this.chemcompForm.controls['ppercentagefrom'].setValue(pperArr[0]);
        this.chemcompForm.controls['ppercentageto'].setValue(pperArr[1]);
        let crper = this.UpdatedCCObj.crpercentage;
        let crperArr = crper.split('-');
        this.chemcompForm.controls['crpercentagefrom'].setValue(crperArr[0]);
        this.chemcompForm.controls['crpercentageto'].setValue(crperArr[1]);
        let niper = this.UpdatedCCObj.nipercentage;
        let niperArr = niper.split('-');
        this.chemcompForm.controls['nipercentagefrom'].setValue(niperArr[0]);
        this.chemcompForm.controls['nipercentageto'].setValue(niperArr[1]);
        let moper = this.UpdatedCCObj.mopercentage;
        let moperArr = moper.split('-');
        this.chemcompForm.controls['mopercentagefrom'].setValue(moperArr[0]);
        this.chemcompForm.controls['mopercentageto'].setValue(moperArr[1]);
        this.selectedCCObjId = event.data.ccid;
        this.chemcompForm.patchValue(event.data);
        $('#updateempModel').modal('show');
    }



    onDeleteChemComp() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.spinner.show();
        this.partyService.removeChemicalComposition(this.ccid, headers).subscribe(data => {
            this.spinner.hide();
            $('#delete').modal('hide');
            // console.log(data);
            this.fetchAllChemComp();
        },
            err => {
                this.spinner.hide();
                $('#delete').modal('hide');
                this.notifier.notify('error', 'Something went wrong please try again! ');
            }
        );
    }
    onUpdateChemComp() {
        if (this.chemcompForm.valid) {
            var x = {
                _id: this.ccid,
                ccId: this.ccId,
                // quantity: this.chemcompForm.get('quantity').value,
                partyName: this.chemcompForm.get('partyname').value,
                clientGrade: this.chemcompForm.get('clientgrade').value,
                cpercentage: this.covertnumberToString(this.chemcompForm.get('cpercentagefrom').value, this.chemcompForm.get('cpercentageto').value),
                mnpercentage: this.covertnumberToString(this.chemcompForm.get('mnpercentagefrom').value, this.chemcompForm.get('mnpercentageto').value),
                sipercentage: this.covertnumberToString(this.chemcompForm.get('sipercentagefrom').value, this.chemcompForm.get('sipercentageto').value),
                spercentage: this.covertnumberToString(this.chemcompForm.get('spercentagefrom').value, this.chemcompForm.get('spercentageto').value),
                ppercentage: this.covertnumberToString(this.chemcompForm.get('ppercentagefrom').value, this.chemcompForm.get('ppercentageto').value),
                crpercentage: this.covertnumberToString(this.chemcompForm.get('crpercentagefrom').value, this.chemcompForm.get('crpercentageto').value),
                nipercentage: this.covertnumberToString(this.chemcompForm.get('nipercentagefrom').value, this.chemcompForm.get('nipercentageto').value),
                mopercentage: this.covertnumberToString(this.chemcompForm.get('mopercentagefrom').value, this.chemcompForm.get('mopercentageto').value),
                remark: this.chemcompForm.get('remark').value,

            };
            const headers = new Headers(
                {
                    'accept': 'application/json',
                    'x-access-token': this.userToken
                });
            this.partyService.updateChemicalComposition(x, headers).subscribe(data => {
                // console.log(data);
                $('#updateempModel').modal('hide');
                this.chemcompForm.reset();
                this.fetchAllChemComp();
            },
                err => console.log(err),
            );
            this.issubmitted = false;
        }
    }

    onUpdateEmp() {
        const roleListId: any = [];
        let result = this.updatedRoleList.filter(function ({ _id }) {
            return !this.has(_id) && this.add(_id);
        }, new Set)
        result.forEach(element => {
            roleListId.push(element._id);
        })
        // console.log('result')
        // console.log(result)

        // console.log('roleListId');
        // console.log(roleListId);
        // console.log('this.setOrgId');
        // console.log(this.setOrgId);
        const existingEmpValue = this.chemcompForm.value;

        // console.log('existingEmpValue====>', existingEmpValue)

        this.UpdatedCCObj.forEach(empObj => {
            empObj.roleList = roleListId;
            empObj.name = existingEmpValue.name;
            empObj.mobileNumber = existingEmpValue.mobileNumber;
            empObj.userName = existingEmpValue.userName;
            empObj.password = existingEmpValue.password;
            empObj.emailId = existingEmpValue.emailId;
            empObj.gender = existingEmpValue.gender;
            empObj.yearsOfExp = existingEmpValue.yearsOfExp;
            empObj.qualification = existingEmpValue.qualification;
            empObj.speciality = existingEmpValue.speciality;
            empObj.centerId = existingEmpValue.centerId;
            empObj.orgId = existingEmpValue.orgId;

        })


        // console.log('this.UpdatedCCObj');
        // console.log(this.UpdatedCCObj[0]);

        // this.empCreation.push(this.chemcompForm.value);
        // console.log(this.chemcompForm.controls['centerId'].value);
        this.chemcompForm.get('roleList').setValue(roleListId);
        this.chemcompForm.get('orgId').setValue(this.setOrgId);
        this.chemcompForm.get('gender').setValue(this.getGenderVaue);
        // console.log('this.chemcompForm.value');
        // console.log(this.chemcompForm.value);

        $('#updateempModel').modal('hide');

        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        this.spinner.show();
        this.adminService.updateEmployee(this.UpdatedCCObj[0], headers).subscribe(data => {
            // console.log(data);
            this.spinner.hide();
            this.getAllEmployee();
        },
            err => {
                this.spinner.hide();
                this.notifier.notify('error', 'Something went wrong please try again!');
            })

    }




    deleteChemComp() {
        $('#updateempModel').modal('hide');
        $('#delete').modal('show');


    }
    Onclear() {
        // this.newEmp = true;
        // this.updatedEmp = false;
        this.chemcompForm.reset();
        // this.chemcompForm.get('gender').setValue(this.makeFalseGender());

    }

    makeFalseGender() {
        this.maleGenderActive = false;
        this.feMaleGenderActive = false;
        this.otherGenderActive = false;
    }
    onNotify() {
        this.chemcompForm.get('gender').setValue(this.getActiveGender);
        const empobject = this.chemcompForm.value;



        const deleteEmp = this.empCreation;

        this.empCreation = deleteEmp.filter((empObj) => empObj.empName !== empobject.empName);





        // console.log(deleteEmp);


        // let empArray = [];
        //  for (let key in empobject) {
        //      empArray.push(Object.assign(empobject[key], {key}));
        //  }
        //  console.log( empArray);

        // let data = this.toArray(empobject);
        // console.log(data)

        //      let myData = Object.keys(empobject).map(key => {
        //     return empobject[key];
        // })
        // console.log(myData);

        // let empArray = [];
        //  for( let item in empobject){
        //      empArray.push(empobject[item]);
        //  }

        // console.log(empArray);

        // const getShortMessages = messages => messages
        // .filter(obj => obj.message)
        // .map(obj => obj.message);
        // let test = getShortMessages(this.chemcompForm.value);
        // const peopleArray = Object.keys(empobject).map(i => empobject[i])

        // let  empdata = empArray.filter((emp) => console.log(emp));



        // let empdata = this.empCreation.filter((emp) => {
        //   console.log(emp);
        //   emp.empName !== empobject.empName});
        // console.log(empdata);


        // console.log(peopleArray);


        // let empdata = this.chemcompForm.value;
        // empdata = empdata.filter((emp) => console.log(emp));
        // empdata = empdata.filter((emp) => emp.age !== emp.age);
        // console.log(empdata);
        // window.alert('You will be notified when the product goes on sale');
    }

    toArray(obj_obj) {
        return Object.keys(obj_obj).map(i => obj_obj[i]);
    }
    getRoleId(role) {
        // console.log(role);
    }


    conversionEncrypt(encrypt) {
        try {
            return CryptoJS.AES.encrypt(JSON.stringify(encrypt), this.secret).toString();
        } catch (e) {
            console.log(e);
        }
    }
    conversionDecrypt(decrypt) {
        try {
            const bytes = CryptoJS.AES.decrypt(decrypt, this.secret);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return decrypt;
        } catch (e) {
            console.log(e);
        }
    }

    onKeyup() {
        let filteredList = [];
        if (this.search.length == 0) {
            this.listCount = this.chemcompList.length;
        }
        else {
            // console.log('dataata' + JSON.stringify(this.chemcompList));
            filteredList = this.filter.transform(this.chemcompList, this.search);
            this.listCount = filteredList.length;
        }
    }


    convertPoDetailsExcel() {
        let value = [];
        this.filteredList = this.filter.transform(this.chemcompList, this.search);
        // console.log('dataata' + JSON.stringify(this.filteredList));

        this.filteredList.forEach(fuel => {
            // console.log('fuel' + JSON.stringify(fuel));
            let subValueArray = [];
            subValueArray.push(fuel.ccId);
            subValueArray.push(fuel.partyName);
            subValueArray.push(fuel.cpercentage);
            subValueArray.push(fuel.mnpercentage);
            subValueArray.push(fuel.sipercentage);
            subValueArray.push(fuel.spercentage);
            subValueArray.push(fuel.ppercentage);
            subValueArray.push(fuel.crpercentage);
            subValueArray.push(fuel.nipercentage);
            subValueArray.push(fuel.mopercentage);
            subValueArray.push(fuel.remark);
            value.push(subValueArray);
        });

        // let headerName = "Fuel Summary Report (" + this.fromDate.formatted + " to " + this.toDate.formatted + ")";
        let headerName = "ChemicalCompositionList Report";
        let body = {
            "headerName": headerName,
            "excelName": "PoDetailsReport",
            "header": [
                "CCID",
                "PARTYNAME",
                "C%",
                "MN%",
                "SI%",
                "S%",
                "P%",
                "CR%",
                "NI%",
                "MO%",
                "REMARK%"
            ],
            "value": value
        }
        this.downloadExcelSheet(body).subscribe(blob => {
            // Doing it this way allows you to name the file
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = "ChemicalCompositionList.xlsx";
            link.click();
        }, error => console.log("Error downloading the file."),
            () => console.log('Completed file download.'));
    }

    downloadExcelSheet(reqBody): Observable<Object[]> {
        //this.loading = true;
        return Observable.create(observer => {
            let xhr = new XMLHttpRequest();
            xhr.open('POST', AppConstants.EXCEL_DOWNLOAD_URL, true);
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.responseType = 'blob';

            xhr.onreadystatechange = () => {
                //this.loading = false;
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {

                        var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        var blob = new Blob([xhr.response], { type: contentType });
                        observer.next(blob);
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            xhr.send(JSON.stringify(reqBody));

        });
    }

}
