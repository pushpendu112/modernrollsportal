import { Component, OnInit, AfterViewInit, DoCheck, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, ValidatorFn, AbstractControl, NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ThrowStmt } from '@angular/compiler';
import { Observable } from 'rxjs';
import * as CryptoJS from 'crypto-js';
import { POService } from '../po.service';
import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';
import { PartyDetails } from 'src/app/shared/modals/partyDetails';
import { PartyService } from 'src/app/shared/services/party.service';
import { forkJoin, Subject } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';
import { Ng2SearchPipe } from 'ng2-search-filter';
import { AppConstants } from 'src/app/base/appconstants';
declare var $: any;   // not required


@Component({
    selector: 'app-party-creation',
    templateUrl: './party-creation.component.html',
    styleUrls: ['./party-creation.component.scss']
})
export class PartyCreationComponent implements OnInit, AfterViewInit {
    cols: any[];

    unsubscribe = new Subject();
    private readonly notifier: NotifierService;
    roleForms: FormGroup;
    partyDetails = { name: '', address: '', contactperson: '', contactmobile: '', contactemail: '', gstin: '' };
    updatedParty = { name: '', address: '', contactperson: '', contactmobile: '', contactemail: '', gstin: '', partyId: '' };
    updatedIndex = null;
    partyList = [];
    listCount: any;
    issubmitted = false;
    isname = false;
    isaddress = false;
    iscontactperson = false;
    filteredList: any = [];
    iscontactmobile = false;
    iscontactemail = false;
    isgstin = false;
    submitted = false;
    roleList: any;
    roleData: any = [];
    roleName = { roleName: '' };
    selectedFeatureName = [];
    featureNameError: Boolean = true;
    result = [];
    roleUpdate = true;
    roleId: any;
    updatedName = { roleName: '' };
    updatedFeature = [];
    search: any;
    userToken: any;
    secret = 'i_have_some_small_master_secret_live_pin';
    setOrgId: any;
    baseFeaturList: any;
    selectedPizzaGroup: AbstractControl;
    selectedFeature: string[] = [];
    selctedone = false;
    title = 'Modern Rolls | Add Party';
    deleteRoleId: any;
    partylist: any = [];
    _id: string;
    partyDetail: PartyDetails;
    filter: Ng2SearchPipe;
    constructor(private _fb: FormBuilder,
        private adminService: POService,
        private notifierService: NotifierService,
        private spinner: NgxSpinnerService,
        private titleService: Title, private partyService: PartyService) {
        this.notifier = notifierService;
        this.filter = new Ng2SearchPipe();
    }
    ngAfterViewInit() {

    }
    ngOnInit() {
        this.roleForms = this._fb.group({
            roleName: [null],
            featuresCollactionList: this._fb.array([])
        });
        this.titleService.setTitle(this.title);
        const getjwt = localStorage.getItem('jwttoken');
        this.userToken = this.conversionDecrypt(getjwt);
        this.setOrgId = JSON.parse(localStorage.getItem('orgId'));
        this.getbaseFeatureList();
        this.outsideCaptureEvent();
        this.fetchAllParty();
        this.cols = [
            { field: 'partyId', header: 'Party ID', width: '8%' },
            { field: 'name', header: 'Party Name', width: '20%' },
            { field: 'address', header: 'Party Adrress', width: '20%' },
            { field: 'contactperson', header: 'Contact Person', width: '10%' },
            { field: 'contactmobile', header: 'Contact Number', width: '10%' },
            { field: 'contactemail', header: 'EmailID', width: '20%' },
            { field: 'gstin', header: 'GSTNo.', width: '12%' },
        ];
    }

    outsideCaptureEvent() {
        $("#roleCreation").on("hidden.bs.modal", () => {
            this.baseFeaturList.forEach(feature => {
                this.onChange(feature, false);
            });
            this.roleUpdate = true;
        });
        $("#UpdateCreation").on("hidden.bs.modal", () => {
            this.baseFeaturList.forEach(feature => {
                this.onUpdateChange(feature, false);
            });
            this.roleUpdate = true;
        });
    }

    onUpdateChange(value: string, checked) {
        if (checked) {
            this.updatedFeature.push(value);
        } else {
            // console.log('else');
            const index = this.updatedFeature.indexOf(value);
            this.updatedFeature.splice(index, 1);
        }
    }

    getbaseFeatureList() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.spinner.show();
        this.adminService.getUpdatedRoleFromDB(this.setOrgId, headers).subscribe(data => {
            this.spinner.hide();
            data.roleData.forEach(element => {
                this.roleList = element.rolelist;
                // console.log('roleList#################################33');
                // console.log(this.roleList)
            })

            let getData: any;
            const baseFeatureLis = [];
            getData = data.roleData.forEach(elemet => {
                baseFeatureLis.push(elemet.baseFeatureList);
            });
            this.baseFeaturList = baseFeatureLis[0];
        },
            err => {
                this.spinner.hide();
                // console.log(err);
                this.notifier.notify('error', 'Please Check ' + err.statusText);
            },
            // () => this.notifier.notify('success', 'Role List Is Done!!! ')

        );

    }



    patchValues() {
        // console.log('patch called');

        this.partyDetails = { name: '', address: '', contactperson: '', contactmobile: '', contactemail: '', gstin: '' };
        this.updatedParty = { name: '', address: '', contactperson: '', contactmobile: '', contactemail: '', gstin: '', partyId: '' };
        // this.baseFeaturList.forEach(feature => {
        //     this.onChange(feature, false);
        //     this.onUpdateChange(feature, false);
        // });
    }
    onSubmit() {
        //this.spinner.show();
        // this.partyList.push(this.partyDetails);
        this.partyDetails = { name: '', address: '', contactperson: '', contactmobile: '', contactemail: '', gstin: '' };
        // this.roleForms.reset();
        // const role = {
        //     roleName: this.roleName.roleName,
        //     checkedFeatureList: this.selectedFeature
        // };
        // const obj = {
        //     orgId: this.setOrgId,
        //     role: role
        // };
        // const headers = new Headers(
        //     {
        //         'accept': 'application/json',
        //         'x-access-token': this.userToken
        //     });

        // this.adminService.createRole(obj, headers).subscribe(data => {
        //     this.getbaseFeatureList();
        //     this.spinner.hide();
        // },
        //     err => {
        //         console.log(err);
        //         this.spinner.hide();
        //         this.notifier.notify('error', 'Please Check ' + err.statusText);
        //     },
        //     //() => this.notifier.notify('success', 'You are awesome! You are successfully created Role!'),
        // );


    }
    checkedFeature(fName) {
        // console.log(fName);
    }



    isUpdated(value: string): boolean {
        return this.updatedFeature.indexOf(value) >= 0;
    }

    isSelected(value: string): boolean {
        return this.selectedFeature.indexOf(value) >= 0;
    }

    onChange(value: string, checked) {
        if (checked) {
            this.selectedFeature.push(value);
        } else {
            // console.log('else');
            const index = this.selectedFeature.indexOf(value);
            this.selectedFeature.splice(index, 1);
        }
    }




    get firstName() {
        return this.roleForms.get('roleName');
    }

    get featuresArray() {
        return <FormArray>this.roleForms.get('featuresName');
    }

    getSelectedFeatureValue() {
        this.selectedFeatureName = [];
        this.featuresArray.controls.forEach((control, i) => {
            // console.log('control.value');
            // console.log(control.value);
            if (control.value) {
                // console.log(this.featuresNameList[i]);
                // this.selectedFeatureName.push(this.featuresNameList[i]);
            }
        });

        this.featureNameError = this.selectedFeatureName.length > 0 ? false : true;
    }


    onRoleSubmit() {
        const obj = {
            id: null,
            'roleName': '',
            'featuresName': []
        };
        const role = this.roleForms.value;
        const selectedFeature = this.selectedFeatureName;
        obj.roleName = role.roleName;
        obj.featuresName = selectedFeature;
        // obj.id = this.roleNameArray.length + 1;
        // this.roleNameArray.push(obj);
        // console.log(this.roleForms.value);

        $('#roleCreation').modal('hide');

    }

    onRoleEdit(index) {
        // alert('dfdfdfdf')
        // this.roleUpdate = false;
        // this.roleId = role._id;
        // console.log('role');
        // console.log(role);
        // this.selectedFeature = role.checkedFeatureList;

        this.updatedIndex = index;
        this.updatedParty.name = this.partyList[index].name;
        this.updatedParty.address = this.partyList[index].address;
        //this.selectedFeature = role.checkedFeatureList;

        $('#UpdateCreation').modal('show');
    }
    getRoleIdOnClick(index) {
        this.deleteRoleId = index;
        //this.partyList.slice(index,1);
        // console.log(role);
        // console.log(this.deleteRoleId);
    }
    onRoleDelete() {

        this.partyList.splice(this.deleteRoleId, 1);
        // console.log('test');
        this.deleteRoleId = null;
        $('#deleteModal').modal('hide');
        // const reqBody = this.deleteRoleId
        // const headers = new Headers(
        //     {
        //         'accept': 'application/json',
        //         'x-access-token': this.userToken
        //     });

        // console.log(reqBody);
        // this.adminService.removeRoleInOrg(reqBody, headers).subscribe(removeRoleObj => {
        //     this.spinner.hide();
        //     this.getbaseFeatureList();
        //     $('#deleteModal').modal('hide');
        // },
        //     err => {
        //         this.spinner.hide()
        //         console.log(err);
        //         $('#deleteModal').modal('hide');
        //         this.notifier.notify('error', 'Please Check ' + err.statusText);
        //     },
        //     // () => this.notifier.notify('success', 'Role List Is Done!!! ')
        // );
        // this.roleNameArray = this.roleNameArray.filter(roleId => roleId.id !== role.id);

    }

    addFeatureEditControls(editFeature) {
        const arr = editFeature.map(item => {
            return this._fb.control(item);
        });
        return this._fb.array(arr);
    }


    onFormReset() {
        this.roleForms.reset();

    }

    getMatch(a, b) {
        const matches = [];
        for (let i = 0; i < a.length; i++) {
            for (let e = 0; e < b.length; e++) {
                // if (a[i] === b[e]) { matches.push(a[i]); }
                if (a[i] === b[e]) {

                }
            }
        }
        return matches;
    }

    createRoleHandler() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
    }


    conversionDecrypt(decrypt) {
        try {
            const bytes = CryptoJS.AES.decrypt(decrypt, this.secret);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return decrypt;
        } catch (e) {
            console.log(e);
        }
    }
    onUpdate() {
        this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyDetail = new PartyDetails();
        this.partyDetail = new PartyDetails();
        if (this.updatedParty.name == "") {
            this.isname = true;
        }
        else {
            this.isname = false;
        }
        if (this.updatedParty.address == "") {
            this.isaddress = true;
        }
        else {
            this.isaddress = false;
        }
        if (this.updatedParty.contactperson == "") {
            this.iscontactperson = true;
        }
        else {
            this.iscontactperson = false;
        }
        if (this.updatedParty.contactmobile == "") {
            this.iscontactmobile = true;
        }
        else {
            this.iscontactmobile = false;
        }
        if (this.updatedParty.contactemail == "") {
            this.iscontactemail = true;
        }
        else {
            this.iscontactemail = false;
        }
        if (this.updatedParty.gstin == "") {
            this.isgstin = true;
        }
        else {
            this.isgstin = false;
        }
        if (this.updatedParty.name != '' && this.updatedParty.address != '' && this.updatedParty.contactperson != ''
            && this.updatedParty.contactmobile != '' && this.updatedParty.contactemail != '' && this.updatedParty.gstin != '') {
            var x = {
                _id: this._id,
                name: this.updatedParty.name,
                address: this.updatedParty.address,
                contactperson: this.updatedParty.contactperson,
                contactmobile: this.updatedParty.contactmobile,
                contactemail: this.updatedParty.contactemail,
                gstin: this.updatedParty.gstin,
                partyId: this.updatedParty.partyId
            }
            this.partyService.updateParty(x, headers).subscribe(data => {
                // console.log(data);
                $('#UpdateCreation').modal('hide');
                this.fetchAllParty();
                this.spinner.hide();
                this.updatedParty = { name: '', address: '', contactperson: '', contactmobile: '', contactemail: '', gstin: '', partyId: '' };
                this.partyDetails = { name: '', address: '', contactperson: '', contactmobile: '', contactemail: '', gstin: '' };
            },
                err => console.log(err),
            );
        }


    }

    fetchAllParty() {
        this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.getAllParty(headers).subscribe(data => {
            this.partylist = data.partyData;
            this.listCount = this.partylist.length;
            if (this.partyList != undefined) {
                this.spinner.hide();
            }
            // console.log('list here' + JSON.stringify(this.partyList));
        })
    }

    addParty() {
        this.spinner.show();
        // console.log('token' + this.userToken);


        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyDetail = new PartyDetails();
        this.partyDetail.name = this.partyDetails.name;
        this.partyDetail.address = this.partyDetails.address;
        this.partyDetail.contactperson = this.partyDetails.contactperson;
        this.partyDetail.contactmobile = this.partyDetails.contactmobile;
        this.partyDetail.contactemail = this.partyDetails.contactemail;
        this.partyDetail.gstin = this.partyDetails.gstin;
        // console.log('par' + JSON.stringify(this.partyDetail));
        // console.log(this.partyDetail.address != '');
        if (this.partyDetail.name == "") {
            this.isname = true;
        }
        else {
            this.isname = false;
        }
        if (this.partyDetail.address == "") {
            this.isaddress = true;
        }
        else {
            this.isaddress = false;
        }
        if (this.partyDetail.contactperson == "") {
            this.iscontactperson = true;
        }
        else {
            this.iscontactperson = false;
        }
        if (this.partyDetail.contactmobile == "") {
            this.iscontactmobile = true;
        }
        else {
            this.iscontactmobile = false;
        }
        if (this.partyDetail.contactemail == "") {
            this.iscontactemail = true;
        }
        else {
            this.iscontactemail = false;
        }
        if (this.partyDetail.gstin == "") {
            this.isgstin = true;
        }
        else {
            this.isgstin = false;
        }


        if (this.partyDetail.name != '' && this.partyDetail.address != '' && this.partyDetail.contactperson != ''
            && this.partyDetail.contactmobile != '' && this.partyDetail.contactemail != '' && this.partyDetail.gstin != '') {
            // console.log('ch' + JSON.stringify(this.partyDetail));
            this.partyService.createParty(this.partyDetail, headers).subscribe(data => {
                // console.log(data);
                $('#roleCreation').modal('hide');
                this.fetchAllParty();
                this.spinner.hide();
                this.updatedParty = { name: '', address: '', contactperson: '', contactmobile: '', contactemail: '', gstin: '', partyId: '' };
                this.partyDetails = { name: '', address: '', contactperson: '', contactmobile: '', contactemail: '', gstin: '' };
            },
                err => console.log(err),
            );
        }

    }

    getPartyValue(event) {
        // console.log('event' + JSON.stringify(event));
        this.updatedParty.name = event.data.name;
        this.updatedParty.address = event.data.address;
        this.updatedParty.contactperson = event.data.contactperson;
        this.updatedParty.contactmobile = event.data.contactmobile;
        this.updatedParty.contactemail = event.data.contactemail;
        this.updatedParty.gstin = event.data.gstin;
        this.updatedParty.partyId = event.data.partyId;
        this._id = event.data._id;
        // console.log('updated' + JSON.stringify(this.updatedParty));
        if (this.updatedParty != undefined && this.updatedParty.name != undefined && this.updatedParty.address != undefined && this.updatedParty.contactemail != undefined && this.updatedParty.contactmobile != undefined && this.updatedParty.contactperson != undefined && this.updatedParty.gstin != undefined) {
            $('#UpdateCreation').modal('show');
        }
    }

    deleteParty() {
        $('#delete').modal('show');
    }

    onDeleteChemComp() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.spinner.show();
        this.partyService.removeParty(this._id, headers).subscribe(data => {
            this.spinner.hide();
            $('#UpdateCreation').modal('hide');
            $('#delete').modal('hide');
            // console.log(data);
            this.fetchAllParty();
        },
            err => {
                this.spinner.hide();
                $('#delete').modal('hide');
                this.notifier.notify('error', 'Something went wrong please try again! ');
            }
        );
    }

    clean() {
        // console.log('clean called');

        this.updatedParty = { name: '', address: '', contactperson: '', contactmobile: '', contactemail: '', gstin: '', partyId: '' };
        this.partyDetails = { name: '', address: '', contactperson: '', contactmobile: '', contactemail: '', gstin: '' };
        this.isname = false;
        this.isaddress = false;
        this.iscontactemail = false;
        this.iscontactmobile = false;
        this.iscontactperson = false;
        this.isgstin = false;
    }

    onKeyup() {
        let filteredList = [];
        if (this.search.length == 0) {
            this.listCount = this.partylist.length;
        }
        else {
            filteredList = this.filter.transform(this.partylist, this.search);
            this.listCount = filteredList.length;
        }
    }


    convertPoDetailsExcel() {
        let value = [];
        this.filteredList = this.filter.transform(this.partylist, this.search);
        // console.log('dataata' + JSON.stringify(this.filteredList));

        this.filteredList.forEach(fuel => {
            // console.log('fuel' + JSON.stringify(fuel));
            let subValueArray = [];
            subValueArray.push(fuel.partyId);
            subValueArray.push(fuel.name);
            subValueArray.push(fuel.address);
            subValueArray.push(fuel.contactperson);
            subValueArray.push(fuel.contactmobile);
            subValueArray.push(fuel.contactemail);
            subValueArray.push(fuel.gstin);
            value.push(subValueArray);
        });

        // let headerName = "Fuel Summary Report (" + this.fromDate.formatted + " to " + this.toDate.formatted + ")";
        let headerName = "PartyList Report";
        let body = {
            "headerName": headerName,
            "excelName": "PoDetailsReport",
            "header": [
                "PARTYID",
                "PARTYNAME",
                "PARTYADDRESS",
                "CONTACTCPERSON",
                "CONTACTNUMBER",
                "EMAILID",
                "GSTNO."
            ],
            "value": value
        }
        this.downloadExcelSheet(body).subscribe(blob => {
            // Doing it this way allows you to name the file
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = "PartytList.xlsx";
            link.click();
        }, error => console.log("Error downloading the file."),
            () => console.log('Completed file download.'));
    }

    downloadExcelSheet(reqBody): Observable<Object[]> {
        //this.loading = true;
        return Observable.create(observer => {
            let xhr = new XMLHttpRequest();
            xhr.open('POST', AppConstants.EXCEL_DOWNLOAD_URL, true);
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.responseType = 'blob';

            xhr.onreadystatechange = () => {
                //this.loading = false;
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {

                        var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        var blob = new Blob([xhr.response], { type: contentType });
                        observer.next(blob);
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            xhr.send(JSON.stringify(reqBody));

        });
    }

}