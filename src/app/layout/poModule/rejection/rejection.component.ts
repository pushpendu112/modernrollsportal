import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { PartyService } from 'src/app/shared/services/party.service';
import * as CryptoJS from 'crypto-js';
import { AppConstants } from '../../../base/appconstants';
import { NgxSpinnerService } from 'ngx-spinner';
import { QueueStatus } from '../../../models/queueStatus';
import { jsonpCallbackContext } from '@angular/common/http/src/module';
import { Ng2SearchPipe } from 'ng2-search-filter';
import { Observable } from 'rxjs';
import { FormBuilder } from '@angular/forms';
declare var jquery: any;   // not required
declare var $: any;   // not required
@Component({
    selector: 'app-rejection',
    templateUrl: './rejection.component.html',
    styleUrls: ['./rejection.component.scss'],
    // encapsulation: ViewEncapsulation.None
})
export class RejectionComponent implements OnInit {
    constructor(private partyService: PartyService, public fb: FormBuilder, private spinner: NgxSpinnerService) {
        this.filter = new Ng2SearchPipe();
        this.search = '';
        this.fromDate = '';
        this.toDate = '';
    }
    registrationForm = this.fb.group({
        gender: []
    })
    filter: Ng2SearchPipe;
    filteredList: any = [];
    listCount: any;
    fromDate: any;
    toDate: any;
    productType: any;
    selectedProductType: any;
    tempduplicatepoProductList: any = [];
    poProductList: any = [];
    duplicatepoProductList: any = [];
    duplicatePlannerList: any = [];
    userToken: any;
    remark: string = '';
    index: number;
    search: any;
    image: any;
    pocols: any[];
    pcols: any[];
    moveCast: boolean;
    data: any;
    rejectionQueue: any;
    rejectedRoleDisplayId: any;
    selectedRoleDisplayId: any;
    history: boolean = false;
    colorMap = {
        "ADAMITS": "red",
        "CHILLS": "blue",
        "SG-IRON": "green",
        "SG-ACC": "#c4c233"
    }
    ngOnInit() {
        const getjwt = localStorage.getItem('jwttoken');
        this.userToken = this.conversionDecrypt(getjwt);
        this.fetchAllPoProduct(false);
        this.pocols = [
            { field: 'idList.displayId', header: 'Roll Id', width: '7%' },
            // { field: 'processtype', header: 'Process Type', width: '8%' },
            // { field: 'grade', header: 'Grade', width: '7%' },
            { field: 'producttype', header: 'Prod. Type', width: '7%' },
            { field: 'itemtype', header: 'Item Type', width: '7%' },
            { field: 'partyData.partyname', header: 'PartyName', width: '8%' },
            // { field: 'uom', header: 'UOM', width: '8%' },
            // { field: 'priceperuom', header: 'Price/UOM', width: '8%' },
            { field: 'weightperpiece', header: 'Weight/Piece', width: '9%' },
            // { field: 'idList.currentQueue', header: 'Status', width: '7%' },
            { field: 'idList.rejection.rejectionRemark', header: 'RejectionRemark', width: '7%' },
            { field: 'reqhardness', header: 'Req. Hard', width: '8%' },
            { field: 'dimensionFormString', header: 'Dimension', width: '7%' },
            { field: 'remark', header: 'Remark', width: '7%' },

        ];
        this.pcols = [
            { field: 'idList.displayId', header: 'Roll Id', width: '5%' },
            { field: 'grade', header: 'Grade', width: '8%' },
            { field: 'producttype', header: 'Prod. Type', width: '8%', color: 'yellow' },
            { field: 'itemtype', header: 'Item Type', width: '8%' },
            { field: 'partyData.partyname', header: 'PartyName', width: '8%' },
            // { field: 'weightperpiece', header: 'Weight/Piece', width: '8%' },
            { field: 'idList.currentQueue', header: 'Status', width: '7%' },
            // { field: 'reqhardness', header: 'Req. Hard', width: '8%' },
            { field: 'dimensionFormString', header: 'Dimension', width: '7%' },
        ];
    }
    conversionDecrypt(decrypt) {
        try {
            const bytes = CryptoJS.AES.decrypt(decrypt, AppConstants.secret);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return decrypt;
        } catch (e) {
            console.log(e);
        }
    }



    fetchAllPoProduct(isCheck) {
        let filteredList = [];
        this.spinner.show();
        let id;

        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        if (this.history == false) {
            id = 0;
        }
        else {
            id = 6;
        }
        this.duplicatepoProductList = [];
        this.tempduplicatepoProductList = [];
        this.poProductList = [];
        // console.log('id' + id);

        if (id == 0) {
            this.partyService.getAllPOProductByQueueStatus(headers, id).subscribe(data => {
                this.poProductList = data.pOProductData;
                if (this.poProductList.length < 1) {
                    this.spinner.hide();
                }
                this.duplicatepoProductList = JSON.parse(JSON.stringify(this.poProductList));
                if (this.search.length == 0) {
                    this.listCount = this.duplicatepoProductList.length;
                }
                else {
                    filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                    this.listCount = filteredList.length;
                }
                this.duplicatepoProductList.forEach(element => {
                    if (element.processtype == 0) {
                        element.processtype = 'FORGING';
                    }
                    else {
                        element.processtype = 'ROLL CASTING';
                    }
                    if (element.itemtype == 0) {
                        element.itemtype = 'ROLLS';
                    }
                    else if (element.itemtype == 1) {
                        element.itemtype = 'STEP ROLLS';
                    }
                    else if (element.itemtype == 2) {
                        element.itemtype = 'REEL';
                    }
                    else {
                        element.itemtype = 'OTHER';
                    }
                    if (element.uom == 0) {
                        element.uom = 'Per-KG';
                    }
                    else if (element.uom == 1) {
                        element.uom = 'MT';
                    }
                    else {
                        element.uom = 'Per-Piece';
                    }
                    if (element.idList.currentQueue == '1') {
                        element.idList.currentQueue = 'Planning';
                    }
                    else if (element.idList.currentQueue == '2') {
                        element.idList.currentQueue = 'Roll Casting';
                    }
                    let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                    element['dimensionFormString'] = toString(element.dimensionForm);
                    element.idList.queueUpdateTimeStamp = element.idList.queueUpdateTimeStamp.substring(0, 4) + '-' + element.idList.queueUpdateTimeStamp.substring(5, 7) + '-' + element.idList.queueUpdateTimeStamp.substring(8, 10);
                    this.tempduplicatepoProductList = this.duplicatepoProductList;
                    if (isCheck == true) {
                        this.fromDate = '';
                        this.toDate = '';
                    }
                    if (this.fromDate != '' && this.toDate != '') {
                        this.onDateSelection();
                    }
                    this.spinner.hide();

                });

            });
        }
        else {
            this.partyService.fetchAllPoProducts(headers).subscribe(data => {
                this.poProductList = data.pOProductData;
                if (this.poProductList.length < 1) {
                    this.spinner.hide();
                }
                // console.log('enetr' + JSON.stringify(this.poProductList));
                // if (this.poProductList != undefined)
                this.poProductList.forEach(element => {
                    if (element.idList.rejection.rejectionQueue != '') {
                        this.duplicatepoProductList.push(element);
                        // console.log('enetr' + JSON.stringify(this.duplicatepoProductList));

                        this.duplicatepoProductList.forEach(element => {
                            if (element.processtype == 0) {
                                element.processtype = 'FORGING';
                            }
                            else {
                                element.processtype = 'ROLL CASTING';
                            }
                            if (element.itemtype == 0) {
                                element.itemtype = 'ROLLS';
                            }
                            else if (element.itemtype == 1) {
                                element.itemtype = 'STEP ROLLS';
                            }
                            else if (element.itemtype == 2) {
                                element.itemtype = 'REEL';
                            }
                            else {
                                element.itemtype = 'OTHER';
                            }
                            if (element.uom == 0) {
                                element.uom = 'Per-KG';
                            }
                            else if (element.uom == 1) {
                                element.uom = 'MT';
                            }
                            else {
                                element.uom = 'Per-Piece';
                            }
                            if (element.idList.currentQueue == '1') {
                                element.idList.currentQueue = 'Planning';
                            }
                            else if (element.idList.currentQueue == '2') {
                                element.idList.currentQueue = 'Roll Casting';
                            }
                            let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                            element['dimensionFormString'] = toString(element.dimensionForm);
                            element.idList.queueUpdateTimeStamp = element.idList.queueUpdateTimeStamp.substring(0, 4) + '-' + element.idList.queueUpdateTimeStamp.substring(5, 7) + '-' + element.idList.queueUpdateTimeStamp.substring(8, 10);
                            this.tempduplicatepoProductList = this.duplicatepoProductList;
                            if (isCheck == false) {
                                this.fromDate = '';
                                this.toDate = '';
                            }
                            else {
                                this.onDateSelection();
                            }
                            this.spinner.hide();

                        });
                    }
                });
            });
        }

    }

    moveToRollCasting(event) {
        let data = event;
        this.data = event;
        let x = {
            poDetailsId: data.poDetailsId,
            productId: data._id,
            displayId: data.idList.displayId,
            queueUpdateTimeStamp: data.idList.queueUpdateTimeStamp,
            upcomingQueue: QueueStatus.Planning,
            currentQueue: QueueStatus.Rejected,
        };
        this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.updateQueueStatus(x, headers).subscribe(data => {
            // console.log(data);
            this.updateQueueData();
            this.fetchAllPoProduct(false);
            this.spinner.hide();
        });
    }

    moveToRejection() {
        let data = this.data;
        let x = {
            poDetailsId: data.poDetailsId,
            productId: data._id,
            displayId: data.idList.displayId,
            queueUpdateTimeStamp: data.idList.queueUpdateTimeStamp,
            upcomingQueue: QueueStatus.Rejected,
            currentQueue: QueueStatus.RollCasting,
        };
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.updateQueueStatus(x, headers).subscribe(data => {
            // console.log(data);
            this.updateQueueData();
        });
    }

    getProductDetails() {

    }

    remarkData(event) {
        this.remark = '';
        this.data = event;
        this.rejectionQueue = this.data.idList.rejection.rejectionQueue;
        this.rejectedRoleDisplayId = this.data.idList.displayId;
        this.fetchPlannerData();
    }

    transferData(event) {
        this.selectedRoleDisplayId = event.idList.displayId;
    }

    returnData(event) {
        this.data = event;
    }

    returnBack() {
        let data = this.data;
        let x = {
            poDetailsId: data.poDetailsId,
            productId: data._id,
            displayId: data.idList.displayId,
            queueUpdateTimeStamp: data.idList.queueUpdateTimeStamp,
            upcomingQueue: data.idList.rejection.rejectionQueue,
            currentQueue: QueueStatus.Rejected,
        };
        this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.updateQueueStatus(x, headers).subscribe(data => {
            // console.log(data);
            // this.updateQueueData();
            this.fetchAllPoProduct(false);
            this.spinner.hide();
        });
    }

    transferBack() {
        this.spinner.show();
        let x = {
            rejectionQueue: this.rejectionQueue,
            rejectedRoleDisplayId: this.rejectedRoleDisplayId,
            selectedRoleDisplayId: this.selectedRoleDisplayId,
        };
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.tranferDisplayId(x, headers).subscribe(data => {
            $('#planner').modal('hide');
            $('#popUp').modal('hide');
            this.fetchAllPoProduct(false);
            this.spinner.hide();
            // console.log(data);
        });
    }

    updateQueueData() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        let data = this.data.idList;
        let id = this.data._id;
        // console.log('final data' + JSON.stringify(data));
        let timeStamp = new Date();
        // console.log('date' + JSON.stringify(timeStamp));
        let x = {
            queueUpdateTimeStamp: data.queueUpdateTimeStamp,
            displayId: data.displayId,
            currentQueue: QueueStatus.Planning,
            isRejected: data.isRejected,
            isDispacthed: false,
            isPacked: false,
            isTCDone: false,
            mechiningData: {
                "mechineNo": "",
                "operatorName": "",
                "extraField": ""
            },
            rejection: data.rejection,
            qualityCheckData: {
                "sizeInBlack": "",
                "extraField": ""
            },
            inspectionData: {
                "actualDimension": "",
                "actualHardness": "",
                "extraField": ""
            },
            gACFinishingData: {
                "isGrooved": false,
                "extraField": ""
            },
            bASFinishingData: {
                "isRoughing": false,
                "isBoaring": false,
                "isKeyWayCut": false,
                "isGrinding": false,
                "isShapper": false,
                "extraField": ""
            },
            gACInspectionData: {
                "grooveInspection": "",
                "inspectorName": "",
                "remark": "",
                "extraField": ""
            },
            bASInspectionData: {
                "finalDimension": "",
                "inspectorName": "",
                "remark": "",
                "actualBearingDia": [

                ],
                "extraField": ""
            },
            testCertificateData: {
                "height": "",
                "width": "",
                "heatno": "",
                "quality": "",
                "quantity": "",
                "cpercentage": "",
                "mnpercentage": "",
                "sipercentage": "",
                "spercentage": "",
                "ppercentage": "",
                "crpercentage": "",
                "nipercentage": "",
                "mopercentage": "",
                "remark": "",
                "ccId": ""
            },
            queueDurationList: []
        };
        // console.log('x data' + JSON.stringify(x));

        this.spinner.show();
        this.partyService.updateQueueData(x, headers, id).subscribe(data => {
            // console.log(data);
            this.fetchAllPoProduct(false);
            this.spinner.hide();
        });
    }

    previewImage(event) {
        this.image = this.poProductList[event].productimg;
        // console.log('dataaiMage' + JSON.stringify(this.image));

    }
    castingHistory() {
        this.history = !this.history;
        // console.log('boolean' + this.history);

        this.fetchAllPoProduct(false);
    }

    fetchPlannerData() {
        this.spinner.show();
        let id;
        // console.log('hidtory' + this.history);

        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.duplicatePlannerList = [];
        this.partyService.getAllPOProductByQueueStatus(headers, 1).subscribe(data => {
            this.poProductList = data.pOProductData;
            this.duplicatePlannerList = JSON.parse(JSON.stringify(this.poProductList));
            this.duplicatePlannerList.forEach(element => {
                if (element.processtype == 0) {
                    element.processtype = 'FORGING';
                }
                else {
                    element.processtype = 'ROLL CASTING';
                }
                if (element.itemtype == 0) {
                    element.itemtype = 'ROLLS';
                }
                else if (element.itemtype == 1) {
                    element.itemtype = 'STEP ROLLS';
                }
                else if (element.itemtype == 2) {
                    element.itemtype = 'REEL';
                }
                else {
                    element.itemtype = 'OTHER';
                }
                if (element.uom == 0) {
                    element.uom = 'Per-KG';
                }
                else if (element.uom == 1) {
                    element.uom = 'MT';
                }
                else {
                    element.uom = 'Per-Piece';
                }
                if (element.idList.currentQueue == '1') {
                    element.idList.currentQueue = 'Planning';
                }
                else if (element.idList.currentQueue == '2') {
                    element.idList.currentQueue = 'Roll Casting';
                }
                let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                element['dimensionFormString'] = toString(element.dimensionForm);
                element.idList.queueUpdateTimeStamp = element.idList.queueUpdateTimeStamp.substring(0, 4) + '-' + element.idList.queueUpdateTimeStamp.substring(5, 7) + '-' + element.idList.queueUpdateTimeStamp.substring(8, 10);
            });
            // console.log('here' + JSON.stringify(this.duplicatePlannerList));
            this.spinner.hide();
        })
    }

    onDateSelection() {
        let fromdateutc: any;
        let todateutc: any;
        if (this.fromDate != '' && this.toDate != '') {
            if (JSON.stringify(this.fromDate) == JSON.stringify(this.toDate)) {
                this.toDate.day = this.toDate.day + 1;
            }
        }
        this.duplicatepoProductList = this.tempduplicatepoProductList;
        fromdateutc = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
        fromdateutc = fromdateutc.getTime();
        todateutc = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
        todateutc = todateutc.getTime();
        let duplicatepoProductList = [];
        duplicatepoProductList = this.duplicatepoProductList;
        this.duplicatepoProductList = [];
        duplicatepoProductList.forEach(element => {
            element.filterDate = new Date(element.idList.queueUpdateTimeStamp).getTime();
        });
        duplicatepoProductList.forEach(element => {
            if (element.filterDate >= fromdateutc && element.filterDate <= todateutc) {
                this.duplicatepoProductList.push(element);
            }
        });
        this.listCount = this.duplicatepoProductList.length;
    }

    onKeyup() {
        let filteredList = [];
        if (this.search.length == 0) {
            this.listCount = this.duplicatepoProductList.length;
        }
        else {
            // console.log('dataata' + JSON.stringify(this.duplicatepoProductList));
            filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
            this.listCount = filteredList.length;
            // console.log('count'+this.listCount);

        }
    }

    convertPoDetailsExcel() {
        let value = [];

        this.filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
        // console.log('dataata' + JSON.stringify(this.filteredList));

        this.filteredList.forEach(fuel => {
            // console.log('fuel' + JSON.stringify(fuel));
            let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
            fuel.dimensionForm = toString(fuel.dimensionForm);
            let subValueArray = [];
            subValueArray.push(fuel.idList.displayId);
            subValueArray.push(fuel.producttype);
            subValueArray.push(fuel.itemtype);
            subValueArray.push(fuel.partyData.partyname);
            subValueArray.push(fuel.weightperpiece);
            subValueArray.push(fuel.idList.rejection.rejectionRemark);
            subValueArray.push(fuel.reqhardness);
            subValueArray.push(fuel.dimensionForm);
            subValueArray.push(fuel.grade);
            subValueArray.push(fuel.productimg);
            value.push(subValueArray);
        });

        // let headerName = "Fuel Summary Report (" + this.fromDate.formatted + " to " + this.toDate.formatted + ")";
        let headerName = "Rejection Report";
        let body = {
            "headerName": headerName,
            "excelName": "Rejection",
            "header": [
                "ROLLID",
                "PRODUCTTYPE",
                "ITEMTYPE",
                "PARTYNAME",
                "WEIGHT/PIECE",
                "REMARK",
                "REQ.HARDNESS",
                "DIMENSION",
                "GRADE",
                "PROD.IMG"
            ],
            "value": value
        }
        this.downloadExcelSheet(body).subscribe(blob => {
            // Doing it this way allows you to name the file
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = "Rejection.xlsx";
            link.click();
        }, error => console.log("Error downloading the file."),
            () => console.log('Completed file download.'));
    }

    downloadExcelSheet(reqBody): Observable<Object[]> {
        //this.loading = true;
        return Observable.create(observer => {
            let xhr = new XMLHttpRequest();
            xhr.open('POST', AppConstants.EXCEL_DOWNLOAD_URL, true);
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.responseType = 'blob';

            xhr.onreadystatechange = () => {
                //this.loading = false;
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {

                        var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        var blob = new Blob([xhr.response], { type: contentType });
                        observer.next(blob);
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            xhr.send(JSON.stringify(reqBody));

        });
    }

    onPath() {
        this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        let x = {
            roleDisplayId: this.data.idList.displayId,
            poDetailsId: this.data.poDetailsId,
            rejectionQueue: this.data.idList.rejection.rejectionQueue
        };
        this.partyService.pathRoll(x, headers).subscribe(data => {
            this.fetchAllPoProduct(false);
            this.spinner.hide();
        });
    }

    moveTo(data) {
        this.data = data;
        this.productType = this.data.producttype;
        // console.log('data' + JSON.stringify(this.productType));
        $('#moveTo').modal('show');
    }

    moveProcess() {
        let type = this.registrationForm.value;
        $('#moveTo').modal('hide');
        this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        let x = {
            roleDisplayId: this.data.idList.displayId,
            poDetailsId: this.data.poDetailsId,
            toWhichQueue: type.gender
        };
        this.partyService.moveRollToProcess(x, headers).subscribe(data => {
            this.fetchAllPoProduct(false);
            this.spinner.hide();
        });
    }

    onHold() {
        // console.log('data -----' + JSON.stringify(this.data));
        let reqBody;
        this.poProductList.forEach(product => {
            if (this.data.idList.displayId == product.idList.displayId) {
                reqBody = product;
            }
        });
        this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.holdAndReplanRoll(reqBody, headers).subscribe(data => {
            this.fetchAllPoProduct(false);
            this.spinner.hide();
        });
    }
}