import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { POPlannerRoutingModule } from './po-routing.module';
import { DataTableModule } from 'primeng/datatable';
import { AccordionModule } from 'primeng/accordion';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxSpinnerModule } from 'ngx-spinner'
import { NgbModule, NgbCalendar, NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { POComponent } from './po.component';
import { PoCreationComponent } from './po-creation/po-creation.component';
import { PartyCreationComponent } from './party-creation/party-creation.component';
import { CCCreationComponent } from './cc-creation/cc-creation.component';
import { RollPlannerComponent } from './roll-planner/roll-planner.component';
import { RollCastingComponent } from './roll-casting/roll-casting.component';
import { QualityCheckComponent } from './quality-check/quality-check.component';
import { MachiningEditorComponent } from './machining-editor/machining-editor.component';
import { InspectionEditorComponent } from './inspection-editor/inspection-editor.component';
import { GrooveCentreComponent } from './groove-centre/groove-centre.component';
import { BearingShapperComponent } from './bearing-shapper/bearing-shapper.component';
import { GrooveCentreFinalComponent } from './groove-centre-final/groove-centre-final.component';
import { BearingShapperFinalComponent } from './bearing-shapper-final/bearing-shapper-final.component';
import { TransportDispatchComponent } from './transport-dispatch/transport-dispatch.component';
import { TestCertificateComponent } from './test-certificate/test-certificate.component';
import { PackagingComponent } from './packaging/packaging.component';
import { RejectionComponent } from './rejection/rejection.component';
import { MasterTableComponent } from './master-table/master-table.component';
import { NgxViewerModule } from 'ngx-viewer';
import { HoldRollsComponent } from './holdRolls/holdRolls.component';
const notifierDefaultOptions: NotifierOptions = {
    position: {
        horizontal: {
            position: 'right',
            distance: 12
        },
        vertical: {
            position: 'bottom',
            distance: 12,
            gap: 10
        }
    },
    theme: 'material',
    behaviour: {
        autoHide: 5000,
        onClick: false,
        onMouseover: 'pauseAutoHide',
        showDismissButton: true,
        stacking: 4
    },
    animations: {
        enabled: true,
        show: {
            preset: 'slide',
            speed: 300,
            easing: 'ease'
        },
        hide: {
            preset: 'fade',
            speed: 300,
            easing: 'ease',
            offset: 50
        },
        shift: {
            speed: 300,
            easing: 'ease'
        },
        overlap: 150
    }
};

@NgModule({
    declarations: [POComponent, PoCreationComponent, PartyCreationComponent, CCCreationComponent, RollPlannerComponent, RollCastingComponent, QualityCheckComponent, MachiningEditorComponent, InspectionEditorComponent, GrooveCentreComponent, BearingShapperComponent, GrooveCentreFinalComponent, BearingShapperFinalComponent, TransportDispatchComponent, TestCertificateComponent, PackagingComponent, RejectionComponent, MasterTableComponent,HoldRollsComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        POPlannerRoutingModule,
        DataTableModule,
        AccordionModule,
        NotifierModule.withConfig(notifierDefaultOptions),
        Ng2SearchPipeModule,
        NgxSpinnerModule,
        NgbModule,
        NgxViewerModule

    ]
})
export class POModule { }
