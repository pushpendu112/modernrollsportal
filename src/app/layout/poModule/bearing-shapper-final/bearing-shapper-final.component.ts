import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { PartyService } from 'src/app/shared/services/party.service';
import * as CryptoJS from 'crypto-js';
import { AppConstants } from '../../../base/appconstants';
import { NgxSpinnerService } from 'ngx-spinner';
import { QueueStatus } from '../../../models/queueStatus';
import { NotifierService } from 'angular-notifier';
import { Ng2SearchPipe } from 'ng2-search-filter';
import { Observable } from 'rxjs';
@Component({
    selector: 'app-bearing-shapper-final',
    templateUrl: './bearing-shapper-final.component.html',
    styleUrls: ['./bearing-shapper-final.component.scss'],
    // encapsulation: ViewEncapsulation.None
})
export class BearingShapperFinalComponent implements OnInit {
    searchtype: any = "0";
    basicSearchTerm: any = "";

    constructor(private partyService: PartyService, private spinner: NgxSpinnerService, private notifierService: NotifierService) {
        this.filter = new Ng2SearchPipe();
        this.search = '';
        this.fromDate = '';
        this.toDate = '';
    }
    filter: Ng2SearchPipe;
    filteredList: any = [];
    listCount: any;
    fromDate: any;
    toDate: any;
    tempduplicatepoProductList: any = [];
    poProductList: any = [];
    duplicatepoProductList: any = [];
    userToken: any;
    image: any;
    remark: string = '';
    index: number;
    pocols: any[];
    search: any;
    moveCast: boolean;
    machineNo: string;
    OperatorName: string;
    finalDimension: any;
    finalInspectorName: any;
    finalRemark: any;
    data: any;
    dia1: any;
    dia2: any;
    dia3: any;
    dia4: any;
    dia5: any;
    dia6: any;
    dia7: any;
    dia8: any;
    dia9: any;
    dia10: any;
    dia11: any;
    dia12: any;
    dia13: any;
    dia14: any;
    dia15: any;
    dia16: any;
    dia17: any;
    history: boolean = false;
    colorMap = {
        "ADAMITS": "red",
        "CHILLS": "blue",
        "SG-IRON": "green",
        "SG-ACC": "#c4c233"
    }
    ngOnInit() {
        const getjwt = localStorage.getItem('jwttoken');
        this.userToken = this.conversionDecrypt(getjwt);
        // this.fetchAllPoProduct(false);
        this.pocols = [
            { field: 'idList.displayId', header: 'Roll Id', width: '7%' },
            // { field: 'processtype', header: 'Process Type', width: '8%' },
            // { field: 'grade', header: 'Grade', width: '7%' },
            { field: 'producttype', header: 'Prod. Type', width: '7%' },
            // { field: 'itemtype', header: 'Item Type', width: '7%' },
            { field: 'partyData.partyname', header: 'PartyName', width: '8%' },
            // { field: 'uom', header: 'UOM', width: '8%' },
            // { field: 'priceperuom', header: 'Price/UOM', width: '8%' },
            // { field: 'weightperpiece', header: 'Weight/Piece', width: '9%' },
            // { field: 'idList.currentQueue', header: 'Status', width: '7%' },
            { field: 'idList.queueUpdateTimeStamp', header: 'B&S/Fin.Date', width: '6%' },
            { field: 'idList.bASInspectionData.finalDimension', header: 'FinalDimension', width: '7%' },
            { field: 'idList.bASInspectionData.finalInspectorName', header: 'Insp. Name', width: '8%' },
            { field: 'idList.bASInspectionData.finalRemark', header: 'remark', width: '8%' },
            { field: 'dimensionFormString', header: 'Dimension', width: '7%' },
            { field: 'idList.inspectionData.actualDimension', header: 'ActualDimension', width: '7%' },
            // { field: 'idList.mechiningData.mechineNo', header: 'Machnine Num', width: '7%' },
            // { field: 'idList.mechiningData.operatorName', header: 'Operator', width: '7%' }
            { field: 'remark', header: 'Remark', width: '7%' },
        ];
    }
    conversionDecrypt(decrypt) {
        try {
            const bytes = CryptoJS.AES.decrypt(decrypt, AppConstants.secret);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return decrypt;
        } catch (e) {
            console.log(e);
        }
    }

    searchProduct(isCheck) {

        let filteredList = [];
        let id;
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        if (this.history == false) {
            id = 9;
        }
        else {
            id = 0;
        }

        this.duplicatepoProductList = [];
        this.tempduplicatepoProductList = [];
        this.poProductList = [];
        this.spinner.show();
        if (id == 9) {
            let x = {
                currentQueue: "9",
                searchTerm: this.basicSearchTerm,
                searchtype: this.searchtype
            };
            // console.log("x ---" + JSON.stringify(x));
            if (this.basicSearchTerm == "") {
                this.spinner.hide();
                return;
            }
            this.partyService.searchPOProductByQueueStatus(x, headers).subscribe(data => {
                this.poProductList = data.pOProductData;
                if (this.poProductList.length < 1) {
                    this.spinner.hide();
                }
                // if (this.history == false) {
                // this.poProductList.forEach(element => {
                // if (element.idList.currentQueue == 1) {
                this.duplicatepoProductList = JSON.parse(JSON.stringify(this.poProductList));
                if (this.search.length == 0) {
                    this.listCount = this.duplicatepoProductList.length;
                }
                else {
                    filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                    this.listCount = filteredList.length;
                }

                this.duplicatepoProductList.forEach(element => {
                    if (element.processtype == 0) {
                        element.processtype = 'FORGING';
                    }
                    else {
                        element.processtype = 'ROLL CASTING';
                    }
                    if (element.itemtype == 0) {
                        element.itemtype = 'ROLLS';
                    }
                    else if (element.itemtype == 1) {
                        element.itemtype = 'STEP ROLLS';
                    }
                    else if (element.itemtype == 2) {
                        element.itemtype = 'REEL';
                    }
                    else {
                        element.itemtype = 'OTHER';
                    }
                    if (element.uom == 0) {
                        element.uom = 'Per-KG';
                    }
                    else if (element.uom == 1) {
                        element.uom = 'MT';
                    }
                    else {
                        element.uom = 'Per-Piece';
                    }
                    if (element.idList.currentQueue == '1') {
                        element.idList.currentQueue = 'Planning';
                    }
                    else if (element.idList.currentQueue == '2') {
                        element.idList.currentQueue = 'Roll Casting';
                    }
                    let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                    element['dimensionFormString'] = toString(element.dimensionForm);
                    element.idList.queueUpdateTimeStamp = element.idList.queueUpdateTimeStamp.substring(0, 4) + '-' + element.idList.queueUpdateTimeStamp.substring(5, 7) + '-' + element.idList.queueUpdateTimeStamp.substring(8, 10);
                    this.tempduplicatepoProductList = this.duplicatepoProductList;
                    if (isCheck == true) {
                        this.fromDate = '';
                        this.toDate = '';
                    }
                    if (this.fromDate != '' && this.toDate != '') {
                        this.onDateSelection();
                    }
                    this.spinner.hide();
                });
                if (isCheck == true) {
                    this.fromDate = '';
                    this.toDate = '';
                }
                if (this.fromDate != '' && this.toDate != '') {
                    this.onDateSelection();
                }
                this.spinner.hide();
            })
        } else {
            let x = {
                currentQueue: "9",
                searchTerm: this.basicSearchTerm,
                searchtype: this.searchtype
            };
            this.partyService.fetchAllPoProductsForHistory(x, headers).subscribe(data => {
                this.poProductList = data.pOProductData;
                if (this.poProductList.length < 1) {
                    this.spinner.hide();
                }
                // console.log('enetr' + JSON.stringify(this.poProductList));
                // if (this.poProductList != undefined)
                this.poProductList.forEach(element => {
                    if (element.producttype == 'BEARING') {
                        this.duplicatepoProductList.push(element);
                        // console.log('enetr' + JSON.stringify(this.duplicatepoProductList));
                        if (this.search.length == 0) {
                            this.listCount = this.duplicatepoProductList.length;
                        }
                        else {
                            filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                            this.listCount = filteredList.length;
                        }
                        this.duplicatepoProductList.forEach(element => {
                            if (element.processtype == 0) {
                                element.processtype = 'FORGING';
                            }
                            else {
                                element.processtype = 'ROLL CASTING';
                            }
                            if (element.itemtype == 0) {
                                element.itemtype = 'ROLLS';
                            }
                            else if (element.itemtype == 1) {
                                element.itemtype = 'STEP ROLLS';
                            }
                            else if (element.itemtype == 2) {
                                element.itemtype = 'REEL';
                            }
                            else {
                                element.itemtype = 'OTHER';
                            }
                            if (element.uom == 0) {
                                element.uom = 'Per-KG';
                            }
                            else if (element.uom == 1) {
                                element.uom = 'MT';
                            }
                            else {
                                element.uom = 'Per-Piece';
                            }
                            if (element.idList.currentQueue == '1') {
                                element.idList.currentQueue = 'Planning';
                            }
                            else if (element.idList.currentQueue == '2') {
                                element.idList.currentQueue = 'Roll Casting';
                            }
                            let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                            element['dimensionFormString'] = toString(element.dimensionForm);
                            element.idList.queueUpdateTimeStamp = element.idList.queueUpdateTimeStamp.substring(0, 4) + '-' + element.idList.queueUpdateTimeStamp.substring(5, 7) + '-' + element.idList.queueUpdateTimeStamp.substring(8, 10);
                            this.tempduplicatepoProductList = this.duplicatepoProductList;
                            if (isCheck == true) {
                                this.fromDate = '';
                                this.toDate = '';
                            }
                            if (this.fromDate != '' && this.toDate != '') {
                                this.onDateSelection();
                            }

                        });
                    }
                });
                this.spinner.hide();
            });
        }
    }


    fetchAllPoProduct(isCheck) {
        let filteredList = [];
        this.spinner.show();
        let id;

        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        if (this.history == false) {
            id = 9;
        }
        else {
            id = 0;
        }
        this.duplicatepoProductList = [];
        this.tempduplicatepoProductList = [];
        this.poProductList = [];
        // console.log('id' + id);

        if (id == 9) {
            this.partyService.getAllPOProductByQueueStatus(headers, id).subscribe(data => {
                this.poProductList = data.pOProductData;
                if (this.poProductList.length < 1) {
                    this.spinner.hide();
                }
                this.duplicatepoProductList = JSON.parse(JSON.stringify(this.poProductList));
                if (this.search.length == 0) {
                    this.listCount = this.duplicatepoProductList.length;
                }
                else {
                    filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                    this.listCount = filteredList.length;
                }
                this.duplicatepoProductList.forEach(element => {
                    if (element.processtype == 0) {
                        element.processtype = 'FORGING';
                    }
                    else {
                        element.processtype = 'ROLL CASTING';
                    }
                    if (element.itemtype == 0) {
                        element.itemtype = 'ROLLS';
                    }
                    else if (element.itemtype == 1) {
                        element.itemtype = 'STEP ROLLS';
                    }
                    else if (element.itemtype == 2) {
                        element.itemtype = 'REEL';
                    }
                    else {
                        element.itemtype = 'OTHER';
                    }
                    if (element.uom == 0) {
                        element.uom = 'Per-KG';
                    }
                    else if (element.uom == 1) {
                        element.uom = 'MT';
                    }
                    else {
                        element.uom = 'Per-Piece';
                    }
                    if (element.idList.currentQueue == '1') {
                        element.idList.currentQueue = 'Planning';
                    }
                    else if (element.idList.currentQueue == '2') {
                        element.idList.currentQueue = 'Roll Casting';
                    }
                    let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                    element['dimensionFormString'] = toString(element.dimensionForm);
                    element.idList.queueUpdateTimeStamp = element.idList.queueUpdateTimeStamp.substring(0, 4) + '-' + element.idList.queueUpdateTimeStamp.substring(5, 7) + '-' + element.idList.queueUpdateTimeStamp.substring(8, 10);
                    this.tempduplicatepoProductList = this.duplicatepoProductList;
                    if (isCheck == true) {
                        this.fromDate = '';
                        this.toDate = '';
                    }
                    if (this.fromDate != '' && this.toDate != '') {
                        this.onDateSelection();
                    }
                    this.spinner.hide();

                });

            });
        }
        else {
            this.partyService.fetchAllPoProducts(headers).subscribe(data => {
                this.poProductList = data.pOProductData;
                if (this.poProductList.length < 1) {
                    this.spinner.hide();
                }
                // console.log('enetr' + JSON.stringify(this.poProductList));
                // if (this.poProductList != undefined)
                this.poProductList.forEach(element => {
                    if ((element.idList.currentQueue > 9 && element.producttype == 'BEARING') || element.idList.currentQueue == 0) {
                        this.duplicatepoProductList.push(element);
                        // console.log('enetr' + JSON.stringify(this.duplicatepoProductList));
                        if (this.search.length == 0) {
                            this.listCount = this.duplicatepoProductList.length;
                        }
                        else {
                            filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                            this.listCount = filteredList.length;
                        }
                        this.duplicatepoProductList.forEach(element => {
                            if (element.processtype == 0) {
                                element.processtype = 'FORGING';
                            }
                            else {
                                element.processtype = 'ROLL CASTING';
                            }
                            if (element.itemtype == 0) {
                                element.itemtype = 'ROLLS';
                            }
                            else if (element.itemtype == 1) {
                                element.itemtype = 'STEP ROLLS';
                            }
                            else if (element.itemtype == 2) {
                                element.itemtype = 'REEL';
                            }
                            else {
                                element.itemtype = 'OTHER';
                            }
                            if (element.uom == 0) {
                                element.uom = 'Per-KG';
                            }
                            else if (element.uom == 1) {
                                element.uom = 'MT';
                            }
                            else {
                                element.uom = 'Per-Piece';
                            }
                            if (element.idList.currentQueue == '1') {
                                element.idList.currentQueue = 'Planning';
                            }
                            else if (element.idList.currentQueue == '2') {
                                element.idList.currentQueue = 'Roll Casting';
                            }
                            let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                            element['dimensionFormString'] = toString(element.dimensionForm);
                            element.idList.queueUpdateTimeStamp = element.idList.queueUpdateTimeStamp.substring(0, 4) + '-' + element.idList.queueUpdateTimeStamp.substring(5, 7) + '-' + element.idList.queueUpdateTimeStamp.substring(8, 10);
                            this.tempduplicatepoProductList = this.duplicatepoProductList;
                            if (isCheck == true) {
                                this.fromDate = '';
                                this.toDate = '';
                            }
                            if (this.fromDate != '' && this.toDate != '') {
                                this.onDateSelection();
                            }
                            this.spinner.hide();

                        });
                    }
                });
            });
        }

    }

    moveToGrovingBearing(event) {
        // console.log('event' + JSON.stringify(event));
        let data = event;
        // console.log('data' + JSON.stringify(data));
        if (data.idList.bASInspectionData.finalDimension != "" && data.idList.bASInspectionData.finalInspectorName != "" && data.idList.bASInspectionData.finalRemark != "") {
            let x = {
                poDetailsId: data.poDetailsId,
                productId: data._id,
                displayId: data.idList.displayId,
                queueUpdateTimeStamp: data.idList.queueUpdateTimeStamp,
                upcomingQueue: QueueStatus.FinalInspection,
                currentQueue: QueueStatus.BearingShapperFinal,
            };
            this.spinner.show();
            const headers = new Headers(
                {
                    'accept': 'application/json',
                    'x-access-token': this.userToken
                });
            this.partyService.updateQueueStatus(x, headers).subscribe(data => {
                // console.log(data);
                // this.fetchAllPoProduct(false);
                this.searchProduct(false);
                this.spinner.hide();
            });
        }
        else {
            this.notifierService.notify('error', 'Fill Final Data!');
        }
    }

    moveToRejection() {
        // console.log('event' + JSON.stringify(this.index));
        let data = this.data;
        // console.log('data' + JSON.stringify(data));
        let x = {
            poDetailsId: data.poDetailsId,
            productId: data._id,
            displayId: data.idList.displayId,
            queueUpdateTimeStamp: data.idList.queueUpdateTimeStamp,
            upcomingQueue: QueueStatus.Rejected,
            currentQueue: QueueStatus.BearingShapperFinal,
        };
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.updateQueueStatus(x, headers).subscribe(data => {
            // console.log(data);
            this.updateQueueData();
        });
    }

    getProductDetails() {

    }

    remarkData(event) {
        this.remark = '';
        this.data = event;
    }

    updateQueueData() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        let data = this.data.idList;
        let id = this.data._id;
        // console.log('final data' + JSON.stringify(data));
        let timeStamp = new Date();
        // console.log('date' + JSON.stringify(timeStamp));
        data.rejection.rejectionRemark = this.remark;
        data.rejection.rejectionQueue = QueueStatus.BearingShapperFinal;
        data.rejection.rejectionTimeStamp = timeStamp;
        let x = {
            queueUpdateTimeStamp: data.queueUpdateTimeStamp,
            displayId: data.displayId,
            currentQueue: QueueStatus.Rejected,
            isRejected: true,
            qualityCheckData: data.qualityCheckData,
            isDispacthed: data.isDispacthed,
            isPacked: data.isPacked,
            isTCDone: data.isTCDone,
            mechiningData: data.mechiningData,
            rejection: data.rejection,
            bAndSTestCertificateData: data.bAndSTestCertificateData,
            inspectionData: data.inspectionData,
            gACFinishingData: data.gACFinishingData,
            bASFinishingData: data.bASFinishingData,
            gACInspectionData: data.gACInspectionData,
            bASInspectionData: data.bASInspectionData,
            testCertificateData: data.testCertificateData,
            queueDurationList: data.queueDurationList
        };
        // console.log('x data' + JSON.stringify(x));

        this.spinner.show();
        this.partyService.updateQueueData(x, headers, id).subscribe(data => {
            // console.log(data);
            // this.fetchAllPoProduct(false);
            this.searchProduct(false);
            this.spinner.hide();
        });
    }

    updateMachineData() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        let data = this.data.idList;
        let id = this.data._id;
        let timeStamp = new Date();
        // console.log('date' + JSON.stringify(timeStamp));
        data.bASInspectionData.finalDimension = this.finalDimension;
        data.bASInspectionData.finalInspectorName = this.finalInspectorName;
        data.bASInspectionData.finalRemark = this.finalRemark;
        // console.log('final data' + JSON.stringify(data.bASInspectionData));
        let x = {
            queueUpdateTimeStamp: data.queueUpdateTimeStamp,
            displayId: data.displayId,
            currentQueue: data.currentQueue,
            isRejected: data.isRejected,
            qualityCheckData: data.qualityCheckData,
            isDispacthed: data.isDispacthed,
            isPacked: data.isPacked,
            isTCDone: data.isTCDone,
            mechiningData: data.mechiningData,
            rejection: data.rejection,
            bAndSTestCertificateData: data.bAndSTestCertificateData,
            inspectionData: data.inspectionData,
            gACFinishingData: data.gACFinishingData,
            bASFinishingData: data.bASFinishingData,
            gACInspectionData: data.gACInspectionData,
            bASInspectionData: data.bASInspectionData,
            testCertificateData: data.testCertificateData,
            queueDurationList: data.queueDurationList
        };
        // console.log('x data' + JSON.stringify(x));

        this.spinner.show();
        this.partyService.updateQueueData(x, headers, id).subscribe(data => {
            // console.log(data);
            // this.fetchAllPoProduct(false);
            this.searchProduct(false);
            this.spinner.hide();
        });
    }

    previewImage(event) {
        this.image = this.poProductList[event].productimg;
        // console.log('dataaiMage' + JSON.stringify(this.image));

    }

    dimensionData(event) {
        this.data = event;
        this.finalDimension = this.data.idList.bASInspectionData.finalDimension;
        this.finalInspectorName = this.data.idList.bASInspectionData.finalInspectorName;
        this.finalRemark = this.data.idList.bASInspectionData.finalRemark;
    }

    bearingData(event) {
        this.data = event;
        this.dia1 = this.data.idList.bASInspectionData.actualBearingDia[0];
        this.dia2 = this.data.idList.bASInspectionData.actualBearingDia[1];
        this.dia3 = this.data.idList.bASInspectionData.actualBearingDia[2];
        this.dia4 = this.data.idList.bASInspectionData.actualBearingDia[3];
        this.dia5 = this.data.idList.bASInspectionData.actualBearingDia[4];
        this.dia6 = this.data.idList.bASInspectionData.actualBearingDia[5];
        this.dia7 = this.data.idList.bASInspectionData.actualBearingDia[6];
        this.dia8 = this.data.idList.bASInspectionData.actualBearingDia[7];
        this.dia9 = this.data.idList.bASInspectionData.actualBearingDia[8];
        this.dia10 = this.data.idList.bASInspectionData.actualBearingDia[9];
        this.dia11 = this.data.idList.bASInspectionData.actualBearingDia[10];
        this.dia12 = this.data.idList.bASInspectionData.actualBearingDia[11];
        this.dia13 = this.data.idList.bASInspectionData.actualBearingDia[12];
        this.dia14 = this.data.idList.bASInspectionData.actualBearingDia[13];
        this.dia15 = this.data.idList.bASInspectionData.actualBearingDia[14];
        this.dia16 = this.data.idList.bASInspectionData.actualBearingDia[15];
        this.dia17 = this.data.idList.bASInspectionData.actualBearingDia[16];
    }

    updatediaData() {
        let diaData = [];
        if (this.dia1 != "") {
            diaData.push(this.dia1);
        }
        if (this.dia2 != "") {
            diaData.push(this.dia2);
        }
        if (this.dia3 != "") {
            diaData.push(this.dia3);
        }
        if (this.dia4 != "") {
            diaData.push(this.dia4);
        }
        if (this.dia5 != "") {
            diaData.push(this.dia5);
        }
        if (this.dia6 != "") {
            diaData.push(this.dia6);
        }
        if (this.dia7 != "") {
            diaData.push(this.dia7);
        }
        if (this.dia8 != "") {
            diaData.push(this.dia8);
        }
        if (this.dia9 != "") {
            diaData.push(this.dia9);
        }
        if (this.dia10 != "") {
            diaData.push(this.dia10);
        }
        if (this.dia11 != "") {
            diaData.push(this.dia11);
        }
        if (this.dia12 != "") {
            diaData.push(this.dia12);
        }
        if (this.dia13 != "") {
            diaData.push(this.dia13);
        }
        if (this.dia14 != "") {
            diaData.push(this.dia14);
        }
        if (this.dia15 != "") {
            diaData.push(this.dia15);
        }
        if (this.dia16 != "") {
            diaData.push(this.dia16);
        }
        if (this.dia17 != "") {
            diaData.push(this.dia17);
        }
        // console.log('diaData' + JSON.stringify(diaData));
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        let data = this.data.idList;
        let id = this.data._id;
        let timeStamp = new Date();
        // console.log('date' + JSON.stringify(timeStamp));
        data.bASInspectionData.actualBearingDia = diaData;
        // console.log('final data' + JSON.stringify(data.bASInspectionData.finalDimension));
        let x = {
            queueUpdateTimeStamp: data.queueUpdateTimeStamp,
            displayId: data.displayId,
            currentQueue: data.currentQueue,
            isRejected: data.isRejected,
            qualityCheckData: data.qualityCheckData,
            mechiningData: data.mechiningData,
            rejection: data.rejection,
            isDispacthed: data.isDispacthed,
            bAndSTestCertificateData: data.bAndSTestCertificateData,
            isPacked: data.isPacked,
            isTCDone: data.isTCDone,
            inspectionData: data.inspectionData,
            gACFinishingData: data.gACFinishingData,
            bASFinishingData: data.bASFinishingData,
            gACInspectionData: data.gACInspectionData,
            bASInspectionData: data.bASInspectionData,
            testCertificateData: data.testCertificateData,
            queueDurationList: data.queueDurationList
        };
        // console.log('x data' + JSON.stringify(x));

        this.spinner.show();
        this.partyService.updateQueueData(x, headers, id).subscribe(data => {
            // console.log(data);
            this.searchProduct(false);
            this.spinner.hide();
        });
    }

    castingHistory() {
        this.history = !this.history;
        // this.searchProduct(false);
        this.listCount = 0;
        this.duplicatepoProductList = [];
        this.basicSearchTerm = "";
        this.searchtype = "0"
    }

    onDateSelection() {
        let fromdateutc: any;
        let todateutc: any;
        if (this.fromDate != '' && this.toDate != '') {
            if (JSON.stringify(this.fromDate) == JSON.stringify(this.toDate)) {
                this.toDate.day = this.toDate.day + 1;
            }
        }
        this.duplicatepoProductList = this.tempduplicatepoProductList;
        fromdateutc = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
        fromdateutc = fromdateutc.getTime();
        todateutc = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
        todateutc = todateutc.getTime();
        let duplicatepoProductList = [];
        duplicatepoProductList = this.duplicatepoProductList;
        this.duplicatepoProductList = [];
        if (this.history) {
            duplicatepoProductList.forEach(element => {
                for (let index = element.idList.queueDurationList.length - 1; index >= 0; index--) {
                    const durationObject = element.idList.queueDurationList[index];
                    if (durationObject.queue == "9") {
                        element.idList.tempFilteringDate = durationObject.endTime;
                    }
                }
            });

            duplicatepoProductList.forEach(element => {
                element.filterDate = new Date(element.idList.tempFilteringDate).getTime();
            });
        } else {
            duplicatepoProductList.forEach(element => {
                element.filterDate = new Date(element.idList.queueUpdateTimeStamp).getTime();
            });
        }

        duplicatepoProductList.forEach(element => {
            if (element.filterDate >= fromdateutc && element.filterDate <= todateutc) {
                this.duplicatepoProductList.push(element);
            }
        });
        this.listCount = this.duplicatepoProductList.length;
    }

    onKeyup() {
        let filteredList = [];
        if (this.search.length == 0) {
            this.listCount = this.duplicatepoProductList.length;
        }
        else {
            // console.log('dataata' + JSON.stringify(this.duplicatepoProductList));
            filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
            this.listCount = filteredList.length;
        }
    }

    convertPoDetailsExcel() {
        let value = [];

        this.filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
        // console.log('dataata' + JSON.stringify(this.filteredList));

        this.filteredList.forEach(fuel => {
            // console.log('fuel' + JSON.stringify(fuel));
            let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
            fuel.dimensionForm = toString(fuel.dimensionForm);
            // console.log('fdime' + JSON.stringify(fuel.dimensionForm));

            let subValueArray = [];
            subValueArray.push(fuel.idList.displayId);
            subValueArray.push(fuel.producttype);
            subValueArray.push(fuel.partyData.partyname);
            subValueArray.push(fuel.idList.queueUpdateTimeStamp);
            subValueArray.push(fuel.idList.bASInspectionData.finalDimension);
            subValueArray.push(fuel.idList.bASInspectionData.finalInspectorName);
            subValueArray.push(fuel.idList.bASInspectionData.finalRemark);
            subValueArray.push(fuel.dimensionForm);
            subValueArray.push(fuel.grade);
            subValueArray.push(fuel.productimg);
            value.push(subValueArray);
        });

        // let headerName = "Fuel Summary Report (" + this.fromDate.formatted + " to " + this.toDate.formatted + ")";
        let headerName = "Bearing&ShapperFinal Report";
        let body = {
            "headerName": headerName,
            "excelName": "Bearing&ShapperFinal",
            "header": [
                "ROLLID",
                "PRODUCTTYPE",
                "PARTYNAME",
                "B&F/FINAL.DATE",
                "FINALDIMENSION",
                "FINALINSPECTIONNAME",
                "FINALREMARK",
                "DIMENSION",
                "GRADE",
                "PROD.IMG"
            ],
            "value": value
        }
        this.downloadExcelSheet(body).subscribe(blob => {
            // Doing it this way allows you to name the file
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = "Bearing&ShapperFinal.xlsx";
            link.click();
        }, error => console.log("Error downloading the file."),
            () => console.log('Completed file download.'));
    }

    downloadExcelSheet(reqBody): Observable<Object[]> {
        //this.loading = true;
        return Observable.create(observer => {
            let xhr = new XMLHttpRequest();
            xhr.open('POST', AppConstants.EXCEL_DOWNLOAD_URL, true);
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.responseType = 'blob';

            xhr.onreadystatechange = () => {
                //this.loading = false;
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {

                        var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        var blob = new Blob([xhr.response], { type: contentType });
                        observer.next(blob);
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            xhr.send(JSON.stringify(reqBody));

        });
    }
}