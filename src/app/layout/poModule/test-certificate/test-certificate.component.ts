import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { PartyService } from 'src/app/shared/services/party.service';
import * as CryptoJS from 'crypto-js';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppConstants } from '../../../base/appconstants';
import { NgxSpinnerService } from 'ngx-spinner';
import { QueueStatus } from '../../../models/queueStatus';
import { NotifierService } from 'angular-notifier';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Ng2SearchPipe } from 'ng2-search-filter';
import { Observable } from 'rxjs';
declare var jquery: any;   // not required
declare var $: any;   // not required
@Component({
    selector: 'app-test-certificate',
    templateUrl: './test-certificate.component.html',
    styleUrls: ['./test-certificate.component.scss'],
    // encapsulation: ViewEncapsulation.None
})
export class TestCertificateComponent implements OnInit {

    searchtype: any = "0";
    basicSearchTerm: any = "";

    constructor(private partyService: PartyService, private spinner: NgxSpinnerService, private formBuilder: FormBuilder, private notifierService: NotifierService) {
        this.filter = new Ng2SearchPipe();
        this.search = '';
        this.fromDate = '';
        this.toDate = '';
    }
    filter: Ng2SearchPipe;
    filteredList: any = [];
    listCount: any;
    fromDate: any;
    toDate: any;
    data: any;
    tempduplicatepoProductList: any = [];
    poProductList: any = [];
    finalString: any;
    duplicatepoProductList: any = [];
    userToken: any;
    image: any;
    remark: string = '';
    index: number;
    pocols: any[];
    search: any;
    moveCast: boolean;
    machineNo: string;
    OperatorName: string;
    chemCompData: any;
    chemcompForm: FormGroup;
    chemcompForm1: FormGroup;
    dia1: any;
    dia2: any;
    dia3: any;
    dia4: any;
    dia5: any;
    dia6: any;
    dia7: any;
    dia8: any;
    dia9: any;
    dia10: any;
    dia11: any;
    dia12: any;
    dia13: any;
    dia14: any;
    dia15: any;
    dia16: any;
    dia17: any;
    inspection: any;
    history: boolean = false;
    colorMap = {
        "ADAMITS": "red",
        "CHILLS": "blue",
        "SG-IRON": "green",
        "SG-ACC": "#c4c233"
    }
    ngOnInit() {
        const getjwt = localStorage.getItem('jwttoken');
        this.userToken = this.conversionDecrypt(getjwt);
        // this.fetchAllPoProduct(false, false);
        this.empRegisterForms();
        this.testCertificateForm();
        this.pocols = [
            { field: 'idList.displayId', header: 'Roll Id', width: '7%' },
            // { field: 'processtype', header: 'Process Type', width: '8%' },
            // { field: 'grade', header: 'Grade', width: '7%' },
            { field: 'producttype', header: 'Prod. Type', width: '7%' },
            { field: 'itemtype', header: 'Item Type', width: '7%' },
            { field: 'partyData.partyname', header: 'PartyName', width: '8%' },
            // { field: 'uom', header: 'UOM', width: '8%' },
            // { field: 'priceperuom', header: 'Price/UOM', width: '8%' },
            // { field: 'weightperpiece', header: 'Weight/Piece', width: '9%' },
            // { field: 'idList.currentQueue', header: 'Status', width: '7%' },
            { field: 'idList.queueUpdateTimeStamp', header: 'B&S/Fin.Date', width: '6%' },
            { field: 'reqhardness', header: 'Req. Hard', width: '8%' },
            { field: 'dimensionFormString', header: 'Dimension', width: '7%' },
            { field: 'idList.inspectionData.actualHardness', header: 'ActualHardness', width: '7%' },
            // { field: 'idList.gACInspectionData.grooveInspection', header: 'GrooveInspection', width: '7%' },
            // { field: 'idList.mechiningData.mechineNo', header: 'Machnine Num', width: '7%' },
            // { field: 'idList.mechiningData.operatorName', header: 'Operator', width: '7%' },
            // { field: 'idList.inspectionData.actualDimension', header: 'ActualDimension', width: '7%' }
            { field: 'remark', header: 'Remark', width: '7%' },
        ];
    }
    conversionDecrypt(decrypt) {
        try {
            const bytes = CryptoJS.AES.decrypt(decrypt, AppConstants.secret);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return decrypt;
        } catch (e) {
            console.log(e);
        }
    }

    empRegisterForms() {
        this.chemcompForm = this.formBuilder.group({
            partyname: ['', Validators.required],
            clientgrade: ['', Validators.required],
            cpercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            cpercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mnpercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mnpercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            sipercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            sipercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            spercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            spercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            ppercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            ppercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            crpercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            crpercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            nipercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            nipercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mopercentagefrom: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mopercentageto: ['', [Validators.required, Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            remark: ['', Validators.required]

        });
    }

    testCertificateForm() {
        this.chemcompForm1 = this.formBuilder.group({
            partyname: ['', Validators.required],
            clientgrade: ['', Validators.required],
            cpercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mnpercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            sipercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            spercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            ppercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            crpercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            nipercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            mopercentage: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            remark: [''],
            drgNumber: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            quality: [''],
            sizeofRoll: ['', [Validators.pattern(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)]],
            orderHardBarrel: [''],
            actualHardBarrel: [''],
            remarks: [''],
            rollFor: [''],
            itemNo: [''],
            dateOfOrder: [''],
            ourOrderNo: [''],
            dateOfTest: [''],
            dia1: [''],
            dia2: [''],
            dia3: [''],
            dia4: [''],
            dia5: [''],
            dia6: [''],
            dia7: [''],
            dia8: [''],
            dia9: [''],
            dia10: [''],
            dia11: [''],
            dia12: [''],
            dia13: [''],
            dia14: [''],
            dia15: [''],
            dia16: [''],
            dia17: [''],
            dia18: [''],
            long1: [''],
            long2: [''],
            long3: [''],
            long4: [''],
            long5: [''],
            long6: [''],
            long7: [''],
            long8: [''],
            long9: [''],
            long10: [''],
            long11: [''],
            long12: [''],
            long13: [''],
            long14: [''],
            long15: [''],
            long16: [''],
            long17: [''],
            long18: [''],
            act1: [''],
            act2: [''],
            act3: [''],
            act4: [''],
            act5: [''],
            act6: [''],
            act7: [''],
            act8: [''],
            act9: [''],
            act10: [''],
            act11: [''],
            act12: [''],
            act13: [''],
            act14: [''],
            act15: [''],
            act16: [''],
            act17: [''],
            act18: [''],
            lact1: [''],
            lact2: [''],
            lact3: [''],
            lact4: [''],
            lact5: [''],
            lact6: [''],
            lact7: [''],
            lact8: [''],
            lact9: [''],
            lact10: [''],
            lact11: [''],
            lact12: [''],
            lact13: [''],
            lact14: [''],
            lact15: [''],
            lact16: [''],
            lact17: [''],
            lact18: ['']
        });
    }

    fetchChemComp(event) {
        let data = event;
        this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        let ccId = data.chemicalcomp;
        this.partyService.getChemCompByCCId(headers, ccId).subscribe(data => {
            this.chemCompData = data.chemCompData[0];
            // console.log('chemdata' + JSON.stringify(this.chemCompData));
            if (this.chemCompData != undefined) {
                let cper = this.chemCompData.cpercentage;
                // console.log('cper' + this.chemCompData.cpercentage);

                let cperArr = cper.split('-');
                this.chemcompForm.controls['partyname'].setValue(this.chemCompData.partyName);
                this.chemcompForm.controls['clientgrade'].setValue(this.chemCompData.clientGrade);
                this.chemcompForm.controls['remark'].setValue(this.chemCompData.remark);
                this.chemcompForm.controls['cpercentagefrom'].setValue(cperArr[0]);
                this.chemcompForm.controls['cpercentageto'].setValue(cperArr[1]);
                let mnper = this.chemCompData.mnpercentage;
                let mnperArr = mnper.split('-');
                this.chemcompForm.controls['mnpercentagefrom'].setValue(mnperArr[0]);
                this.chemcompForm.controls['mnpercentageto'].setValue(mnperArr[1]);
                let siper = this.chemCompData.sipercentage;
                let siperArr = siper.split('-');
                this.chemcompForm.controls['sipercentagefrom'].setValue(siperArr[0]);
                this.chemcompForm.controls['sipercentageto'].setValue(siperArr[1]);
                let sper = this.chemCompData.spercentage;
                let sperArr = sper.split('-');
                this.chemcompForm.controls['spercentagefrom'].setValue(sperArr[0]);
                this.chemcompForm.controls['spercentageto'].setValue(sperArr[1]);
                let pper = this.chemCompData.ppercentage;
                let pperArr = pper.split('-');
                this.chemcompForm.controls['ppercentagefrom'].setValue(pperArr[0]);
                this.chemcompForm.controls['ppercentageto'].setValue(pperArr[1]);
                let crper = this.chemCompData.crpercentage;
                let crperArr = crper.split('-');
                this.chemcompForm.controls['crpercentagefrom'].setValue(crperArr[0]);
                this.chemcompForm.controls['crpercentageto'].setValue(crperArr[1]);
                let niper = this.chemCompData.nipercentage;
                let niperArr = niper.split('-');
                this.chemcompForm.controls['nipercentagefrom'].setValue(niperArr[0]);
                this.chemcompForm.controls['nipercentageto'].setValue(niperArr[1]);
                let moper = this.chemCompData.mopercentage;
                let moperArr = moper.split('-');
                this.chemcompForm.controls['mopercentagefrom'].setValue(moperArr[0]);
                this.chemcompForm.controls['mopercentageto'].setValue(moperArr[1]);
            }
            this.spinner.hide();
        });
    }

    searchProduct(isAlreadyLoading: boolean, isCheck) {
        let filteredList = [];
        if (!isAlreadyLoading)
            this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        let x = {
            searchTerm: this.basicSearchTerm,
            searchtype: this.searchtype
        };
        if (this.basicSearchTerm == "") {
            this.spinner.hide();
            return;
        }
        this.partyService.searchPOProductByQueueStatusForMasterTable(x, headers).subscribe(data => {
            let list = [];
            this.poProductList = [];
            this.duplicatepoProductList = [];
            this.tempduplicatepoProductList = [];
            list = data.pOProductData;
            list.forEach(element => {
                if (element.idList.isTCDone != true && !this.history && element.idList.currentQueue == '10') {
                    this.poProductList.push(element);
                }
                else if (element.idList.isTCDone == true && this.history && element.idList.currentQueue == '10') {
                    this.poProductList.push(element);
                }
            });
            this.duplicatepoProductList = JSON.parse(JSON.stringify(this.poProductList));
            if (this.search.length == 0) {
                this.listCount = this.duplicatepoProductList.length;
            }
            else {
                filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                this.listCount = filteredList.length;
            }
            this.duplicatepoProductList.forEach(element => {
                if (element.processtype == 0) {
                    element.processtype = 'FORGING';
                }
                else {
                    element.processtype = 'ROLL CASTING';
                }
                if (element.itemtype == 0) {
                    element.itemtype = 'ROLLS';
                }
                else if (element.itemtype == 1) {
                    element.itemtype = 'STEP ROLLS';
                }
                else if (element.itemtype == 2) {
                    element.itemtype = 'REEL';
                }
                else {
                    element.itemtype = 'OTHER';
                }
                if (element.uom == 0) {
                    element.uom = 'Per-KG';
                }
                else if (element.uom == 1) {
                    element.uom = 'MT';
                }
                else {
                    element.uom = 'Per-Piece';
                }
                if (element.idList.currentQueue == '1') {
                    element.idList.currentQueue = 'Planning';
                }
                else if (element.idList.currentQueue == '2') {
                    element.idList.currentQueue = 'Roll Casting';
                }
                let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                element['dimensionFormString'] = toString(element.dimensionForm);
                element.idList.queueUpdateTimeStamp = element.idList.queueUpdateTimeStamp.substring(0, 4) + '-' + element.idList.queueUpdateTimeStamp.substring(5, 7) + '-' + element.idList.queueUpdateTimeStamp.substring(8, 10);
            });
            this.tempduplicatepoProductList = this.duplicatepoProductList;
            if (isCheck == true) {
                this.fromDate = '';
                this.toDate = '';
            }
            if (this.fromDate != '' && this.toDate != '') {
                this.onDateSelection();
            }
            this.spinner.hide();
        })
    }

    fetchAllPoProduct(isAlreadyLoading: boolean, isCheck) {
        let filteredList = [];
        if (!isAlreadyLoading)
            this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.getAllPOProductByQueueStatus(headers, 10).subscribe(data => {
            let list = [];
            this.poProductList = [];
            this.duplicatepoProductList = [];
            this.tempduplicatepoProductList = [];
            list = data.pOProductData;
            list.forEach(element => {
                if (element.idList.isTCDone != true && !this.history) {
                    this.poProductList.push(element);
                }
                else if (element.idList.isTCDone == true && this.history) {
                    this.poProductList.push(element);
                }
            });
            this.duplicatepoProductList = JSON.parse(JSON.stringify(this.poProductList));
            if (this.search.length == 0) {
                this.listCount = this.duplicatepoProductList.length;
            }
            else {
                filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                this.listCount = filteredList.length;
            }
            this.duplicatepoProductList.forEach(element => {
                if (element.processtype == 0) {
                    element.processtype = 'FORGING';
                }
                else {
                    element.processtype = 'ROLL CASTING';
                }
                if (element.itemtype == 0) {
                    element.itemtype = 'ROLLS';
                }
                else if (element.itemtype == 1) {
                    element.itemtype = 'STEP ROLLS';
                }
                else if (element.itemtype == 2) {
                    element.itemtype = 'REEL';
                }
                else {
                    element.itemtype = 'OTHER';
                }
                if (element.uom == 0) {
                    element.uom = 'Per-KG';
                }
                else if (element.uom == 1) {
                    element.uom = 'MT';
                }
                else {
                    element.uom = 'Per-Piece';
                }
                if (element.idList.currentQueue == '1') {
                    element.idList.currentQueue = 'Planning';
                }
                else if (element.idList.currentQueue == '2') {
                    element.idList.currentQueue = 'Roll Casting';
                }
                let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                element['dimensionFormString'] = toString(element.dimensionForm);
                element.idList.queueUpdateTimeStamp = element.idList.queueUpdateTimeStamp.substring(0, 4) + '-' + element.idList.queueUpdateTimeStamp.substring(5, 7) + '-' + element.idList.queueUpdateTimeStamp.substring(8, 10);
            });
            this.tempduplicatepoProductList = this.duplicatepoProductList;
            if (isCheck == true) {
                this.fromDate = '';
                this.toDate = '';
            }
            if (this.fromDate != '' && this.toDate != '') {
                this.onDateSelection();
            }
            this.spinner.hide();



        })
    }

    moveToGrovingBearing(event) {
        // console.log('event' + JSON.stringify(event));
        let data = this.poProductList[event];
        // console.log('data' + JSON.stringify(data));
        let x = {
            poDetailsId: data.poDetailsId,
            productId: data._id,
            displayId: data.idList.displayId,
            queueUpdateTimeStamp: data.idList.queueUpdateTimeStamp,
            upcomingQueue: QueueStatus.FinalReport,
            currentQueue: QueueStatus.TestCertificateFinish,
        };
        this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.updateQueueStatus(x, headers).subscribe(data => {
            // console.log(data);
            this.searchProduct(true, false);
        });
    }

    updateQueueData(event) {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        let data = event.idList;
        let id = event._id;
        let timeStamp = new Date();
        data.isTCDone = true;
        if (event.producttype == "GROOVE" || event.producttype == "CENTER" || event.producttype == "OTHERS") {
            if (data.testCertificateData.cpercentage != "" && data.testCertificateData.mnpercentage != "") {
                let x = {
                    queueUpdateTimeStamp: data.queueUpdateTimeStamp,
                    displayId: data.displayId,
                    currentQueue: data.currentQueue,
                    isRejected: data.isRejected,
                    isDispacthed: data.isDispacthed,
                    isPacked: data.isPacked,
                    qualityCheckData: data.qualityCheckData,
                    isTCDone: data.isTCDone,
                    mechiningData: data.mechiningData,
                    rejection: data.rejection,
                    bAndSTestCertificateData: data.bAndSTestCertificateData,
                    inspectionData: data.inspectionData,
                    gACFinishingData: data.gACFinishingData,
                    bASFinishingData: data.bASFinishingData,
                    gACInspectionData: data.gACInspectionData,
                    bASInspectionData: data.bASInspectionData,
                    testCertificateData: data.testCertificateData,
                    queueDurationList: data.queueDurationList,
                    dispatchedQuantity: -1
                };
                this.spinner.show();
                this.partyService.updateQueueData(x, headers, id).subscribe(data => {
                    this.searchProduct(true, false);
                });
            }
            else {
                this.notifierService.notify('error', 'Fill ActualCertificate!');
            }
        }
        else {
            if (data.bAndSTestCertificateData.ccData.cpercentage != "" && data.bAndSTestCertificateData.ccData.mnpercentage != "") {
                let x = {
                    queueUpdateTimeStamp: data.queueUpdateTimeStamp,
                    displayId: data.displayId,
                    currentQueue: data.currentQueue,
                    isRejected: data.isRejected,
                    isDispacthed: data.isDispacthed,
                    isPacked: data.isPacked,
                    qualityCheckData: data.qualityCheckData,
                    isTCDone: data.isTCDone,
                    mechiningData: data.mechiningData,
                    rejection: data.rejection,
                    bAndSTestCertificateData: data.bAndSTestCertificateData,
                    inspectionData: data.inspectionData,
                    gACFinishingData: data.gACFinishingData,
                    bASFinishingData: data.bASFinishingData,
                    gACInspectionData: data.gACInspectionData,
                    bASInspectionData: data.bASInspectionData,
                    testCertificateData: data.testCertificateData,
                    queueDurationList: data.queueDurationList,
                    dispatchedQuantity: -1
                };
                this.spinner.show();
                this.partyService.updateQueueData(x, headers, id).subscribe(data => {
                    this.searchProduct(true, false);
                });
            }
            else {
                this.notifierService.notify('error', 'Fill ActualCertificate!');
            }
        }
    }


    getProductDetails() {

    }


    previewImage(event) {
        this.image = this.poProductList[event].productimg;
        // console.log('dataaiMage' + JSON.stringify(this.image));

    }

    bearingData(event) {
        this.data = event;
        this.dia1 = this.data.idList.bASInspectionData.actualBearingDia[0];
        this.dia2 = this.data.idList.bASInspectionData.actualBearingDia[1];
        this.dia3 = this.data.idList.bASInspectionData.actualBearingDia[2];
        this.dia4 = this.data.idList.bASInspectionData.actualBearingDia[3];
        this.dia5 = this.data.idList.bASInspectionData.actualBearingDia[4];
        this.dia6 = this.data.idList.bASInspectionData.actualBearingDia[5];
        this.dia7 = this.data.idList.bASInspectionData.actualBearingDia[6];
        this.dia8 = this.data.idList.bASInspectionData.actualBearingDia[7];
        this.dia9 = this.data.idList.bASInspectionData.actualBearingDia[8];
        this.dia10 = this.data.idList.bASInspectionData.actualBearingDia[9];
        this.dia11 = this.data.idList.bASInspectionData.actualBearingDia[10];
        this.dia12 = this.data.idList.bASInspectionData.actualBearingDia[11];
        this.dia13 = this.data.idList.bASInspectionData.actualBearingDia[12];
        this.dia14 = this.data.idList.bASInspectionData.actualBearingDia[13];
        this.dia15 = this.data.idList.bASInspectionData.actualBearingDia[14];
        this.dia16 = this.data.idList.bASInspectionData.actualBearingDia[15];
        this.dia17 = this.data.idList.bASInspectionData.actualBearingDia[16];
        this.inspection = this.data.idList.gACInspectionData.grooveInspection;
        if (this.data.producttype == "GROOVE" || this.data.producttype == "CENTER" || this.data.producttype == "OTHERS") {

            $('#grooveInsp').modal('show');
        }
        else {
            $('#bearingData').modal('show');
        }

    }

    castingHistory() {
        this.history = !this.history;
        // console.log('boolean' + this.history);
        // this.fetchAllPoProduct(false, false);
        this.listCount = 0;
        this.duplicatepoProductList = [];
        this.basicSearchTerm = "";
        this.searchtype = "0"
    }

    updateChemComp(event) {
        // console.log('event' + JSON.stringify(event));

        this.data = event;
        this.chemcompForm1.reset();
        this.chemcompForm.reset();
        let data = this.data.idList.testCertificateData;
        if (this.data.producttype == "GROOVE" || this.data.producttype == "CENTER" || this.data.producttype == "OTHERS") {
            // console.log('hello');

            $('#exampleModalCenter1').modal('show');
        }
        else {
            // console.log('BYE' + JSON.stringify(this.poProductList[index].idList.bAndSTestCertificateData));
            let dateoforder = this.data.partyData.ordercnfdate.substring(0, 4) + '-' + this.data.partyData.ordercnfdate.substring(5, 7) + '-' + this.data.partyData.ordercnfdate.substring(8, 10);
            this.chemcompForm1.controls['rollFor'].setValue(this.data.partyData.partyname);
            this.chemcompForm1.controls['itemNo'].setValue(this.data.idList.displayId);
            this.chemcompForm1.controls['dateOfOrder'].setValue(dateoforder);
            this.chemcompForm1.controls['ourOrderNo'].setValue(this.data.partyData.partyid);

            if (this.data && this.data.idList && this.data.idList.bAndSTestCertificateData && this.data.idList.bAndSTestCertificateData.ccData) {
                this.chemcompForm1.controls['cpercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.cpercentage);
                this.chemcompForm1.controls['spercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.spercentage);
                this.chemcompForm1.controls['sipercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.sipercentage);
                this.chemcompForm1.controls['remark'].setValue(this.data.idList.bAndSTestCertificateData.ccData.remark);
                this.chemcompForm1.controls['ppercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.ppercentage);
                this.chemcompForm1.controls['nipercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.nipercentage);
                this.chemcompForm1.controls['mopercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.mopercentage);
                this.chemcompForm1.controls['mnpercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.mnpercentage);
                this.chemcompForm1.controls['crpercentage'].setValue(this.data.idList.bAndSTestCertificateData.ccData.crpercentage);
            }

            if (this.data.idList.bAndSTestCertificateData && this.data.idList.bAndSTestCertificateData.diaDrg.length > 0) {
                this.chemcompForm1.controls['drgNumber'].setValue(this.data.idList.bAndSTestCertificateData.drgNumber);
                this.chemcompForm1.controls['dateOfTest'].setValue(this.data.idList.bAndSTestCertificateData.dateOfTest);
                this.chemcompForm1.controls['quality'].setValue(this.data.idList.bAndSTestCertificateData.quality);
                this.chemcompForm1.controls['sizeofRoll'].setValue(this.data.idList.bAndSTestCertificateData.sizeofRoll);
                this.chemcompForm1.controls['orderHardBarrel'].setValue(this.data.idList.bAndSTestCertificateData.orderHardBarrel);
                this.chemcompForm1.controls['actualHardBarrel'].setValue(this.data.idList.bAndSTestCertificateData.actualHardBarrel);
                this.chemcompForm1.controls['remarks'].setValue(this.data.idList.bAndSTestCertificateData.remarks);
                this.chemcompForm1.controls['dia1'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[0]);
                this.chemcompForm1.controls['dia2'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[1]);
                this.chemcompForm1.controls['dia3'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[2]);
                this.chemcompForm1.controls['dia4'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[3]);
                this.chemcompForm1.controls['dia5'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[4]);
                this.chemcompForm1.controls['dia6'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[5]);
                this.chemcompForm1.controls['dia7'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[6]);
                this.chemcompForm1.controls['dia8'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[7]);
                this.chemcompForm1.controls['dia9'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[8]);
                this.chemcompForm1.controls['dia10'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[9]);
                this.chemcompForm1.controls['dia11'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[10]);
                this.chemcompForm1.controls['dia12'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[11]);
                this.chemcompForm1.controls['dia13'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[12]);
                this.chemcompForm1.controls['dia14'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[13]);
                this.chemcompForm1.controls['dia15'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[14]);
                this.chemcompForm1.controls['dia16'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[15]);
                this.chemcompForm1.controls['dia17'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[16]);
                this.chemcompForm1.controls['dia18'].setValue(this.data.idList.bAndSTestCertificateData.diaDrg[17]);
            }
            if (this.data.idList.bAndSTestCertificateData && this.data.idList.bAndSTestCertificateData.longDrg.length > 0) {
                this.chemcompForm1.controls['long1'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[0]);
                this.chemcompForm1.controls['long2'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[1]);
                this.chemcompForm1.controls['long3'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[2]);
                this.chemcompForm1.controls['long4'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[3]);
                this.chemcompForm1.controls['long5'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[4]);
                this.chemcompForm1.controls['long6'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[5]);
                this.chemcompForm1.controls['long7'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[6]);
                this.chemcompForm1.controls['long8'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[7]);
                this.chemcompForm1.controls['long9'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[8]);
                this.chemcompForm1.controls['long10'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[9]);
                this.chemcompForm1.controls['long11'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[10]);
                this.chemcompForm1.controls['long12'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[11]);
                this.chemcompForm1.controls['long13'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[12]);
                this.chemcompForm1.controls['long14'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[13]);
                this.chemcompForm1.controls['long15'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[14]);
                this.chemcompForm1.controls['long16'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[15]);
                this.chemcompForm1.controls['long17'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[16]);
                this.chemcompForm1.controls['long18'].setValue(this.data.idList.bAndSTestCertificateData.longDrg[17]);
            }
            if (this.data.idList.bAndSTestCertificateData && this.data.idList.bAndSTestCertificateData.diaActual.length > 0) {
                this.chemcompForm1.controls['act1'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[0]);
                this.chemcompForm1.controls['act2'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[1]);
                this.chemcompForm1.controls['act3'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[2]);
                this.chemcompForm1.controls['act4'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[3]);
                this.chemcompForm1.controls['act5'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[4]);
                this.chemcompForm1.controls['act6'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[5]);
                this.chemcompForm1.controls['act7'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[6]);
                this.chemcompForm1.controls['act8'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[7]);
                this.chemcompForm1.controls['act9'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[8]);
                this.chemcompForm1.controls['act10'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[9]);
                this.chemcompForm1.controls['act11'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[10]);
                this.chemcompForm1.controls['act12'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[11]);
                this.chemcompForm1.controls['act13'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[12]);
                this.chemcompForm1.controls['act14'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[13]);
                this.chemcompForm1.controls['act15'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[14]);
                this.chemcompForm1.controls['act16'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[15]);
                this.chemcompForm1.controls['act17'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[16]);
                this.chemcompForm1.controls['act18'].setValue(this.data.idList.bAndSTestCertificateData.diaActual[17]);
            }
            if (this.data.idList.bAndSTestCertificateData && this.data.idList.bAndSTestCertificateData.longActual.length > 0) {
                this.chemcompForm1.controls['lact1'].setValue(this.data.idList.bAndSTestCertificateData.longActual[0]);
                this.chemcompForm1.controls['lact2'].setValue(this.data.idList.bAndSTestCertificateData.longActual[1]);
                this.chemcompForm1.controls['lact3'].setValue(this.data.idList.bAndSTestCertificateData.longActual[2]);
                this.chemcompForm1.controls['lact4'].setValue(this.data.idList.bAndSTestCertificateData.longActual[3]);
                this.chemcompForm1.controls['lact5'].setValue(this.data.idList.bAndSTestCertificateData.longActual[4]);
                this.chemcompForm1.controls['lact6'].setValue(this.data.idList.bAndSTestCertificateData.longActual[5]);
                this.chemcompForm1.controls['lact7'].setValue(this.data.idList.bAndSTestCertificateData.longActual[6]);
                this.chemcompForm1.controls['lact8'].setValue(this.data.idList.bAndSTestCertificateData.longActual[7]);
                this.chemcompForm1.controls['lact9'].setValue(this.data.idList.bAndSTestCertificateData.longActual[8]);
                this.chemcompForm1.controls['lact10'].setValue(this.data.idList.bAndSTestCertificateData.longActual[9]);
                this.chemcompForm1.controls['lact11'].setValue(this.data.idList.bAndSTestCertificateData.longActual[10]);
                this.chemcompForm1.controls['lact12'].setValue(this.data.idList.bAndSTestCertificateData.longActual[11]);
                this.chemcompForm1.controls['lact13'].setValue(this.data.idList.bAndSTestCertificateData.longActual[12]);
                this.chemcompForm1.controls['lact14'].setValue(this.data.idList.bAndSTestCertificateData.longActual[13]);
                this.chemcompForm1.controls['lact15'].setValue(this.data.idList.bAndSTestCertificateData.longActual[14]);
                this.chemcompForm1.controls['lact16'].setValue(this.data.idList.bAndSTestCertificateData.longActual[15]);
                this.chemcompForm1.controls['lact17'].setValue(this.data.idList.bAndSTestCertificateData.longActual[16]);
                this.chemcompForm1.controls['lact18'].setValue(this.data.idList.bAndSTestCertificateData.longActual[17]);
            }
            $('#exampleModalCenter3').modal('show');
        }
        let cper = data.cpercentage;
        let cperArr = cper.split('-');
        // this.chemcompForm.controls['partyname'].setValue(data.partyName);
        // this.chemcompForm.controls['clientgrade'].setValue(this.chemCompData.clientGrade);
        this.chemcompForm.controls['remark'].setValue(data.remark);
        this.chemcompForm.controls['cpercentagefrom'].setValue(cperArr[0]);
        this.chemcompForm.controls['cpercentageto'].setValue(cperArr[1]);
        let mnper = data.mnpercentage;
        let mnperArr = mnper.split('-');
        this.chemcompForm.controls['mnpercentagefrom'].setValue(mnperArr[0]);
        this.chemcompForm.controls['mnpercentageto'].setValue(mnperArr[1]);
        let siper = data.sipercentage;
        let siperArr = siper.split('-');
        this.chemcompForm.controls['sipercentagefrom'].setValue(siperArr[0]);
        this.chemcompForm.controls['sipercentageto'].setValue(siperArr[1]);
        let sper = data.spercentage;
        let sperArr = sper.split('-');
        this.chemcompForm.controls['spercentagefrom'].setValue(sperArr[0]);
        this.chemcompForm.controls['spercentageto'].setValue(sperArr[1]);
        let pper = data.ppercentage;
        let pperArr = pper.split('-');
        this.chemcompForm.controls['ppercentagefrom'].setValue(pperArr[0]);
        this.chemcompForm.controls['ppercentageto'].setValue(pperArr[1]);
        let crper = data.crpercentage;
        let crperArr = crper.split('-');
        this.chemcompForm.controls['crpercentagefrom'].setValue(crperArr[0]);
        this.chemcompForm.controls['crpercentageto'].setValue(crperArr[1]);
        let niper = data.nipercentage;
        let niperArr = niper.split('-');
        this.chemcompForm.controls['nipercentagefrom'].setValue(niperArr[0]);
        this.chemcompForm.controls['nipercentageto'].setValue(niperArr[1]);
        let moper = data.mopercentage;
        let moperArr = moper.split('-');
        this.chemcompForm.controls['mopercentagefrom'].setValue(moperArr[0]);
        this.chemcompForm.controls['mopercentageto'].setValue(moperArr[1]);
    }

    groovingCertificate() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        let data = this.data.idList;
        let id = this.data._id;
        let timeStamp = new Date();
        if (this.chemcompForm.get('cpercentagefrom').value != "" && this.chemcompForm.get('cpercentageto').value != "") {
            data.testCertificateData.cpercentage = this.covertnumberToString(this.chemcompForm.get('cpercentagefrom').value, this.chemcompForm.get('cpercentageto').value);
        }
        if (this.chemcompForm.get('mnpercentagefrom').value != "" && this.chemcompForm.get('mnpercentageto').value != "") {
            data.testCertificateData.mnpercentage = this.covertnumberToString(this.chemcompForm.get('mnpercentagefrom').value, this.chemcompForm.get('mnpercentageto').value);
        }
        if (this.chemcompForm.get('sipercentagefrom').value != "" && this.chemcompForm.get('sipercentageto').value != "") {
            data.testCertificateData.sipercentage = this.covertnumberToString(this.chemcompForm.get('sipercentagefrom').value, this.chemcompForm.get('sipercentageto').value);
        }
        if (this.chemcompForm.get('spercentagefrom').value != "" && this.chemcompForm.get('spercentageto').value != "") {
            data.testCertificateData.spercentage = this.covertnumberToString(this.chemcompForm.get('spercentagefrom').value, this.chemcompForm.get('spercentageto').value);
        }
        if (this.chemcompForm.get('ppercentagefrom').value != "" && this.chemcompForm.get('ppercentageto').value != "") {
            data.testCertificateData.ppercentage = this.covertnumberToString(this.chemcompForm.get('ppercentagefrom').value, this.chemcompForm.get('ppercentageto').value);
        }
        if (this.chemcompForm.get('crpercentagefrom').value != "" && this.chemcompForm.get('crpercentageto').value != "") {
            data.testCertificateData.crpercentage = this.covertnumberToString(this.chemcompForm.get('crpercentagefrom').value, this.chemcompForm.get('crpercentageto').value);
        }
        if (this.chemcompForm.get('nipercentagefrom').value != "" && this.chemcompForm.get('nipercentageto').value != "") {
            data.testCertificateData.nipercentage = this.covertnumberToString(this.chemcompForm.get('nipercentagefrom').value, this.chemcompForm.get('nipercentageto').value);
        }
        if (this.chemcompForm.get('mopercentagefrom').value != "" && this.chemcompForm.get('mopercentageto').value != "") {
            data.testCertificateData.mopercentage = this.covertnumberToString(this.chemcompForm.get('mopercentagefrom').value, this.chemcompForm.get('mopercentageto').value);
        }
        if (this.chemcompForm.get('remark').value != "") {
            data.testCertificateData.remark = this.chemcompForm.get('remark').value;
        }
        let x = {
            queueUpdateTimeStamp: data.queueUpdateTimeStamp,
            displayId: data.displayId,
            currentQueue: data.currentQueue,
            isRejected: data.isRejected,
            qualityCheckData: data.qualityCheckData,
            isDispacthed: data.isDispacthed,
            isPacked: data.isPacked,
            isTCDone: data.isTCDone,
            bAndSTestCertificateData: data.bAndSTestCertificateData,
            mechiningData: data.mechiningData,
            rejection: data.rejection,
            inspectionData: data.inspectionData,
            gACFinishingData: data.gACFinishingData,
            bASFinishingData: data.bASFinishingData,
            gACInspectionData: data.gACInspectionData,
            bASInspectionData: data.bASInspectionData,
            testCertificateData: data.testCertificateData,
            queueDurationList: data.queueDurationList,
            dispatchedQuantity: -1
        };
        // console.log('x data' + JSON.stringify(x));

        this.spinner.show();
        this.partyService.updateQueueData(x, headers, id).subscribe(data => {
            // console.log(data);
            $('#exampleModalCenter1').modal('hide');
            this.searchProduct(true, false);
        });
    }

    covertnumberToString(a, b) {
        let string1: string;
        let string2: string;
        string1 = a.toString();
        string2 = b.toString();
        this.finalString = string1 + "-" + string2;
        return this.finalString;
    }

    bearingCertificate() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        let data = this.data.idList;
        let id = this.data._id;
        // console.log('final data' + JSON.stringify(data));
        let timeStamp = new Date();
        // console.log('date' + JSON.stringify(timeStamp));
        data.bAndSTestCertificateData.dateOfTest = this.chemcompForm1.controls['dateOfTest'].value;
        data.bAndSTestCertificateData.drgNumber = this.chemcompForm1.controls['drgNumber'].value;
        data.bAndSTestCertificateData.quality = this.chemcompForm1.controls['quality'].value;
        data.bAndSTestCertificateData.sizeofRoll = this.chemcompForm1.controls['sizeofRoll'].value;
        data.bAndSTestCertificateData.orderHardBarrel = this.chemcompForm1.controls['orderHardBarrel'].value;
        data.bAndSTestCertificateData.actualHardBarrel = this.chemcompForm1.controls['actualHardBarrel'].value;
        data.bAndSTestCertificateData.remarks = this.chemcompForm1.controls['remarks'].value;
        data.bAndSTestCertificateData.ccData.cpercentage = this.chemcompForm1.controls['cpercentage'].value;
        data.bAndSTestCertificateData.ccData.crpercentage = this.chemcompForm1.controls['crpercentage'].value;
        data.bAndSTestCertificateData.ccData.mnpercentage = this.chemcompForm1.controls['mnpercentage'].value;
        data.bAndSTestCertificateData.ccData.mopercentage = this.chemcompForm1.controls['mopercentage'].value;
        data.bAndSTestCertificateData.ccData.nipercentage = this.chemcompForm1.controls['nipercentage'].value;
        data.bAndSTestCertificateData.ccData.remark = this.chemcompForm1.controls['remark'].value;
        data.bAndSTestCertificateData.ccData.sipercentage = this.chemcompForm1.controls['sipercentage'].value;
        data.bAndSTestCertificateData.ccData.spercentage = this.chemcompForm1.controls['spercentage'].value;
        data.bAndSTestCertificateData.ccData.ppercentage = this.chemcompForm1.controls['ppercentage'].value;
        data.bAndSTestCertificateData.diaDrg = [];
        data.bAndSTestCertificateData.longDrg = [];
        data.bAndSTestCertificateData.diaActual = [];
        data.bAndSTestCertificateData.longActual = [];
        if (this.chemcompForm1.controls['dia1'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia1'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia2'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia2'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia3'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia3'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia4'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia4'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia5'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia5'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia6'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia6'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia7'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia7'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia8'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia8'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia9'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia9'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia10'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia10'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia11'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia11'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia12'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia12'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia13'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia13'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia14'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia14'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia15'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia15'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia16'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia16'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia17'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia17'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }
        if (this.chemcompForm1.controls['dia18'].value != '') {
            data.bAndSTestCertificateData.diaDrg.push(this.chemcompForm1.controls['dia18'].value);
        }
        else {
            data.bAndSTestCertificateData.diaDrg.push(0);
        }

        if (this.chemcompForm1.controls['long1'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long1'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long2'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long2'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long3'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long3'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long4'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long4'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long5'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long5'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long6'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long6'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long7'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long7'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long8'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long8'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long9'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long9'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long10'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long10'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long11'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long11'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long12'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long12'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long13'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long13'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long14'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long14'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long15'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long15'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long16'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long16'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long17'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long17'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }
        if (this.chemcompForm1.controls['long18'].value != '') {
            data.bAndSTestCertificateData.longDrg.push(this.chemcompForm1.controls['long18'].value);
        }
        else {
            data.bAndSTestCertificateData.longDrg.push(0);
        }

        if (this.chemcompForm1.controls['act1'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act1'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act2'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act2'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act3'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act3'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act4'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act4'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act5'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act5'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act6'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act6'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act7'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act7'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act8'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act8'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act9'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act9'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act10'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act10'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act11'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act11'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act12'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act12'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act13'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act13'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act14'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act14'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act15'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act15'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act16'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act16'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act17'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act17'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }
        if (this.chemcompForm1.controls['act18'].value != '') {
            data.bAndSTestCertificateData.diaActual.push(this.chemcompForm1.controls['act18'].value);
        }
        else {
            data.bAndSTestCertificateData.diaActual.push(0);
        }

        if (this.chemcompForm1.controls['lact1'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact1'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact2'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact2'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact3'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact3'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact4'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact4'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact5'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact5'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact6'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact6'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact7'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact7'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact8'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact8'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact9'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact9'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact10'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact10'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact11'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact11'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact12'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact12'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact13'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact13'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact14'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact14'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact15'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact15'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact16'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact16'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact17'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact17'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }
        if (this.chemcompForm1.controls['lact18'].value != '') {
            data.bAndSTestCertificateData.longActual.push(this.chemcompForm1.controls['lact18'].value);
        }
        else {
            data.bAndSTestCertificateData.longActual.push(0);
        }

        let x = {
            queueUpdateTimeStamp: data.queueUpdateTimeStamp,
            displayId: data.displayId,
            currentQueue: data.currentQueue,
            isRejected: data.isRejected,
            qualityCheckData: data.qualityCheckData,
            isDispacthed: data.isDispacthed,
            isPacked: data.isPacked,
            isTCDone: data.isTCDone,
            bAndSTestCertificateData: data.bAndSTestCertificateData,
            mechiningData: data.mechiningData,
            rejection: data.rejection,
            inspectionData: data.inspectionData,
            gACFinishingData: data.gACFinishingData,
            bASFinishingData: data.bASFinishingData,
            gACInspectionData: data.gACInspectionData,
            bASInspectionData: data.bASInspectionData,
            testCertificateData: data.testCertificateData,
            queueDurationList: data.queueDurationList,
            dispatchedQuantity: -1
        };
        // console.log('x data' + JSON.stringify(x));

        this.spinner.show();
        this.partyService.updateQueueData(x, headers, id).subscribe(data => {
            // console.log(data);
            $('#exampleModalCenter3').modal('hide');
            this.searchProduct(true, false);
        });
    }

    onDateSelection() {
        let fromdateutc: any;
        let todateutc: any;
        if (this.fromDate != '' && this.toDate != '') {
            if (JSON.stringify(this.fromDate) == JSON.stringify(this.toDate)) {
                this.toDate.day = this.toDate.day + 1;
            }
        }
        this.duplicatepoProductList = this.tempduplicatepoProductList;
        fromdateutc = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
        fromdateutc = fromdateutc.getTime();
        todateutc = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
        todateutc = todateutc.getTime();
        let duplicatepoProductList = [];
        duplicatepoProductList = this.duplicatepoProductList;
        this.duplicatepoProductList = [];
        duplicatepoProductList.forEach(element => {
            element.filterDate = new Date(element.idList.queueUpdateTimeStamp).getTime();
        });
        duplicatepoProductList.forEach(element => {
            if (element.filterDate >= fromdateutc && element.filterDate <= todateutc) {
                this.duplicatepoProductList.push(element);
            }
        });
        this.listCount = this.duplicatepoProductList.length;
    }

    onKeyup() {
        let filteredList = [];
        if (this.search.length == 0) {
            this.listCount = this.duplicatepoProductList.length;
        }
        else {
            // console.log('dataata' + JSON.stringify(this.duplicatepoProductList));
            filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
            this.listCount = filteredList.length;
            // console.log('count'+this.listCount);

        }
    }

    convertPoDetailsExcel() {
        let value = [];

        this.filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
        // console.log('dataata' + JSON.stringify(this.filteredList));

        this.filteredList.forEach(fuel => {
            // console.log('fuel' + JSON.stringify(fuel));
            let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
            fuel.dimensionForm = toString(fuel.dimensionForm);
            let subValueArray = [];
            subValueArray.push(fuel.idList.displayId);
            subValueArray.push(fuel.producttype);
            subValueArray.push(fuel.itemtype);
            subValueArray.push(fuel.partyData.partyname);
            subValueArray.push(fuel.idList.queueUpdateTimeStamp);
            subValueArray.push(fuel.reqhardness);
            subValueArray.push(fuel.dimensionForm);
            subValueArray.push(fuel.idList.inspectionData.actualHardness);
            subValueArray.push(fuel.grade);
            subValueArray.push(fuel.productimg);
            value.push(subValueArray);
        });

        // let headerName = "Fuel Summary Report (" + this.fromDate.formatted + " to " + this.toDate.formatted + ")";
        let headerName = "TestCertificate Report";
        let body = {
            "headerName": headerName,
            "excelName": "TestCertificate",
            "header": [
                "ROLLID",
                "PRODUCTTYPE",
                "ITEMTYPE",
                "PARTYNAME",
                "B&SFINA.DATE",
                "REQ.HARDNESS",
                "DIMENSION",
                "ACTUALHARDNESS",
                "GRADE",
                "PROD.IMG"
            ],
            "value": value
        }
        this.downloadExcelSheet(body).subscribe(blob => {
            // Doing it this way allows you to name the file
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = "TestCertificate.xlsx";
            link.click();
        }, error => console.log("Error downloading the file."),
            () => console.log('Completed file download.'));
    }

    downloadExcelSheet(reqBody): Observable<Object[]> {
        //this.loading = true;
        return Observable.create(observer => {
            let xhr = new XMLHttpRequest();
            xhr.open('POST', AppConstants.EXCEL_DOWNLOAD_URL, true);
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.responseType = 'blob';

            xhr.onreadystatechange = () => {
                //this.loading = false;
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {

                        var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        var blob = new Blob([xhr.response], { type: contentType });
                        observer.next(blob);
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            xhr.send(JSON.stringify(reqBody));

        });
    }

    generatePdf() {
        console.log("this.data.idList ===", this.data.idList);

        let reqBody = this.data.idList.bAndSTestCertificateData;
        if (this.data.idList && this.data.idList.bAndSTestCertificateData) {
            reqBody.dateOfTest = this.data.idList.bAndSTestCertificateData.dateOfTest.year + '-' +
                this.data.idList.bAndSTestCertificateData.dateOfTest.month + '-' +
                this.data.idList.bAndSTestCertificateData.dateOfTest.day;
        }
        reqBody.rollFor = this.data.partyData.partyname;
        reqBody.itemNumber = this.data.idList.displayId;
        reqBody.dateOfOrder = this.data.partyData.ordercnfdate.substring(0, 4) + '-' +
            this.data.partyData.ordercnfdate.substring(5, 7) + '-' +
            this.data.partyData.ordercnfdate.substring(8, 10);
        reqBody.ourOrderNumber = this.data.partyData.partyid;
        this.getPDF(reqBody);
    }

    getPDF(reqBody) {
        this.getPdfObservable(reqBody).subscribe(blob => {
            // Doing it this way allows you to name the file
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = "Report.pdf";
            link.click();
        }, error => console.log("Error downloading the file."),
            () => console.log('Completed file download.'));
    }

    getPdfObservable(reqBody): Observable<Object[]> {
        return Observable.create(observer => {
            let xhr = new XMLHttpRequest();
            xhr.open('POST', AppConstants.post_Web_URl + 'basicNode/services/generateBAndSPdf', true);
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.responseType = 'blob';

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {

                        var contentType = 'application/pdf';
                        var blob = new Blob([xhr.response], { type: contentType });
                        observer.next(blob);
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            xhr.send(JSON.stringify(reqBody));

        });
    }
}