import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { POComponent } from './po.component';
import { PoCreationComponent } from './po-creation/po-creation.component';
import { PartyCreationComponent } from './party-creation/party-creation.component';
import { CCCreationComponent } from './cc-creation/cc-creation.component';
import { RollPlannerComponent } from './roll-planner/roll-planner.component';
import { RollCastingComponent } from './roll-casting/roll-casting.component';
import { QualityCheckComponent } from './quality-check/quality-check.component';
import { MachiningEditorComponent } from './machining-editor/machining-editor.component';
import { InspectionEditorComponent } from './inspection-editor/inspection-editor.component';
import { GrooveCentreComponent } from './groove-centre/groove-centre.component';
import { BearingShapperComponent } from './bearing-shapper/bearing-shapper.component';
import { GrooveCentreFinalComponent } from './groove-centre-final/groove-centre-final.component';
import { BearingShapperFinalComponent } from './bearing-shapper-final/bearing-shapper-final.component';
import { TransportDispatchComponent } from './transport-dispatch/transport-dispatch.component';
import { TestCertificateComponent } from './test-certificate/test-certificate.component';
import { PackagingComponent } from './packaging/packaging.component';
import { RejectionComponent } from './rejection/rejection.component';
import { MasterTableComponent } from './master-table/master-table.component';
import { HoldRollsComponent } from './holdRolls/holdRolls.component';
const routes: Routes = [
  {
    path: '', component: POComponent,
    children: [
      {
        path: 'Add PO',
        component: PoCreationComponent
      },
      {
        path: 'Add Party',
        component: PartyCreationComponent
      },
      {
        path: 'Add CC',
        component: CCCreationComponent
      },
      {
        path: 'Roll Planner',
        component: RollPlannerComponent
      },
      {
        path: 'Roll Casting',
        component: RollCastingComponent
      },
      {
        path: 'Quality Check',
        component: QualityCheckComponent
      },
      {
        path: 'Machining Editor',
        component: MachiningEditorComponent
      },
      {
        path: 'Inspection Editor',
        component: InspectionEditorComponent
      },
      {
        path: 'Groove&CentreFinishing',
        component: GrooveCentreComponent
      },
      {
        path: 'Bearing&ShapperFinishing',
        component: BearingShapperComponent
      },
      {
        path: 'Groove&CentreFinal',
        component: GrooveCentreFinalComponent
      },
      {
        path: 'Bearing&ShapperFinal',
        component: BearingShapperFinalComponent
      },
      {
        path: 'Transport&Dispatch',
        component: TransportDispatchComponent
      },
      {
        path: 'TestCertificate',
        component: TestCertificateComponent
      },
      {
        path: 'Packaging',
        component: PackagingComponent
      },
      {
        path: 'Rejection',
        component: RejectionComponent
      },
      {
        path: 'HoldRolls',
        component: HoldRollsComponent
      },
      {
        path: 'MasterTable',
        component: MasterTableComponent
      },
      {
        path: '',
        redirectTo: 'PO Creation'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class POPlannerRoutingModule { }
