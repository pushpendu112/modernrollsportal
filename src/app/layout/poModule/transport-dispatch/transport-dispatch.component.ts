import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { PartyService } from 'src/app/shared/services/party.service';
import * as CryptoJS from 'crypto-js';
import { AppConstants } from '../../../base/appconstants';
import { NgxSpinnerService } from 'ngx-spinner';
import { QueueStatus } from '../../../models/queueStatus';
import { Ng2SearchPipe } from 'ng2-search-filter';
import { Observable } from 'rxjs';
@Component({
    selector: 'app-transport-dispatch',
    templateUrl: './transport-dispatch.component.html',
    styleUrls: ['./transport-dispatch.component.scss'],
    // encapsulation: ViewEncapsulation.None
})
export class TransportDispatchComponent implements OnInit {

    searchtype: any = "0";
    basicSearchTerm: any = "";

    constructor(private partyService: PartyService, private spinner: NgxSpinnerService) {
        this.filter = new Ng2SearchPipe();
        this.search = '';
        this.fromDate = '';
        this.toDate = '';
    }
    filter: Ng2SearchPipe;
    filteredList: any = [];
    listCount: any;
    fromDate: any;
    toDate: any;
    tempduplicatepoProductList: any = [];
    poProductList: any = [];
    duplicatepoProductList: any = [];
    userToken: any;
    image: any;
    remark: string = '';
    index: number;
    pocols: any[];
    search: any;
    moveCast: boolean;
    machineNo: string;
    OperatorName: string;
    history: boolean = false;
    colorMap = {
        "ADAMITS": "red",
        "CHILLS": "blue",
        "SG-IRON": "green",
        "SG-ACC": "#c4c233"
    }
    ngOnInit() {
        const getjwt = localStorage.getItem('jwttoken');
        this.userToken = this.conversionDecrypt(getjwt);
        // this.fetchAllPoProduct(false, false);
        this.pocols = [
            { field: 'idList.displayId', header: 'Roll Id', width: '7%' },
            // { field: 'processtype', header: 'Process Type', width: '8%' },
            // { field: 'grade', header: 'Grade', width: '7%' },
            { field: 'producttype', header: 'Prod. Type', width: '7%' },
            { field: 'itemtype', header: 'Item Type', width: '7%' },
            { field: 'partyData.partyname', header: 'PartyName', width: '8%' },
            { field: 'partyData.partyid', header: 'Partyid', width: '8%' },
            // { field: 'uom', header: 'UOM', width: '8%' },
            // { field: 'priceperuom', header: 'Price/UOM', width: '8%' },
            { field: 'weightperpiece', header: 'Weight/Piece', width: '9%' },
            // { field: 'idList.currentQueue', header: 'Status', width: '7%' },
            // { field: 'idList.queueUpdateTimeStamp', header: 'B&S/Fin.Date', width: '6%' },
            // { field: 'reqhardness', header: 'Req. Hard', width: '8%' },
            { field: 'dimensionFormString', header: 'Dimension', width: '7%' },
            // { field: 'idList.mechiningData.mechineNo', header: 'Machnine Num', width: '7%' },
            // { field: 'idList.mechiningData.operatorName', header: 'Operator', width: '7%' }
            { field: 'remark', header: 'Remark', width: '7%' },
        ];
    }
    conversionDecrypt(decrypt) {
        try {
            const bytes = CryptoJS.AES.decrypt(decrypt, AppConstants.secret);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return decrypt;
        } catch (e) {
            console.log(e);
        }
    }

    searchProduct(isAlreadyLoading: boolean, isCheck) {
        let filteredList = [];
        if (!isAlreadyLoading)
            this.spinner.show();
        // let id;
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        let x = {
            searchTerm: this.basicSearchTerm,
            searchtype: this.searchtype
        };
        if (this.basicSearchTerm == "") {
            this.spinner.hide();
            return;
        }
        this.partyService.searchPOProductByQueueStatusForMasterTable(x, headers).subscribe(data => {
            // this.poProductList = data.pOProductData;
            let list = [];
            this.duplicatepoProductList = [];
            this.tempduplicatepoProductList = [];
            this.poProductList = [];
            list = data.pOProductData;
            list.forEach(element => {
                if (element.idList.isDispacthed != true && !this.history && element.idList.currentQueue == '10') {
                    this.poProductList.push(element);
                }
                else if (element.idList.isDispacthed == true && this.history && element.idList.currentQueue == '10') {
                    this.poProductList.push(element);
                }
            });
            this.duplicatepoProductList = JSON.parse(JSON.stringify(this.poProductList));
            if (this.search.length == 0) {
                this.listCount = this.duplicatepoProductList.length;
            }
            else {
                filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                this.listCount = filteredList.length;
            }
            this.duplicatepoProductList.forEach(element => {
                if (element.processtype == 0) {
                    element.processtype = 'FORGING';
                }
                else {
                    element.processtype = 'ROLL CASTING';
                }
                if (element.itemtype == 0) {
                    element.itemtype = 'ROLLS';
                }
                else if (element.itemtype == 1) {
                    element.itemtype = 'STEP ROLLS';
                }
                else if (element.itemtype == 2) {
                    element.itemtype = 'REEL';
                }
                else {
                    element.itemtype = 'OTHER';
                }
                if (element.uom == 0) {
                    element.uom = 'Per-KG';
                }
                else if (element.uom == 1) {
                    element.uom = 'MT';
                }
                else {
                    element.uom = 'Per-Piece';
                }
                if (element.idList.currentQueue == '1') {
                    element.idList.currentQueue = 'Planning';
                }
                else if (element.idList.currentQueue == '2') {
                    element.idList.currentQueue = 'Roll Casting';
                }
                let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                element['dimensionFormString'] = toString(element.dimensionForm);
                element.idList.queueUpdateTimeStamp = element.idList.queueUpdateTimeStamp.substring(0, 4) + '-' + element.idList.queueUpdateTimeStamp.substring(5, 7) + '-' + element.idList.queueUpdateTimeStamp.substring(8, 10);
            });
            this.tempduplicatepoProductList = this.duplicatepoProductList;
            if (isCheck == true) {
                this.fromDate = '';
                this.toDate = '';
            }
            if (this.fromDate != '' && this.toDate != '') {
                this.onDateSelection();
            }
            // console.log('here' + JSON.stringify(this.duplicatepoProductList));
            this.spinner.hide();
        })
    }

    fetchAllPoProduct(isAlreadyLoading: boolean, isCheck) {
        let filteredList = [];
        if (!isAlreadyLoading)
            this.spinner.show();
        // let id;
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        this.partyService.getAllPOProductByQueueStatus(headers, 10).subscribe(data => {
            // this.poProductList = data.pOProductData;
            let list = [];
            this.duplicatepoProductList = [];
            this.tempduplicatepoProductList = [];
            this.poProductList = [];
            list = data.pOProductData;
            list.forEach(element => {
                if (element.idList.isDispacthed != true && !this.history) {
                    this.poProductList.push(element);
                }
                else if (element.idList.isDispacthed == true && this.history) {
                    this.poProductList.push(element);
                }
            });
            this.duplicatepoProductList = JSON.parse(JSON.stringify(this.poProductList));
            if (this.search.length == 0) {
                this.listCount = this.duplicatepoProductList.length;
            }
            else {
                filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
                this.listCount = filteredList.length;
            }
            this.duplicatepoProductList.forEach(element => {
                if (element.processtype == 0) {
                    element.processtype = 'FORGING';
                }
                else {
                    element.processtype = 'ROLL CASTING';
                }
                if (element.itemtype == 0) {
                    element.itemtype = 'ROLLS';
                }
                else if (element.itemtype == 1) {
                    element.itemtype = 'STEP ROLLS';
                }
                else if (element.itemtype == 2) {
                    element.itemtype = 'REEL';
                }
                else {
                    element.itemtype = 'OTHER';
                }
                if (element.uom == 0) {
                    element.uom = 'Per-KG';
                }
                else if (element.uom == 1) {
                    element.uom = 'MT';
                }
                else {
                    element.uom = 'Per-Piece';
                }
                if (element.idList.currentQueue == '1') {
                    element.idList.currentQueue = 'Planning';
                }
                else if (element.idList.currentQueue == '2') {
                    element.idList.currentQueue = 'Roll Casting';
                }
                let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
                element['dimensionFormString'] = toString(element.dimensionForm);
                element.idList.queueUpdateTimeStamp = element.idList.queueUpdateTimeStamp.substring(0, 4) + '-' + element.idList.queueUpdateTimeStamp.substring(5, 7) + '-' + element.idList.queueUpdateTimeStamp.substring(8, 10);
            });
            this.tempduplicatepoProductList = this.duplicatepoProductList;
            if (isCheck == true) {
                this.fromDate = '';
                this.toDate = '';
            }
            if (this.fromDate != '' && this.toDate != '') {
                this.onDateSelection();
            }
            // console.log('here' + JSON.stringify(this.duplicatepoProductList));
            this.spinner.hide();
        })
    }

    moveToGrovingBearing(event) {
        // console.log('event' + JSON.stringify(event));
        let data = this.poProductList[event];
        // console.log('data' + JSON.stringify(data));
        let x = {
            poDetailsId: data.poDetailsId,
            productId: data._id,
            displayId: data.idList.displayId,
            queueUpdateTimeStamp: data.idList.queueUpdateTimeStamp,
            upcomingQueue: QueueStatus.FinalReport,
            currentQueue: QueueStatus.TransportFinish,
        };
        this.spinner.show();
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.partyService.updateQueueStatus(x, headers).subscribe(data => {
            // console.log(data);
            this.searchProduct(false, false);
            this.spinner.hide();
        });
    }

    updateQueueData(event) {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        let data = event.idList;
        let id = event._id;
        // console.log('final data' + JSON.stringify(data));
        let timeStamp = new Date();
        // console.log('date' + JSON.stringify(timeStamp));
        data.isDispacthed = true;
        let date = new Date();
        // let time = date.getTime();
        let x = {
            queueUpdateTimeStamp: date,
            displayId: data.displayId,
            currentQueue: data.currentQueue,
            isRejected: data.isRejected,
            isDispacthed: data.isDispacthed,
            isPacked: data.isPacked,
            qualityCheckData: data.qualityCheckData,
            isTCDone: data.isTCDone,
            mechiningData: data.mechiningData,
            bAndSTestCertificateData: data.bAndSTestCertificateData,
            rejection: data.rejection,
            inspectionData: data.inspectionData,
            gACFinishingData: data.gACFinishingData,
            bASFinishingData: data.bASFinishingData,
            gACInspectionData: data.gACInspectionData,
            bASInspectionData: data.bASInspectionData,
            testCertificateData: data.testCertificateData,
            queueDurationList: data.queueDurationList,
            dispatchedQuantity: event.dispatchedQuantity + 1
        };
        // console.log('x data' + JSON.stringify(x));

        this.spinner.show();
        this.partyService.updateQueueData(x, headers, id).subscribe(data => {
            this.searchProduct(true, false);
        });
    }


    getProductDetails() {

    }


    // previewImage(event) {
    //     this.image = this.poProductList[event].productimg;
    //     console.log('dataaiMage' + JSON.stringify(this.image));

    // }

    castingHistory() {
        this.history = !this.history;
        // console.log('boolean' + this.history);
        // this.fetchAllPoProduct(false, false);
        this.listCount = 0;
        this.duplicatepoProductList = [];
        this.basicSearchTerm = "";
        this.searchtype = "0"
    }

    onDateSelection() {
        let fromdateutc: any;
        let todateutc: any;
        if (this.fromDate != '' && this.toDate != '') {
            if (JSON.stringify(this.fromDate) == JSON.stringify(this.toDate)) {
                this.toDate.day = this.toDate.day + 1;
            }
        }
        this.duplicatepoProductList = this.tempduplicatepoProductList;
        fromdateutc = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
        fromdateutc = fromdateutc.getTime();
        todateutc = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
        todateutc = todateutc.getTime();
        let duplicatepoProductList = [];
        duplicatepoProductList = this.duplicatepoProductList;
        this.duplicatepoProductList = [];
        duplicatepoProductList.forEach(element => {
            element.filterDate = new Date(element.idList.queueUpdateTimeStamp).getTime();
        });
        duplicatepoProductList.forEach(element => {
            if (element.filterDate >= fromdateutc && element.filterDate <= todateutc) {
                this.duplicatepoProductList.push(element);
            }
        });
        this.listCount = this.duplicatepoProductList.length;
    }

    onKeyup() {
        let filteredList = [];
        if (this.search.length == 0) {
            this.listCount = this.duplicatepoProductList.length;
        }
        else {
            // console.log('dataata' + JSON.stringify(this.duplicatepoProductList));
            filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
            this.listCount = filteredList.length;
            // console.log('count'+this.listCount);

        }
    }

    convertPoDetailsExcel() {
        let value = [];

        this.filteredList = this.filter.transform(this.duplicatepoProductList, this.search);
        // console.log('dataata' + JSON.stringify(this.filteredList));

        this.filteredList.forEach(fuel => {
            // console.log('fuel' + JSON.stringify(fuel));
            let toString = obj => Object.entries(obj).map(([k, v]) => `${k}: ${v}`).join(', ');
            fuel.dimensionForm = toString(fuel.dimensionForm);
            let subValueArray = [];
            subValueArray.push(fuel.idList.displayId);
            subValueArray.push(fuel.producttype);
            subValueArray.push(fuel.itemtype);
            subValueArray.push(fuel.partyData.partyname);
            subValueArray.push(fuel.partyData.partyid);
            subValueArray.push(fuel.weightperpiece);
            subValueArray.push(fuel.dimensionForm);
            subValueArray.push(fuel.grade);
            value.push(subValueArray);
        });

        // let headerName = "Fuel Summary Report (" + this.fromDate.formatted + " to " + this.toDate.formatted + ")";
        let headerName = "Transport&Dispatch Report";
        let body = {
            "headerName": headerName,
            "excelName": "Transport&Dispatch",
            "header": [
                "ROLLID",
                "PRODUCTTYPE",
                "ITEMTYPE",
                "PARTYNAME",
                "PARTYID",
                "WEIGHT/PIECE",
                "DIMENSION",
                "GRADE",
            ],
            "value": value
        }
        this.downloadExcelSheet(body).subscribe(blob => {
            // Doing it this way allows you to name the file
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = "Transport&Dispatch.xlsx";
            link.click();
        }, error => console.log("Error downloading the file."),
            () => console.log('Completed file download.'));
    }

    downloadExcelSheet(reqBody): Observable<Object[]> {
        //this.loading = true;
        return Observable.create(observer => {
            let xhr = new XMLHttpRequest();
            xhr.open('POST', AppConstants.EXCEL_DOWNLOAD_URL, true);
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.responseType = 'blob';

            xhr.onreadystatechange = () => {
                //this.loading = false;
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {

                        var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        var blob = new Blob([xhr.response], { type: contentType });
                        observer.next(blob);
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            xhr.send(JSON.stringify(reqBody));

        });
    }
}