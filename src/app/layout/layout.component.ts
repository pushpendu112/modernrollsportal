import { Component, OnInit } from '@angular/core';
import { TestDataService } from '../shared/services/test-data.service';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

    collapedSideBar: boolean;
    isExpanded = true;
    private subscription: Subscription;


    constructor(private sideNaveData: TestDataService,
                private spinner: NgxSpinnerService) {}

    ngOnInit() {
        this.spinner.hide();
        // reciving sideBar boolan value of onClick in header
        this.subscription = this.sideNaveData.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('option') && res.option === 'event-boolean') {
                console.log(res.value);
                this.isExpanded = res.value;
                // perform your other action from here
            }
            if (res.hasOwnProperty('option') && res.option === 'event-hideNave') {
                console.log(res.value);
                this.isExpanded = res.value;
                 // alert(this.isExpanded);
                // perform your other action from here
            }
        });
    }

    receiveCollapsed($event) {
        this.collapedSideBar = $event;
    }
}
