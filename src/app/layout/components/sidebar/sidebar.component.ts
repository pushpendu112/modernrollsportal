import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TestDataService } from 'src/app/shared/services/test-data.service';
import { Subscription } from 'rxjs';
import { RoleService } from 'src/app/shared/services/role.service';
import { NgxSpinnerService } from 'ngx-spinner';



@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    isActive: boolean;
    collapsed: boolean;
    showMenu: string;
    pushRightClass: string;
    private subscription: Subscription;
    public reciveDataValue: any = [];
    isExpanded = true;
    doctorRoleData: any = [];


    roleRouting: any;


    @Output() collapsedEvent = new EventEmitter<boolean>();

    constructor(private translate: TranslateService,
        public router: Router,
        private childReciveData: TestDataService,
        private getRoleService: RoleService,
        private spinner: NgxSpinnerService) {
        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        this.isActive = false;
        this.collapsed = false;
        this.showMenu = '';
        this.pushRightClass = 'push-right';
        // this.subscription = this.childReciveData.notifyObservable$.subscribe((res) => {
        //     if (res.hasOwnProperty('option') && res.option === 'call_child') {
        //         console.log('Feature Value form header files');
        //         console.log(res.value);
        //         this.reciveDataValue = res.value;
        //         // perform your other action from here
        //     }
        // });
        this.spinner.show();
        this.subscription = this.childReciveData.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('option') && res.option === 'call_child') {
                // console.log('Feature Value form header files');
                // console.log('Recive Value', res.value);
                setTimeout(() => {
                    // console.log("reciveDataValue", res.value);
                    // console.log("roleRouting", this.reciveDataValue.roleName);
                    this.reciveDataValue = res.value;
                    this.roleRouting = this.reciveDataValue.roleName;
                    this.spinner.hide();
                }, 1000);
                // this.reciveDataValue = res.value;
                // perform your other action from here
            }
        });
        // reciving sideBar boolan value of onClick in header
        this.subscription = this.childReciveData.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('option') && res.option === 'event-boolean') {
                // console.log(res.value);
                this.isExpanded = res.value;
                // perform your other action from here
            }
        });
        // reciving sideBar boolan value of onClick in header
        this.subscription = this.childReciveData.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('option') && res.option === 'event-hideNave') {
                // console.log(res.value);
                this.isExpanded = res.value;
                // perform your other action from here
            }
        });

        this.getRoleDetailofDoctor();
        // this.getRolsDetailsByFilter();
    }


    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    toggleCollapsed() {
        this.collapsed = !this.collapsed;
        this.collapsedEvent.emit(this.collapsed);
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }

    getRoleDetailofDoctor() {
        this.getRoleService.getRole().subscribe(roles => this.doctorRoleData = roles);
    }
    getRolsDetailsByFilter() {
        const id = 1;
        // console.log('SIDE BAR DOCCTOR');
        // console.log(this.doctorRoleData.roleList.filter(x => x.id === id));
        return this.doctorRoleData.roleList.filter(x => x.id === id);
    }
}
