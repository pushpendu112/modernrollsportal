import { Component, OnInit, AfterViewInit, ChangeDetectorRef, DoCheck } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TestDataService } from 'src/app/shared/services/test-data.service';
import { RoleService } from 'src/app/shared/services/role.service';
import { ConditionalExpr } from '@angular/compiler';
import { AuthenticationService } from 'src/app/shared/services';
import { User, Role } from 'src/app/models';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit, DoCheck {

    currentUser: User;

    // Initilaizing variables
    isDocterActive = true;
    // isDocterInactive = false;
    isVitalActive = false;
    isPharmacyActive = false;
    isOnbordingActive = false;
    isLabinvestigatorActive = false;
    isPOCreatorActive = false;
    isAdminActive = false;
    isBillingActive = false;

    roleName: any;
    recievedRoleObject: any;
    headerData = [];
    featureList = [];
    isSuperAdminRoleActive = false;
    isAdminRoleActive = false;
    isDoctorRoleActive = false;
    isNurseRoleActive = false;
    isJuniorDoctorRoleActive = false;
    isPharmacyRoleActive = false;
    isLabinvestigatorRoleActive = false;
    isPOCreatorRoleActive = false;
    isManagerRoleActive = false;
    currentloginUser;
    loginData: any;
    loginName: any;
    currentuserMobileNumber;
    currentuserEmailId;
    private subscription: Subscription;






    public pushRightClass: string;
    shortnav = true;
    public roleData: any = [];

    constructor(private translate: TranslateService,
        public router: Router,
        private parentPassData: TestDataService,
        private getRoleService: RoleService,
        private authenticationService: AuthenticationService,
        private cdr: ChangeDetectorRef) {

        const res = localStorage.getItem('roleFeatureList');
        this.featureList = JSON.parse(res);

        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });

    }

    ngOnInit() {
        this.pushRightClass = 'push-right';
        this.getRoleDetails();
        this.subscription = this.parentPassData.notifyObservable$.subscribe(res => {
            if (res.hasOwnProperty('option') && res.option === 'call_loginData') {
                // console.log(res.value);
                this.loginData = res.value;
                // console.log('loginData***********', JSON.stringify(this.loginData));
                this.loginName = this.loginData.name;
                // console.log('this.loginName&&&', this.loginName);
                // this.currentloginUser = ` ${this.loginName}`;
                localStorage.setItem('currentloginUser', this.loginName);
                localStorage.setItem('currentloginUserMobileNumber', this.loginData.mobileNumber);
                localStorage.setItem('currentloginUserEmailId', this.loginData.emailId);
                // console.log('%%%%%%', this.loginData.emailId);

            }
        });
        // this.onDoctorActiveClick();

        // switch (this.currentUser.role) {
        //     case Role.Doctor: {
        //         setTimeout(() => {
        //             this.onDoctorActiveClick(1);
        //         }, 2000);
        //         break;
        //     }
        //     case Role.JuniorDoctor: {
        //         setTimeout(() => {
        //             this.patientInfoActive(4);
        //         }, 2000);
        //         break;
        //     } case Role.Admin: {
        //         setTimeout(() => {
        //             this.onAdminActiveClick(6);
        //         }, 2000);
        //         break;
        //     } case Role.LabInvestigator: {
        //         setTimeout(() => {
        //             this.onLabinvestigatorActive(5);
        //         }, 2000);
        //         break;
        //     } case Role.Nurse: {
        //         setTimeout(() => {
        //             this.onVitalActiveClick(2);
        //         }, 2000);
        //         break;
        //     } case Role.Pharmacy: {
        //         setTimeout(() => {
        //             this.onPharmacyActive(3);
        //         }, 2000);
        //         break;
        //     } case Role.Billing: {
        //         setTimeout(() => {
        //             this.billingInfoActive(7);
        //         }, 2000);
        //         break;
        //     }
        //     default: {
        //         console.log('No User Found');
        //         break;
        //     }
        // }
    }
    ngDoCheck() {
        const userName = localStorage.getItem('currentloginUser');
        const userNumber = localStorage.getItem('currentloginUserMobileNumber');
        const userEmailId = localStorage.getItem('currentloginUserEmailId');
        this.currentloginUser = userName;
        this.currentuserMobileNumber = userNumber;
        this.currentuserEmailId = userEmailId;
        // .replace(/['"]+/g, ''
    }
    ngAfterViewInit() {
        // console.log('this.featureList[0].roleName');
        // console.log(this.featureList[0].roleName);
        if (this.featureList[0].roleName === 'POCreator') {
            this.isPOCreatorRoleActive = true;
            this.isSuperAdminRoleActive = false;
            this.isDoctorRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isNurseRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = false;
            this.isAdminRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
        }

        if (this.featureList[0].roleName === 'super admin') {
            this.isSuperAdminRoleActive = true;
            this.isDoctorRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isNurseRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = false;
            this.isAdminRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
        } if (this.featureList[0].roleName === 'Doctor') {
            this.isDoctorRoleActive = true;
            this.isSuperAdminRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isNurseRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = false;
            this.isAdminRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
        } if (this.featureList[0].roleName === 'Junior Doctor') {
            this.isJuniorDoctorRoleActive = true;
            this.isDoctorRoleActive = false;
            this.isSuperAdminRoleActive = false;
            this.isNurseRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = false;
            this.isAdminRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
        } if (this.featureList[0].roleName === 'Nuse') {
            this.isNurseRoleActive = true;
            this.isJuniorDoctorRoleActive = false;
            this.isDoctorRoleActive = false;
            this.isSuperAdminRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = false;
            this.isAdminRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
        } if (this.featureList[0].roleName === 'Nurse') {
            this.isNurseRoleActive = true;
            this.isJuniorDoctorRoleActive = false;
            this.isDoctorRoleActive = false;
            this.isSuperAdminRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = false;
            this.isAdminRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
        } if (this.featureList[0].roleName === 'Pharmacy') {
            this.isPharmacyRoleActive = true;
            this.isNurseRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isDoctorRoleActive = false;
            this.isSuperAdminRoleActive = false;
            this.isManagerRoleActive = false;
            this.isAdminRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
        } if (this.featureList[0].roleName === 'Manager') {
            this.isManagerRoleActive = true;
            this.isPharmacyRoleActive = false;
            this.isNurseRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isDoctorRoleActive = false;
            this.isSuperAdminRoleActive = false;
            this.isAdminRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
        } if (this.featureList[0].roleName === 'Lab Investigator') {
            this.isLabinvestigatorRoleActive = true;
            this.isPOCreatorRoleActive = false;
            this.isManagerRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isNurseRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isDoctorRoleActive = false;
            this.isSuperAdminRoleActive = false;
            this.isAdminRoleActive = false;
        } if (this.featureList[0].roleName === 'Admin') {
            this.isAdminRoleActive = true;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
            this.isManagerRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isNurseRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isDoctorRoleActive = false;
            this.isSuperAdminRoleActive = false;
        }

        this.parentPassData.notifyOther({ option: 'call_child', value: this.featureList[0] });
        this.cdr.detectChanges();
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        this.shortnav = !this.shortnav;
        this.parentPassData.notifyOther({ option: 'event-boolean', value: this.shortnav });
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        // localStorage.removeItem('isLoggedin');
        // this.router.navigate(['/']);
        localStorage.clear();
        this.authenticationService.logout();
        this.router.navigate(['/']);
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    test1ToggleSidebar() {
        //this.parentPassData.notifyOther({ option: 'call_child', value: this.parentPassDavaValue });
    }

    test2ToggleSidebar() {
        //this.parentPassData.notifyOther({ option: 'call_child', value: this.parentPassDavaValue2 });
    }
    // Getting role Details
    getRoleDetails() {
        this.getRoleService.getRole().subscribe(roles => this.roleData = roles);
    }
    onDoctorActiveClick(id: number) {
        // const deid = 1;
        this.isDocterActive = true;
        this.isVitalActive = false;
        this.isPharmacyActive = false;
        this.isOnbordingActive = false;
        this.isLabinvestigatorActive = false;
        this.isAdminActive = false;
        this.isBillingActive = false;
        // Navigate to Doctor Router Module.
        this.router.navigate(['/dashboard/Doctor']);

        const doctorActiveData = this.getRolsDetailsByFilter(id);
        // console.log('Doctor_Active_Data');
        // console.log(doctorActiveData);
        this.parentPassData.notifyOther({ option: 'call_child', value: doctorActiveData });

    }
    onVitalActiveClick(id: number) {
        this.isVitalActive = true;
        this.isDocterActive = false;
        this.isPharmacyActive = false;
        this.isOnbordingActive = false;
        this.isLabinvestigatorActive = false;
        this.isAdminActive = false;
        this.isBillingActive = false;
        // Navigate to Nurse Router Module.
        this.router.navigate(['/dashboard/Nurse']);

        const vitalActiveData = this.getRolsDetailsByFilter(id);
        // console.log('Vital_Active_Data');
        // console.log(vitalActiveData);
        this.parentPassData.notifyOther({ option: 'call_child', value: vitalActiveData });
        // if (id === 2) {
        //     if (this.roleData.roleList.id = id) {
        //         alert(this.roleData.roleList.roleName);
        //         console.log(this.roleData.roleList.roleName);
        //     }
        // }
    }

    // OnbordingActive1(id: number) {
    //     this.isVitalActive = false;
    //     this.isDocterActive = false;
    //     this.isPharmacyActive = false;
    //     this.isOnbordingActive = false;
    //     this.isOnbordingActive1 = true;

    //     // Navigate to Nurse Router Module.
    //     this.router.navigate(['/dashboard/Nurse']);

    //     const vitalActiveData = this.getRolsDetailsByFilter(id);
    //     console.log('Vital_Active_Data');
    //     console.log(vitalActiveData);
    //     this.parentPassData.notifyOther({ option: 'call_child', value: vitalActiveData });
    // }

    onPharmacyActive(id: number) {
        this.isPharmacyActive = true;
        this.isVitalActive = false;
        this.isDocterActive = false;
        this.isOnbordingActive = false;
        this.isLabinvestigatorActive = false;
        this.isAdminActive = false;
        this.isBillingActive = false;

        // Navigate to Pharmacy Router Module.
        this.router.navigate(['/dashboard/Pharmacy']);

        const pharmacyActiveData = this.getRolsDetailsByFilter(id);
        // console.log('Pharmacy_Active_Data');
        // console.log(pharmacyActiveData);
        this.parentPassData.notifyOther({ option: 'call_child', value: pharmacyActiveData });
    }
    patientInfoActive(id: number) {
        this.isOnbordingActive = true;
        this.isPharmacyActive = false;
        this.isVitalActive = false;
        this.isDocterActive = false;
        this.isLabinvestigatorActive = false;
        this.isAdminActive = false;
        this.isBillingActive = false;

        // Navigate to Pharmacy Router Module.
        this.router.navigate(['/dashboard/patientInfo']);
        const patientInfoActiveData = this.getRolsDetailsByFilter(id);
        this.parentPassData.notifyOther({ option: 'call_child', value: patientInfoActiveData });
    }

    onLabinvestigatorActive(id: number) {
        // const deid = 1;
        this.isDocterActive = false;
        this.isVitalActive = false;
        this.isPharmacyActive = false;
        this.isOnbordingActive = false;
        this.isLabinvestigatorActive = true;
        this.isAdminActive = false;
        this.isBillingActive = false;
        // Navigate to Doctor Router Module.
        this.router.navigate(['/dashboard/LabInvestigator']);

        const LabinvestigatorActive = this.getRolsDetailsByFilter(id);
        // console.log('Doctor_Active_Data');
        // console.log(LabinvestigatorActive);
        this.parentPassData.notifyOther({ option: 'call_child', value: LabinvestigatorActive });

    }
    onAdminActiveClick(id: number) {
        this.isAdminActive = true;
        this.isOnbordingActive = false;
        this.isPharmacyActive = false;
        this.isVitalActive = false;
        this.isDocterActive = false;
        this.isBillingActive = false;

        // Navigate to Pharmacy Router Module.
        this.router.navigate(['/dashboard/Admin']);
        const adminActiveData = this.getRolsDetailsByFilter(id);
        this.parentPassData.notifyOther({ option: 'call_child', value: adminActiveData });
    }
    billingInfoActive(id: number) {
        this.isAdminActive = false;
        this.isOnbordingActive = false;
        this.isPharmacyActive = false;
        this.isVitalActive = false;
        this.isDocterActive = false;
        this.isBillingActive = true;

        // Navigate to Pharmacy Router Module.
        this.router.navigate(['/dashboard/Billing']);
        const billingInfoActive = this.getRolsDetailsByFilter(id);
        this.parentPassData.notifyOther({ option: 'call_child', value: billingInfoActive });
    }


    onRoleNameClick(roleFeature) {

        // console.log("roleFeature ---" + JSON.stringify(roleFeature));

        if (roleFeature.roleName === 'POCreator') {
            this.isPOCreatorRoleActive = true;
            this.isSuperAdminRoleActive = true;
            this.isDoctorRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = false;
            this.isNurseRoleActive = false;
            this.isAdminRoleActive = false;
        }
        if (roleFeature.roleName === 'super admin') {
            this.isPOCreatorRoleActive = false;
            this.isSuperAdminRoleActive = true;
            this.isDoctorRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = false;
            this.isNurseRoleActive = false;
            this.isAdminRoleActive = false;
        } else if (roleFeature.roleName === 'Doctor') {
            this.isSuperAdminRoleActive = false;
            this.isDoctorRoleActive = true;
            this.isJuniorDoctorRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = false;
            this.isNurseRoleActive = false;
            this.isAdminRoleActive = false;
        } else if (roleFeature.roleName === 'Nuse') {
            this.isSuperAdminRoleActive = false;
            this.isDoctorRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = false;
            this.isNurseRoleActive = true;
            this.isAdminRoleActive = false;
        } else if (roleFeature.roleName === 'Nurse') {
            this.isSuperAdminRoleActive = false;
            this.isDoctorRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = false;
            this.isNurseRoleActive = true;
            this.isAdminRoleActive = false;
        } else if (roleFeature.roleName === 'Junior Doctor') {
            this.isJuniorDoctorRoleActive = true;
            this.isSuperAdminRoleActive = false;
            this.isDoctorRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = false;
            this.isNurseRoleActive = false;
            this.isAdminRoleActive = false;
        } else if (roleFeature.roleName === 'Pharmacy') {
            this.isSuperAdminRoleActive = false;
            this.isDoctorRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
            this.isPharmacyRoleActive = true;
            this.isManagerRoleActive = false;
            this.isNurseRoleActive = false;
            this.isAdminRoleActive = false;
        } else if (roleFeature.roleName === 'Lab Investigator') {
            this.isSuperAdminRoleActive = false;
            this.isDoctorRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isLabinvestigatorRoleActive = true;
            this.isPOCreatorRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = false;
            this.isNurseRoleActive = false;
            this.isAdminRoleActive = false;
        } else if (roleFeature.roleName === 'Manager') {
            this.isSuperAdminRoleActive = false;
            this.isDoctorRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = true;
            this.isNurseRoleActive = false;
            this.isAdminRoleActive = false;
        }
        else if (roleFeature.roleName === 'Admin') {
            this.isSuperAdminRoleActive = false;
            this.isAdminRoleActive = true;
            this.isDoctorRoleActive = false;
            this.isJuniorDoctorRoleActive = false;
            this.isLabinvestigatorRoleActive = false;
            this.isPOCreatorRoleActive = false;
            this.isPharmacyRoleActive = false;
            this.isManagerRoleActive = true;
            this.isNurseRoleActive = false;
        }
        localStorage.setItem('userRoleSelect', JSON.stringify(roleFeature));


        this.router.navigate([`/dashboard/${roleFeature.roleName}`]);
        this.parentPassData.notifyOther({ option: 'call_child', value: roleFeature });
    }

    // onNurseRoleClick() {

    //     this.isNurseRoleActive =true;
    //     localStorage.setItem('userRoleSelect', JSON.stringify(roleFeature));

    //     this.router.navigate([`/dashboard/${roleFeature.roleName}`]);
    //     this.parentPassData.notifyOther({ option: 'call_child', value: roleFeature });
    // }




    getRolsDetailsByFilter(id) {
        return this.roleData.roleList.filter(x => x.id === id);
    }
    get isDoctor() {
        return this.currentUser && this.currentUser.role === Role.Doctor;
    }
    get isNurse() {
        return this.currentUser && this.currentUser.role === Role.Nurse;
    }
    get isjuniorDoctor() {
        return this.currentUser && this.currentUser.role === Role.JuniorDoctor;
    }
    get isAdmin() {
        return this.currentUser && this.currentUser.role === Role.Admin;
    }
    get isLabinvestigator() {
        return this.currentUser && this.currentUser.role === Role.LabInvestigator;
    }
    get isPharmacy() {
        return this.currentUser && this.currentUser.role === Role.Pharmacy;
    }
    get isBilling() {
        return this.currentUser && this.currentUser.role === Role.Billing;
    }

    get isPOCreator() {
        return this.currentUser && this.currentUser.role === Role.POCreator;
    }
}
