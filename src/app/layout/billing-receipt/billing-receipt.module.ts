import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillingReceiptRoutingModule } from './billing-receipt-routing.module';
import { BillingReceiptComponent } from './billing-receipt.component';

@NgModule({
  declarations: [BillingReceiptComponent],
  imports: [
    CommonModule,
    BillingReceiptRoutingModule
  ]
})
export class BillingReceiptModule { }
