import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillingReceiptComponent } from './billing-receipt.component';

const routes: Routes = [
  {
    path: 'billingReceipt',
        component: BillingReceiptComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingReceiptRoutingModule { }
