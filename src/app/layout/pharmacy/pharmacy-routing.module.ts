import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StockComponent } from './components/stock/stock.component';
import { GenerateBillComponent } from './components/generate-bill/generate-bill.component';
import { ReportsComponent } from './components/reports/reports.component';
import { PharmacyComponent } from './pharmacy.component';
import { BillingReceiptComponent } from './components/billing-receipt/billing-receipt.component';

const routes: Routes = [
  {
    path: '', component: PharmacyComponent,
    children: [
      {
        path: 'Pharmacy Queue',
        component: StockComponent
      },
      {
        path: 'Generate Bill',
        component: GenerateBillComponent
      },
      {
        path: 'Reports',
        component: ReportsComponent
      },
      {
        path: 'billingReciept',
        component: BillingReceiptComponent
      },
      {
        path: '',
        redirectTo: 'Pharmacy Queue'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PharmacyRoutingModule { }
