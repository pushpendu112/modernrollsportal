import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TestDataService } from 'src/app/shared/services/test-data.service';
import { TabledataService } from 'src/app/shared/services';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
declare var $: any;   // not required

import * as CryptoJS from 'crypto-js';
import { PharmacyService } from '../../pharmacy.service';
import { JuniorDoctorService } from 'src/app/layout/junior-doctor/junior-doctor.service';


@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {
  private readonly notifier: NotifierService;
  parentPassData: any;
  constructor(private passBoolean: TestDataService,
    private tableDatas: TabledataService,
    private router: Router,
    private juniorDoctorService: JuniorDoctorService,
    private pharmacyService: PharmacyService,
    private spinner: NgxSpinnerService,
              private notifierService: NotifierService) {
                this.notifier = notifierService; 
              }
  public reciveTableData: any = [];
  cols: any[];
  sortF: any;
  statusChange: any;
  ptName;
  ptTime;
  ptDate;
  userToken: any;
  getjwttoken: any;
  pocenterId: any;
  existPatientObjectQueue: any;
  Secret = 'i_have_some_small_master_secret_live_pin';
  @Output() valueChange = new EventEmitter();
  counter = true;




  ngOnInit() {
    this.cols = [
      { field: 'firstName', header: 'Patient Name' },
      { field: 'mobileNumber', header: 'Mobile Numbe' },
      { field: 'gender', header: 'Gender' },
      { field: 'presentStatus', header: 'Status' }
    ];
    // Reciving Table Data Values.
    // this.tableDatas.getTableData().subscribe((data) => this.reciveTableData = data);
    this.getExistingPatientQueuees();
    this.getjwttoken = localStorage.getItem('jwttoken');
    this.userToken = this.decryptData(this.getjwttoken);
  }

  onRowSelect(event) {
    // console.log(event);
  }
  changeSort(event) {
    if (!event.order) {
      this.sortF = 'year';
    } else {
      this.sortF = event.field;
    }
  }
  selectedRowData(data) {
    // console.log(data);

    this.statusChange = data.patientMobile;
  }

  navigateComponent() {
    this.router.navigate(['/dashboard/Pharmacy/Reports']);
  }

  getExistingPatientQueuees() {

    this.pocenterId = JSON.parse(localStorage.getItem('centerId'));

    const reqbody = this.pocenterId;
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });

    this.spinner.show();
    this.juniorDoctorService.getPatientQueueForPoc(reqbody, headers).subscribe(data => {

      this.existPatientObjectQueue = data.patientQueueData;
      data.patientQueueData.forEach(status => {
        if (status.presentQueueType === 0) {
          status.presentStatus = 'Registration Queue';
        } else if (status.presentQueueType === 1) {
          status.presentStatus = 'Investigation Queue';
        } else if (status.presentQueueType === 2) {
          status.presentStatus = 'Doctor Queue';
        } else if (status.presentQueueType === 3) {
          status.presentStatus = 'Manager Queue';
        } else if (status.presentQueueType === 4) {
          status.presentStatus = 'Diagnostics Queue';
        } else if (status.presentQueueType === 5) {
          status.presentStatus = 'Pharmacy Queue';
        } else if (status.presentQueueType === 6) {
          status.presentStatus = 'Medicine Issued';
        }
        this.spinner.hide();
      });
   },
    err=>{
      this.spinner.hide();
      this.notifier.notify('error', 'Something went wrong please try again!')
    });
  }
  decryptData(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, this.Secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }
  MedicineIssue(data) {
    const statusBody = {
      queueId: data.queueId,
      queueEntryDateAndTime: data.queueEntryDateAndTime,
      arrivalDateAndTime: data.arrivalDateAndTime,
      presentQueueType: data.presentQueueType,
      nextQueueType: 6,
    };

    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });

    this.juniorDoctorService.updatePatientQueue(statusBody, headers).subscribe(updateStatus => {
      // console.log(updateStatus);
      this.getExistingPatientQueuees();
    });
  }

  valueChanged(data) { // You can give any function name
    // console.log('azzzzzzzzzzaaaaaaaa', data);
    this.ptName = data.firstName + ' ' + data.lastName;
    this.ptTime = data.arrivalDateAndTime;
    this.ptDate = data.queueEntryDateAndTime;
    const queueId = data.queueId;
    const queueIdEncrypt = this.encryptData(queueId);
    localStorage.setItem('queId', queueIdEncrypt);
    const patinetId = data.patientId;
    const patinetID = this.encryptData(patinetId);
    localStorage.setItem('patientId', patinetID);
    // this.valueChange.emit(this.Counter);
    // alert('');
    // this.passBoolean.notifyOther({ option: 'patient-queue', value: this.counter });
    this.passBoolean.notifyOther({ option: 'pt-data', value: data });
  }

  onPayment() {

    this.router.navigate(['/dashboard/Pharmacy/billingReciept']);
    // $('#exampleModal').modal('hide');
  }
  encryptData(data) {
    try {
      return CryptoJS.AES.encrypt(JSON.stringify(data), this.Secret).toString();
    } catch (e) {
      console.log(e);
    }
  }
}

