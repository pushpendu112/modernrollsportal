import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PharmacyRoutingModule } from './pharmacy-routing.module';
import { StockComponent } from './components/stock/stock.component';
import { GenerateBillComponent } from './components/generate-bill/generate-bill.component';
import { ReportsComponent } from './components/reports/reports.component';
import { PharmacyComponent } from './pharmacy.component';
import { DataTableModule } from 'primeng/datatable';
import { BillingReceiptComponent } from './components/billing-receipt/billing-receipt.component';

@NgModule({
  declarations: [StockComponent, GenerateBillComponent, ReportsComponent, PharmacyComponent, BillingReceiptComponent],
  imports: [
    CommonModule,
    PharmacyRoutingModule,
    DataTableModule,
    FormsModule
  ]
})
export class PharmacyModule { }
