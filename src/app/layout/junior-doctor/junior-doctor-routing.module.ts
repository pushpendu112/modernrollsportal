import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JuniorDoctorComponent } from './junior-doctor.component';

import { PersonalInformationComponent } from './components/personal-information/personal-information.component';
import { ReportedSymptomsComponent } from './components/reported-symptoms/reported-symptoms.component';
import { PersonalLifestyleHistoryComponent } from './components/personal-lifestyle-history/personal-lifestyle-history.component';
import { MedicationHistoryComponent } from './components/medication-history/medication-history.component';
import { FamilyMedicalHistoryComponent } from './components/family-medical-history/family-medical-history.component';
import { GeneralInformationComponent } from './components/general-information/general-information.component';

import { ObsGynacHistoryComponent } from './components/obs-gynac-history/obs-gynac-history.component';
import { TreatmentHistoryComponent } from './components/treatment-history/treatment-history.component';
import { LabInvestigationsComponent } from './components/lab-investigations/lab-investigations.component';
import { ClinicalFindingsComponent } from './components/clinical-findings/clinical-findings.component';
import { GeneticConstitutionComponent } from './components/genetic-constitution/genetic-constitution.component';
import { PhysicalMakeupComponent } from './components/physical-makeup/physical-makeup.component';
import { PhysicalGeneralsComponent } from './components/physical-generals/physical-generals.component';
import { MentalGeneralsComponent } from './components/mental-generals/mental-generals.component';
import { SummaryComponent } from './components/summary/summary.component';
import { PatientqueueComponent } from './components/patientqueue/patientqueue.component';

import { ChiefComplaintsComponent } from './components/chief-complaints/chief-complaints.component';
import { PastMedicalHistoryComponent } from './components/past-medical-history/past-medical-history.component';


const routes: Routes = [
  {
    path: '', component: JuniorDoctorComponent,
    children: [
      {
        path: 'PastMedicalHistory',
        component: PastMedicalHistoryComponent
      },
       {
        path: 'PersonalInfo',
        component: PersonalInformationComponent
      },
      {
        path: 'ReportedSymptoms',
        component: ReportedSymptomsComponent
      },
      {
        path: 'PersonalLifestyle',
        component: PersonalLifestyleHistoryComponent
      },
      {
        path: 'MedicationHistory',
        component: MedicationHistoryComponent
      },
      {
        path: 'FamilyMedicalHistory',
        component: FamilyMedicalHistoryComponent
      },
      {
        path: 'GeneralInformation',
        component: GeneralInformationComponent
      },
      {
        path: 'ChiefComplaints',
        component: ChiefComplaintsComponent
      },
      {
        path: 'ObsGynacHistory',
        component: ObsGynacHistoryComponent
      },
      {
        path: 'TreatmentHistory',
        component: TreatmentHistoryComponent
      },
      {
        path: 'LabInvestigations',
        component: LabInvestigationsComponent
      },
      {
        path: 'ClinicalFindings',
        component: ClinicalFindingsComponent
      },
      {
        path: 'GeneticConstitution',
        component: GeneticConstitutionComponent
      },
      {
        path: 'PhysicalMakeup',
        component: PhysicalMakeupComponent
      },
      {
        path: 'PhysicalGenerals',
        component: PhysicalGeneralsComponent
      },
      {
        path: 'MentalGenerals',
        component: MentalGeneralsComponent
      },
      {
        path: 'Summary',
        component: SummaryComponent
      },
      {
        path: 'Investigation Queue',
        component: PatientqueueComponent
      },
      {
        path: '',
        redirectTo: 'Investigation Queue'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JuniorDoctorRoutingModule { }
