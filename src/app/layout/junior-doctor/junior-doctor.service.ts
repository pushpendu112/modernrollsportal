import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/base/http.service';
import { AppConstants } from 'src/app/base/appconstants';

@Injectable({
  providedIn: 'root'
})
export class JuniorDoctorService {

  constructor(private httpservice: HttpService) { }

  postpatientdetails(patientformvalues: any) {
    const reqbody = {
      orgId: patientformvalues.orgId,
      centerId: patientformvalues.centerId,
      employeeId: patientformvalues.employeeId,
      mobileNumber: patientformvalues.mobileNumber,
      firstName: patientformvalues.firstName,
      lastName: patientformvalues.lastName,
      dob: patientformvalues.dob,
      age: patientformvalues.age,
      emailId: patientformvalues.emailId,
      gender: patientformvalues.gender,
      imageUrl: patientformvalues.imageUrl,
      height: patientformvalues.height,
      weight: patientformvalues.weight,
      address: patientformvalues.address,
      maritalLife: patientformvalues.maritalLife,
      diteType: patientformvalues.diteType,
      usedHomeopathic: patientformvalues.usedHomeopathic,
      maleChildren: patientformvalues.maleChildren,
      femaleChildren: patientformvalues.femaleChildren
    };

    return this.httpservice.httpPostObservable(AppConstants.post_Create_Patient, reqbody, '');
  }

  createPatientQueue(patinetDetails: any) {
    const reqbody = {
      patientId: patinetDetails.patientId,
      orgId: patinetDetails.orgId,
      centerId: patinetDetails.centerId,
      presentQueueType: patinetDetails.presentQueueType,
      durationList: patinetDetails.durationList
    };
    return this.httpservice.httpPostObservable(AppConstants.post_Create_PatientQueue, reqbody, '');
  }

  getExistigPatientDetails(reqbody, headers) {
    return this.httpservice.httpGetObserevable(AppConstants.post_getPatient_ByMobileNumber + reqbody, '', headers);
  }

  // getPatientQueueForPoc
  getPatientQueueForPoc(reqbody, headers) {
    return this.httpservice.httpGetObserevable(AppConstants.get_PatientQueueForPoc + reqbody, '', headers);
  }

  updatePatientQueue(body, headers) {
    const reqbody = {
      queueId: body.queueId,
      queueEntryDateAndTime: body.queueEntryDateAndTime,
      arrivalDateAndTime: body.arrivalDateAndTime,
      presentQueueType: body.presentQueueType,
      nextQueueType: body.nextQueueType,
    };
    return this.httpservice.httpPostObservable(AppConstants.post_update_patient_queue, reqbody, '', headers);
  }

  updatePatient(patientInfoformvalues, headers) {
    const reqbody = {
      _id: patientInfoformvalues._id,
      orgId: patientInfoformvalues.orgId,
      centerId: patientInfoformvalues.centerId,
      employeeId: patientInfoformvalues.employeeId,
      mobileNumber: patientInfoformvalues.mobileNumber,
      firstName: patientInfoformvalues.firstName,
      lastName: patientInfoformvalues.lastName,
      enrolmentId: patientInfoformvalues.enrolmentId,
      dob: patientInfoformvalues.dob,
      age: patientInfoformvalues.age,
      emailId: patientInfoformvalues.emailId,
      gender: patientInfoformvalues.gender,
      imageUrl: patientInfoformvalues.imageUrl,
      height: patientInfoformvalues.height,
      weight: patientInfoformvalues.weight,
      address: patientInfoformvalues.address,
      maritalLife: patientInfoformvalues.maritalLife,
      diteType: patientInfoformvalues.diteType,
      usedHomeopathic: patientInfoformvalues.usedHomeopathic,
      maleChildren: patientInfoformvalues.maleChildren,
      femaleChildren: patientInfoformvalues.femaleChildren,
      lastVisitedDate: patientInfoformvalues.lastVisitedDate,
      __v: patientInfoformvalues.__v,

    };

    return this.httpservice.httpPostObservable(AppConstants.post_update_patient, reqbody, '', headers);
  }
  saveMedicalHistory(reqbody, headers) {
    return this.httpservice.httpPostObservable(AppConstants.post_save_medical_history, reqbody, '', headers);
  }
  updateMedicalHistory(reqbody, headers) {
    return this.httpservice.httpPostObservable(AppConstants.post_update_medical_history, reqbody, '', headers);
  }

  getMedicalHistoryByPatientId(reqbody, headers) {
    return this.httpservice.httpGetObserevable(AppConstants.get_medical_history_by_patientId + reqbody, '', headers);
  }
  createTransaction(reqbody, headers) {
    return this.httpservice.httpPostObservable(AppConstants.post_create_transaction, reqbody, '', headers);
  }
  getTransactionForQueue(reqbody, headers) {
    return this.httpservice.httpGetObserevable(AppConstants.get_transaction_for_queue + reqbody, '', headers);
  }
}
