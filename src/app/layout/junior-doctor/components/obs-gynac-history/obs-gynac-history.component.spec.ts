import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObsGynacHistoryComponent } from './obs-gynac-history.component';

describe('ObsGynacHistoryComponent', () => {
  let component: ObsGynacHistoryComponent;
  let fixture: ComponentFixture<ObsGynacHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObsGynacHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObsGynacHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
