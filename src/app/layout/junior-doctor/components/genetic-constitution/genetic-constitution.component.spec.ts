import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneticConstitutionComponent } from './genetic-constitution.component';

describe('GeneticConstitutionComponent', () => {
  let component: GeneticConstitutionComponent;
  let fixture: ComponentFixture<GeneticConstitutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneticConstitutionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneticConstitutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
