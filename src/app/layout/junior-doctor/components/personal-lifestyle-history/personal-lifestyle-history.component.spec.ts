import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalLifestyleHistoryComponent } from './personal-lifestyle-history.component';

describe('PersonalLifestyleHistoryComponent', () => {
  let component: PersonalLifestyleHistoryComponent;
  let fixture: ComponentFixture<PersonalLifestyleHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalLifestyleHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalLifestyleHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
