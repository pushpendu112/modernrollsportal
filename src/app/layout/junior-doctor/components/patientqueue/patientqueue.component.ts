import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TestDataService } from 'src/app/shared/services/test-data.service';
import { TabledataService } from 'src/app/shared/services';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';

import * as CryptoJS from 'crypto-js';
import { JuniorDoctorService } from '../../junior-doctor.service';
import { JuniorDoctorComponent } from '../../junior-doctor.component';

@Component({
  selector: 'app-patientqueue',
  templateUrl: './patientqueue.component.html',
  styleUrls: ['./patientqueue.component.scss']
})
export class PatientqueueComponent implements OnInit {
    private readonly notifier: NotifierService;

  public reciveTableData: any = [];
  cols: any[];
  sortF: any;
  statusChange: any;

  getjwttoken: any;
  userToken: any;
  existPatientObjectQueue: any;
  pocenterId: any;
  Secret = 'i_have_some_small_master_secret_live_pin';


  @Output() valueChange = new EventEmitter();
  counter = true;
  patientId: any;
  constructor(private passBoolean: TestDataService,
    private tableDatas: TabledataService,
    private juniorDoctorService: JuniorDoctorService,
    private radioActiveTab: JuniorDoctorComponent,
    private spinner: NgxSpinnerService,
    private notifierService: NotifierService,) {
    this.notifier = notifierService; }

  ngOnInit() {
    this.cols = [
      { field: 'firstName', header: 'Patient Name' },
      { field: 'mobileNumber', header: 'Mobile Numbe' },
      { field: 'gender', header: 'Gender' },
      { field: 'presentStatus', header: 'Status' }
    ];
    // Reciving Table Data Values.
    // this.tableDatas.getTableData().subscribe((data) => this.reciveTableData = data);
    this.getExistingPatientQueuees();
    this.getjwttoken = localStorage.getItem('jwttoken');
    this.userToken = this.decryptData(this.getjwttoken);
  }
  valueChanged(data) { // You can give any function name
    // this.valueChange.emit(this.Counter);
    // alert('');
    // console.log(data);
    this.radioActiveTab.activeRadioButton('generalInfo');
    console.log(data.patientId);
    console.log(data.mobileNumber);
    const patientId = JSON.stringify(data.patientId);
    const mobileNumber = JSON.stringify(data.mobileNumber);

    console.log(patientId);

    localStorage.setItem('patientId', patientId);
    localStorage.setItem('mobileNumber', mobileNumber);



    // const ptid = localStorage.getItem('patientId');
    // this.patientId = JSON.parse(ptid);
    // const getjwt = localStorage.getItem('masterToken');
    // this.userToken = this.decryptData(getjwt);
    this.passBoolean.notifyOther({ option: 'patient-queue', value: this.counter });

  }

  onRowSelect(event) {
    console.log(event);
  }
  changeSort(event) {
    if (!event.order) {
      this.sortF = 'year';
    } else {
      this.sortF = event.field;
    }
  }
  selectedRowData(data) {
    // console.log(data);

    this.statusChange = data.patientMobile;
  }

  getExistingPatientQueuees() {

    this.pocenterId = JSON.parse(localStorage.getItem('centerId'));

    const reqbody = this.pocenterId;
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });
    this.spinner.show();
    this.juniorDoctorService.getPatientQueueForPoc(reqbody, headers).subscribe(data => {
      this.spinner.hide();
      console.log(`data.patientQueueData`);
      console.log(data.patientQueueData);


      this.existPatientObjectQueue = data.patientQueueData;
      data.patientQueueData.forEach(status => {
        if (status.presentQueueType === 0) {
          status.presentStatus = 'Registration Queue';
        } else if (status.presentQueueType === 1) {
          status.presentStatus = 'Investigation Queue';
        } else if (status.presentQueueType === 2) {
          status.presentStatus = 'Doctor Queue';
        } else if (status.presentQueueType === 3) {
          status.presentStatus = 'Manager Queue';
        } else if (status.presentQueueType === 4) {
          status.presentStatus = 'Diagnostics Queue';
        } else if (status.presentQueueType === 5) {
          status.presentStatus = 'Pharmacy Queue';
        } else if (status.presentQueueType === 5) {
          status.presentStatus = 'Medicine Issued';
        }

      });

      // console.log('ssssssssss', this.existPatientObjectQueue);
       this.existPatientObjectQueue.forEach(exitPtObj => {
        const enrolmentId1 = JSON.stringify(exitPtObj.enrolmentId);
        // localStorage.setItem('enrolmentId', JSON.stringify(exitPtObj.enrolmentId));
        // console.log('enrolmentId###########################');
        // console.log(enrolmentId);
        localStorage.setItem('enrolmentId', enrolmentId1);
        localStorage.setItem('lastVisitedDate', JSON.stringify(exitPtObj.lastVisitedDate));

      });
      // const test = JSON.stringify(enrolmentId);
      // const enrolmentId = JSON.stringify(this.existPatientObjectQueue.enrolmentId);
      // const lastVisitedDate = JSON.stringify(this.existPatientObjectQueue.lastVisitedDate);
      // localStorage.setItem('lastVisitedDate',  JSON.stringify(this.existPatientObjectQueue.lastVisitedDate));
      // localStorage.setItem('enrolmentId', enrolmentId);
     },err=>{
              this.spinner.hide();
              this.notifier.notify('error', 'Something went wrong please try again! ')
              console.log(err);
    });

  }
  decryptData(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, this.Secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }
  goToDoctor(data) {
    const statusBody = {
      queueId: data.queueId,
      queueEntryDateAndTime: data.queueEntryDateAndTime,
      arrivalDateAndTime: data.arrivalDateAndTime,
      presentQueueType: data.presentQueueType,
      nextQueueType: 2,
    };

    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });
    this.spinner.show()
    this.juniorDoctorService.updatePatientQueue(statusBody, headers).subscribe(updateStatus => {
      //console.log(updateStatus);
      this.spinner.hide();
      this.getExistingPatientQueuees();
    },err=>{
              this.spinner.hide();
              this.notifier.notify('error', 'Something went wrong please try again! ')
              console.log(err);
    });
  }

  goToManger(data) {
    const statusBody = {
      queueId: data.queueId,
      queueEntryDateAndTime: data.queueEntryDateAndTime,
      arrivalDateAndTime: data.arrivalDateAndTime,
      presentQueueType: data.presentQueueType,
      nextQueueType: 3,
    };

    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });
    this.spinner.show();
    this.juniorDoctorService.updatePatientQueue(statusBody, headers).subscribe(updateStatus => {
      // console.log(updateStatus);
      this.spinner.hide();
      this.getExistingPatientQueuees();
    },err=>{
              this.spinner.hide();
              this.notifier.notify('error', 'Something went wrong please try again! ')
              console.log(err);
    });
  }

  goToDiagnostics(data) {
    const statusBody = {
      queueId: data.queueId,
      queueEntryDateAndTime: data.queueEntryDateAndTime,
      arrivalDateAndTime: data.arrivalDateAndTime,
      presentQueueType: data.presentQueueType,
      nextQueueType: 4,
    };

    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });

    this.spinner.show();
    this.juniorDoctorService.updatePatientQueue(statusBody, headers).subscribe(updateStatus => {
      console.log(updateStatus);
      this.getExistingPatientQueuees();
      this.spinner.hide();
    },err=>{
              this.spinner.hide();
              this.notifier.notify('error', 'Something went wrong please try again! ')
              console.log(err);
    });
  }
}
