import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientqueueComponent } from './patientqueue.component';

describe('PatientqueueComponent', () => {
  let component: PatientqueueComponent;
  let fixture: ComponentFixture<PatientqueueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientqueueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientqueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
