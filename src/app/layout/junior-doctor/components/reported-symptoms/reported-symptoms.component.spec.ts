import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportedSymptomsComponent } from './reported-symptoms.component';

describe('ReportedSymptomsComponent', () => {
  let component: ReportedSymptomsComponent;
  let fixture: ComponentFixture<ReportedSymptomsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportedSymptomsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportedSymptomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
