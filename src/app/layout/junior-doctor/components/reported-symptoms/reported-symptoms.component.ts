import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reported-symptoms',
  templateUrl: './reported-symptoms.component.html',
  styleUrls: ['./reported-symptoms.component.scss']
})
export class ReportedSymptomsComponent implements OnInit {
        reportedSymptoms = [{
          id: 1,
          quetion: 'Do you have any ailments now?',
          answer: ['Select', 'Yes', 'No']
        },
        {
          id: 2,
          heading: 'Cardiovascular System',
          quetion: 'Shortness of breath',
          // tslint:disable-next-line:max-line-length
          answer: ['Select', 'Breathlessness with exertion', 'Breathlessness with daily activities', 'Breathless at rest', 'No Breathlessness']
        },
        {
          id: 3,
          heading: 'Respiratory System',
          quetion: 'Have you experienced recurrent episodes of breathlessness?',
          answer: ['Select', 'Yes', 'No']
        },
        {
          id: 4,
          quetion: 'Do you snore while sleeping(as observed by partner)?',
          answer: ['Select', 'Yes', 'No']
        },
        {
          id: 5,
          heading: 'Nervous System',
          quetion: 'Have you experienced Fits?',
          answer: ['Select', 'Yes', 'No']
        },
        {
          id: 6,
          heading: 'Psychological Status',
          quetion: 'Are you feeling overwhelmed and/or losing control at home or at work?',
          answer: ['Select', 'Yes', 'No']
        },
        {
          id: 7,
          quetion: 'Are you getting easily irritable and/or frustrated- more so than your usual self?',
          answer: ['Select', 'Yes', 'No']
        },
        {
          id: 8,
          quetion: 'Have you been feeling low/depressed or anxious?',
          answer: ['Select', 'Yes', 'No']
        },
        {
          id: 9,
          quetion: 'Do you have frequent aches, pains, tense muscles?',
          answer: ['Select', 'Yes', 'No']
        },
        {
          id: 10,
          quetion: 'Have you been experiencing frequent colds and infections?',
          answer: ['Select', 'Yes', 'No']
        }
      ];

  constructor() { }

  ngOnInit() {
  }

}
