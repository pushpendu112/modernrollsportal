import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalGeneralsComponent } from './physical-generals.component';

describe('PhysicalGeneralsComponent', () => {
  let component: PhysicalGeneralsComponent;
  let fixture: ComponentFixture<PhysicalGeneralsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysicalGeneralsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicalGeneralsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
