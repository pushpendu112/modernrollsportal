import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import * as CryptoJS from 'crypto-js';
import {JuniorDoctorService } from './../../junior-doctor.service';
import { JuniorDoctorComponent } from '../../junior-doctor.component';

@Component({
  selector: 'app-physical-generals',
  templateUrl: './physical-generals.component.html',
  styleUrls: ['./physical-generals.component.scss']
})
export class PhysicalGeneralsComponent implements OnInit {
   private readonly notifier: NotifierService;
  physicalGeneral: FormGroup;
  secret = 'i_have_some_small_master_secret_live_pin';
  empId;
  orgId;
  centerId;
  patientId;
  userToken: any;
  patientObj: any;
  physicalGeneralObj: any;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private juniorDoctorService : JuniorDoctorService,
              private radioStatusActive: JuniorDoctorComponent,
              private spinner: NgxSpinnerService,
              private notifierService: NotifierService) { 
              this.notifier = notifierService;  }


  ngOnInit() {
    this.PatientRegisterForms();
     const ptid = localStorage.getItem('_id');
    this.patientId = JSON.parse(ptid)
    const getjwt = localStorage.getItem('jwttoken');
    this.userToken = this.conversionDecrypt(getjwt);
    this.getMedicalHistoryByPatientId();
  }

  PatientRegisterForms() {
    this.physicalGeneral = this.formBuilder.group({
      appetite: this.formBuilder.group({
        increase: [''],
        decreased: [''],
        easysatiety: [''],
        normal: [''],
        appetiteExtra: [''],
      }),
      thirst: this.formBuilder.group({
        thirsty: [''],
        thistles: [''],
        litersperday: [''],
        thirstyExtra: ['']
      }),
      desires: [''],
      addictions: this.formBuilder.group({
        tea: [''],
        coffee: [''],
        coffeeperday: [''],
        alcohal: [''],
        chronic: [''],
        alcohalpegsperday: [''],
        socialSmoking: [''],
        smokeChronic: [''],
        smokeChronicperday: [''],
        addictionsExtra: ['']
      }),
      aversions: [''],
      intolerance: [''],
      aggravationFrom: [''],
      stools: [''],
      urine: [''],
      perspiration: this.formBuilder.group({
        profuse: [''],
        scanty: [''],
        stainingLinen: [''],
        offensiveness: [''],
        perspirationothers: ['']
      }),

      sleep: [''],
      dreams: [''],
      thermalReaction: this.formBuilder.group({
        cold: [''],
        colddampColdAgg: [''],
        colddryColdAgg: [''],
        warmth: [''],
        warmthdampColdAgg: [''],
        warmthdryColdAgg: [''],
        thermalReactionExtra: [''],
      }),

      timeofAggravation: [''],

      sideofbodyeffected: this.formBuilder.group({
        right: [''],
        left: [''],
        rightupperLeftlower: [''],
        rightlowerLeftupper: [''],
        bilateral: [''],
        sideofBodyAffectedOthers: ['']
      }),
      physicalRestlessness: [''],
      occupationHazards: [''],
      otherFeatures: ['']
    });
  }
  onSubmit() {
    console.log(this.physicalGeneral.value);
    if(this.patientObj.length ===0){
      this.spinner.hide();
      this.notifier.notify('error', 'Something went wrong please try again later!')
    }

    this.physicalGeneralObj = this.physicalGeneral.value
      this.patientObj.forEach(physicalgeneral=>{
         const physicalGenObj = physicalgeneral.physicalGeneralHistoryObj;
         physicalGenObj.appetite.push(this.physicalGeneralObj.appetite);
         physicalGenObj.thirst.push(this.physicalGeneralObj.thirst);
         physicalGenObj.addictionsValues.push(this.physicalGeneralObj.addictions);
         physicalGenObj.perspiration.push(this.physicalGeneralObj.perspiration);
         physicalGenObj.thermalReaction.push(this.physicalGeneralObj.thermalReaction);
         physicalGenObj.sideOdBodyAffected.push(this.physicalGeneralObj.sideofbodyeffected);
         physicalGenObj.desireComment = this.physicalGeneralObj.desires;
         physicalGenObj.aversionsComment = this.physicalGeneralObj.aversions;
         physicalGenObj.intolerancesComment = this.physicalGeneralObj.intolerance;
         physicalGenObj.aggravationFromComment = this.physicalGeneralObj.aggravationFrom;
         physicalGenObj.stoolsComment = this.physicalGeneralObj.stools;
         physicalGenObj.urineComment = this.physicalGeneralObj.urine;
         physicalGenObj.sleepComment = this.physicalGeneralObj.sleep;
         physicalGenObj.dreamsComment = this.physicalGeneralObj.dreams;
         physicalGenObj.timeOfAggravationComment = this.physicalGeneralObj.timeofAggravation;
         physicalGenObj.physicalRestlessnessComment = this.physicalGeneralObj.physicalRestlessness;
         physicalGenObj.occupationHazardsComment = this.physicalGeneralObj.occupationHazards;
         physicalGenObj.otherFeaturesComment = this.physicalGeneralObj.otherFeatures;

         // console.log('physicalGenObj');
         // console.log(physicalGenObj);
         this.spinner.show();
         this.updateMedicalHistory(this.patientObj[0]);
     });
    // this.router.navigate(['/dashboard/patientInfo/MentalGenerals']);
   
  }


  getMedicalHistoryByPatientId(){
   const headers = new Headers(
        {
            'accept': 'application/json',
            'x-access-token': this.userToken
        });
     this.juniorDoctorService.getMedicalHistoryByPatientId(this.patientId, headers).subscribe(getPatientDetail=>{
         this.patientObj = getPatientDetail.patientData;
         this.setMedicalHistoryByPatientId();
         console.log('this.patientObj'); 
         console.log(this.patientObj);
          this.patientObj.forEach(obsGynac=>{
            const obsGynacObj = obsGynac.oBSAndGynacHistoryObj
       
          })   
     })
  }

  updateMedicalHistory(patientHistory){
     const headers = new Headers(
          {
              'accept': 'application/json',
              'x-access-token': this.userToken
          });
     this.juniorDoctorService.updateMedicalHistory(patientHistory, headers).subscribe(data=>{
       // console.log('data data data ')
       // console.log(data);
        this.spinner.hide();
        this.router.navigate(['/dashboard/Junior Doctor/MentalGenerals']);
        this.radioStatusActive.activeRadioButton('mentalGenerals');

     },
     err=>{
        this.spinner.hide();
        this.notifier.notify('error', 'Something went wrong please try again!')
     });
  }

  setMedicalHistoryByPatientId(){
    this.patientObj.forEach(ptObject=>{
      const ptObjectValue = ptObject.physicalGeneralHistoryObj;
      ptObjectValue.appetite.forEach(appetite=> {
            this.physicalGeneral.get('appetite').patchValue(appetite);
      });
      ptObjectValue.thirst.forEach(thirst=> {
            this.physicalGeneral.get('thirst').patchValue(thirst);
      });
      ptObjectValue.addictionsValues.forEach(addictions=> {
            this.physicalGeneral.get('addictions').patchValue(addictions);
      });
      ptObjectValue.perspiration.forEach(perspiration=> {
            this.physicalGeneral.get('perspiration').patchValue(perspiration);
      });
      ptObjectValue.thermalReaction.forEach(thermalReaction=> {
            this.physicalGeneral.get('thermalReaction').patchValue(thermalReaction);
      });
      ptObjectValue.sideOdBodyAffected.forEach(sideOdBodyAffected=> {
            this.physicalGeneral.get('sideofbodyeffected').patchValue(sideOdBodyAffected);
      });
      ptObjectValue.sideOdBodyAffected.forEach(sideOdBodyAffected=> {
            this.physicalGeneral.get('sideofbodyeffected').patchValue(sideOdBodyAffected);
      });

      this.physicalGeneral.get('desires').patchValue(ptObjectValue.desireComment);
      this.physicalGeneral.get('aversions').patchValue(ptObjectValue.aversionsComment);
      this.physicalGeneral.get('intolerance').patchValue(ptObjectValue.intolerancesComment);
      this.physicalGeneral.get('aggravationFrom').patchValue(ptObjectValue.aggravationFromComment);
      this.physicalGeneral.get('stools').patchValue(ptObjectValue.stoolsComment);
      this.physicalGeneral.get('urine').patchValue(ptObjectValue.urineComment);
      this.physicalGeneral.get('sleep').patchValue(ptObjectValue.sleepComment);
      this.physicalGeneral.get('dreams').patchValue(ptObjectValue.dreamsComment);
      this.physicalGeneral.get('timeofAggravation').patchValue(ptObjectValue.timeOfAggravationComment);
      this.physicalGeneral.get('physicalRestlessness').patchValue(ptObjectValue.physicalRestlessnessComment);
      this.physicalGeneral.get('occupationHazards').patchValue(ptObjectValue.occupationHazardsComment);
      this.physicalGeneral.get('otherFeatures').patchValue(ptObjectValue.otherFeaturesComment);

       console.log('ptObjectValue');
       console.log(ptObjectValue);
    });
  }


 conversionEncrypt(encrypt) {
      try {
          return CryptoJS.AES.encrypt(JSON.stringify(encrypt), this.secret).toString();
      } catch (e) {
          console.log(e);
      }
  }
 conversionDecrypt(decrypt) {
      try {
          const bytes = CryptoJS.AES.decrypt(decrypt, this.secret);
          if (bytes.toString()) {
              return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
          }
          return decrypt;
      } catch (e) {
          console.log(e);
      }
  }
}
