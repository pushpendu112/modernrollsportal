import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import * as CryptoJS from 'crypto-js';
import { JuniorDoctorComponent } from '../../junior-doctor.component';
import {JuniorDoctorService } from './../../junior-doctor.service';
@Component({
  selector: 'app-physical-makeup',
  templateUrl: './physical-makeup.component.html',
  styleUrls: ['./physical-makeup.component.scss']
})
export class PhysicalMakeupComponent implements OnInit {
   private readonly notifier: NotifierService;
  physicalMakeup: FormGroup;
  secret = 'i_have_some_small_master_secret_live_pin';
  empId;
  orgId;
  centerId;
  patientId;
  userToken: any;
  patientObj: any;
  physicalMakeupObj: any;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private juniorDoctorService : JuniorDoctorService,
              private radioStatusActive: JuniorDoctorComponent,
              private spinner: NgxSpinnerService,
              private notifierService: NotifierService) { 
              this.notifier = notifierService; 
            }

  ngOnInit() {
    this.PatientRegisterForms();

     const ptid = localStorage.getItem('_id');
    this.patientId = JSON.parse(ptid)
    const getjwt = localStorage.getItem('jwttoken');
    this.userToken = this.conversionDecrypt(getjwt);
    this.getMedicalHistoryByPatientId();
  }
  // nextpage() {
  //   this.router.navigate(['/dashboard/patientInfo/PhysicalGenerals']);
  // }
  PatientRegisterForms() {
    this.physicalMakeup = this.formBuilder.group({

      appearance: this.formBuilder.group({
        fastidious: [''],
        raged: [''],
        normal: [''],
        apperanceothers: ['']
      }),
      built: this.formBuilder.group({
        thin: [''],
        moderate: [''],
        well: [''],
        overWeight: [''],
        obese: [''],
        tall: [''],
        shortStatured: [''],
        normalBuilt: [''],
        dwarfish: [''],
        builtOthers: ['']
      }),
      nourishment: this.formBuilder.group({
        under: [''],
        nourisModerate: [''],
        nourishWell: [''],
        anemic: [''],
        nourishmentOthers: ['']
      }),
      face: this.formBuilder.group({
        round: [''],
        ovel: [''],
        angular: [''],
        faceOthers: ['']
      }),
      nose: this.formBuilder.group({
        sharp: [''],
        carbon: [''],
        blunt: [''],
        noseOthers: ['']
      }),
      eysColor: [''],
      ear: [''],
      foreHead: this.formBuilder.group({
        broad: [''],
        small: [''],
        foreheadOthers: ['']
      }),
      wrinkleOnForeHead: this.formBuilder.group({
        vertical: [''],
        horizontal: [''],
        wrinklesonforeheadOthers: ['']
      }),
      hairAppearance: this.formBuilder.group({
        soft: [''],
        hard: [''],
        curly: [''],
        hairappearanceOthers: ['']
      }),
      hairDistribution: [''],
      baldhead: this.formBuilder.group({
        complete: [''],
        frontal: [''],
        parietal: [''],
        notBald: [''],
        baldheadOthers: ['']
      }),
      grayHair: this.formBuilder.group({
        severe: [''],
        mild: [''],
        grayHarirModerate: [''],
        grayhairOthers: [''],
      }),
      tongueCoating: ['']
    });
  }



  onSubmit() {
    // console.log(this.PhysicalMakeup.value);
     if(this.patientObj.length ===0){
      this.spinner.hide();
      this.notifier.notify('error', 'Something went wrong please try again later!')
    }
    
    this.physicalMakeupObj = this.physicalMakeup.value
     this.patientObj.forEach(physicalObj=>{
       const physicalObjValues = physicalObj.physicalMakeUpHistoryObj;
       physicalObjValues.appearance.push( this.physicalMakeupObj.appearance)
       physicalObjValues.built.push( this.physicalMakeupObj.built)
       physicalObjValues.nourishment.push( this.physicalMakeupObj.nourishment)
       physicalObjValues.face.push( this.physicalMakeupObj.face)
       physicalObjValues.nose.push( this.physicalMakeupObj.nose)
       physicalObjValues.foreHead.push( this.physicalMakeupObj.foreHead)
       physicalObjValues.wrinkleOnForeHead.push( this.physicalMakeupObj.wrinkleOnForeHead)
       physicalObjValues.hairAppearance.push( this.physicalMakeupObj.hairAppearance)
       physicalObjValues.baldhead.push( this.physicalMakeupObj.baldhead)
       physicalObjValues.greyHead.push( this.physicalMakeupObj.grayHair)
       physicalObjValues.eysColorComment = this.physicalMakeupObj.eysColor
       physicalObjValues.earComment = this.physicalMakeupObj.ear
       physicalObjValues.hairDistributionComment = this.physicalMakeupObj.hairDistribution
       physicalObjValues.tongueCoatingComment = this.physicalMakeupObj.tongueCoating;
       // console.log('physicalObjValues');
       // console.log(physicalObjValues);
       this.spinner.show();
       this.updateMedicalHistory(this.patientObj[0]);
     })
    // this.router.navigate(['/dashboard/patientInfo/PhysicalGenerals']);
  }



  getMedicalHistoryByPatientId(){
   const headers = new Headers(
        {
            'accept': 'application/json',
            'x-access-token': this.userToken
        });
     this.juniorDoctorService.getMedicalHistoryByPatientId(this.patientId, headers).subscribe(getPatientDetail=>{
         this.patientObj = getPatientDetail.patientData;
         this.setMedicalHistoryByPatientId();
         console.log('this.patientObj'); 
         console.log(this.patientObj);
          this.patientObj.forEach(obsGynac=>{
            const obsGynacObj = obsGynac.oBSAndGynacHistoryObj;

          })
     });
  }

  updateMedicalHistory(patientHistory) {
     const headers = new Headers(
          {
              'accept': 'application/json',
              'x-access-token': this.userToken
          });
     this.juniorDoctorService.updateMedicalHistory(patientHistory, headers).subscribe(data=> {
       // console.log('data data data ')
       // console.log(data);
    this.spinner.hide();
    this.router.navigate(['/dashboard/Junior Doctor/PhysicalGenerals']);
    this.radioStatusActive.activeRadioButton('physicalGenerals');

     },
     err=>{
        this.spinner.hide();
        this.notifier.notify('error', 'Something went wrong please try again!')
     });
  }

  setMedicalHistoryByPatientId() {
    this.patientObj.forEach(patientHistory=> {
      const physicalMakeup = patientHistory.physicalMakeUpHistoryObj;
       console.log('physicalMakeup');
       console.log(physicalMakeup);
      physicalMakeup.appearance.forEach(appearance => {
        this.physicalMakeup.get('appearance').patchValue(appearance);
      });
      physicalMakeup.built.forEach(built => {
        this.physicalMakeup.get('built').patchValue(built);
      });
      physicalMakeup.nourishment.forEach(nourishment => {
        this.physicalMakeup.get('nourishment').patchValue(nourishment);
      });
      physicalMakeup.face.forEach(face => {
        this.physicalMakeup.get('face').patchValue(face);
      });
      physicalMakeup.nose.forEach(nose => {
        this.physicalMakeup.get('nose').patchValue(nose);
      });
      physicalMakeup.foreHead.forEach(foreHead => {
        this.physicalMakeup.get('foreHead').patchValue(foreHead);
      });
      physicalMakeup.wrinkleOnForeHead.forEach(wrinkleOnForeHead => {
        this.physicalMakeup.get('wrinkleOnForeHead').patchValue(wrinkleOnForeHead);
      });
      physicalMakeup.hairAppearance.forEach(hairAppearance => {
        this.physicalMakeup.get('hairAppearance').patchValue(hairAppearance);
      });
      physicalMakeup.baldhead.forEach(baldhead => {
        this.physicalMakeup.get('baldhead').patchValue(baldhead);
      });
      physicalMakeup.greyHead.forEach(greyHead => {
        this.physicalMakeup.get('grayHair').patchValue(greyHead);
      });
      this.physicalMakeup.get('eysColor').patchValue(physicalMakeup.eysColorComment);
      this.physicalMakeup.get('ear').patchValue(physicalMakeup.earComment);
      this.physicalMakeup.get('hairDistribution').patchValue(physicalMakeup.hairDistributionComment);
      this.physicalMakeup.get('tongueCoating').patchValue(physicalMakeup.tongueCoatingComment);
    });
  }


 conversionEncrypt(encrypt) {
      try {
          return CryptoJS.AES.encrypt(JSON.stringify(encrypt), this.secret).toString();
      } catch (e) {
          console.log(e);
      }
  }
 conversionDecrypt(decrypt) {
      try {
          const bytes = CryptoJS.AES.decrypt(decrypt, this.secret);
          if (bytes.toString()) {
              return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
          }
          return decrypt;
      } catch (e) {
          console.log(e);
      }
  }
}
