import { Component, OnInit, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import * as CryptoJS from 'crypto-js';
import { SubSink } from 'subsink';

import { JuniorDoctorComponent } from '../../junior-doctor.component';
import {JuniorDoctorService } from './../../junior-doctor.service';

@Component({
  selector: 'app-family-medical-history',
  templateUrl: './family-medical-history.component.html',
  styleUrls: ['./family-medical-history.component.scss']
})
export class FamilyMedicalHistoryComponent implements OnInit, OnDestroy {
   private readonly notifier: NotifierService;
   private subs = new SubSink();
   familyHistory:any;
   secret = 'i_have_some_small_master_secret_live_pin';
   empId;
   orgId;
   centerId;
   patientId;
   userToken: any;
   patientObj: any;

  constructor(private router: Router,
              private modalService: NgbModal,
              private juniorDoctorService : JuniorDoctorService,
              private radioStatusActive: JuniorDoctorComponent ,
              private spinner: NgxSpinnerService,
              private notifierService: NotifierService) {
                this.notifier = notifierService;
    this.data = {};
    this.data.isAllSelected = false;
    this.data.isAllCollapsed = false;


    this.data.ParentChildchecklist = [
       {
        id: 1, value: 'Do you have a Family History of Allergy?', isSelected: false, isClosed: false,
        childList: [
          {
            title: 'Maternal',
            maternal: [
              {
                id: 1, value: 'Mother', isSelected: false
              },
              {
                id: 2, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 3, value: 'Gr. Father', isSelected: false
              },
              {
                id: 4, value: 'Uncle', isSelected: false
              },
              {
                id: 5, value: 'Aunt', isSelected: false
              }
            ]
          }
        ],
        childList2: [
          {
            title: 'Paternal',
            paternal: [
              {
                id: 6, value: 'Mother', isSelected: false
              },
              {
                id: 7, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 8, value: 'Gr. Father', isSelected: false
              },
              {
                id: 9, value: 'Uncle', isSelected: false
              },
              {
                id: 10, value: 'Aunt', isSelected: false
              }
            ]
          }
        ]
      },
      {
        id: 2, value: 'Do you have a Family History of Asthma?', isSelected: false, isClosed: false,
        childList: [
          {
            title: 'Maternal',
            maternal: [
              {
                id: 1, value: 'Mother', isSelected: false
              },
              {
                id: 2, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 3, value: 'Gr. Father', isSelected: false
              },
              {
                id: 4, value: 'Uncle', isSelected: false
              },
              {
                id: 5, value: 'Aunt', isSelected: false
              }
            ]
          }
        ],
        childList2: [
          {
            title: 'Paternal',
            paternal: [
              {
                id: 6, value: 'Mother', isSelected: false
              },
              {
                id: 7, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 8, value: 'Gr. Father', isSelected: false
              },
              {
                id: 9, value: 'Uncle', isSelected: false
              },
              {
                id: 10, value: 'Aunt', isSelected: false
              }
            ]
          }
        ]
      },
      {
        id: 3, value: 'Do you have a Family History of Cancer?', isSelected: false, isClosed: false,
        childList: [
          {
            title: 'Maternal',
            maternal: [
              {
                id: 1, value: 'Mother', isSelected: false
              },
              {
                id: 2, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 3, value: 'Gr. Father', isSelected: false
              },
              {
                id: 4, value: 'Uncle', isSelected: false
              },
              {
                id: 5, value: 'Aunt', isSelected: false
              }
            ]
          }
        ],
        childList2: [
          {
            title: 'Paternal',
            paternal: [
              {
                id: 6, value: 'Mother', isSelected: false
              },
              {
                id: 7, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 8, value: 'Gr. Father', isSelected: false
              },
              {
                id: 9, value: 'Uncle', isSelected: false
              },
              {
                id: 10, value: 'Aunt', isSelected: false
              }
            ]
          }
        ]
      },
      {
        id: 4, value: 'Do you have a Family History of DM?', isSelected: false, isClosed: false,
        childList: [
          {
            title: 'Maternal',
            maternal: [
              {
                id: 1, value: 'Mother', isSelected: false
              },
              {
                id: 2, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 3, value: 'Gr. Father', isSelected: false
              },
              {
                id: 4, value: 'Uncle', isSelected: false
              },
              {
                id: 5, value: 'Aunt', isSelected: false
              }
            ]
          }
        ],
        childList2: [
          {
            title: 'Paternal',
            paternal: [
              {
                id: 6, value: 'Mother', isSelected: false
              },
              {
                id: 7, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 8, value: 'Gr. Father', isSelected: false
              },
              {
                id: 9, value: 'Uncle', isSelected: false
              },
              {
                id: 10, value: 'Aunt', isSelected: false
              }
            ]
          }
        ]
      },
      {
        id: 5, value: 'Do you have a Family History of Epilepsy?', isSelected: false, isClosed: false,
        childList: [
          {
            title: 'Maternal',
            maternal: [
              {
                id: 1, value: 'Mother', isSelected: false
              },
              {
                id: 2, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 3, value: 'Gr. Father', isSelected: false
              },
              {
                id: 4, value: 'Uncle', isSelected: false
              },
              {
                id: 5, value: 'Aunt', isSelected: false
              }
            ]
          }
        ],
        childList2: [
          {
            title: 'Paternal',
            paternal: [
              {
                id: 6, value: 'Mother', isSelected: false
              },
              {
                id: 7, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 8, value: 'Gr. Father', isSelected: false
              },
              {
                id: 9, value: 'Uncle', isSelected: false
              },
              {
                id: 10, value: 'Aunt', isSelected: false
              }
            ]
          }
        ]
      },
      {
        id: 6, value: 'Do you have a Family History of HTN?', isSelected: false, isClosed: false,
        childList: [
          {
            title: 'Maternal',
            maternal: [
              {
                id: 1, value: 'Mother', isSelected: false
              },
              {
                id: 2, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 3, value: 'Gr. Father', isSelected: false
              },
              {
                id: 4, value: 'Uncle', isSelected: false
              },
              {
                id: 5, value: 'Aunt', isSelected: false
              }
            ]
          }
        ],
        childList2: [
          {
            title: 'Paternal',
            paternal: [
              {
                id: 6, value: 'Mother', isSelected: false
              },
              {
                id: 7, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 8, value: 'Gr. Father', isSelected: false
              },
              {
                id: 9, value: 'Uncle', isSelected: false
              },
              {
                id: 10, value: 'Aunt', isSelected: false
              }
            ]
          }
        ]
      },
      {
        id: 8, value: 'Do you have a Family History of Obesity?', isSelected: false, isClosed: false,
        childList: [
          {
            title: 'Maternal',
            maternal: [
              {
                id: 1, value: 'Mother', isSelected: false
              },
              {
                id: 2, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 3, value: 'Gr. Father', isSelected: false
              },
              {
                id: 4, value: 'Uncle', isSelected: false
              },
              {
                id: 5, value: 'Aunt', isSelected: false
              }
            ]
          }
        ],
        childList2: [
          {
            title: 'Paternal',
            paternal: [
              {
                id: 6, value: 'Mother', isSelected: false
              },
              {
                id: 7, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 8, value: 'Gr. Father', isSelected: false
              },
              {
                id: 9, value: 'Uncle', isSelected: false
              },
              {
                id: 10, value: 'Aunt', isSelected: false
              }
            ]
          }
        ]
      },
      {
        id: 9, value: 'Do you have a Family History of Skin?', isSelected: false, isClosed: false,
        childList: [
          {
            title: 'Maternal',
            maternal: [
              {
                id: 1, value: 'Mother', isSelected: false
              },
              {
                id: 2, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 3, value: 'Gr. Father', isSelected: false
              },
              {
                id: 4, value: 'Uncle', isSelected: false
              },
              {
                id: 5, value: 'Aunt', isSelected: false
              }
            ]
          }
        ],
        childList2: [
          {
            title: 'Paternal',
            paternal: [
              {
                id: 6, value: 'Mother', isSelected: false
              },
              {
                id: 7, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 8, value: 'Gr. Father', isSelected: false
              },
              {
                id: 9, value: 'Uncle', isSelected: false
              },
              {
                id: 10, value: 'Aunt', isSelected: false
              }
            ]
          }
        ]
      },
      {
        id: 10, value: 'Do you have a Family History of T.B?', isSelected: false, isClosed: false,
        childList: [
          {
            title: 'Maternal',
            maternal: [
              {
                id: 1, value: 'Mother', isSelected: false
              },
              {
                id: 2, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 3, value: 'Gr. Father', isSelected: false
              },
              {
                id: 4, value: 'Uncle', isSelected: false
              },
              {
                id: 5, value: 'Aunt', isSelected: false
              }
            ]
          }
        ],
        childList2: [
          {
            title: 'Paternal',
            paternal: [
              {
                id: 6, value: 'Mother', isSelected: false
              },
              {
                id: 7, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 8, value: 'Gr. Father', isSelected: false
              },
              {
                id: 9, value: 'Uncle', isSelected: false
              },
              {
                id: 10, value: 'Aunt', isSelected: false
              }
            ]
          }
        ]
      },
      {
        id: 20, value: 'Do you have a Family History of Thyroid?', isSelected: false, isClosed: false,
        childList: [
          {
            title: 'Maternal',
            maternal: [
              {
                id: 1, value: 'Mother', isSelected: false
              },
              {
                id: 2, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 3, value: 'Gr. Father', isSelected: false
              },
              {
                id: 4, value: 'Uncle', isSelected: false
              },
              {
                id: 5, value: 'Aunt', isSelected: false
              }
            ]
          }
        ],
        childList2: [
          {
            title: 'Paternal',
            paternal: [
              {
                id: 6, value: 'Mother', isSelected: false
              },
              {
                id: 7, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 8, value: 'Gr. Father', isSelected: false
              },
              {
                id: 9, value: 'Uncle', isSelected: false
              },
              {
                id: 10, value: 'Aunt', isSelected: false
              }
            ]
          }
        ]
      },
      {
        id: 30, value: 'Do you have a Family History of N A D?', isSelected: false, isClosed: false,
        childList: [
          {
            title: 'Maternal',
            maternal: [
              {
                id: 1, value: 'Mother', isSelected: false
              },
              {
                id: 2, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 3, value: 'Gr. Father', isSelected: false
              },
              {
                id: 4, value: 'Uncle', isSelected: false
              },
              {
                id: 5, value: 'Aunt', isSelected: false
              }
            ]
          }
        ],
        childList2: [
          {
            title: 'Paternal',
            paternal: [
              {
                id: 6, value: 'Mother', isSelected: false
              },
              {
                id: 7, value: 'Gr. Mother', isSelected: false
              },
              {
                id: 8, value: 'Gr. Father', isSelected: false
              },
              {
                id: 9, value: 'Uncle', isSelected: false
              },
              {
                id: 10, value: 'Aunt', isSelected: false
              }
            ]
          }
        ]
      }
    ];
  }

  data: any;


  model: NgbDateStruct;
   ngOnInit() {
    const ptid = localStorage.getItem('_id');
    this.patientId = JSON.parse(ptid)
    const getjwt = localStorage.getItem('jwttoken');
    this.userToken = this.conversionDecrypt(getjwt);
    this.getMedicalHistoryByPatientId();
  }


  // Click event on parent checkbox
  parentCheck(parentObj) {
    for (var i = 0; i < parentObj.childList.length; i++) {
      parentObj.childList[i].isSelected = parentObj.isSelected;
    }
    if (parentObj.isSelected === true) {
      
      for (let i = 0; i < parentObj.childList.length; i++) {
        // parentObj.childList[0].maternal.isSelected = parentObj.isSelected;
        // console.log(parentObj.childList[0].maternal.isSelected = parentObj.isSelected);
      }
      for (let i = 0; i < parentObj.childList2.length; i++) {
        // parentObj.childList2[0].maternal.isSelected = parentObj.isSelected;
        // console.log(parentObj.childList2[0].paternal.isSelected = parentObj.isSelected);
      }
   
    }


  }

  // Click event on child checkbox
  childCheck(parentObj) {
    console.log(parentObj);
    this.familyHistory= this.data.ParentChildchecklist;
    console.log('parentObj ===>>>'+JSON.stringify(parentObj));
    // console.log('childList ===>>>'+JSON.stringify(childList));

   

    const maternal = parentObj.childList[0].maternal;
    const paternal = parentObj.childList2[0].paternal;
    const maternalPaternal = [...maternal, ...paternal];
    console.log('merged');
     console.log(maternalPaternal);

    parentObj.isSelected = maternalPaternal.find(function (itemChild: any) {
       return itemChild.isSelected === true;

    });

    // console.log('paternal.isSelected');
    // console.log(parentObj.isSelected)




  }

  submitFamilyHistory(){
   this.spinner.show();
   if(this.familyHistory===undefined){
      this.spinner.hide();
      this.notifier.notify('error', 'No Family History Selected');
      this.router.navigate(['/dashboard/Junior Doctor/ObsGynacHistory']);
      this.radioStatusActive.activeRadioButton('obsGynacHistory');
   }

  this.patientObj.forEach(familyHistory=> {
           familyHistory.familyMedicalHistoryObj=this.familyHistory;

          console.log('familyHistory');
         console.log(this.patientObj[0]);
         this.updateMedicalHistory(this.patientObj[0]);
     });
  }


  childCheck2(parentObj, childList) {
    this.valueCheck('', childList);
    // console.log(childList);
    // parentObj.isSelected = childObj.every(function (itemChild: any) {
    // parentObj.isSelected = childList.find(function (itemChild: any) {

    //   return itemChild.isSelected === true;

    // });
  }
  valueCheck(value1?: any, value2?: any) {
    let object1;
    let object2;
    if (value1 && value2) {
      object1 = value1;
      object2 = value2;
      // const merged = { ...object1, ...object2 };
      // console.log('merged');
      // console.log(merged);
    } else if (value1) {
      object1 = value1;
      // const merged2 = { ...object1 };
      // console.log('merged2');
      // console.log(merged2);
    } else if (value2) {

      object2 = value2;
      // const merged3 = { ...object2 };
      // console.log('merged3');
      // console.log(merged3);
    }
    if (object1) {
      object1.find(function (itemChild: any) {

        return itemChild.isSelected === true;

      });
    }


  }


 getMedicalHistoryByPatientId() {
   const headers = new Headers(
        {
            'accept': 'application/json',
            'x-access-token': this.userToken
        });
    this.subs.sink = this.juniorDoctorService.getMedicalHistoryByPatientId(this.patientId, headers).subscribe(getPatientDetail=> {
         this.patientObj = getPatientDetail.patientData;
         this.setMedicalHistoryByPatientId();
         console.log('this.patientObj');
         console.log(this.patientObj);
          this.patientObj.forEach(obsGynac=> {
            const obsGynacObj = obsGynac.oBSAndGynacHistoryObj;

          });
     });
  }

  updateMedicalHistory(patientHistory) {
     const headers = new Headers(
          {
              'accept': 'application/json',
              'x-access-token': this.userToken
          });
    this.subs.sink =  this.juniorDoctorService.updateMedicalHistory(patientHistory, headers).subscribe(data => {
        this.spinner.hide();
        this.router.navigate(['/dashboard/Junior Doctor/ObsGynacHistory']);
        this.radioStatusActive.activeRadioButton('obsGynacHistory');
       //console.log(data);

     },
     err=>{
       this.spinner.hide();
       this.notifier.notify('error', 'Something went wrong please try again!')
     });
  }

  setMedicalHistoryByPatientId() {
    this.patientObj.forEach(familyHistory => {
      if (familyHistory.familyMedicalHistoryObj.length === 0) {
        this.data.ParentChildchecklist;
      } else {
        this.data.ParentChildchecklist = familyHistory.familyMedicalHistoryObj;
      }
    });
  }


 conversionEncrypt(encrypt) {
      try {
          return CryptoJS.AES.encrypt(JSON.stringify(encrypt), this.secret).toString();
      } catch (e) {
          console.log(e);
      }
  }
 conversionDecrypt(decrypt) {
      try {
          const bytes = CryptoJS.AES.decrypt(decrypt, this.secret);
          if (bytes.toString()) {
              return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
          }
          return decrypt;
      } catch (e) {
          console.log(e);
      }
  }


  // Click event on master select
  selectUnselectAll(obj) {
    obj.isAllSelected = !obj.isAllSelected;
    for (let i = 0; i < obj.ParentChildchecklist.length; i++) {
      obj.ParentChildchecklist[i].isSelected = obj.isAllSelected;
      for (let j = 0; j < obj.ParentChildchecklist[i].childList.length; j++) {
        obj.ParentChildchecklist[i].childList[j].isSelected = obj.isAllSelected;
      }
    }
  }

  // Expand/Collapse event on each parent
  expandCollapse(obj) {
    obj.isClosed = !obj.isClosed;
  }

  // Master expand/ collapse event
  expandCollapseAll(obj) {
    for (let i = 0; i < obj.ParentChildchecklist.length; i++) {
      obj.ParentChildchecklist[i].isClosed = !obj.isAllCollapsed;
    }
    obj.isAllCollapsed = !obj.isAllCollapsed;
  }



  setPatientConformation(value) {
    console.log(value);
    console.log(this.model);
    this.modalService.dismissAll();

  }

  // onSubmit() {
  //   console.log(this.FamilyMedical.value);
  //   this.router.navigate(['/dashboard/patientInfo/ObsGynacHistory']);
  // }

ngOnDestroy() {
  this.subs.unsubscribe();
  console.log(`SubSinkComponent destroyed`)
}


}
