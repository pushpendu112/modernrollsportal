import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import * as CryptoJS from 'crypto-js';

import {JuniorDoctorService } from './../../junior-doctor.service';
import { JuniorDoctorComponent } from '../../junior-doctor.component';

@Component({
  selector: 'app-past-medical-history',
  templateUrl: './past-medical-history.component.html',
  styleUrls: ['./past-medical-history.component.scss']
})
export class PastMedicalHistoryComponent implements OnInit {
    private readonly notifier: NotifierService;
    pastmedical: FormGroup;
    pastMedicalObj = { other: '',
                          antenatal:'',
                          postnatal:'' 
                        };

    selectedHistory: string[] = [];
    medicalHistoryData = ['Asthma', 'Chicken Pox', 'Epilepsy', 'Jaundice', 'Malaria', 'Measles', 'Typhoid','T B', 'Thyroid',
    'N A D'];
  profile: any;
   secret = 'i_have_some_small_master_secret_live_pin';
   empId;
   orgId;
   centerId;
   patientId;
   userToken: any;
   patientObj: any;
   patientHistoryText: any;
   patientHistoryCheckbox: any;


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private juniorDoctorService: JuniorDoctorService,
              private radioStatusActive: JuniorDoctorComponent,
              private spinner: NgxSpinnerService,
              private notifierService: NotifierService) {
                this.notifier = notifierService;
               }


  ngOnInit() {
    this.PatientRegisterForms();
    const getEmpId = localStorage.getItem('_id');
    this.empId = this.conversionDecrypt(getEmpId)
    this.orgId  = localStorage.getItem('orgId');
    this.centerId  = localStorage.getItem('centerId');
    const ptid = localStorage.getItem('patientId');
    this.patientId = JSON.parse(ptid)

    const getjwt = localStorage.getItem('jwttoken');
    this.userToken = this.conversionDecrypt(getjwt);
    // console.log('this.userToken');
    // console.log(this.userToken);
    //  console.log('empId', this.empId, 'orgId', this.orgId, 'CenterID',this.centerId,'PatientId', this.patientId )
    this.getMedicalHistoryByPatientId()


  }


  isSelected(value: string): boolean {
        return this.selectedHistory.indexOf(value) >= 0;
    }
    
  onChange(value: string, checked: boolean) {
       // console.log(value, checked);
        if (checked) {
            this.selectedHistory.push(value);
        } else {
            const index = this.selectedHistory.indexOf(value);
            this.selectedHistory.splice(index, 1);
        }
        // console.log(this.selectedHistory);
    }

  PatientRegisterForms() {
    this.pastmedical = this.formBuilder.group({
      antenatal: [''],
      postnatal: [''],
      role: ['']
    });
  }


  onSubmit() {
    console.log(this.pastmedical.value);
  }


  onPastMedicalHistory(){
    this.spinner.show();
    if(this.selectedHistory.length ===0){ 
    this.spinner.hide();
    this.radioStatusActive.activeRadioButton('familyHistory');
    this.router.navigate(['/dashboard/Junior Doctor/FamilyMedicalHistory']);
    }
    // console.log(this.pastMedicalHistory);
    // console.log(this.selectedHistory)
     this.patientObj.forEach(ptObj=>{
         ptObj.pastMedicalHistoryObj.push(this.pastMedicalObj)
         ptObj.pastMedicalHistoryObj.push(this.selectedHistory);
         // console.log( 'this.patientObj');
         // console.log( this.patientObj[0]);
         this.updateMedicalHistory(this.patientObj[0]);
     });

  }


  getMedicalHistoryByPatientId() {
   const headers = new Headers(
        {
            'accept': 'application/json',
            'x-access-token': this.userToken
        });
     this.juniorDoctorService.getMedicalHistoryByPatientId(this.patientId, headers).subscribe(getPatientDetail=> {
         this.patientObj = getPatientDetail.patientData;
         this.setMedicalHistoryByPatientId();
         // console.log('Get the Patient Medical Object');
         // console.log(this.patientObj);
     });
  }

  updateMedicalHistory(patientHistory) {
     const headers = new Headers(
          {
              'accept': 'application/json',
              'x-access-token': this.userToken
          });
     this.juniorDoctorService.updateMedicalHistory(patientHistory, headers).subscribe(data => {
          this.spinner.hide();
          this.radioStatusActive.activeRadioButton('familyHistory');
          this.router.navigate(['/dashboard/Junior Doctor/FamilyMedicalHistory']);
     }, err=>{
          this.spinner.hide();
          this.notifier.notify('error', 'Something went wrong please try again!')
     });
  }

  setMedicalHistoryByPatientId() {
    this.patientObj.forEach(patientHistory => {
      const pastHistory = patientHistory.pastMedicalHistoryObj;
      // console.log('pastHistory');
      // console.log(pastHistory);
      // console.log('pastHistory 0');
      // console.log(pastHistory[0]);
      // console.log('pastHistory 1');
      // console.log(pastHistory[1]);
     this.patientHistoryText = pastHistory[0];
     this.patientHistoryCheckbox = pastHistory[1];
     // console.log('this.patientHistoryText', this.patientHistoryText);
     // console.log('this.patientHistoryCheckbox', this.patientHistoryCheckbox);

     this.pastMedicalObj = this.patientHistoryText;
     this.medicalHistoryData.forEach(baseHistor => {
       this.patientHistoryCheckbox.forEach(dbHistory => {
         if (baseHistor === dbHistory) {
           this.onChange(baseHistor, true);
         }
       });
     });

    });
  }

   conversionEncrypt(encrypt) {
        try {
            return CryptoJS.AES.encrypt(JSON.stringify(encrypt), this.secret).toString();
        } catch (e) {
            console.log(e);
        }
    }
    conversionDecrypt(decrypt) {
        try {
            const bytes = CryptoJS.AES.decrypt(decrypt, this.secret);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return decrypt;
        } catch (e) {
            console.log(e);
        }
    }

}
