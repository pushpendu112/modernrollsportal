import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FileUploadModule } from 'primeng/fileupload';
import { DataTableModule } from 'primeng/datatable';
import { NgxSpinnerModule } from 'ngx-spinner';



import { JuniorDoctorRoutingModule } from './junior-doctor-routing.module';
import { JuniorDoctorComponent } from './junior-doctor.component';
import { FamilyMedicalHistoryComponent } from './components/family-medical-history/family-medical-history.component';
import { PersonalInformationComponent } from './components/personal-information/personal-information.component';
import { ReportedSymptomsComponent } from './components/reported-symptoms/reported-symptoms.component';
import { PersonalLifestyleHistoryComponent } from './components/personal-lifestyle-history/personal-lifestyle-history.component';
import { MedicationHistoryComponent } from './components/medication-history/medication-history.component';
import { GeneralInformationComponent } from './components/general-information/general-information.component';
import { ObsGynacHistoryComponent } from './components/obs-gynac-history/obs-gynac-history.component';
import { TreatmentHistoryComponent } from './components/treatment-history/treatment-history.component';
import { PhysicalMakeupComponent } from './components/physical-makeup/physical-makeup.component';
import { ClinicalFindingsComponent } from './components/clinical-findings/clinical-findings.component';
import { LabInvestigationsComponent } from './components/lab-investigations/lab-investigations.component';
import { GeneticConstitutionComponent } from './components/genetic-constitution/genetic-constitution.component';
import { PhysicalGeneralsComponent } from './components/physical-generals/physical-generals.component';
import { MentalGeneralsComponent } from './components/mental-generals/mental-generals.component';
import { SummaryComponent } from './components/summary/summary.component';
import { PatientqueueComponent } from './components/patientqueue/patientqueue.component';
import { NurseModule } from '../nurse/nurse.module';
import { ChiefComplaintsComponent } from './components/chief-complaints/chief-complaints.component';
import { PastMedicalHistoryComponent } from './components/past-medical-history/past-medical-history.component';


@NgModule({
  declarations: [JuniorDoctorComponent,
    PastMedicalHistoryComponent,
    FamilyMedicalHistoryComponent,
    PersonalInformationComponent,
    ReportedSymptomsComponent,
    PersonalLifestyleHistoryComponent,
    MedicationHistoryComponent,
    GeneralInformationComponent,
    ObsGynacHistoryComponent,
    TreatmentHistoryComponent,
    PhysicalMakeupComponent,
    ClinicalFindingsComponent,
    LabInvestigationsComponent,
    GeneticConstitutionComponent,
    ChiefComplaintsComponent,
    PhysicalGeneralsComponent,
    MentalGeneralsComponent,
    SummaryComponent,
    PatientqueueComponent],
  imports: [
    CommonModule,
    JuniorDoctorRoutingModule,
    NurseModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FileUploadModule,
    DataTableModule,
    NgxSpinnerModule
  ],
  exports: [JuniorDoctorComponent]
})
export class JuniorDoctorModule { }
