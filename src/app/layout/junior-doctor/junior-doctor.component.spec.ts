import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JuniorDoctorComponent } from './junior-doctor.component';

describe('JuniorDoctorComponent', () => {
  let component: JuniorDoctorComponent;
  let fixture: ComponentFixture<JuniorDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JuniorDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JuniorDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
