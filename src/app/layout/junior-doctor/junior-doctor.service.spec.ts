import { TestBed } from '@angular/core/testing';

import { JuniorDoctorService } from './junior-doctor.service';

describe('JuniorDoctorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JuniorDoctorService = TestBed.get(JuniorDoctorService);
    expect(service).toBeTruthy();
  });
});
