import { Component, OnInit } from '@angular/core';
import { TestDataService } from 'src/app/shared/services/test-data.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.scss']
})
export class DoctorComponent implements OnInit {

  title = 'Modern Rolls | Senior Doctor';

  generalInfoBoolean = true;

  investigationqueue = false;

  chiefComplaintsBoolean = true;
  chiefComplaintsActiveBoolean = false;
  chiefComplaintsCompletedBoolean = false;

  pastMedicalHistoryBoolean = true;
  pastMedicalHistoryActiveBoolean = false;
  pastMedicalHistoryCompletedBoolean = false;

  familyHistoryBoolean = true;
  familyHistoryActiveBoolean = false;
  familyHistoryCompletedBoolean = false;

  obsGynacHistoryBoolean = true;
  obsGynacHistoryActiveBoolean = false;
  obsGynacHistoryCompletedBoolean = false;

  treatmentHistoryBoolean = true;
  treatmentHistoryActiveBoolean = false;
  treatmentHistoryCompletedBoolean = false;

  labInvestigationHistoryBoolean = true;
  labInvestigationActiveHistoryBoolean = false;
  labInvestigationCompletedHistoryBoolean = false;

  clinicalFindingBoolean = true;
  clinicalFindingActiveBoolean = false;
  clinicalFindingCompletedBoolean = false;

  geneticConstitutionBoolean = true;
  geneticConstitutionActiveBoolean = false;
  geneticConstitutionCompletedBoolean = false;


  physicalMakeupBoolean = true;
  physicalMakeupActiveBoolean = false;
  physicalMakeupCompletedBoolean = false;


  physicalGeneralsBoolean = true;
  physicalGeneralsActiveBoolean = false;
  physicalGeneralsCompletedBoolean = false;


  mentalGeneralsBoolean = true;
  mentalGeneralsActiveBoolean = false;
  mentalGeneralsCompletedBoolean = false;


  summaryBoolean = false;

  // Progress Bar
  generalInfoComplted;
  chiefComplaintsComplted;
  pastMedicalComplted;
  familyHistoryComplted;
  obsGynacHistoryComplted;
  treatmentHistoryComplted;
  labInvestigationComplted;
  clinicalFindingComplted;
  labTestComplted;

  physicalMakeupCompleted;
  physicalGeneralsCompleted;
  mentalGeneralsCompleted;


  private subscription: Subscription;
  public reciveDataValue: any;


// url
family: any;
  radioButtonValueInfo: any;
  constructor(private route: Router,
    private reciveinvestigationqueue: TestDataService,
    private titleService: Title,
    private spinner: NgxSpinnerService
  ) {


  }

  // ngOnInit() {

  //   // if (result === 'FamilyMedicalHistory') {
  //   //     this.family = true;
  //   // }
  //   // console.log('familyMedicalHistory');
  //   // console.log(familyMedicalHistory);
  //   // console.log('routeurl');
  //   // console.log(this.route.url);

  //   this.subscription = this.reciveinvestigationqueue.notifyObservable$.subscribe((res) => {
  //     if (res.hasOwnProperty('option') && res.option === 'patient-queue') {
  //         this.investigationqueue = res.value;
  //         if (this.investigationqueue === true) {
  //           this.route.navigate(['/dashboard/Doctor/GeneralInformation']);
  //         }
  //     }
  // });
  // }
  // ngDoCheck() {
  //   const url = this.route.url;
  //   const spliting = url.lastIndexOf('/');
  //   const result = url.substring(spliting + 1);
  //   console.log(' URL results');
  //   console.log(result);
  //   if (result === 'Investigation%20Queue') {
  //     return this.investigationqueue = false;
  //   }
  // }
  ngOnInit() {
    this.spinner.hide();
    this.titleService.setTitle(this.title);
    // if (result === 'FamilyMedicalHistory') {
    //     this.family = true;
    // }
    // console.log('familyMedicalHistory');
    // console.log(familyMedicalHistory);
    // console.log('routeurl');
    // console.log(this.route.url);

    this.subscription = this.reciveinvestigationqueue.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('option') && res.option === 'patient-queue') {
        this.investigationqueue = res.value;
        if (this.investigationqueue === true) {
          this.route.navigate(['/dashboard/Doctor/GeneralInformation']);
        }
      }
    });

    // const getradioVale = localStorage.getItem('radioValue');
    // const radioValue = JSON.parse(getradioVale);
    // console.log('radioValue');
    // console.log(radioValue);
    // this.activeRadioButton(radioValue)
    const url = this.route.url;
    const spliting = url.lastIndexOf('/');
    const result = url.substring(spliting + 1);
    // console.log(' URL results');
    // console.log(result);
    if (result === 'Investigation%20Queue') {
      return this.investigationqueue = false;
    }
    if (result === 'GeneralInformation') {
      this.activeRadioButton('generalInfo');
    } else if (result === 'ChiefComplaints') {
      this.activeRadioButton('chiefComplaints');
    } else if (result === 'PastMedicalHistory') {
      this.activeRadioButton('pastMedicalHistory');
    } else if (result === 'FamilyMedicalHistory') {
      this.activeRadioButton('familyHistory');
    } else if (result === 'ObsGynacHistory') {
      this.activeRadioButton('obsGynacHistory');
    } else if (result === 'TreatmentHistory') {
      this.activeRadioButton('treatmentHistory');
    } else if (result === 'LabInvestigations') {
      this.activeRadioButton('labInvestigation');
    } else if (result === 'PhysicalMakeup') {
      this.activeRadioButton('physicalMakeup');
    } else if (result === 'PhysicalGenerals') {
      this.activeRadioButton('physicalGenerals');
    } else if (result === 'MentalGenerals') {
      this.activeRadioButton('mentalGenerals');
    } else if (result === 'Summary') {
      this.activeRadioButton('summary');
    }

  }
  ngDoCheck() {
    const url = this.route.url;
    const spliting = url.lastIndexOf('/');
    const result = url.substring(spliting + 1);
    // console.log(' URL results');
    // console.log(result);
    if (result === 'Investigation%20Queue') {
      return this.investigationqueue = false;
    }
  }
  // personalHealthRecord(event) {
  //   const target = event.target || event.srcElement || event.currentTarget;
  //   const idAttr = target.attributes.id;
  //   const value = idAttr.nodeValue;

  personalHealthRecord(event) {
    const target = event.target || event.srcElement || event.currentTarget;
    const idAttr = target.attributes.id;
    this.radioButtonValueInfo = idAttr.nodeValue;
    // localStorage.setItem('radioValue', JSON.stringify(this.radioButtonValueInfo));
    // console.log(this.radioButtonValueInfo);
    this.activeRadioButton(this.radioButtonValueInfo);
    // const setvalue = localStorage.setItem('healthRecordValue', value);

  }
    activeRadioButton(value) {
    if (value === 'generalInfo') {
      this.activeStatusofCss(true,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            false);
      this.generalInfoComplted = '0%';
      this.chiefComplaintsComplted = '0%';
      this.pastMedicalComplted = '0%';
      this.familyHistoryComplted = '0%';
      this.obsGynacHistoryComplted = '0%';
      this.treatmentHistoryComplted = '0%';
      this.labInvestigationComplted = '0%';
      this.clinicalFindingComplted = '0%';
      this.physicalMakeupCompleted = '0%';
      this.physicalGeneralsCompleted = '0%';
      this.mentalGeneralsCompleted = '0%';

      this.route.navigate(['/dashboard/Doctor/GeneralInformation']);

    } else if (value === 'chiefComplaints') {
      this.activeStatusofCss(false,
                            false, true, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            false);

      this.generalInfoComplted = '100%';
      this.chiefComplaintsComplted = '0%';
      this.pastMedicalComplted = '0%';
      this.familyHistoryComplted = '0%';
      this.obsGynacHistoryComplted = '0%';
      this.treatmentHistoryComplted = '0%';
      this.labInvestigationComplted = '0%';
      this.clinicalFindingComplted = '0%';
      this.physicalMakeupCompleted = '0%';
      this.physicalGeneralsCompleted = '0%';
      this.mentalGeneralsCompleted = '0%';

      this.route.navigate(['/dashboard/Doctor/ChiefComplaints']);

    } else if (value === 'pastMedicalHistory') {

      this.activeStatusofCss(false,
                            false, false, true,
                            false, true, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            false);

      this.generalInfoComplted = '100%';
      this.chiefComplaintsComplted = '100%';
      this.pastMedicalComplted = '0%';
      this.familyHistoryComplted = '0%';
      this.obsGynacHistoryComplted = '0%';
      this.treatmentHistoryComplted = '0%';
      this.labInvestigationComplted = '0%';
      this.clinicalFindingComplted = '0%';
      this.physicalMakeupCompleted = '0%';
      this.physicalGeneralsCompleted = '0%';
      this.mentalGeneralsCompleted = '0%';

      this.route.navigate(['/dashboard/Doctor/PastMedicalHistory']);

    } else if (value === 'familyHistory') {

       this.activeStatusofCss(false,
                            false, false, true,
                            false, false, true,
                            false, true, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            false);
      this.generalInfoComplted = '100%';
      this.chiefComplaintsComplted = '100%';
      this.pastMedicalComplted = '100%';
      this.familyHistoryComplted = '0%';
      this.obsGynacHistoryComplted = '0%';
      this.treatmentHistoryComplted = '0%';
      this.labInvestigationComplted = '0%';
      this.clinicalFindingComplted = '0%';
      this.physicalMakeupCompleted = '0%';
      this.physicalGeneralsCompleted = '0%';
      this.mentalGeneralsCompleted = '0%';

      this.route.navigate(['/dashboard/Doctor/FamilyMedicalHistory']);


    } else if (value === 'obsGynacHistory') {

       this.activeStatusofCss(false,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, true, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            false);

      this.generalInfoComplted = '100%';
      this.chiefComplaintsComplted = '100%';
      this.pastMedicalComplted = '100%';
      this.familyHistoryComplted = '100%';
      this.obsGynacHistoryComplted = '0%';
      this.treatmentHistoryComplted = '0%';
      this.labInvestigationComplted = '0%';
      this.clinicalFindingComplted = '0%';
      this.physicalMakeupCompleted = '0%';
      this.physicalGeneralsCompleted = '0%';
      this.mentalGeneralsCompleted = '0%';

      this.route.navigate(['/dashboard/Doctor/ObsGynacHistory']);

    } else if (value === 'treatmentHistory') {
       this.activeStatusofCss(false,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, true, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            false);

      this.generalInfoComplted = '100%';
      this.chiefComplaintsComplted = '100%';
      this.pastMedicalComplted = '100%';
      this.familyHistoryComplted = '100%';
      this.obsGynacHistoryComplted = '100%';
      this.treatmentHistoryComplted = '0%';
      this.labInvestigationComplted = '0%';
      this.clinicalFindingComplted = '0%';
      this.physicalMakeupCompleted = '0%';
      this.physicalGeneralsCompleted = '0%';
      this.mentalGeneralsCompleted = '0%';

      this.route.navigate(['/dashboard/Doctor/TreatmentHistory']);

    } else if (value === 'labInvestigation') {
       this.activeStatusofCss(false,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, true, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            false);

      this.generalInfoComplted = '100%';
      this.chiefComplaintsComplted = '100%';
      this.pastMedicalComplted = '100%';
      this.familyHistoryComplted = '100%';
      this.obsGynacHistoryComplted = '100%';
      this.treatmentHistoryComplted = '100%';
      this.labInvestigationComplted = '0%';
      this.clinicalFindingComplted = '0%';
      this.physicalMakeupCompleted = '0%';
      this.physicalGeneralsCompleted = '0%';
      this.mentalGeneralsCompleted = '0%';

      this.route.navigate(['/dashboard/Doctor/LabInvestigations']);

    } else if (value === 'clinicalFinding') {
       this.activeStatusofCss(false,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, true, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            true, false, false,
                            false);

      this.generalInfoComplted = '100%';
      this.chiefComplaintsComplted = '100%';
      this.pastMedicalComplted = '100%';
      this.familyHistoryComplted = '100%';
      this.obsGynacHistoryComplted = '100%';
      this.treatmentHistoryComplted = '100%';
      this.labInvestigationComplted = '100%';
      this.clinicalFindingComplted = '0%';
      this.physicalMakeupCompleted = '0%';
      this.physicalGeneralsCompleted = '0%';
      this.mentalGeneralsCompleted = '0%';

      this.route.navigate(['/dashboard/Doctor/ClinicalFindings']);

    } else if (value === 'physicalMakeup') {
       this.activeStatusofCss(false,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,

                            false, false, true,
                            false, false, true,

                            false, true, false,

                            false, false, false,

                            true, false, false,
                            true, false, false,
                            false);

      this.generalInfoComplted = '100%';
      this.chiefComplaintsComplted = '100%';
      this.pastMedicalComplted = '100%';
      this.familyHistoryComplted = '100%';
      this.obsGynacHistoryComplted = '100%';
      this.treatmentHistoryComplted = '100%';
      this.labInvestigationComplted = '100%';
      this.clinicalFindingComplted = '100%';
      this.physicalMakeupCompleted = '0%';
      this.physicalGeneralsCompleted = '0%';
      this.mentalGeneralsCompleted = '0%';

      this.route.navigate(['/dashboard/Doctor/PhysicalMakeup']);

    } else if (value === 'physicalGenerals') {
       this.activeStatusofCss(false,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, true, false,
                            true, false, false,
                            false);

      this.generalInfoComplted = '100%';
      this.chiefComplaintsComplted = '100%';
      this.pastMedicalComplted = '100%';
      this.familyHistoryComplted = '100%';
      this.obsGynacHistoryComplted = '100%';
      this.treatmentHistoryComplted = '100%';
      this.labInvestigationComplted = '100%';
      this.clinicalFindingComplted = '100%';
      this.physicalMakeupCompleted = '100%';
      this.physicalGeneralsCompleted = '0%';
      this.mentalGeneralsCompleted = '0%';

      this.route.navigate(['/dashboard/Doctor/PhysicalGenerals']);
    } else if (value === 'mentalGenerals') {
       this.activeStatusofCss(false,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, true, false,
                            false);

      this.generalInfoComplted = '100%';
      this.chiefComplaintsComplted = '100%';
      this.pastMedicalComplted = '100%';
      this.familyHistoryComplted = '100%';
      this.obsGynacHistoryComplted = '100%';
      this.treatmentHistoryComplted = '100%';
      this.labInvestigationComplted = '100%';
      this.clinicalFindingComplted = '100%';
      this.physicalMakeupCompleted = '100%';
      this.physicalGeneralsCompleted = '100%';
      this.mentalGeneralsCompleted = '0%';

      this.route.navigate(['/dashboard/Doctor/MentalGenerals']);
    } else if (value === 'summary') {
       this.activeStatusofCss(false,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, false, true,
                            false, true,  true,
                            true,  false, true,
                            false, false, true,
                            false, false, true,
                            true);

      this.generalInfoComplted = '100%';
      this.chiefComplaintsComplted = '100%';
      this.pastMedicalComplted = '100%';
      this.familyHistoryComplted = '100%';
      this.obsGynacHistoryComplted = '100%';
      this.treatmentHistoryComplted = '100%';
      this.labInvestigationComplted = '100%';
      this.clinicalFindingComplted = '100%';
      this.physicalMakeupCompleted = '100%';
      this.physicalGeneralsCompleted = '100%';
      this.mentalGeneralsCompleted = '100%';

      this.route.navigate(['/dashboard/Doctor/Summary']);
    }
  }

  activeStatusofCss(generalInfo, chiefComplaints, chiefComplaintsActive, chiefComplaintsCompleted,
                     pastMedicalHistory, pastMedicalHistoryActive, pastMedicalHistoryCompleted,
                     familyHistory, familyHistoryActive, familyHistoryCompleted,
                     obsGynacHistory, obsGynacHistoryActive, obsGynacHistoryCompleted,
                     treatmentHistory, treatmentHistoryActive, treatmentHistoryCompleted,
                     labInvestigationHistory, labInvestigationActiveHistory, labInvestigationCompletedHistory,
                     clinicalFinding, clinicalFindingActive, clinicalFindingCompleted,
                     geneticConstitution, geneticConstitutionActive, geneticConstitutionCompleted,
                     physicalMakeup, physicalMakeupActive, physicalMakeupCompleted,
                     physicalGenerals, physicalGeneralsActive, physicalGeneralsCompleted,
                     mentalGenerals, mentalGeneralsActive, mentalGeneralsCompleted,
                     summary

                     ) {
      this.generalInfoBoolean = generalInfo;

      this.chiefComplaintsBoolean = chiefComplaints;
      this.chiefComplaintsActiveBoolean = chiefComplaintsActive;
      this.chiefComplaintsCompletedBoolean = chiefComplaintsCompleted;

      this.pastMedicalHistoryBoolean = pastMedicalHistory;
      this.pastMedicalHistoryActiveBoolean = pastMedicalHistoryActive;
      this.pastMedicalHistoryCompletedBoolean = pastMedicalHistoryCompleted;

      this.familyHistoryBoolean = familyHistory;
      this.familyHistoryActiveBoolean = familyHistoryActive;
      this.familyHistoryCompletedBoolean = familyHistoryCompleted;

      this.obsGynacHistoryBoolean = obsGynacHistory;
      this.obsGynacHistoryActiveBoolean = obsGynacHistoryActive;
      this.obsGynacHistoryCompletedBoolean = obsGynacHistoryCompleted;

      this.treatmentHistoryBoolean = treatmentHistory;
      this.treatmentHistoryActiveBoolean = treatmentHistoryActive;
      this.treatmentHistoryCompletedBoolean = treatmentHistoryCompleted;

      this.labInvestigationHistoryBoolean = labInvestigationHistory;
      this.labInvestigationActiveHistoryBoolean = labInvestigationActiveHistory;
      this.labInvestigationCompletedHistoryBoolean = labInvestigationCompletedHistory;

      this.clinicalFindingBoolean = clinicalFinding;
      this.clinicalFindingActiveBoolean = clinicalFindingActive;
      this.clinicalFindingCompletedBoolean = clinicalFindingCompleted;

      this.geneticConstitutionBoolean = geneticConstitution;
      this.geneticConstitutionActiveBoolean = geneticConstitutionActive;
      this.geneticConstitutionCompletedBoolean = geneticConstitutionCompleted;

      this.physicalMakeupBoolean = physicalMakeup;
      this.physicalMakeupActiveBoolean = physicalMakeupActive;
      this.physicalMakeupCompletedBoolean = physicalMakeupCompleted;

      this.physicalGeneralsBoolean = physicalGenerals;
      this.physicalGeneralsActiveBoolean = physicalGeneralsActive;
      this.physicalGeneralsCompletedBoolean = physicalGeneralsCompleted;

      this.mentalGeneralsBoolean = mentalGenerals;
      this.mentalGeneralsActiveBoolean = mentalGeneralsActive;
      this.mentalGeneralsCompletedBoolean = mentalGeneralsCompleted;

      this.summaryBoolean = summary;
  }

  displayCounter($event) {
    this.investigationqueue = $event;
  }
  }
