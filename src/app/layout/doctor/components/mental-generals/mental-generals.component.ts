import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import * as CryptoJS from 'crypto-js';
import { DoctorComponent } from '../../doctor.component';
import { DoctorService } from '../../doctor.service';
import { Title } from '@angular/platform-browser';
@Component({
  selector: 'app-mental-generals',
  templateUrl: './mental-generals.component.html',
  styleUrls: ['./mental-generals.component.scss']
})
export class MentalGeneralsComponent implements OnInit {
   private readonly notifier: NotifierService;
  title = 'Modern Rolls | Senior Doctor Mental Generals';
  mentalGeneral: FormGroup;
  secret = 'i_have_some_small_master_secret_live_pin';
  empId;
  orgId;
  centerId;
  patientId;
  userToken: any;
  patientObj: any;
  mntalGeneralObj:any

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private seniorDoctorService : DoctorService,
              private radioStatusActive: DoctorComponent,
              private spinner: NgxSpinnerService,
              private notifierService: NotifierService,
              private titleService: Title) { 
              this.notifier = notifierService; 

               }
  countries = ['select', 'Yes', 'No'];



  ngOnInit() {
        this.titleService.setTitle(this.title);
    this.patientRegisterForms();
    const ptid = localStorage.getItem('_id');
    this.patientId = JSON.parse(ptid)
    const getjwt = localStorage.getItem('jwttoken');
    this.userToken = this.conversionDecrypt(getjwt);
    this.getMedicalHistoryByPatientId();
  }
  patientRegisterForms() {
    this.mentalGeneral = this.formBuilder.group({
      ailmentsFrom: this.formBuilder.group({
        extraverted: [''],
        introverted: [''],
        loquacity: [''],
        talkIndisposed: [''],
        eiltothers: [''],
        conscientious: [''],
        careless: [''],
        ccothers: [''],
        hurried: [''],
        anxious: [''],
        speechHistory: [''],
        stammering: [''],
        hassothers: [''],
        sensitive: [''],
        emotional: [''],
        weeping: [''],
        rude: [''],
        sewrothers: [''],
        brooding: [''],
        reproching: [''],
        others: [''],
        herself: [''],
        brohothers: [''],
        dominating: [''],
        dictatorial: [''],
        milld: [''],
        timid: [''],
        bashful: [''],
        childish: [''],
        slowness: [''],
        idiotic: [''],
        ddmtbsothers: ['']
      }),
      fearsAndFobia: this.formBuilder.group({
        haughty: [''],
        effeminate: [''],
        heothers: [''],
        anger: [''],
        rage: [''],
        fury: [''],
        arfothers: [''],
        avarice: [''],
        sqaunderer: [''],
        asothers: [''],
        desire: [''],
        aversion: [''],
        cdaothers: [''],
        startlling: [''],
        sensesAcute: [''],
        ssothers: [''],
        sympathetic: [''],
        charitable: [''],
        scothers: [''],
        yielding: [''],
        obstinate: [''],
        yoothers: [''],
        suicidal: [''],
        impulsive: [''],
        quarrelsome: [''],
        sarcastic: [''],
        siqsthers: [''],
        nymphomania: [''],
        satyriasis: [''],
        nsothers: [''],
        artistic: [''],
        creative: [''],
        acothers: [''],
        jesting: [''],
        witty: [''],
        cheerful: [''],
        gjwcothers: [''],
        mentalResitiessness: [''],
        otherSymptoms: ['']
      }),
    });
  }
  onSubmit() {
    console.log(this.mentalGeneral.value); 
     if(this.patientObj.length ===0){
      this.spinner.hide();
      this.notifier.notify('error', 'Something went wrong please try again later!')
    }
    this.mntalGeneralObj = this.mentalGeneral.value;
    this.patientObj.forEach(mentalGen=>{
         const mentalGenValue = mentalGen.mentalGeneralHistoryObj;
         mentalGenValue.ailmentsFrom.push(this.mntalGeneralObj.ailmentsFrom)
         mentalGenValue.fearsAndFobia.push(this.mntalGeneralObj.fearsAndFobia);
         this.spinner.show();
         this.updateMedicalHistory(this.patientObj[0]);
     })
  }

  getMedicalHistoryByPatientId(){
   const headers = new Headers(
        {
            'accept': 'application/json',
            'x-access-token': this.userToken
        });
     this.seniorDoctorService.getMedicalHistoryByPatientId(this.patientId, headers).subscribe(getPatientDetail=>{
         this.patientObj = getPatientDetail.patientData;
           this.setMedicalHistoryByPatientId();
     },
     err=>{
        this.notifier.notify('error', 'Something went wrong please try again!')
     });
  }
  updateMedicalHistory(patientHistory){
     const headers = new Headers(
          {
              'accept': 'application/json',
              'x-access-token': this.userToken
          });
     this.seniorDoctorService.updateMedicalHistory(patientHistory, headers).subscribe(data=>{
       // console.log('data data data ');
       // console.log(data);
      this.spinner.hide();
      this.router.navigate(['/dashboard/Junior Doctor/Summary']);
      this.radioStatusActive.activeRadioButton('summary');

     },
     err=>{
        this.spinner.hide();
        this.notifier.notify('error', 'Something went wrong please try again!')
     });
  }

  setMedicalHistoryByPatientId() {
    this.patientObj.forEach(mentalGeneral=> {
            const mentalGeneralFormValue = this.mentalGeneral.value;
            const obsGynacObj = mentalGeneral.mentalGeneralHistoryObj

            obsGynacObj.ailmentsFrom.forEach(ailments=> {
            this.mentalGeneral.get('ailmentsFrom').patchValue(ailments);
            })
            obsGynacObj.fearsAndFobia.forEach(fearsAndFobia=> {
            this.mentalGeneral.get('fearsAndFobia').patchValue(fearsAndFobia);
            })

          },
     err=>{
        this.notifier.notify('error', 'Something went wrong please try again!')
     });
  }


 conversionEncrypt(encrypt) {
      try {
          return CryptoJS.AES.encrypt(JSON.stringify(encrypt), this.secret).toString();
      } catch (e) {
          console.log(e);
      }
  }
 conversionDecrypt(decrypt) {
      try {
          const bytes = CryptoJS.AES.decrypt(decrypt, this.secret);
          if (bytes.toString()) {
              return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
          }
          return decrypt;
      } catch (e) {
          console.log(e);
      }
  }
}
