import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentalGeneralsComponent } from './mental-generals.component';

describe('MentalGeneralsComponent', () => {
  let component: MentalGeneralsComponent;
  let fixture: ComponentFixture<MentalGeneralsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentalGeneralsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentalGeneralsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
