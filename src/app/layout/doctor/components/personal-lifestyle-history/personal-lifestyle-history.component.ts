import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-personal-lifestyle-history',
  templateUrl: './personal-lifestyle-history.component.html',
  styleUrls: ['./personal-lifestyle-history.component.scss']
})
export class PersonalLifestyleHistoryComponent implements OnInit {

  constructor() { }
    personalLifeStyle = [{
        id: 1,
        heading: 'Drug and Reaction',
        quetion: 'Are you allergic to any of these drugs?',
        answer: ['Select', 'No', 'Amoxicillin', 'Ampicillin', 'Aspirin', 'Azithromycin', 'Cephalosporin', 'Ciprofloxacin', 'Diclofenac', 'Ibuprofen', 'Metronidazole']
      },
      {
        id: 2,
        heading: 'Environment and Reaction',
        quetion: 'Are you allergic to any of these environmental allergies?',
        // tslint:disable-next-line:max-line-length
        answer: ['Select', 'No', 'Animal Dander', 'Dust', 'Pollen', 'Grass', 'Insect Sting', 'Mold', 'Others', 'Unknown']
      },
      {
        id: 3,
        heading: 'Food and Reaction',
        quetion: 'Are you allergic to any of these food types?',
        answer: ['Select', 'No', 'Nuts', 'Dairy Products', 'Vegetables', 'Fruits', 'Meat', 'Egg', 'Others', 'Unknown']
      },
      {
        id: 4,
        heading: 'Smoking',
        quetion: 'Do you smoke?',
        answer: ['Select', 'No', 'Current Smoker', 'Ex Smoker stopped < 15 yrs ago', 'Ex Smoker stopped > 15 yrs ago', 'Non Smoker']
      },
      {
        id: 5,
        heading: 'Alcohol Use',
        quetion: 'Do you consume alcohol?',
        answer: ['Select', 'Occasionally', 'Daily/Weekly', 'Non Alcoholic']
      },
      {
        id: 6,
        heading: 'Physical Activity',
        quetion: 'How many minutes of Physical activity do you do in a week?',
        // tslint:disable-next-line:max-line-length
        answer: ['Select', '150 min. moderate/75 min rigorous per week', '100 min. moderate/50 min rigorous per week', '50 min. moderate/25 min rigorous per week', 'No Exercise']
      },
      {
        id: 7,
        heading: 'Sleep Activity',
        quetion: 'How many hours do you sleep in a day?',
        answer: ['Select', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
      },
      {
        id: 8,
        quetion: 'Do you sleep during daytime?',
        answer: ['Select', 'Yes', 'No']
      },
      {
        id: 9,
        quetion: 'Are you not able to go to sleep at night?',
        answer: ['Select', 'Yes', 'No']
      },
      {
        id: 10,
        quetion: 'Do you use sleeping pills to sleep?',
        answer: ['Select', 'Yes', 'No']
      },
      {
        id: 11,
        quetion: 'Do your legs often move or jerk while sleeping',
        answer: ['Select', 'Yes', 'No']
      }
    ];
  ngOnInit() {
  }

}
