import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbCalendar, NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import { Title } from '@angular/platform-browser';
import * as CryptoJS from 'crypto-js';
import { SubSink } from 'subsink';
import { DoctorComponent } from '../../doctor.component';
import { DoctorService } from '../../doctor.service';



@Component({
  selector: 'app-obs-gynac-history',
  templateUrl: './obs-gynac-history.component.html',
  styleUrls: ['./obs-gynac-history.component.scss']
})
export class ObsGynacHistoryComponent implements OnInit, OnDestroy  {
  title = 'Modern Rolls | Senior Doctor Obs Gynac History';
   private readonly notifier: NotifierService;
   private subs = new SubSink();
  // Form Field Declaration
  menses = { regular: '', irregular: '', frequency: '', };
  bleeding = { scanty: '', profuse: '', normal: '', clots: '', duration: '', bleedingExtra: '' };
  pain = { severe: '', moderate: '', mild: '', painothers: '', };
  deliveries = { deliNormal: '', delimoderate: '', deliveriesExtra: '' };
  whiteDischarge = { profuse: '', curdy: '', whiteItching: '', whiteSmell: '', whiteDischargeExtra: '' };
  menarcheAge;
  gValue;
  pValue;
  lValue;
  aValue;
  dValue;
  lcbValue;
  tubectomyAge='';
  hysterectomyAge;
  menoPauseAge;
  model: NgbDateStruct;

  // model = {
  //   day : '',
  //   month : '',
  //   year:''
  // }
  secret = 'i_have_some_small_master_secret_live_pin';
  empId;
  orgId;
  centerId;
  patientId;
  userToken: any;
  patientObj: any;
  patientHistoryText: any;
  patientHistoryCheckbox: any;
  date;
  date1;
  month1;
  year1;
  date2;
  dateUTC;

  constructor(private calendar: NgbCalendar,
    private router: Router,
    private seniorDoctorService: DoctorService,
    private radioStatusActive: DoctorComponent,
     private spinner: NgxSpinnerService,
              private notifierService: NotifierService,
              private titleService: Title) {
                this.notifier = notifierService; 
              }

  isDisabled = (date: NgbDate, current: { month: number }) => date.month !== current.month;
  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6;

  ngOnInit() {
        this.titleService.setTitle(this.title);
    // this.PatientRegisterForms();
    const ptid = localStorage.getItem('_id');
    this.patientId = JSON.parse(ptid);

    const getjwt = localStorage.getItem('jwttoken');
    this.userToken = this.conversionDecrypt(getjwt);
    // console.log('this.userToken');
    // console.log(this.userToken);
    this.getMedicalHistoryByPatientId();
  }

  // nextpage() {
  //   this.router.navigate(['/dashboard/patientInfo/TreatmentHistory']);
  // }
  setPatientConformation(value) {
    // console.log('value=========>', value)
    // console.log('this.model========>', this.model)
    if(this.model === undefined){
      this.dateUTC = ''; 
      }else if(this.model === null){
          this.dateUTC = ''; 
     } else{
          this.date = this.model;
          this.date1 = (this.date.day);
          this.month1 = this.date.month;
          this.year1 = this.date.year;
          this.date2 = new Date(this.month1 + '/' + this.date1 + '/' + this.year1);
          this.dateUTC = this.date2.toISOString();
     }
    if(this.patientObj.length ===0){
      this.spinner.hide();
      this.router.navigate(['/dashboard/Junior Doctor/TreatmentHistory']);
      this.radioStatusActive.activeRadioButton('treatmentHistory');
      this.notifier.notify('error', 'Something went wrong please try again later!')
    }
    this.patientObj.forEach(obsGynac => {
      const obsGynacObj = obsGynac.oBSAndGynacHistoryObj;
      obsGynacObj.menses.push(value.menses);
      obsGynacObj.bleeding.push(value.bleeding);
      obsGynacObj.pain.push(value.pain);
      obsGynacObj.deliveries.push(value.deliveries);
      obsGynacObj.whiteDischarge.push(value.whiteDischarge);
      obsGynacObj.menarcheAge = value.menarcheAge;
      obsGynacObj.gValue = value.gValue;
      obsGynacObj.pValue = value.pValue;
      obsGynacObj.lValue = value.lValue;
      obsGynacObj.aValue = value.aValue;
      obsGynacObj.dValue = value.dValue;
      obsGynacObj.lMPDt = this.dateUTC;
      obsGynacObj.tubectomyAge = value.tubectomyAge;
      obsGynacObj.hysterectomyAge = value.hysterectomyAge;
      obsGynacObj.menoPauseAge = value.menoPauseAge;
      console.log('this.patientObj');
      console.log(this.patientObj);
     
       this.spinner.show();
      this.updateMedicalHistory(this.patientObj[0]);
    });

    // this.modalService.dismissAll();
    // this.router.navigate(['/dashboard/patientInfo/TreatmentHistory']);
  }

  getMedicalHistoryByPatientId() {
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });
    this.subs.sink = this.seniorDoctorService.getMedicalHistoryByPatientId(this.patientId, headers).subscribe(getPatientDetail => {
      this.patientObj = getPatientDetail.patientData;
      this.setMedicalHistoryByPatientId();
      // console.log('Get the Patient Medical Object');
      console.log('this.patientObj');
      console.log(this.patientObj);
    },
    err=>{
      this.notifier.notify('error', 'Something went wrong please try again!')
    });
  }

  updateMedicalHistory(patientHistory) {
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });
    this.subs.sink = this.seniorDoctorService.updateMedicalHistory(patientHistory, headers).subscribe(data => {
      this.spinner.hide();
      this.router.navigate(['/dashboard/Junior Doctor/TreatmentHistory']);
      this.radioStatusActive.activeRadioButton('treatmentHistory');
      // console.log(data);

    },
    err=>{
      this.spinner.hide();
      this.notifier.notify('error', 'Something went wrong please try again!')
    });
  }

  setMedicalHistoryByPatientId() {
    this.patientObj.forEach(oBSAndGynacHistor => {
      const oBSAndGynac = oBSAndGynacHistor.oBSAndGynacHistoryObj;
      oBSAndGynac.menses.forEach(menses => {
        this.menses = menses;
      });
      oBSAndGynac.bleeding.forEach(bleeding => {
        this.bleeding = bleeding;
      });
      oBSAndGynac.pain.forEach(pain => {
        this.bleeding = pain;
      });
      oBSAndGynac.deliveries.forEach(deliveries => {
        this.deliveries = deliveries;
      });
      oBSAndGynac.whiteDischarge.forEach(whiteDischarge => {
        this.whiteDischarge = whiteDischarge;
      });
      this.menarcheAge = oBSAndGynac.menarcheAge;
      this.gValue = oBSAndGynac.gValue;
      this.pValue = oBSAndGynac.pValue;
      this.lValue = oBSAndGynac.lValue;
      this.aValue = oBSAndGynac.aValue;
      this.dValue = oBSAndGynac.dValue;
      this.lcbValue = oBSAndGynac.lcbValue;
      this.tubectomyAge = oBSAndGynac.tubectomyAge;
      this.hysterectomyAge = oBSAndGynac.hysterectomyAge;
      this.menoPauseAge = oBSAndGynac.menoPauseAge;
      let date = oBSAndGynac.lMPDt
      const dateObj = {
        day: new Date(date).getDate(),
        month:  new Date(date).getMonth()+1,
        year:  new Date(date).getFullYear(),

      }
      this.model = dateObj;

    });
  }
  conversionEncrypt(encrypt) {
    try {
      return CryptoJS.AES.encrypt(JSON.stringify(encrypt), this.secret).toString();
    } catch (e) {
      console.log(e);
    }
  }
  conversionDecrypt(decrypt) {
    try {
      const bytes = CryptoJS.AES.decrypt(decrypt, this.secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return decrypt;
    } catch (e) {
      console.log(e);
    }
  }
ngOnDestroy() {
  // this.subs.unsubscribe();
  console.log(`SubSinkComponent destroyed`)
}


}
