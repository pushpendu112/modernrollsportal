import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Title } from '@angular/platform-browser';

import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';
import * as CryptoJS from 'crypto-js';

import { DoctorComponent } from '../../doctor.component';
import { DoctorService } from '../../doctor.service';

@Component({
  selector: 'app-general-information',
  templateUrl: './general-information.component.html',
  styleUrls: ['./general-information.component.scss'],
  providers: [DatePipe]
})
export class GeneralInformationComponent implements OnInit {
  title = 'Modern Rolls | Senior Doctor General Information';
  private readonly notifier: NotifierService;
  patientInfoForm: FormGroup;
  date: any;
  date1: any;
  month1: any;
  year1: any;
  date2: any;




  getjwttoken: string;
  centerId: any;
  userToken: any;
  lorgId: any;
  lempId: any;

  Secret = 'i_have_some_small_master_secret_live_pin';
  getGenderValue: any;
  newexistpatient: string;
  existPatientObject: any;
  date4: Date;
  year5: string;
  month5: string;
  date5: string;
  loader = false;
  buttonDisabled: boolean;
  radioAge1: boolean;
  radioDOB1: boolean;
  existpatientId: any;
  mobileNumber: any;
  ppatientId: any;
  patinetId: any;
  patientId: any;
  lpatientId: any;
  lmobileNumber: JSON;
  submitted = false;
  lenrolmentId: any;
  llastVisitedDate: any;
  updatePatientData: any;
  getjwttokens: string;
  diteTypeValue: number;
  maritalStatusvalue: number;
  homeopathicvalue: number;
  patientDataObj;
  patientSubmitObj;
  newAddress: any;
  date6: NgbDateStruct;
  radioAge2 = false;
  newgender: any;
  newgender1: any;


  constructor(private formBuilder: FormBuilder,
              private calendar: NgbCalendar,
              private router: Router,
              private modalService: NgbModal,
              private seniorDoctorService: DoctorService,
              private datePipe: DatePipe,
              private radioStatusActive: DoctorComponent,
              private spinner: NgxSpinnerService,
              private notifierService: NotifierService,
              private titleService: Title) { 
              this.notifier = notifierService;
            }

  bmiFeet = ['Select Feet', 1, 2, 3, 4, 5, 6, 7, 8, 9];
  bmiInch = ['Select Inch', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
  bmiHeightOption = ['feet-Inch', 'cms'];
  bmiWeightOption = ['Kgs', 'lbs'];
  maritalStatusOption = [{ 'name': 'Married', 'value': 0 }, { 'name': 'Un-Married', 'value': 1 }];
  children = [0, 1, 2, 3, 4];
  // diteOption = ['Select', 'Vegetarian', 'Mixed'];
  //  PaymentType: any = [{ 'name': 'Cash', 'value': 0 }, { 'name': 'Online', 'value': 1 }];
  diteOption = [{ 'name': 'Vegetarian', 'value': 0 }, { 'name': 'Mixed', 'value': 1 }];
  // homeopathicMedicineOption = ['Select', 'Used', 'Not Used'];
  homeopathicMedicineOption = [{ 'name': 'Used', 'value': 0 }, { 'name': 'NotUsed', 'value': 1 }];
  bmiValue: any;
  selectedFeet = true;
  selectedCms = false;
  feetValue: Number;
  inchValue: Number;
  testHight: any;

  selected = 'dob';
  dobSelected = true;
  ageSelected = false;

  maleGenderActive = false;
  feMaleGenderActive = false;
  otherGenderActive = false;
  model: NgbDateStruct;

  private selectedLink = 'DOB';
  selectedLink2 = false;
  selectedLink3 = false;
  selectedLink4 = false;

  isDisabled1 = true;

  bmiWeight = '';
  bmiHeight = '';
  bmiCalcuation = '';
  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.createPatientInfoForm();
    // this.getjwttoken = JSON.parse(localStorage.getItem('jwttoken'));
    // this.getjwttokens = this.getjwttoken;
    // console.log('this.getjwttoken', this.getjwttoken);

    this.userToken = this.decryptData(this.getjwttokens);
    this.centerId = JSON.parse(localStorage.getItem('centerId'));
    this.lorgId = JSON.parse(localStorage.getItem('orgId'));
    this.lempId = JSON.parse(localStorage.getItem('_id'));
    this.lpatientId = JSON.parse(localStorage.getItem('patientId'));
    this.lmobileNumber = JSON.parse(localStorage.getItem('mobileNumber'));
    this.lenrolmentId = JSON.parse(localStorage.getItem('enrolmentId'));
    this.llastVisitedDate = JSON.parse(localStorage.getItem('lastVisitedDate'));
    this.onChanges();
    this.getExistingpatientData();
  }
  createPatientInfoForm() {
    this.patientInfoForm = this.formBuilder.group({
      _id: '',
      orgId: '',
      centerId: '',
      employeeId: '',
      enrolmentId: '',
      mobileNumber: ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10)])],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      dob: [''],
      age: 0,
      emailId: ['', [Validators.required, Validators.email]],
      gender: '',
      imageUrl: '',
      height: ['', Validators.required],
      weight: ['', Validators.required],
      address: ['', Validators.required],
      maritalLife: [0, Validators.required],
      maritalStatus: [, Validators.required],
      diteType: [, Validators.required],
      usedHomeopathic: 0,
      maleChildren: 0,
      femaleChildren: 0,
      bmiCalcuation: '',
      lastVisitedDate: [''],
      __v: 0
    });
  }




  // this.myForm.controls['bmiHeight'].valueChanges.subscribe(value => {
  //   this.testHight = value;
  //   //  this.myForm.controls['bmiCalcuation'].setValue(value);
  // });
  //   this.myForm.controls['bmiWeight'].valueChanges.subscribe(weight => {
  //     console.log(this.testHight);
  //     const heightCal = this.testHight / 100 * this.testHight / 100;
  //     // heightCal = heightCal * 0.025;
  //     const newValue = weight / heightCal;
  //     console.log('newValue');
  //     console.log(newValue);

  //     this.myForm.controls['bmiCalcuation'].setValue(Math.round(newValue));
  //     // this.myForm.controls['bmiCalcuation'].setValue(weight / (this.myForm.value.bmiWeight));
  //   });
  // }
  // createPatientInfoForm() {
  //   throw new Error("Method not implemented.");
  // }
  // createPatientInfoForm() {
  //   throw new Error("Method not implemented.");
  // }

  // onChangeHeightOption(heightOption) {
  //   if (heightOption === 'feet-Inch') {
  //     this.selectedFeet = true;
  //     this.selectedCms = false;
  //   } else if (heightOption === 'cms') {
  //     this.selectedCms = true;
  //     this.selectedFeet = false;
  //   }
  // }

  // onChangeFeetOption(feetOption) {
  //   this.feetValue = feetOption;
  // }
  // onChangeInchOption(inchOption) {
  //   this.inchValue = inchOption;
  // }

  // onHeightChange(e) {
  //    const heigh = e.currentTarget.value;
  //   this.myForm.controls['bmiCalcuation'].setValue(heigh * this.myForm.value.amount);
  // }
  // onWeightChange(e) {
  //   const Weight = e.currentTarget.value;
  //   this.myForm.controls['bmiCalcuation'].setValue(Weight / (this.myForm.value.bmiHeight * 12) * 0.25);

  // }




  genderActive(event) {
    const target = event.target || event.srcElement || event.currentTarget;
    const idAttr = target.attributes.id;
    const value = idAttr.nodeValue;
    if (value === 'Male') {
      this.maleGenderActive = true;
      this.feMaleGenderActive = false;
      this.otherGenderActive = false;
      this.getGenderValue = value;
    } else if (value === 'FeMale') {
      this.feMaleGenderActive = true;
      this.maleGenderActive = false;
      this.otherGenderActive = false;
      this.getGenderValue = value;
    } else if (value === 'Other') {
      this.otherGenderActive = true;
      this.maleGenderActive = false;
      this.feMaleGenderActive = false;
      this.otherGenderActive = true;
      this.getGenderValue = value;
    } else {
      this.maleGenderActive = false;
      this.feMaleGenderActive = false;
      this.otherGenderActive = false;
      this.getGenderValue = '';
    }
  }
  genderSetActive(value) {
    if (value === 'Male') {
      this.maleGenderActive = true;
      this.feMaleGenderActive = false;
      this.otherGenderActive = false;
      this.getGenderValue = value;
    } else if (value === 'FeMale') {
      this.feMaleGenderActive = true;
      this.maleGenderActive = false;
      this.otherGenderActive = false;
      this.getGenderValue = value;
    } else if (value === 'Other') {
      this.otherGenderActive = true;
      this.maleGenderActive = false;
      this.feMaleGenderActive = false;
      this.otherGenderActive = true;
      this.getGenderValue = value;
    } else {
      this.maleGenderActive = false;
      this.feMaleGenderActive = false;
      this.otherGenderActive = false;
    }
  }

  // addResidentialAddress() {
  //   this.selectedLink4 = !this.selectedLink4;
  // }

  // addOfficialAddress() {
  //   this.selectedLink2 = !this.selectedLink2;
  // }
  // addEmergencyContact() {
  //   this.selectedLink3 = !this.selectedLink3;
  // }

  isDisabled = (date: NgbDate, current: { month: number }) => date.month !== current.month;
  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6;


  onDobSelection(dateObj) {
    this.dobSelected = true;
    this.ageSelected = false;
    this.model = dateObj;
  }
  onAgeSelection(value) {
    this.dobSelected = false;
    this.ageSelected = true;
  }

  // nextpage() {
  //   this.router.navigate(['/dashboard/patientInfo/ChiefComplaints']);
  // }
  changeMaritalStatus($event) {
    this.maritalStatusvalue = parseInt($event.target.value);
    console.log('this.maritalStatusvalue', this.maritalStatusvalue);
    this.patientInfoForm.get('maritalStatus').setValue(this.maritalStatusvalue, {
      onlySelf: true
    });
  }
  changeusedHomeopathic($event) {
    this.homeopathicvalue = parseInt($event.target.value);
    console.log('this.homeopathicvalue', this.homeopathicvalue);
    this.patientInfoForm.get('usedHomeopathic').setValue(this.homeopathicvalue, {
      onlySelf: true
    });
  }
  changediteType($event) {
    // console.log($event.target.value);
    this.diteTypeValue = parseInt($event.target.value);
    console.log('this.paymentmodevalue', this.diteTypeValue);
    this.patientInfoForm.get('diteType').setValue(this.diteTypeValue, {
      onlySelf: true
    });
  }
  change($event) {
    this.maritalStatusvalue = parseInt($event.target.value);
    console.log('this.maritalStatusvalue', this.maritalStatusvalue);
    this.patientInfoForm.get('maritalStatusOption').setValue(this.maritalStatusvalue, {
      onlySelf: true
    });
  }
  // get dditeType() {
  //   return this.patientInfoForm.get('diteType');
  // }

  get f() {
    return this.patientInfoForm.controls;
  }

  onSubmit() {
    this.spinner.show();

    this.date = this.model;
    this.date1 = (this.date.day);
    this.month1 = this.date.month;
    this.year1 = this.date.year;
    this.date2 = new Date(this.month1 + '/' + this.date1 + '/' + this.year1);


    console.log('this.gender', this.getGenderValue);
    console.log('dob', this.date2 );






    this.patientInfoForm.get('_id').setValue(this.lpatientId);
    this.patientInfoForm.get('orgId').setValue(this.lorgId);
    this.patientInfoForm.get('employeeId').setValue(this.lempId);
    this.patientInfoForm.get('centerId').setValue(this.centerId);
    this.patientInfoForm.get('enrolmentId').setValue(this.lenrolmentId);
    this.patientInfoForm.get('lastVisitedDate').setValue(this.llastVisitedDate);


    // console.log('this.patientInfoForm.get(\'dob\').setValue(this.date2)', this.patientInfoForm.get('dob').setValue(this.date2));

    console.log(`this.patientInfoForm.value`);
    console.log(this.patientInfoForm.value);
    this.patientSubmitObj = this.patientInfoForm.value;
    this.patientDataObj.forEach(ptInfo => {
      const address = ptInfo.address[0];
      address.address1 = this.patientSubmitObj.address;
      console.log('address');
      console.log(address);
      this.newAddress = address.address1;
      console.log('address');
      console.log(this.patientDataObj);
      ptInfo.mobileNumber = this.patientSubmitObj.mobileNumber;
      ptInfo.firstName = this.patientSubmitObj.firstName;
      ptInfo.lastName = this.patientSubmitObj.lastName;
      ptInfo.emailId = this.patientSubmitObj.emailId;
      ptInfo.height = this.patientSubmitObj.height;
      ptInfo.weight = this.patientSubmitObj.weight;
      ptInfo.maritalStatus = this.patientSubmitObj.maritalStatus;
      ptInfo.maritalLife = this.patientSubmitObj.maritalLife;
      ptInfo.diteType = this.patientSubmitObj.diteType;
      ptInfo.usedHomeopathic = this.patientSubmitObj.usedHomeopathic;
      ptInfo.maleChildren = this.patientSubmitObj.maleChildren;
      ptInfo.femaleChildren = this.patientSubmitObj.femaleChildren;
      ptInfo.gender = (this.getGenderValue);
      ptInfo.dob = (this.date2.toISOString());
      // ptInfo.get('gender').setValue(this.getGenderValue);
      // ptInfo.get('dob').setValue(this.date2);
      // console.log('pt.infogender', ptInfo.gender);
      // console.log('ptInfo.dob', ptInfo.dob);
      // console.log('asdasda', JSON.stringify(ptInfo.gender));
    });
    // this.patientInfoForm.get('gender').setValue(this.genderSetActive(this.newgender1));
    console.log('this.patientDataObj[0]######################');
    console.log(this.patientDataObj[0]);
        this.patientInfoForm.get('gender').setValue(this.getGenderValue);
    this.patientInfoForm.get('dob').setValue(this.date2.toISOString());
    console.log('patinetformmvalue', this.patientInfoForm.value );
    this.submitted = true;
    if (this.patientInfoForm.invalid) {
    this.spinner.hide();
    this.notifier.notify('error', 'Please fill all the required field! ')
      return;

    }
    // this.updatePatientDetails(this.patientDataObj[0]);

    // const statusBody = {
    //   queueId: value.queueId,
    //   queueEntryDateAndTime: value.queueEntryDateAndTime,
    //   arrivalDateAndTime: value.arrivalDateAndTime,
    //   presentQueueType: value.presentQueueType,
    //   nextQueueType: 1,
    // };
    // // data.patientQueueData.forEach(status => {
    // //   if (status.presentQueueType !== 0) {
    // //     this.buttonDisabled = true;
    // //   }
    // // });

    // const headers = new Headers(
    //   {
    //     'accept': 'application/json',
    //     'x-access-token': this.userToken
    //   });

    // this.seniorDoctorService.updatePatientQueue(statusBody, headers).subscribe(updateStatus => {
    //   // alert('');
    //   console.log(updateStatus);

    // });
    // console.log(typeof (this.date));
    // console.log('@@@@@@@@@@@@@@@@@@@@@@@@@', this.date);
    // this.date1 = new date();
    // this.month1 = this.date.month;
    // this.year1 = this.date.year;
    // this.date2 = new Date(this.month1 + '/' + this.date1 + '/' + this.year1);

    // this.modalService.dismissAll();

    this.updatePatientDetails(this.patientDataObj[0]);


    this.radioStatusActive.activeRadioButton('chiefComplaints');

    this.router.navigate(['/dashboard/Junior Doctor/ChiefComplaints']);
  }

  onChanges(): void {
    this.patientInfoForm.get('height').valueChanges.subscribe(value => {
      this.testHight = value;
    });
    this.patientInfoForm.get('weight').valueChanges.subscribe(weight => {
      const heightCal = this.testHight / 100 * this.testHight / 100;
      // heightCal = heightCal * 0.025;
      const newValue = weight / heightCal;
      // console.log('newValue');
      // console.log(newValue);
      this.patientInfoForm.get('bmiCalcuation').setValue(Math.round(newValue));

    });
  }
  decryptData(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, this.Secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }

  // onKey($event) {
  //   this.mobileNumber = $event.target.value;
  //   if (this.mobileNumber.length === 10) {
  //     this.loader = true;
  //     // alert('iam in onKeyup');

  //     const headers = new Headers(
  //       {
  //         'accept': 'application/json',
  //         'x-access-token': this.userToken
  //       });
  //     const reqbody = this.mobileNumber;
  //     this.seniorDoctorService.getExistigPatientDetails(reqbody, headers).subscribe(data => {
  //       this.existPatientObject = JSON.parse(JSON.stringify(data));
  //       console.log('vvvvvvvvvvvbbbbbbbbbbbnnnnnnnnn', data);
  //       // console.log('qqqqqqqqqqqqqqqqqq', this.existPatientObject.patientData.age);
  //       if (this.existPatientObject.patientData.length === 0) {
  //         this.loader = false;

  //         //   if (data.patientQueueData.length === 0) {
  //         //     this.isPayNowDisabled = false;
  //         //  } else if (data.patientQueueData.length !== 0) {
  //         //    this.isPayNowDisabled = true;
  //         // }
  //         // console.log('xxxxxxxxxyyyyyyyy', data.patientQueueData.length);
  //       }

  //       if (this.existPatientObject.patientData.length !== 0) {
  //         this.loader = false;
  //         // this.existphone = true;
  //         // this.newphone = false;
  //         this.buttonDisabled = true;

  //         this.existPatientObject.patientData.forEach(element => {
  //           const age = element.age;
  //           if (age === 0) {
  //             // alert('iam in age2');
  //             this.radioAge1 = true;
  //             this.patientInfoForm.get('gender').setValue(this.genderSetActive(element.gender));
  //           } else {
  //             this.radioAge1 = false;
  //           }
  //           // const dateofbirth = element.dob;
  //           // if (dateofbirth === 0) {
  //           //   // alert('iam in dob');
  //           //   this.patientInfoForm.get('age').setValue(this.onAgeSelection(element.age));
  //           //   this.radioDOB1 = true;
  //           // }

  //           this.existpatientId = element._id;
  //           console.log('iddddddddddddddddddddd', this.existpatientId);
  //         });
  //         console.log(JSON.stringify(this.existPatientObject.patientData));
  //       }
  //       console.log(`##############this.existPatientObject.patientData`);
  //       console.log(JSON.stringify(this.existPatientObject.patientData));
  //       // console.log('aaaaaaaaaaaaaaaaaaaaa', data.patientData.age);

  //       this.newexistpatient = JSON.stringify(this.existPatientObject.patientData);

  //       this.existPatientObject.patientData.forEach(element => {
  //         if (element.firstName) {
  //           this.patientInfoForm.patchValue({
  //             firstName: element.firstName,
  //             lastName: element.lastName,
  //             emailId: element.emailId,
  //             imageUrl: element.imageUrl,
  //             gender: element.gender,
  //             dob: element.dob,
  //             age: element.age
  //           });

  //           this.date4 = new Date(element.dob);
  //           // this.date4 = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
  //           // this.year5 = this.date4.day;
  //           // this.month5 = this.date4.month;
  //           // this.date5 = this.date4.year;
  //           this.year5 = this.datePipe.transform(new Date(element.dob), 'yyyy');
  //           this.month5 = this.datePipe.transform(new Date(element.dob), 'MM');
  //           this.date5 = this.datePipe.transform(new Date(element.dob), 'dd');

  //           // console.log(this.year5);
  //           // console.log(this.month5);
  //           // console.log(this.date5);

  //           const dateObj = {

  //             year: parseInt(this.year5),
  //             month: parseInt(this.month5),
  //             day: parseInt(this.date5)
  //           };
  //           // console.log(dateObj);

  //           this.patientInfoForm.get('dob').setValue(this.onDobSelection(dateObj));
  //           // this.patientForm.get('gender').setValue(this.genderSetActive(element.gender));
  //           // this.patientForm.get('age').setValue(this.onAgeSelection(element.age));
  //         }
  //       });
  //     });
  //   }
  // }

  getExistingpatientData() {
    // this.loader = true;
    // alert('iam in getExisting Patinet data');
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });
    const reqbody = this.lmobileNumber;
    this.seniorDoctorService.getExistigPatientDetails(reqbody, headers).subscribe(data => {
      this.existPatientObject = JSON.parse(JSON.stringify(data));
      console.log('vvvvvvvvvvvbbbbbbbbbbbnnnnnnnnn', data);
      this.patientDataObj = data.patientData;
      console.log('zzzzzzxxxxxxxxxxccccccccccccvvvvvvvv', this.patientDataObj);

      this.patientDataObj.forEach(dateofbirth => {
        console.log('dateofBirth', dateofbirth.dob);
        if (dateofbirth.dob.length !== 0) {
          this.radioAge2 = true;
        }
        if (dateofbirth.dob.length === 0) {
          this.radioAge2 = false;
        }
      });


      // console.log(JSON.stringify(this.existPatientObject.patientData));
      // console.log('this.existPatientObject.patientData.address', this.existPatientObject.patientData[0].address[0].address1);
      // this.existPatientObject.patientData[0].address[0].address1;

      // console.log('aaaaaaaaaaaaaaaaaaaaa', data.patientData.age);

      this.newexistpatient = JSON.stringify(this.existPatientObject.patientData);

      this.existPatientObject.patientData.forEach(element => {
        if (element.firstName) {
          this.patientInfoForm.patchValue({
            mobileNumber: element.mobileNumber,
            firstName: element.firstName,
            lastName: element.lastName,
            emailId: element.emailId,
            imageUrl: element.imageUrl,
            gender: element.gender,
            dob: element.dob,
            age: element.age,
            address: element.address[0].address1,
            height: element.height,
            weight: element.weight,
            maritalStatus: element.maritalStatus,
            maritalLife: element.maritalLife,
            diteType: element.diteType,
            usedHomeopathic: element.usedHomeopathic,
            maleChildren: element.maleChildren,
            femaleChildren: element.femaleChildren
          });

          this.date4 = element.dob;
          // xconsole.log('11111111111111111111111111111', this.date4);

          // this.date4 = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
          // this.year5 = this.date4.day;
          // this.month5 = this.date4.month;
          // this.date5 = this.date4.year;
          this.year5 = this.datePipe.transform(new Date(element.dob), 'yyyy');
          this.month5 = this.datePipe.transform(new Date(element.dob), 'MM');
          this.date5 = this.datePipe.transform(new Date(element.dob), 'dd');

          // console.log(this.year5);
          // console.log(this.month5);
          // console.log(this.date5);

          const dateObj = {

            year: parseInt(this.year5),
            month: parseInt(this.month5),
            day: parseInt(this.date5)
          };
          console.log('dddddddeeeeeeeeeeeffffffffffffggggggggggg', dateObj);
          this.patientInfoForm.get('dob').setValue(this.onDobSelection(dateObj));
          this.patientInfoForm.get('gender').setValue(this.genderSetActive(element.gender));
        }
      });
    });
  }

  updatePatientDetails(ptObj) {

    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });
    this.patientInfoForm.get('orgId').setValue(this.lorgId);
    this.patientInfoForm.get('employeeId').setValue(this.lempId);
    this.patientInfoForm.get('centerId').setValue(this.centerId);
    this.patientInfoForm.get('_id').setValue(this.lpatientId);
    this.patientInfoForm.get('enrolmentId').setValue(this.lenrolmentId);
    this.patientInfoForm.get('gender').setValue(this.getGenderValue);
    // this.patientInfoForm.get('gender').setValue(this.getGenderValue);

    this.patientInfoForm.get('lastVisitedDate').setValue(this.llastVisitedDate);
    // this.patientInfoForm.get('address').setValue( this.newAddress);



    // console.log('this.patientInfoForm.value');
    // console.log(this.patientInfoForm.value);


    this.seniorDoctorService.updatePatient(ptObj, headers).subscribe(data => {
      console.log('vvvvvvvvvvvbbbbbbbbbbbnnnnnnnnnttttttttttt', data);

    this.spinner.hide();

      // console.log(this.patientInfoForm.value);
      // console.log(this.patientInfoForm.value.gender);
      // console.log(this.patientInfoForm.value.dob);
      this.newgender = this.patientInfoForm.value.gender;
      // console.log('this.newgender', this.newgender);
    },
    err=>{
    this.spinner.hide();
    this.notifier.notify('error', 'Something went wrong please try again! ')

    }
    );
  }
}
