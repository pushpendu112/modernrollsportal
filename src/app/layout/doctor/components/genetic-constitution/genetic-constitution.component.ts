import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-genetic-constitution',
  templateUrl: './genetic-constitution.component.html',
  styleUrls: ['./genetic-constitution.component.scss']
})
export class GeneticConstitutionComponent implements OnInit {

  Genetic: FormGroup;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.PatientRegisterForms();
  }

  PatientRegisterForms() {
    this.Genetic = this.formBuilder.group({
      Patient: ['']
    });
  }

  onSubmit() {
    console.log(this.Genetic.value);
  }
}
