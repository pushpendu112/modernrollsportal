import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit {

  title = 'Modern Rolls | Senior Doctor | Patient Personal Information';

  bmiFeet = ['Select Feet', 1, 2, 3, 4, 5, 6, 7, 8, 9];
  bmiInch = ['Select Inch', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
  bmiHeightOption = ['feet-Inch', 'cms'];
  bmiWeightOption = ['Kgs', 'lbs'];
  bmiValue: any;
  selectedFeet = true;
  selectedCms = false;
  feetValue: Number;
  inchValue: Number;
  bmiWeight: Number;
  cm: any;
  weight: any;
  //  inputChange = new Subject();
  constructor(private titleService: Title) { }

  ngOnInit() {   
       this.titleService.setTitle(this.title);

    console.log(this.bmiWeight);
  }
  onChangeHeightOption(heightOption) {
    if (heightOption === 'feet-Inch') {
      this.selectedFeet = true;
      this.selectedCms = false;
    } else if (heightOption === 'cms') {
      this.selectedCms = true;
      this.selectedFeet = false;
    }
  }
  onChangeFeetOption(feetOption) {
    this.feetValue = feetOption;
  }
  onChangeInchOption(inchOption) {
    this.inchValue = inchOption;
  }
  onChangeCms(cms) {
    this.cm =  cms;
    // console.log(this.cm);
  }

  onChangeWeight(weight) {
    this.weight =  weight;
    // console.log(this.weight);
  }
  functionBmicaluculation() {
    this.cm  /= 39.3700787;
    this.weight /= 2.20462;
    const BMI = this.weight / Math.pow(this.cm, 2);
    console.log(BMI);
  }
}
