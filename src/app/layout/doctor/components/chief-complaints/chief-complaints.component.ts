import { Component, OnInit, Input, ViewChild, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgbCalendar, NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import { DomSanitizer } from '@angular/platform-browser';
import { Title } from '@angular/platform-browser';

import * as CryptoJS from 'crypto-js';
import { TestDataService } from 'src/app/shared/services/test-data.service';
import { DoctorComponent } from '../../doctor.component';
import { DoctorService } from '../../doctor.service';

declare var $: any;   // not required


@Component({
  selector: 'app-chief-complaints',
  templateUrl: './chief-complaints.component.html',
  styleUrls: ['./chief-complaints.component.scss']
})
export class ChiefComplaintsComponent implements OnInit {
  title = 'Modern Rolls | Senior Doctro  Chief Complaints';
  private readonly notifier: NotifierService;
  ChiefcomplaintsForm: FormGroup;
  model: NgbDateStruct;
   @Input() name: string;
  @ViewChild('sigPad') sigPad;
  sigPadElement;
  context;
  isDrawing = false;
  img;

  isDisabled;
  isWeekend;
  cols: any[];
  ptName;
  ptTime;
  ptDate;
  patientId;
  newComplaints = false;


  pocenterId: any;
  getjwttoken: any;
  userToken: any;
  patientObj: any;
  existPatientObjectQueue: any;
  counter = true;
  cheifComplaintObj: any;
  addCheifComplaintObj = [];
  isVisbaleAddFile = false;

  secret = 'i_have_some_small_master_secret_live_pin';
  addrow = [];

  docObj = {
    docUrl: '',
    comment: '',
    title: '',
    submissionDate: '',
    uiDatedate: '',
    base64Img: ''
  };

  date;
  date1;
  month1;
  year1;
  date2;
  uiDatedate;
  dateUTC;
  base64: string;
  cheifCompdocUrl;
  cheifComptitle;
  cheifCompcomment;
  cheifCompsubmissionDate;
  imageUrl;
  base64ImageTest = '';
  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private calendar: NgbCalendar,
    private doctorService: DoctorService,
    private passBoolean: TestDataService,
    private sanitizer: DomSanitizer,
    private radioStatusActive: DoctorComponent,
    private spinner: NgxSpinnerService,
    private notifierService: NotifierService,
    private titleService: Title) {
      this.notifier = notifierService;
     }

  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.empRegisterForms();
    this.isDisabled = (date: NgbDate, current: { month: number }) => date.month !== current.month;
    this.isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6;

    this.cols = [
      { field: 'firstName', header: 'Patient Name' },
      { field: 'mobileNumber', header: 'Mobile Numbe' },
      { field: 'gender', header: 'Gender' },
      { field: 'presentStatus', header: 'Status' }
    ];


    this.getjwttoken = localStorage.getItem('jwttoken');
    this.userToken = this.decryptData(this.getjwttoken);
    this.getExistingPatientQueuees();
    const ptid = localStorage.getItem('patientId');
    this.patientId = JSON.parse(ptid);

    const getjwt = localStorage.getItem('masterToken');
    this.userToken = this.conversionDecrypt(getjwt);
    console.log('this.userToken');
    console.log(this.userToken);
    this.getMedicalHistoryByPatientId();

      this.sigPadElement = this.sigPad.nativeElement;
    this.context = this.sigPadElement.getContext('2d');
    this.context.strokeStyle = '#3742fa';
  }

  newComplaint() {
    this.newComplaints = !this.newComplaints;
  }

  getMyProperty(chiefObj) {

    if (chiefObj.docUrl.startsWith('data:image/png;base64,')) {
      this.imageUrl = this.sanitizer.bypassSecurityTrustResourceUrl(chiefObj.docUrl);
      // this.imgURL = imgUrls;
      // console.log(imgUrl);
    } else {
      this.imageUrl = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64, ' + chiefObj.docUrl);
      // this.imgURL = imgUrls;
      // console.log(imgUrl);
    }

  }

  onAddRow() {
    this.isVisbaleAddFile = true;
  }

  addFieldValue(chefcomplain) {
    // if(chefcomplain.docUrl === '' || chefcomplain.comment ===''  || chefcomplain.title ===''  || chefcomplain.submissionDate ===''){
    //   alert("Please fill all the field")
    // }

     if(this.model === undefined){
      chefcomplain.submissionDate = ""
      chefcomplain.uiDatedate = ""
    } else if(this.model === null){
      chefcomplain.submissionDate = ""
      chefcomplain.uiDatedate = ""
    } else{
    this.date = this.model;
    this.date1 = (this.date.day);
    this.month1 = this.date.month;
    this.year1 = this.date.year;
    this.uiDatedate = this.month1 + '/' + this.date1 + '/' + this.year1;
    this.date2 = new Date(this.month1 + '/' + this.date1 + '/' + this.year1);
    chefcomplain.uiDatedate = this.uiDatedate;
    chefcomplain.submissionDate = this.date2.toISOString();
    chefcomplain.base64Img = this.base64;
    this.addCheifComplaintObj.push(chefcomplain);

    this.docObj = {
      docUrl: '',
      comment: '',
      title: '',
      submissionDate: '',
      uiDatedate: '',
      base64Img: ''
    };
    // this.model = null;
    this.isVisbaleAddFile = false;
    }
  }

  deleteFieldValue(index) {
    this.cheifComplaintObj.splice(index, 1);
  }

  chiefComplaintAdd() {
    this.spinner.show();
    if(this.addCheifComplaintObj.length ===0){
      this.spinner.hide();
      this.router.navigate(['/dashboard/Junior Doctor/PastMedicalHistory']);
      this.radioStatusActive.activeRadioButton('pastMedicalHistory');
      this.notifier.notify('error', 'No Document Added')
    }
    this.addCheifComplaintObj.forEach(addComplaint => {
      this.patientObj.forEach(cheifComplaint => {

        const cheifCompObj = {
          docUrl: addComplaint.base64Img,
          title: addComplaint.title,
          comment: addComplaint.comment,
          submissionDate: addComplaint.submissionDate
        };
        cheifComplaint.cheifComplaintsObj.push(cheifCompObj);
        this.updateMedicalHistory(this.patientObj[0]);  
      });


    });

  }
  deleteFieldaddCheif(index) {
    this.addCheifComplaintObj.splice(index, 1);
  }
  hideChiefComplaint() {
    this.isVisbaleAddFile = false;
  }

  empRegisterForms() {
    this.ChiefcomplaintsForm = this.formBuilder.group({
      complainttextbox: ['']
    });
  }

  onSubmit() {
    // this.submitted = true;
    //  $('#exampleModalCenter').modal('hide');
    //  this.empCreation.push(this.empregisterForm.value);
    console.log(this.ChiefcomplaintsForm.value);

    //  this.ChiefcomplaintsForm.reset();

  }




  getExistingPatientQueuees() {

    this.pocenterId = JSON.parse(localStorage.getItem('centerId'));

    const reqbody = this.pocenterId;
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });

    this.doctorService.getPatientQueueForPoc(reqbody, headers).subscribe(data => {

      console.log(`data.patientQueueData`);
      console.log(data.patientQueueData);

      this.existPatientObjectQueue = data.patientQueueData;
      data.patientQueueData.forEach(status => {
        if (status.presentQueueType === 0) {
          status.presentStatus = 'Registration Queue';
        } else if (status.presentQueueType === 1) {
          status.presentStatus = 'Investigation Queue';
        } else if (status.presentQueueType === 2) {
          status.presentStatus = 'Pharmacy Queue';
        } else if (status.presentQueueType === 3) {
          status.presentStatus = 'Diagnostics Queue';
        }

      });
      console.log(this.existPatientObjectQueue);
    });
  }
  decryptData(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, this.secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }
  goToDoctor(data) {
    const statusBody = {
      queueId: data.queueId,
      queueEntryDateAndTime: data.queueEntryDateAndTime,
      arrivalDateAndTime: data.arrivalDateAndTime,
      presentQueueType: data.presentQueueType,
      nextQueueType: 2,
    };

    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });

    this.doctorService.updatePatientQueue(statusBody, headers).subscribe(updateStatus => {

      console.log(updateStatus);
      this.getExistingPatientQueuees();
    });



  }
  valueChanged(data) { // You can give any function name
    console.log(data);
    this.ptName = data.firstName + ' ' + data.lastName;
    this.ptTime = data.arrivalDateAndTime;
    this.ptDate = data.queueEntryDateAndTime;
    // this.valueChange.emit(this.Counter);
    // alert('');
    // this.passBoolean.notifyOther({ option: 'patient-queue', value: this.counter });

  }



  getMedicalHistoryByPatientId() {
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });
    console.log('this.patientId$$$$$$$$$$$$$$$$$$$$$$$$$$$');
    console.log(this.patientId);
    this.doctorService.getMedicalHistoryByPatientId(this.patientId, headers).subscribe(getPatientDetail => {
      this.patientObj = getPatientDetail.patientData;
      this.setMedicalHistoryByPatientId();
      console.log('this.patientObj###############');
      console.log(this.patientObj);

    });
  }

  updateMedicalHistory(patientHistory) {
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });

    this.doctorService.updateMedicalHistory(patientHistory, headers).subscribe(data => {
    

    this.router.navigate(['/dashboard/Junior Doctor/PastMedicalHistory']);
    this.radioStatusActive.activeRadioButton('pastMedicalHistory');
    this.spinner.hide();
    //console.log(data);

    }
      ,
      err => {
        if (err.status === 413) {
          this.spinner.hide();
          this.notifier.notify('error', 'The file size is too large Please select <=100 KB')
          // alert('');
        }
      },
      () => console.log('Good Job'));
  }
   updateMedicalHistoryByCheifComplaint(patientHistory) {
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });

    this.doctorService.updateMedicalHistory(patientHistory, headers).subscribe(data => {
    
    this.spinner.hide();
    //console.log(data);

    }
      ,
      err => {
        if (err.status === 413) {
          this.spinner.hide();
          this.notifier.notify('error', 'The file size is too large Please select <=100 KB');
          // alert('');
        }
      },
      () => console.log('Good Job'));
  }

  setMedicalHistoryByPatientId() {
    this.patientObj.forEach(cheifComplaint => {
      this.cheifComplaintObj = cheifComplaint.cheifComplaintsObj;
    });
  }


  conversionEncrypt(encrypt) {
    try {
      return CryptoJS.AES.encrypt(JSON.stringify(encrypt), this.secret).toString();
    } catch (e) {
      console.log(e);
    }
  }
  conversionDecrypt(decrypt) {
    try {
      const bytes = CryptoJS.AES.decrypt(decrypt, this.secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return decrypt;
    } catch (e) {
      console.log(e);
    }
  }


  public picked(event) {
    // this.currentId = field;
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      // if (field == 1) {
      // this.sellersPermitFile = file;
      this.handleInputChange(file); // turn into base64
      // }
    } else {
      alert('No file selected');
    }
  }

  handleInputChange(files) {
    const file = files;
    const pattern = /image-*/;
    const reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onloadend = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleReaderLoaded(e) {
    const reader = e.target;
    const base64result = reader.result.substr(reader.result.indexOf(',') + 1);
    // this.imageSrc = base64result;
    this.base64 = base64result;
    // let id = this.currentId;
    // switch (id) {
    //   case 1:
    //     this.base64 = base64result;
    //     break;
    // }

    console.log(this.base64);

  }

  @HostListener('document:mouseup', ['$event'])

  onMouseUp(e) {
    // console.log('@@@@@@@@@@@@@@@@@@@@@', e);
    this.isDrawing = false;
  }

  onMouseDown(e) {
    // console.log('########', e);
    this.isDrawing = true;
    const coords = this.relativeCoords(e);
    this.context.moveTo(coords.x, coords.y);
  }

  onMouseMove(e) {
    // console.log('$$$$$$$$$', e);
    if (this.isDrawing) {
      const coords = this.relativeCoords(e);
      this.context.lineTo(coords.x, coords.y);
      this.context.stroke();
    }
  }
  private relativeCoords(event) {
    // console.log('relativeCoords', event);
    const bounds = event.target.getBoundingClientRect();
    const x = event.clientX - bounds.left;
    const y = event.clientY - bounds.top;
    return { x: x, y: y };
  }

  clear() {
    this.context.clearRect(0, 0, this.sigPadElement.width, this.sigPadElement.height);
    this.context.beginPath();
  }

  // save() {


  // }

  AddBase64CheifComplaint() {
    this.img = this.sigPadElement.toDataURL('image/png');

    console.log(this.img);
    if(this.patientObj.length === 0){
      this.notifier.notify('error', 'Somthing went wrong please try again later')
    }
    this.patientObj.forEach(cheifComplaint => {
      const newcheifCompObj = {
        docUrl: this.img,
        title: 'prescription',
        comment: 'prescription',
        submissionDate: new Date().toISOString()
      };
      this.spinner.show();
      cheifComplaint.cheifComplaintsObj.push(newcheifCompObj);
      this.updateMedicalHistoryByCheifComplaint(this.patientObj[0]);

    });
  }






}
