import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalMakeupComponent } from './physical-makeup.component';

describe('PhysicalMakeupComponent', () => {
  let component: PhysicalMakeupComponent;
  let fixture: ComponentFixture<PhysicalMakeupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysicalMakeupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicalMakeupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
