import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicalFindingsComponent } from './clinical-findings.component';

describe('ClinicalFindingsComponent', () => {
  let component: ClinicalFindingsComponent;
  let fixture: ComponentFixture<ClinicalFindingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicalFindingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicalFindingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
