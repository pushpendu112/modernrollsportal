import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import { Title } from '@angular/platform-browser';
import * as CryptoJS from 'crypto-js';

import { DoctorComponent } from '../../doctor.component';
import { DoctorService } from '../../doctor.service';
@Component({
  selector: 'app-clinical-findings',
  templateUrl: './clinical-findings.component.html',
  styleUrls: ['./clinical-findings.component.scss']
})
export class ClinicalFindingsComponent implements OnInit {
  title = 'Modern Rolls | Senior Doctro Clinical Findings';
  private readonly notifier: NotifierService;
  ChiefcomplaintsForm: FormGroup;
  clinicalFindings: FormGroup;
  secret = 'i_have_some_small_master_secret_live_pin';
  empId;
  orgId;
  centerId;
  patientId;
  clinicalFindingsObj:any;
  userToken: any;
  patientObj: any;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private doctorService: DoctorService,
              private radioStatusActive: DoctorComponent,
              private spinner: NgxSpinnerService,
              private notifierService: NotifierService,
              private titleService: Title) {
                this.notifier = notifierService; 
              }

  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.PatientRegisterForms();
    const ptid = localStorage.getItem('_id');
    this.patientId = JSON.parse(ptid)
    const getjwt = localStorage.getItem('jwttoken');
    this.userToken = this.conversionDecrypt(getjwt);
    this.getMedicalHistoryByPatientId();
  }


  PatientRegisterForms() {
    this.clinicalFindings = this.formBuilder.group({
      git: [''],
      rs: [''],
      lungs: [''],
      cns: [''],
      pulse: [''],
      bp: [''],
      geneticConstitution: [''],
    });
  }
  onSubmit() {
     if(this.patientObj.length ===0){
      this.spinner.hide();

      this.radioStatusActive.activeRadioButton('clinicalFinding');
      this.notifier.notify('error', 'Something went wrong please try again later!')
    }
    console.log(this.clinicalFindings.value);
    this.clinicalFindingsObj = this.clinicalFindings.value
     this.patientObj.forEach(clinicalFinding=>{
         const clinicalFind = clinicalFinding.clinicalAndGeneticHistoryObj;
         clinicalFind.git = this.clinicalFindingsObj.git;
         clinicalFind.rs = this.clinicalFindingsObj.rs;
         clinicalFind.cns = this.clinicalFindingsObj.cns;
         clinicalFind.lungs = this.clinicalFindingsObj.lungs;
         clinicalFind.pulse = this.clinicalFindingsObj.pulse;
         clinicalFind.bp = this.clinicalFindingsObj.bp;
         clinicalFind.geneticConstitution = this.clinicalFindingsObj.geneticConstitution;
         this.spinner.show();
         this.updateMedicalHistory(this.patientObj[0]);
     })
   // this.router.navigate(['/dashboard/patientInfo/PhysicalMakeup']);
  

  }

   getMedicalHistoryByPatientId(){
   const headers = new Headers(
        {
            'accept': 'application/json',
            'x-access-token': this.userToken
        });
     this.doctorService.getMedicalHistoryByPatientId(this.patientId, headers).subscribe(getPatientDetail=>{
         this.patientObj = getPatientDetail.patientData;
         this.setMedicalHistoryByPatientId();
          this.patientObj.forEach(obsGynac=>{
            const obsGynacObj = obsGynac.oBSAndGynacHistoryObj;

          });
     });
  }
  updateMedicalHistory(patientHistory) {
     const headers = new Headers(
          {
              'accept': 'application/json',
              'x-access-token': this.userToken
          });
     this.doctorService.updateMedicalHistory(patientHistory, headers).subscribe(data => {
       // console.log('data data data ')
       // console.log(data);
        this.spinner.hide();
        this.router.navigate(['/dashboard/Junior Doctor/PhysicalMakeup']);
        this.radioStatusActive.activeRadioButton('physicalMakeup');

     },
    err=>{
      this.spinner.hide();
      this.notifier.notify('error', 'Something went wrong please try again!')
    });
  }

  setMedicalHistoryByPatientId() {
    this.patientObj.forEach(patientHistory => {
       const clinicaObj = patientHistory.clinicalAndGeneticHistoryObj;
       this.clinicalFindings.get('git').patchValue(clinicaObj.git);
       this.clinicalFindings.get('rs').patchValue(clinicaObj.rs);
       this.clinicalFindings.get('cns').patchValue(clinicaObj.cns);
       this.clinicalFindings.get('lungs').patchValue(clinicaObj.lungs);
       this.clinicalFindings.get('pulse').patchValue(clinicaObj.pulse);
       this.clinicalFindings.get('bp').patchValue(clinicaObj.bp);
       this.clinicalFindings.get('geneticConstitution').patchValue(clinicaObj.geneticConstitution);
    });
  }


 conversionEncrypt(encrypt) {
      try {
          return CryptoJS.AES.encrypt(JSON.stringify(encrypt), this.secret).toString();
      } catch (e) {
          console.log(e);
      }
  }
 conversionDecrypt(decrypt) {
      try {
          const bytes = CryptoJS.AES.decrypt(decrypt, this.secret);
          if (bytes.toString()) {
              return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
          }
          return decrypt;
      } catch (e) {
          console.log(e);
      }
  }
}
