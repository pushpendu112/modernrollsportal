import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { TabledataService } from 'src/app/shared/services/tabledata.service';
import { Router } from '@angular/router';

import * as CryptoJS from 'crypto-js';
import { DoctorService } from '../../doctor.service';
import { TestDataService } from 'src/app/shared/services/test-data.service';

import { DoctorComponent } from '../../doctor.component';



@Component({
  selector: 'app-patient-queue',
  templateUrl: './patient-queue.component.html',
  styleUrls: ['./patient-queue.component.scss']
})
export class PatientQueueComponent implements OnInit {
  public reciveTableData: any = [];
  cols: any[];
  sortF: any;
  statusChange: any;

  Secret = 'i_have_some_small_master_secret_live_pin';
  userToken: any;
  getjwttoken: any;
  existPatientObjectQueue: any;
  pocenterId: any;

  constructor(private tableDatas: TabledataService, private router: Router,
    private doctorService: DoctorService,
    private passBoolean: TestDataService,
    private radioActiveTab: DoctorComponent ) { }

    @Output() valueChange = new EventEmitter();
    counter = true;

  ngOnInit() {
    this.cols = [
      { field: 'firstName', header: 'Patient Name' },
      { field: 'mobileNumber', header: 'Mobile Numbe' },
      { field: 'gender', header: 'Gender' },
      { field: 'presentStatus', header: 'Status' }
    ];
    // Reciving Table Data Values.
    // this.tableDatas.getTableData().subscribe((data) => this.reciveTableData = data);
    this.getExistingPatientQueuees();
    this.getjwttoken = localStorage.getItem('jwttoken');
    this.userToken = this.decryptData(this.getjwttoken);
  }

  // createPatientQueue() {

  // }
  valueChanged() { // You can give any function name
    // this.valueChange.emit(this.Counter);
    //alert('No');
    this.radioActiveTab.activeRadioButton('generalInfo');
    this.passBoolean.notifyOther({ option: 'patient-queue', value: this.counter });

  }

  onRowSelect(event) {
    console.log(event);
  }
  changeSort(event) {
    if (!event.order) {
      this.sortF = 'year';
    } else {
      this.sortF = event.field;
    }
  }
  selectedRowData(data) {
    // console.log(data);
    this.router.navigate(['/dashboard/Doctor']);
    this.statusChange = data.patientMobile;
  }


  getExistingPatientQueuees() {

    this.pocenterId = JSON.parse(localStorage.getItem('centerId'));
    const reqbody = this.pocenterId;
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });

    this.doctorService.getPatientQueueForPoc(reqbody, headers).subscribe(data => {

      console.log(`data.patientQueueData`);
      console.log(data.patientQueueData);

      this.existPatientObjectQueue = data.patientQueueData;
      data.patientQueueData.forEach(status => {
        if (status.presentQueueType === 0) {
          status.presentStatus = 'Registration Queue';
        } else if (status.presentQueueType === 1) {
          status.presentStatus = 'Investigation Queue';
        } else if (status.presentQueueType === 2) {
          status.presentStatus = 'Doctor Queue';
        } else if (status.presentQueueType === 3) {
          status.presentStatus = 'Manager Queue';
        } else if (status.presentQueueType === 4) {
          status.presentStatus = 'Diagnostics Queue';
        } else if (status.presentQueueType === 5) {
          status.presentStatus = 'Pharmacy Queue';
        } else if (status.presentQueueType === 6) {
          status.presentStatus = 'Medicine Issued';
        }
      });
      console.log(this.existPatientObjectQueue);
    });
  }
  decryptData(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, this.Secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }
  goToDiagnostics(data) {
    const statusBody = {
      queueId: data.queueId,
      queueEntryDateAndTime: data.queueEntryDateAndTime,
      arrivalDateAndTime: data.arrivalDateAndTime,
      presentQueueType: data.presentQueueType,
      nextQueueType: 4,
    };

    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });

    this.doctorService.updatePatientQueue(statusBody, headers).subscribe(updateStatus => {
      console.log(updateStatus);
      this.getExistingPatientQueuees();
    });
  }
  goToManger(data) {
    const statusBody = {
      queueId: data.queueId,
      queueEntryDateAndTime: data.queueEntryDateAndTime,
      arrivalDateAndTime: data.arrivalDateAndTime,
      presentQueueType: data.presentQueueType,
      nextQueueType: 3,
    };

    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });

    this.doctorService.updatePatientQueue(statusBody, headers).subscribe(updateStatus => {
      console.log(updateStatus);
      this.getExistingPatientQueuees();
    });
  }
}

