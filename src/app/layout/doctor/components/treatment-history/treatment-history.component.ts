import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import { Title } from '@angular/platform-browser';
import * as CryptoJS from 'crypto-js';
import { SubSink } from 'subsink';
import { DoctorComponent } from '../../doctor.component';
import { DoctorService } from '../../doctor.service';

@Component({
  selector: 'app-treatment-history',
  templateUrl: './treatment-history.component.html',
  styleUrls: ['./treatment-history.component.scss']
})
export class TreatmentHistoryComponent implements OnInit {

  title = 'Modern Rolls | Senior Doctor | Patient Treatment History';
   private readonly notifier: NotifierService;
   private subs = new SubSink();
  treatmentHistory: FormGroup;
  secret = 'i_have_some_small_master_secret_live_pin';
   empId;
   orgId;
   centerId;
   patientId;
   userToken: any;
   patientObj: any;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
              private seniorDoctorService: DoctorService,
              private radioStatusActive: DoctorComponent,
               private spinner: NgxSpinnerService,
              private notifierService: NotifierService,
              private titleService: Title) {
                this.notifier = notifierService;  }

  ngOnInit() {
     this.titleService.setTitle(this.title);
    this.PatientRegisterForms();
    const ptid = localStorage.getItem('_id');
    this.patientId = JSON.parse(ptid);
    const getjwt = localStorage.getItem('jwttoken');
    this.userToken = this.conversionDecrypt(getjwt);
    this.getMedicalHistoryByPatientId();
  }
  PatientRegisterForms() {
    this.treatmentHistory = this.formBuilder.group({
      consultedbydr: [''],
      specialist: [''],
      genPhysician: [''],
      genPhysician1: [''],
      genPhysician2: [''],
      homeopath: [''],
      homeopath1: [''],
      homeopath2: [''],
      rmp: [''],
      selfRX: [''],
      other: [''],
      otherExtra: [''],
      noRXTaken: [''],
      treatmentHistoryOthers: ['']
    });

  }
  // nextpage() {
  //   this.router.navigate(['/dashboard/patientInfo/LabInvestigations']);
  // }
  onSubmit() {
    console.log(this.treatmentHistory.value.consultedbydr);
     if(this.patientObj.length ===0){
      this.notifier.notify('error', 'Something went wrong please try again later!')
      this.spinner.hide();
    }
     this.patientObj.forEach(treatmentHistory => {
           const obsGynacObj = treatmentHistory.treatmentHistoryObj;
           obsGynacObj.values=this.treatmentHistory.value;
           obsGynacObj.consultedBy = this.treatmentHistory.value.consultedbydr;
         this.spinner.show();
         this.updateMedicalHistory(this.patientObj[0]);
     });
    
    // this.router.navigate(['/dashboard/patientInfo/LabInvestigations']);
  }





  getMedicalHistoryByPatientId() {
   const headers = new Headers(
        {
            'accept': 'application/json',
            'x-access-token': this.userToken
        });
     this.seniorDoctorService.getMedicalHistoryByPatientId(this.patientId, headers).subscribe(getPatientDetail => {
         this.patientObj = getPatientDetail.patientData;
         this.setMedicalHistoryByPatientId();
         console.log('this.patientObj');
         console.log(this.patientObj);
          this.patientObj.forEach(obsGynac => {
            const obsGynacObj = obsGynac.oBSAndGynacHistoryObj;

          });
     });
  }

  updateMedicalHistory(patientHistory) {
     const headers = new Headers(
          {
              'accept': 'application/json',
              'x-access-token': this.userToken
          });
     this.seniorDoctorService.updateMedicalHistory(patientHistory, headers).subscribe(data => {
        this.spinner.hide();
        this.router.navigate(['/dashboard/Junior Doctor/LabInvestigations']);
        this.radioStatusActive.activeRadioButton('labInvestigation');

     },
     err=>{
      this.spinner.hide();
      this.notifier.notify('error', 'Something went wrong please try again!')
     });
  }

  setMedicalHistoryByPatientId() {
    this.patientObj.forEach(treatmentHistory => {
      const treatmentHistoryValue = treatmentHistory.treatmentHistoryObj;
      treatmentHistoryValue.values.forEach(value => {
        this.treatmentHistory.get('consultedbydr').patchValue(value.consultedbydr);
        this.treatmentHistory.get('specialist').patchValue(value.specialist);
        this.treatmentHistory.get('genPhysician').patchValue(value.genPhysician);
        this.treatmentHistory.get('genPhysician1').patchValue(value.genPhysician1);
        this.treatmentHistory.get('genPhysician2').patchValue(value.genPhysician2);
        this.treatmentHistory.get('homeopath').patchValue(value.homeopath);
        this.treatmentHistory.get('homeopath1').patchValue(value.homeopath1);
        this.treatmentHistory.get('homeopath2').patchValue(value.homeopath2);
        this.treatmentHistory.get('rmp').patchValue(value.rmp);
        this.treatmentHistory.get('selfRX').patchValue(value.selfRX);
        this.treatmentHistory.get('other').patchValue(value.other);
        this.treatmentHistory.get('otherExtra').patchValue(value.otherExtra);
        this.treatmentHistory.get('noRXTaken').patchValue(value.noRXTaken);
        this.treatmentHistory.get('treatmentHistoryOthers').patchValue(value.treatmentHistoryOthers);
      });

    });
  }


   conversionEncrypt(encrypt) {
        try {
            return CryptoJS.AES.encrypt(JSON.stringify(encrypt), this.secret).toString();
        } catch (e) {
            console.log(e);
        }
    }
    conversionDecrypt(decrypt) {
        try {
            const bytes = CryptoJS.AES.decrypt(decrypt, this.secret);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return decrypt;
        } catch (e) {
            console.log(e);
        }
    }
}
