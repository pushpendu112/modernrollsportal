import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DoctorRoutingModule } from './doctor-routing.module';
import { PatientQueueComponent } from './components/patient-queue/patient-queue.component';
import { PaymentComponent } from './components/payment/payment.component';
import { ReceptionComponent } from './components/reception/reception.component';
import { DoctorComponent } from './doctor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {FileUploadModule} from 'primeng/fileupload';
import { NgxSpinnerModule } from 'ngx-spinner';

import {DataTableModule} from 'primeng/datatable';
import { AccordionModule } from 'primeng/accordion';
import { SharedModule } from 'src/app/shared/validator-directives/shared.module';
import { BasePatientInfoModule } from 'src/app/base-patient-info/base-patient-info.module';
import { JuniorDoctorModule } from '../junior-doctor/junior-doctor.module';

import { FamilyMedicalHistoryComponent } from './components/family-medical-history/family-medical-history.component';
import { PersonalInformationComponent } from './components/personal-information/personal-information.component';
import { ReportedSymptomsComponent } from './components/reported-symptoms/reported-symptoms.component';
import { PersonalLifestyleHistoryComponent } from './components/personal-lifestyle-history/personal-lifestyle-history.component';
import { MedicationHistoryComponent } from './components/medication-history/medication-history.component';
import { ObsGynacHistoryComponent } from './components/obs-gynac-history/obs-gynac-history.component';
import { GeneralInformationComponent } from './components/general-information/general-information.component';
import { TreatmentHistoryComponent } from './components/treatment-history/treatment-history.component';
import { PhysicalMakeupComponent } from './components/physical-makeup/physical-makeup.component';
import { ClinicalFindingsComponent } from './components/clinical-findings/clinical-findings.component';
import { LabInvestigationsComponent } from './components/lab-investigations/lab-investigations.component';
import { GeneticConstitutionComponent } from './components/genetic-constitution/genetic-constitution.component';

import { PhysicalGeneralsComponent } from './components/physical-generals/physical-generals.component';
import { SummaryComponent } from './components/summary/summary.component';
import { MentalGeneralsComponent } from './components/mental-generals/mental-generals.component';
import { PatientqueueComponent } from './components/patientqueue/patientqueue.component';
import { NurseModule } from '../nurse/nurse.module';
import { PastMedicalHistoryComponent } from './components/past-medical-history/past-medical-history.component';
import { ChiefComplaintsComponent } from './components/chief-complaints/chief-complaints.component';

@NgModule({
  declarations: [PatientQueueComponent, PaymentComponent, ReceptionComponent, DoctorComponent,
    PastMedicalHistoryComponent,
    FamilyMedicalHistoryComponent,
    PersonalInformationComponent,
   ReportedSymptomsComponent,
   PersonalLifestyleHistoryComponent,
   MedicationHistoryComponent,
   GeneralInformationComponent,
   ObsGynacHistoryComponent,
   TreatmentHistoryComponent,
   PhysicalMakeupComponent,
   ClinicalFindingsComponent,
   LabInvestigationsComponent,
   GeneticConstitutionComponent,
   ChiefComplaintsComponent,
   PhysicalGeneralsComponent,
   MentalGeneralsComponent,
   SummaryComponent,
   PatientqueueComponent,

  ],
  imports: [
    CommonModule,
    DoctorRoutingModule,
    AccordionModule,
    DataTableModule,
    SharedModule,
    BasePatientInfoModule,
   FormsModule,
   ReactiveFormsModule,
   NgbModule,
   FileUploadModule,
   NurseModule,
   NgxSpinnerModule
  ]
})
export class DoctorModule { }
