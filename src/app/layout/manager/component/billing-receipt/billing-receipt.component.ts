import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';

// import * as jspdf from 'jspdf';
import * as CryptoJS from 'crypto-js';
import * as jsPDF from 'jspdf';
declare var $: any;   // not required

import html2canvas from 'html2canvas';
import { Location } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { JuniorDoctorService } from 'src/app/layout/junior-doctor/junior-doctor.service';
import { TestDataService } from 'src/app/shared/services/test-data.service';
import { Subscription } from 'rxjs';

interface Payment {
  id?: number;
  particular: string;
  quantity: number;
  amount: number;
}

const PAYMENT: Payment[] = [
  {
    particular: 'Register',
    quantity: 1,
    amount: 100
  },
  {
    particular: 'Register',
    quantity: 1,
    amount: 100
  },
  {
    particular: 'Doctor',
    quantity: 1,
    amount: 1000
  }
];

@Component({
  selector: 'app-billing-receipt',
  templateUrl: './billing-receipt.component.html',
  styleUrls: ['./billing-receipt.component.scss']
})
export class BillingReceiptComponent implements OnInit {
  private readonly notifier: NotifierService;

  constructor(
    private location: Location,
    private juniorDoctorService: JuniorDoctorService,
    private sanitizer: DomSanitizer,
    private el: ElementRef,
    private billingReciveData: TestDataService,
    private spinner: NgxSpinnerService,
    private notifierService: NotifierService) {
      this.notifier = notifierService;  }

    private subscription: Subscription;
    public reciveDataValue: any = [];

  get gstRemainderCal() {
    return (this.totalamount - this.gstValue).toFixed(2);
  }
  @ViewChild('content') content: ElementRef;

  log: any = 'assets/images/masterhomelogo_header.png';

  page = 1;
  pageSize = 4;
  // collectionSize = PAYMENT.length;
  totalamount = null;
  gstValue: any;
  gstRemainder: any;
  modeofPayment = ['Cash', 'Credit Card', 'Check'];
  isPrint = true;
  afterPaidOnly = false;
  beforepaid = true;



  cheifComplaintObj: any;
  pocenterId: any;
  getjwttoken: any;
  userToken: any;
  patientObj: any;
  secret = 'i_have_some_small_master_secret_live_pin';
  patinetdata: any;
  date;
  date1;
  month1;
  year1;
  date2;
  uiDatedate;
  dateUTC;
  base64: string;
  cheifCompdocUrl;
  cheifComptitle;
  cheifCompcomment;
  cheifCompsubmissionDate;
  imageUrl;
  docTitle;
  patientName;
  paymentType;
  setOrgId: any;
  centerId: any;
  employeeId: any;
  queId: any;
  getTransaction: any;

  transactionMedicionDays: any;
  transactionInvideId: any;
  transactionAmount: any;
  transactioDateTime: any;
  transactionTax: any;
  TransactionType: any;


  paymentTypes = [
    { id: 0, value: 'Cash' },
    { id: 1, value: 'Online ' },
  ];

  productType = 3;
  // productType = 3  Manager;

  adminTracsaction = {
    days: '',
    amount: ''
  };

  patientId;
  existPatientObjectQueue: any;


  // get payment(): Payment[] {
  //   return PAYMENT
  //     .map((payment, i) => ({ id: i + 1, ...payment }))
  //     .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  // }
  @Output() valueChange = new EventEmitter();
  counter = true;

  ngOnInit() {
     // this.getTotalAmount();
    // this.gstValueDetaction();

    this.getjwttoken = localStorage.getItem('jwttoken');
    this.userToken = this.conversionDecrypt(this.getjwttoken);
    // const ptid = localStorage.getItem('patientId');
    // this.patientId = JSON.parse(ptid);

    const ptName = localStorage.getItem('patientName');
    this.patientName = JSON.parse(ptName);
    this.setOrgId = JSON.parse(localStorage.getItem('orgId'));
    this.centerId = JSON.parse(localStorage.getItem('centerId'));

    const empId = localStorage.getItem('empId');
    this.employeeId = empId;

    const getjwt = localStorage.getItem('jwttoken');
    this.userToken = this.conversionDecrypt(getjwt);
    console.log('this.userToken');
    console.log(this.userToken);

    const getQueId = localStorage.getItem('queId');
    this.queId = this.conversionDecrypt(getQueId);

 const ptid = localStorage.getItem('patientId');
// console.log('ptid', ptid);
this.patientId = (this.conversionDecrypt(ptid));


    console.log('this.queIdqqqqqqqwwwwwwwwweeeeeeee', this.queId);
    this.getMedicalHistoryByPatientId();

    this.subscription = this.billingReciveData.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('option') && res.option === 'pt-data') {
        console.log('Feature Value form header files');

        // alert('iam in ng onit');
        // this.patinetdata = res.value;
        // console.log('mmmmmmmmmmm',  this.patinetdata);
        this.reciveDataValue = res.value;
        console.log('nnnnnnnnnnnn', this.reciveDataValue);
        sessionStorage.setItem('key', JSON.stringify(this.reciveDataValue));
        // this.roleRouting = this.reciveDataValue.roleName;
        // perform your other action from here
      }
    });
    const getTracsaction = (`${this.queId}${'/'}${this.productType}`);
    this.getTransactionForQueue(getTracsaction);
   

  }


  getAmount(event) {
    console.log(event.target.value);
    this.getTotalAmount(event.target.value);
  }

  getTotalAmount(value) {
    let total = '';
    total += value;
    this.totalamount = total;
    this.gstValueDetaction(this.totalamount);
    // for (let i = 0; i < this.payment.length; i++) {
    //   if (this.payment[i].amount) {
    //     total += this.payment[i].amount;
    //     this.totalamount = total.toFixed(2);
    //   }
    // }
    console.log('Total', total);
    //  return total;
  }

  gstValueDetaction(amount) {
    const gstdetection = 5;
    const gstAmount = amount * ((100 - gstdetection) / 100);
    this.gstValue = gstAmount.toFixed(2);
  }

  // selectChangeHandler (event: any) {
  //   console.log(event.target.value)

  // }

  selected() {
    console.log(this.paymentType);
  }

  public captureScreenpdf() {
    const data = document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      const imgWidth = 208;
      const pageHeight = 295;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      const heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png');
      const pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
      const position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
      pdf.save('MYPdf.pdf'); // Generated PDF
    });
    // html2canvas(document.body,{
    //   onrendered:(canvass)=>{
    //     let img = canvass.toDataURL('image/png');
    //     let doc= new jsPDF();
    //     doc.addImage(img,'JPEG',20,20);
    //     doc.sav('test.pdf')
    //   }
    // })

    // html2canvas(data).then(canvas => {
    //   // Few necessary setting options
    //   // const imgWidth = 208;
    //   // const pageHeight = 295;
    //   // const imgHeight = canvas.height * imgWidth / canvas.width;
    //   // const heightLeft = imgHeight;

    //   const contentDataURL = canvas.toDataURL('image/png');
    //   const pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
    //   const position = 0;
    //   // pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
    //   pdf.addImage(contentDataURL, 'PNG', 0, position);
    //   pdf.save('MYPdf.pdf'); // Generated PDF
    // });
  }

  public makePdf() {
    const doc = new jsPDF();
    const elementHandler = {
      '#editor': function (element, renderer) {
        return true;
      }

    };
    const content = this.content.nativeElement;
    doc.fromHTML(content.innerHTML, 15, 15, {
      'width': 190,
      'elementHandlers': elementHandler
    });
    doc.save('test.pdf');
  }


  print(): void {
    this.isPrint = false;
    this.getTransaction.TransactionData.forEach(transction => {
      this.transactionMedicionDays = transction.days;
      this.transactionInvideId = transction.invoiceId;
      this.transactionAmount = transction.totalAmount;
      this.transactioDateTime = transction.paymentDateAndTime;
      if (transction.paymentType === 0) {
        this.TransactionType = 'Cash';
      } else if (transction.paymentType === 1) {
        this.TransactionType = 'Online';
      }
      transction.productList.forEach(product => {
        this.transactionTax = product.tax;
      });
    });
    setTimeout(() => {
      let printContents, popupWin;
      printContents = document.getElementById('print-section').innerHTML;
      popupWin = window.open('', '_blank');
      popupWin.document.open();
      popupWin.document.write(`
        <html>
          <head>
            <title>Invice ${this.transactionInvideId}</title>
           <style>
           table.borderStyle{
                border: 1px solid black;
            }
            table.borderStyle thead tr th{
              border-bottom: 1px solid black;
            }
             table.borderStyle thead tr th:nth-child(1){
              border-right: 1px solid black;
            }

            table.borderStyle thead tr th:nth-child(2){
              border-right: 1px solid black;
            }

            table.borderStyle thead tr th:nth-child(3){
              border-right: 1px solid black;
            }
            table.borderStyle tbody tr th:nth-child(1){
              border-right: 1px solid black;
            }
            table.borderStyle tbody tr td:nth-child(2){
              border-right: 1px solid black;
                padding-left: 12%;
            }
            table.borderStyle tbody tr td:nth-child(3){
              border-right: 1px solid black;
              padding-left: 13%;
            }
            table.borderStyle tbody tr td:nth-child(4){
              padding-left: 8%;
            }
             body{
             width:100%;
           }
           @media print {
              body, page[size="A4"] {
                margin: 0;
                box-shadow: 0;
              }
           </style>

          </head>

        <body onload="window.print();window.close()">
            <table width="100%" id="print-section" cellspacing="0" cellpadding="0" >
                <tr>
                    <td width="600px" style="border-top:0px; vertical-align: top;border-top:0px;">
                        <a><img src="../../../../../assets/images/masterhomelogo_header.png" alt="logo"></a>
                    </td>
                    <td style='border-top:0px'>
                        <table   class="printImageWidth" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="25%" style="border-top: 0px;" >
                                    <span class="font-weight-bold">Phone</span>
                                </td>
                                <td width="75%" style="border-top: 0px;" >
                                    <span>9787519529</span>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" style="border-top: 0px;">
                                    <span class="font-weight-bold">Fax</span>
                                </td>
                                <td width="75%" style="border-top: 0px;">
                                    <span>024658546</span>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" style="border-top: 0px;">
                                    <span class="font-weight-bold">Email</span>
                                </td>
                                <td width="75%" style="border-top: 0px;">
                                    <span>info@modernrolls.com</span>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" style="border-top: 0px;">
                                    <span class="font-weight-bold">D.No</span>
                                </td>
                                <td width="75%" style="border-top: 0px;">
                                    <span>865/2</span>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" style="border-top: 0px;padding-top: 0px;padding-bottom: 0px;">
                                    <span class="font-weight-bold">Rayala Cheruvu Road</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-top: 0px;padding-top: 0px;padding-bottom: 0px;">
                                    <span class="font-weight-bold">Royal Nagar</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-top: 0px;padding-top: 0px;padding-bottom: 0px;">
                                    <span class="font-weight-bold">Tirupati - 517501</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" style="border-top:0px">
                        <table width="100%" style="margin-top:20px;">
                            <tr>
                                <td style="border-top: 0px;padding-top: 0px;padding-bottom: 0px; white-space: nowrap;" width="10%">
                                    <span class="font-weight-bold">Patient Name</span>
                                </td>
                                <td style="border-top: 0px;padding-top: 0px;padding-bottom: 0px;">
                                    <span>${this.patientName}</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-top: 0px;padding-top: 0px;padding-bottom: 0px;" width="10%">
                                    <span class="font-weight-bold">Patient Id</span>
                                </td>
                                <td style="border-top: 0px;padding-top: 0px;padding-bottom: 0px;">
                                    <span>${this.patientId}</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="100%" id="print-section" cellspacing="0" cellpadding="0" style="padding-top:100px;">
                  <tr>
                      <td width="70%">
                      </td>
                      <td width="30%">
                          <table width="100%" id="print-section" cellspacing="0" cellpadding="0" style="padding-top:100px;">
                              <tr>
                                  <td width="40%">
                                      <span>Days</span>
                                  </td>
                                  <td>
                                    ${this.transactionMedicionDays}
                                  </td>
                              </tr>
                               <tr>
                                  <td>
                                      <span>Amount ₹</span>
                                  </td>
                                  <td>
                                    ${this.transactionAmount}
                                  </td>
                              </tr>
                               <tr>
                                  <td>
                                      <span>GST ₹</span>
                                  </td>
                                  <td>
                                    ${this.transactionTax}
                                  </td>
                              </tr>
                               <tr>
                                  <td>
                                      <span>Payment Type</span>
                                  </td>
                                  <td>
                                    ${this.TransactionType}
                                  </td>
                              </tr>
                               <tr>
                                  <td>
                                      <span>Total ₹</span>
                                  </td>
                                  <td>
                                    ${this.transactionAmount}
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
             </table>
            <table width="100%" id="print-section" cellspacing="0" cellpadding="0" style="padding-top:100px;">
                <tr>
                  <td>
                      ${this.transactioDateTime.toString()}
                  </td>
                </tr>
            </table>

        </body>
        </html>`
      );
      popupWin.document.close();
    }, 1000);

    setTimeout(() => { this.isPrint = true; }, 1000);

  }

  onGoBack() {
    this.location.back();
  }
  paymentButtons() {
    const amountObj = this.adminTracsaction;
    const setBody = {
      queueId: this.queId,
      patientId: this.patientId,
      orgId: this.setOrgId,
      centerId: this.centerId,
      employeeId: this.employeeId,
      paymentType: this.paymentType,
      productType: this.productType,
      totalAmount: this.totalamount,
      days: amountObj.days,
      productList: [
        {
          productName: 'Manager',
          quantity: '',
          rate: '',
          amount: amountObj.amount,
          tax: this.gstRemainderCal
        }
      ]
    };

    console.log('setBody');
    console.log(setBody);
    this.createTransaction(setBody);

  }






  getMyProperty(chiefObj) {
    this.docTitle = chiefObj.title;

    $('#exampleModalLong').modal('show');
    this.imageUrl = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64, ' + chiefObj.docUrl);
    // this.imgURL = imgUrls;
    // console.log(imgUrl);
  }

  getMedicalHistoryByPatientId() {
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });
    this.spinner.show();
    this.juniorDoctorService.getMedicalHistoryByPatientId(this.patientId, headers).subscribe(getPatientDetail => {
      this.patientObj = getPatientDetail.patientData;
      this.setMedicalHistoryByPatientId();
      console.log('this.patientObj');
      console.log(this.patientObj);
      console.log('getPatientDetail');
      console.log(getPatientDetail);this.spinner.hide();

    }, err => {
      this.spinner.hide();
       this.notifier.notify('error', 'Something went wrong please try again!')
        });
  }

  updateMedicalHistory(patientHistory) {
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });

       this.spinner.show();
    this.juniorDoctorService.updateMedicalHistory(patientHistory, headers).subscribe(data => {
      // this.spinner.hide();

      console.log('data data data ');
      console.log(data);
       this.spinner.hide();

    } ,
      err => {
       this.spinner.hide();
       this.notifier.notify('error', 'Something went wrong please try again!')
      },
      () => console.log('Good Job'));
  }

  setMedicalHistoryByPatientId() {
    this.patientObj.forEach(cheifComplaint => {
      this.cheifComplaintObj = cheifComplaint.cheifComplaintsObj;
    });
  }


  conversionEncrypt(encrypt) {
    try {
      return CryptoJS.AES.encrypt(JSON.stringify(encrypt), this.secret).toString();
    } catch (e) {
      console.log(e);
    }
  }
  conversionDecrypt(decrypt) {
    try {
      const bytes = CryptoJS.AES.decrypt(decrypt, this.secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return decrypt;
    } catch (e) {
      console.log(e);
    }
  }

  // getExistingPatientQueuees() {
  //   this.pocenterId = JSON.parse(localStorage.getItem('centerId'));

  //   console.log('this.pocenterId#####################');
  //   console.log(this.pocenterId);

  //   const reqbody = this.pocenterId;
  //   const headers = new Headers(
  //     {
  //       'accept': 'application/json',
  //       'x-access-token': this.userToken
  //     });

  //   this.juniorDoctorService.getPatientQueueForPoc(reqbody, headers).subscribe(data => {
  //     alert('dfffdf');
  //     console.log(`data.patientQueueData`);
  //     console.log(data.patientQueueData);

  //     this.existPatientObjectQueue = data.patientQueueData;
  //     data.patientQueueData.forEach(status => {
  //       if (status.presentQueueType === 0) {
  //         status.presentStatus = 'Registration Queue';
  //       } else if (status.presentQueueType === 1) {
  //         status.presentStatus = 'Investigation Queue';
  //       } else if (status.presentQueueType === 2) {
  //         status.presentStatus = 'Doctor Queue';
  //       } else if (status.presentQueueType === 3) {
  //         status.presentStatus = 'Manager Queue';
  //       } else if (status.presentQueueType === 4) {
  //         status.presentStatus = 'Diagnostics Queue';
  //       } else if (status.presentQueueType === 5) {
  //         status.presentStatus = 'Pharmacy Queue';
  //       } else if (status.presentQueueType === 6) {
  //         status.presentStatus = 'Medicine Issued';
  //       }
  //     },
  //       err => {
  //         console.log('err#############');
  //         console.log(err);
  //       });

  //     console.log('qqqqqqqqqqqqqqq', this.existPatientObjectQueue);
  //   });
  // }
  createTransaction(body) {
    const data = sessionStorage.getItem('key');
    const parseData = JSON.parse(data);

    // alert(parseData);
    // console.log('parseData333333333333333333333333', parseData  );

    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });
    this.spinner.show();
    this.juniorDoctorService.createTransaction(body, headers).subscribe(data => {
      console.log('createTransactions');
      console.log(data);

      // this.passBoolean.notifyOther({ option: 'patient-queue', value: this.counter });
      // alert('iam in create');
      this.goToPharmacy(parseData);

      this.afterPaidOnly = true;
      this.beforepaid = false;
      this.totalamount = '';
      this.adminTracsaction.days = '';
      this.adminTracsaction.amount = '';
      this.gstValue = '';
      this.paymentType = '';
      const getTracsaction = (`${this.queId}${'/'}${this.productType}`);
      this.getTransactionForQueue(getTracsaction);
      this.spinner.hide();
     } ,
      err => {
       this.spinner.hide();
       this.notifier.notify('error', 'Something went wrong please try again!')
      },
      () => console.log('Good Job'));
  }


  getTransactionForQueue(tracsaction) {
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });
    this.spinner.show();
    this.juniorDoctorService.getTransactionForQueue(tracsaction, headers).subscribe(data => {
      // console.log('getTransactionForQueue');
      // console.log(data);
      this.getTransaction = data;
      // console.log('this.getTransaction');
      // console.log(this.getTransaction);
       this.spinner.hide();
    },
      err => {
       this.spinner.hide();
       this.notifier.notify('error', 'Something went wrong please try again!')
      },
      () => console.log('Good Job'));
  }

  goToPharmacy(patinetdata) {
    // alert('iam in GotoPharmacy');
    console.log('patinetdatazzzzzzzzzzzz', patinetdata);
    const statusBody = {
      queueId: patinetdata.queueId,
      queueEntryDateAndTime: patinetdata.queueEntryDateAndTime,
      arrivalDateAndTime: patinetdata.arrivalDateAndTime,
      presentQueueType: patinetdata.presentQueueType,
      nextQueueType: 5,
    };
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });

    this.spinner.show();
    this.juniorDoctorService.updatePatientQueue(statusBody, headers).subscribe(updateStatus => {
      console.log('updateStatus');
      console.log(updateStatus);
      // alert('iam update the status');
      // this.getExistingPatientQueuees();
       this.spinner.hide();
    },err => {
       this.spinner.hide();
       this.notifier.notify('error', 'Something went wrong please try again!')
      },
      () => console.log('Good Job'));
  }

}


