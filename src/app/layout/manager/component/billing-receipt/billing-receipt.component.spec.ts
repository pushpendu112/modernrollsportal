import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingReceiptComponent } from './billing-receipt.component';

describe('BillingReceiptComponent', () => {
  let component: BillingReceiptComponent;
  let fixture: ComponentFixture<BillingReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
