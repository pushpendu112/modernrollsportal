import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ManagerService } from '../../manager.service';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import { JuniorDoctorService } from 'src/app/layout/junior-doctor/junior-doctor.service';
import { TestDataService } from 'src/app/shared/services/test-data.service';

import * as CryptoJS from 'crypto-js';
declare var $: any;   // not required

@Component({
  selector: 'app-manager-queue',
  templateUrl: './manager-queue.component.html',
  styleUrls: ['./manager-queue.component.scss']
})
export class ManagerQueueComponent implements OnInit {
  private readonly notifier: NotifierService;
  pocenterId: any;
  getjwttoken: any;
  userToken: any;
  existPatientObjectQueue: any;
  counter = true;
  Secret = 'i_have_some_small_master_secret_live_pin';
  cols: any[];
  ptName;
  ptTime;
  ptDate;
  private subscription: Subscription;

  constructor(

    private managerService: ManagerService,
    private router: Router,
    private juniorDoctorService: JuniorDoctorService,
    private childReciveData: TestDataService,
    private parentPassData: TestDataService,
    private spinner: NgxSpinnerService,
    private notifierService: NotifierService) {
      this.notifier = notifierService; 
    this.cols = [
      { field: 'firstName', header: 'Patient Name' },
      { field: 'mobileNumber', header: 'Mobile Numbe' },
      { field: 'gender', header: 'Gender' },
      { field: 'presentStatus', header: 'Status' }
    ];
  }

  ngOnInit() {
    this.getjwttoken = localStorage.getItem('jwttoken');
    this.userToken = this.decryptData(this.getjwttoken);
    this.getExistingPatientQueuees();

    this.subscription = this.childReciveData.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('option') && res.option === 'patient-queue') {
          console.log('Feature Value form header files');
          console.log('aaaaaaaxxxxxxxxxxxssssssssss', res.value);
      }
    });
  }
  getExistingPatientQueuees() {
    this.pocenterId = JSON.parse(localStorage.getItem('centerId'));

    console.log('this.pocenterId#####################');
    console.log(this.pocenterId);

    const reqbody = this.pocenterId;
    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });

    this.juniorDoctorService.getPatientQueueForPoc(reqbody, headers).subscribe(data => {

      console.log(`data.patientQueueData`);
      console.log(data.patientQueueData);

      this.existPatientObjectQueue = data.patientQueueData;
      data.patientQueueData.forEach(status => {
        if (status.presentQueueType === 0) {
          status.presentStatus = 'Registration Queue';
        } else if (status.presentQueueType === 1) {
          status.presentStatus = 'Investigation Queue';
        } else if (status.presentQueueType === 2) {
          status.presentStatus = 'Doctor Queue';
        } else if (status.presentQueueType === 3) {
          status.presentStatus = 'Manager Queue';
        } else if (status.presentQueueType === 4) {
          status.presentStatus = 'Diagnostics Queue';
        } else if (status.presentQueueType === 5) {
          status.presentStatus = 'Pharmacy Queue';
        } else if (status.presentQueueType === 6) {
          status.presentStatus = 'Medicine Issued';
        }
      },
        err => {
         this.notifier.notify('error', 'Something went wrong please try again!')
        });

      console.log('qqqqqqqqqqqqqqq', this.existPatientObjectQueue);
    });
  }
  decryptData(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, this.Secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }
  goToPharmacy(data) {
    console.log('aaaaaaaaaadddd', data);
    const statusBody = {
      queueId: data.queueId,
      queueEntryDateAndTime: data.queueEntryDateAndTime,
      arrivalDateAndTime: data.arrivalDateAndTime,
      presentQueueType: data.presentQueueType,
      nextQueueType: 5,
    };

    const headers = new Headers(
      {
        'accept': 'application/json',
        'x-access-token': this.userToken
      });
    this.spinner.show();
    this.managerService.updatePatientQueue(statusBody, headers).subscribe(updateStatus => {
      console.log(updateStatus);
      this.spinner.hide();
      this.getExistingPatientQueuees();
    },
    err=>{
      this.spinner.hide();
      this.notifier.notify('error', 'Something went wrong please try again!')
    });



  }
  valueChanged(data) { // You can give any function name
    console.log('qqqqqqqqqqqqwwwwwwwwwwweeeeeeeeeeee', data);
    this.ptName = data.firstName + ' ' + data.lastName;
    this.ptTime = data.arrivalDateAndTime;
    this.ptDate = data.queueEntryDateAndTime;
    const patientName = JSON.stringify(`${data.firstName}${' '}${data.lastName}`);
    localStorage.setItem('patientName', patientName);
    const queueId = data.queueId;
    const queueIdEncrypt = this.encryptData(queueId);
    localStorage.setItem('queId', queueIdEncrypt);

    const patinetId = data.patientId;
    const patinetID = this.encryptData(patinetId);
    localStorage.setItem('patientId', patinetID);


      // this.parentPassData.notifyOther({ option: 'patient-queue', value: this.counter });
      this.parentPassData.notifyOther({ option: 'pt-data', value: data });

  }

  onPayment() {
    this.router.navigate(['/dashboard/Manager/billingReciept']);
    // $('#exampleModal').modal('hide');

  }

  encryptData(data) {
    try {
      return CryptoJS.AES.encrypt(JSON.stringify(data), this.Secret).toString();
    } catch (e) {
      console.log(e);
    }
  }
}
