import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ManagerRoutingModule } from './manager-routing.module';
import { ManagerQueueComponent } from './component/manager-queue/manager-queue.component';
import { DataTableModule } from 'primeng/datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ManagerComponent } from './manager.component';
import { BillingReceiptComponent } from './component/billing-receipt/billing-receipt.component';



@NgModule({
  declarations: [ManagerQueueComponent, ManagerComponent, BillingReceiptComponent],
  imports: [
    CommonModule,
    ManagerRoutingModule,
    DataTableModule,
    NgbModule,
    FormsModule
  ]
})
export class ManagerModule { }
