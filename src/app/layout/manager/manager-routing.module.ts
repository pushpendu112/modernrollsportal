import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagerQueueComponent } from './component/manager-queue/manager-queue.component';
import { ManagerComponent } from './manager.component';
import { BillingReceiptComponent } from './component/billing-receipt/billing-receipt.component';

const routes: Routes = [
  {
    path: '', component: ManagerComponent,
    children: [
      {
        path: 'Manager Queue',
        component: ManagerQueueComponent
      },
      {
        path: 'billingReciept',
        component: BillingReceiptComponent
      },
      {
        path: '',
        redirectTo: 'Manager Queue'
      }
    ]
  }
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagerRoutingModule { }
