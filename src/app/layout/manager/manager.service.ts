import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/base/http.service';
import { AppConstants } from 'src/app/base/appconstants';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  constructor(private httpservice: HttpService) { }

  getPatientQueueForPoc(reqbody, headers) {
    return this.httpservice.httpGetObserevable(AppConstants.get_PatientQueueForPoc + reqbody, '', headers);
  }

  updatePatientQueue(body, headers) {
    const reqbody = {
      queueId: body.queueId,
      queueEntryDateAndTime: body.queueEntryDateAndTime,
      arrivalDateAndTime: body.arrivalDateAndTime,
      presentQueueType: body.presentQueueType,
      nextQueueType: body.nextQueueType,
    };
    return this.httpservice.httpPostObservable(AppConstants.post_update_patient_queue, reqbody, '', headers);
  }

}
