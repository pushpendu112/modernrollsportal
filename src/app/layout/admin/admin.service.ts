import { Injectable } from '@angular/core';
import { AppConstants } from '../../base/appconstants';
import { HttpService } from 'src/app/base/http.service';


@Injectable({
    providedIn: 'root'
})
export class AdminService {

    constructor(private httpService: HttpService) { }

    cteatePocService(createPocFormValue: any) {
        const reqBody = {
            orgId: createPocFormValue.orgId,
            centerName: createPocFormValue.centerName,
            doorNo: createPocFormValue.doorNo,
            streetName: createPocFormValue.streetName,
            area: createPocFormValue.area,
            city: createPocFormValue.city,
            pinCode: createPocFormValue.pinCode,
            location: createPocFormValue.location
        };
        return this.httpService.httpPostObservable(AppConstants.post_create_poc, reqBody, '');
    }

    cteateEmployeeService(cteateEmployeeFormValue: any) {
        const reqBody = {
            name: cteateEmployeeFormValue.name,
            mobileNumber: cteateEmployeeFormValue.mobileNumber,
            userName: cteateEmployeeFormValue.userName,
            password: cteateEmployeeFormValue.password,
            emailId: cteateEmployeeFormValue.emailId,
            gender: cteateEmployeeFormValue.gender,
            roleList: cteateEmployeeFormValue.roleList,
            qualification: cteateEmployeeFormValue.qualification,
            yearsOfExp: cteateEmployeeFormValue.yearsOfExp,
            speciality: cteateEmployeeFormValue.speciality,
            centerId: cteateEmployeeFormValue.centerId,
            orgId: cteateEmployeeFormValue.orgId
        };
        return this.httpService.httpPostObservable(AppConstants.post_save_users, reqBody, '');
    }

    getUpdatedRoleFromDB(orgId, header: any) {
        const reqBody = {
            orgId: orgId
        };
        return this.httpService.httpPostObservable(AppConstants.post_get_updated_role_fromDB, reqBody, '', header);
    }

    getAllPocsForOrg(reqBody) {
        return this.httpService.httpGetObserevable(AppConstants.get_get_all_pocs_for_Org + reqBody, '');
    }

    createRole(reqBody, header: any) {
        return this.httpService.httpPostObservable(AppConstants.post_create_role_in_org, reqBody, '', header);
    }

    getAddress(zipcode) {
        return this.httpService.httpGetObserevable(AppConstants.google_geo_coding_api_path + zipcode + AppConstants.google_geo_coding_api_key, '', '');
    }
    getLatLngAddressFormat(latlongjson) {
        const latlong = {
            lat: latlongjson.lat,
            lng: latlongjson.lng
        };
        // return this.httpService.httpGetObserevable(AppConstants.google_geo_coding_api_path + `latlng=` + latlong.lat + ',' + latlong.lng  + AppConstants.google_geo_coding_api_key, '', '');
        return this.httpService.httpGetObserevable(AppConstants.google_geo_coding_api_path + AppConstants.google_geo_coding_api_key + '&' + `latlng=` + latlong.lat + ',' + latlong.lng, '', '');
    }
    getAllEmployee(body, headers) {
        const reqBody = {
            orgId: body.orgId,
            centerId: body.centerId
        };
        return this.httpService.httpPostObservable(AppConstants.post_get_all_employee, reqBody, '', headers);
    }

    updatePocForOrg(reqBody, headers) {
        return this.httpService.httpPostObservable(AppConstants.post_update_poc_forOrg, reqBody, '', headers);
    }
    removePocForOrg(reqBody, headers) {
        return this.httpService.httpGetObserevable(AppConstants.get_remove_poc_for_org + reqBody, '', headers);
    }

    removeEmployee(reqBody, headers) {
        return this.httpService.httpGetObserevable(AppConstants.get_remove_employee + reqBody, '', headers);
    }
    removeRoleInOrg(reqBody, headers) {
        return this.httpService.httpGetObserevable(AppConstants.get_remove_role_inOrg + reqBody, '', headers);
    }
    updateRoleInOrg(reqBody, headers) {
         return this.httpService.httpPostObservable(AppConstants.post_update_role_inOrg, reqBody, '', headers);
    }

    updateEmployee(reqBody, headers){
         return this.httpService.httpPostObservable(AppConstants.post_update_employee, reqBody, '', headers);
    }

}
