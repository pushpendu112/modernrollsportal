
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { AdminService } from '../admin.service';
import * as $AB from 'jquery';
import * as CryptoJS from 'crypto-js';

import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';

declare var jquery: any;   // not required
declare var $: any;   // not required



@Component({
    selector: 'app-employee-creation',
    templateUrl: './employee-creation.component.html',
    styleUrls: ['./employee-creation.component.scss']
})
export class EmployeeCreationComponent implements OnInit {
    private readonly notifier: NotifierService;
    empregisterForm: FormGroup;
    submitted = false;
    ismodelClose = false;
    newEmp = true;
    updatedEmp = false;
    getActiveGender;
    saveUsername = true;

    getGenderVaue:any;
    issubmitted = false;

    maleGenderActive = false;
    feMaleGenderActive = false;
    otherGenderActive = false;

    userToken: any;
    secret = 'i_have_some_small_master_secret_live_pin';
    setOrgId: any;
    centerId: any;
    getUpdatedRole: any;
    getcreatedRole: any;
    selectedFeature: any = [];
    getAllPocData: any;
    empList = [];
    selectedEmpObjId: any;
    title = 'Modern Rolls | Employee Creation';


    updatedRoleList = [];
    UpdatedEmpObj = [];
    baseSelecter:any;

    @ViewChild('closeModal') private closeModal: ElementRef;


    centerName: any = ['Select', 'Master Homeo 1', 'Master Homeo 2', 'Master Homeo 3', 'Master Homeo 4', 'Master Homeo 5'];
    roleName: any = ['Select', 'Super Admin', 'Nurse', 'Receptionist', 'Juniour Doctor', 'Senior Doctor', 'Pharmacy'];


    cols: any[];
    sortF: any;
    empCreation = [
        { empName: 'Kundhan', age: '25', gender: 'Male', roleName: 'Super Admin', userName: 'kundhan@gmail.com', password: '123456', centerName: 'Master Homeo 1' },
        { empName: 'Kumar', age: '26', gender: 'Male', roleName: 'Admin', userName: 'Kumar@gmail.com', password: '123456', centerName: 'Master Homeo 2' },
        { empName: 'Vinoth', age: '28', gender: 'Male', roleName: 'Admin', userName: 'Vinoth@gmail.com', password: '123456', centerName: 'Master Homeo 3' },
        { empName: 'Harish', age: '24', gender: 'Male', roleName: 'Admin', userName: 'Harish@gmail.com', password: '123456', centerName: 'Master Homeo 3' },

    ];

    constructor(private formBuilder: FormBuilder, 
                private adminService: AdminService,
                private spinner: NgxSpinnerService,
                private notifierService: NotifierService,
                private titleService: Title) {
                this.notifier = notifierService;
    }

    ngOnInit() {
        this.titleService.setTitle(this.title);
        this.cols = [
            { field: 'name', header: 'Employee Name', sortable: true },
            { field: 'mobileNumber', header: 'MobileNumber' },
            { field: 'qualification', header: 'Qualification' }
        ];

        const getjwt = localStorage.getItem('jwttoken');
        this.userToken = this.conversionDecrypt(getjwt);
        this.setOrgId = JSON.parse(localStorage.getItem('orgId'));
        this.centerId = JSON.parse(localStorage.getItem('centerId'));
        this.getRloeList();

        this.empRegisterForms();
        this.getgetAllPocsForOrg();
        this.getAllEmployee();
         this.outsdiedCaptureEvent();
    }

    outsdiedCaptureEvent(){
    $("#exampleModalCenter").on("hidden.bs.modal", () =>{
     this.getcreatedRole.forEach((dbRole) => {
                 dbRole.forEach(role=>{
                            this.onChange(role, false);
                 })
               });
    });
     $("#updateempModel").on("hidden.bs.modal", () =>{
     this.getcreatedRole.forEach((dbRole) => {
                 dbRole.forEach(role=>{
                 role.isSelected = false;
                   this.updatedRoleList.forEach((element2)=>{
                           if(role.roleName === element2.roleName){
                               role.isSelected = true;

                           }
                      })
                 })
               });

    });
}

    genderActive(event) {
        const target = event.target || event.srcElement || event.currentTarget;
        if (target.attributes) {
            const idAttr = target.attributes.id;
            const value = idAttr.nodeValue;
            this.empregisterForm.get('gender').setValue(value);

            if (value === 'Male') {
                this.maleGenderActive = true;
                this.feMaleGenderActive = false;
                this.otherGenderActive = false;
            this.getGenderVaue = value;
            } else if (value === 'FeMale') {
                this.feMaleGenderActive = true;
                this.maleGenderActive = false;
                this.otherGenderActive = false;
            this.getGenderVaue = value;
            } else if (value === 'Other') {
                this.otherGenderActive = true;
                this.maleGenderActive = false;
                this.feMaleGenderActive = false;
                this.otherGenderActive = true;
            this.getGenderVaue = value;
            } else {
                this.maleGenderActive = false;
                this.feMaleGenderActive = false;
                this.otherGenderActive = false;
            this.getGenderVaue = value;
            }
        }
    }
    genderSetActive(value) {
        if (value === 'Male') {
            this.maleGenderActive = true;
            this.feMaleGenderActive = false;
            this.otherGenderActive = false;
            this.getGenderVaue = value;
        } else if (value === 'FeMale') {
            this.feMaleGenderActive = true;
            this.maleGenderActive = false;
            this.otherGenderActive = false;
            this.getGenderVaue = value;
        } else if (value === 'Other') {
            this.otherGenderActive = true;
            this.maleGenderActive = false;
            this.feMaleGenderActive = false;
            this.otherGenderActive = true;
            this.getGenderVaue = value;
        } else {
            this.maleGenderActive = false;
            this.feMaleGenderActive = false;
            this.otherGenderActive = false;
            this.getGenderVaue = value;
        }
    }

    // convenience getter for easy access to form fields
    get empRegister() { return this.empregisterForm.controls; }

    empRegisterForms() {
        this.empregisterForm = this.formBuilder.group({
            name: ['', Validators.required],
            mobileNumber: ['', Validators.required],
            userName: ['', Validators.required],
            password: ['', Validators.required],
            emailId: ['', [Validators.required, Validators.email]],
            gender: [''],
            roleList: [''],
            qualification: [' '],
            yearsOfExp: [' '],
            speciality: [' '],
            centerId: ['', Validators.required],
            orgId: [' '],
        });
    }
    getRloeList() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.adminService.getUpdatedRoleFromDB(this.setOrgId, headers).subscribe(data => {
            this.getUpdatedRole = data;
            const rolelist = [];
            this.getUpdatedRole.roleData.forEach(elemet => {
                rolelist.push(elemet.rolelist);
            });
            this.getcreatedRole = rolelist;

        },
            err => console.log(err),
            // () => console.log('Get Updated Role FromDB Is Done')
        );

    }

    getgetAllPocsForOrg() {
        this.adminService.getAllPocsForOrg(this.setOrgId).subscribe(data => {
            this.getAllPocData = data.pocData;
        },
            err => console.log(err),
            // () => console.log('Get All Pocs For Org is Done')
            );
    }

    onChange(value: string, checked: boolean) {
        // console.log('value, checked');
        // console.log(value, checked);
        if (checked) {
            this.selectedFeature.push(value);
            console.log(this.selectedFeature);
            console.log('this.selectedFeature');
        } else {
            const index = this.selectedFeature.indexOf(value);
            this.selectedFeature.splice(index, 1);
        }
       // console.log(this.selectedFeature);
    }

    isSelected(value: string): boolean {
        return this.selectedFeature.indexOf(value) >= 0;
    }

     get f() {
        return this.empregisterForm.controls;
    }



    onSubmit() {
       
        this.spinner.show();
        this.issubmitted = true;
        if (this.empregisterForm.invalid) { 
            this.spinner.hide();
            this.notifier.notify('error', 'Please Fill All Mandatory Fields! ')
            return;
        }
        const roleListId: any = [];

        this.selectedFeature.forEach(element => {
            if (element._id) {
                roleListId.push(element._id);
            }
        });
        this.empregisterForm.get('roleList').setValue(roleListId);
        this.empregisterForm.get('orgId').setValue(this.setOrgId);

        console.log('this.empregisterForm.value');
        console.log(this.empregisterForm.value);
        this.adminService.cteateEmployeeService(this.empregisterForm.value).subscribe(data => {
this.spinner.hide();
            console.log(data);
            this.getAllEmployee();
        },
            err =>{
                this.spinner.hide();
                this.notifier.notify('error', 'Something went wrong please try again! ')
                console.log(err);
            },
            //() => console.log('Get All Pocs For Org is Done')
            );

        // console.log(this.empregisterForm.value);
        $('#exampleModalCenter').modal('hide');
        this.empregisterForm.reset();

    }
    updateSelection(centerNameObj) {
        console.log(centerNameObj);
    }


    getAllEmployee() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        const body = {
            orgId: this.setOrgId,
            centerId: this.centerId

        };
        this.spinner.show();
        this.adminService.getAllEmployee(body, headers).subscribe(element => {
            this.spinner.hide();
            this.empList = element.employeesData;
        });
    }
  

      isUpdated(value: string): boolean {
        return this.updatedRoleList.indexOf(value) >= 0;
    }

     onUpdateChange(value: string, checked) {
        if (checked) {
            this.updatedRoleList.push(value);
           // console.log('this.updatedRoleList');
           // console.log(this.updatedRoleList)
        } else {
            const index = this.updatedRoleList.indexOf(value);
            this.updatedRoleList.splice(index, 1);
        }
    }



    getEmpValue(event) {
        console.log(event);
        this.UpdatedEmpObj[0] = event.data;

        this.selectedEmpObjId = event.data._id
        // console.log('this.selectedEmpObjId');
        // console.log(this.selectedEmpObjId);
        this.newEmp = false;
        this.updatedEmp = true;
        // console.log(event.data.gender);
        this.getActiveGender = event.data.gender;
        this.empregisterForm.patchValue(event.data);
        this.empregisterForm.get('gender').setValue(this.genderSetActive(event.data.gender));

        // console.log('event.data.assignedRole');
        const selectedRole = event.data.assignedRole;
        this.baseSelecter = event.data.assignedRole;
           

              this.getcreatedRole.forEach((dbRole) => {
                 dbRole.forEach(role=>{
                 role.isSelected = false;

                   selectedRole.forEach((element2)=>{
                           if(role.roleName === element2.roleName){
                               role.isSelected = true;
                              this.onUpdateChange( element2, true);
                           }
                      })
                 })
             });






        console.log();

        $('#updateempModel').modal('show');
    }
    onDeleteEmp(){
      const headers = new Headers(
        {
            'accept': 'application/json',
            'x-access-token': this.userToken
        });
          this.spinner.show();
        this.adminService.removeEmployee(this.selectedEmpObjId, headers).subscribe(deletedEmp=>{
            this.spinner.hide();
            $('#delete').modal('hide');
            console.log(deletedEmp);
            this.getAllEmployee();
        },
        err=>{
            this.spinner.hide();
            $('#delete').modal('hide');
            this.notifier.notify('error', 'Something went wrong please try again! ');
        }
        );
    }


    onUpdateEmp(){
        const roleListId: any = [];
        let result = this.updatedRoleList.filter(function({_id}) {
              return !this.has(_id) && this.add(_id);
            }, new Set)
        result.forEach(element=>{
            roleListId.push(element._id);
        })
        // console.log('result')
        // console.log(result)

        // console.log('roleListId');
        // console.log(roleListId);
        // console.log('this.setOrgId');
        // console.log(this.setOrgId);
        const existingEmpValue = this.empregisterForm.value;

        console.log('existingEmpValue====>', existingEmpValue)

        this.UpdatedEmpObj.forEach(empObj=>{
            empObj.roleList = roleListId;
            empObj.name = existingEmpValue.name;
            empObj.mobileNumber = existingEmpValue.mobileNumber;
            empObj.userName = existingEmpValue.userName;
            empObj.password = existingEmpValue.password;
            empObj.emailId = existingEmpValue.emailId;
            empObj.gender = existingEmpValue.gender;
            empObj.yearsOfExp = existingEmpValue.yearsOfExp;
            empObj.qualification = existingEmpValue.qualification;
            empObj.speciality = existingEmpValue.speciality;
            empObj.centerId = existingEmpValue.centerId;
            empObj.orgId = existingEmpValue.orgId;

        })


        console.log('this.UpdatedEmpObj');
        console.log(this.UpdatedEmpObj[0]);
        
        // this.empCreation.push(this.empregisterForm.value);
        // console.log(this.empregisterForm.controls['centerId'].value);
        this.empregisterForm.get('roleList').setValue(roleListId);
        this.empregisterForm.get('orgId').setValue(this.setOrgId);
        this.empregisterForm.get('gender').setValue(this.getGenderVaue);
        console.log('this.empregisterForm.value');
        console.log(this.empregisterForm.value);

        $('#updateempModel').modal('hide');

        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        this.spinner.show();
        this.adminService.updateEmployee(this.UpdatedEmpObj[0], headers).subscribe(data=>{
            console.log(data);
            this.spinner.hide();
            this.getAllEmployee();
        },
        err=>{
            this.spinner.hide();
            this.notifier.notify('error', 'Something went wrong please try again!');
        })

    }




deleteModelShow(){
     $('#updateempModel').modal('hide');
     $('#delete').modal('show');
}
    Onclear() {
        this.newEmp = true;
        this.updatedEmp = false;
        this.empregisterForm.reset();
        this.empregisterForm.get('gender').setValue(this.makeFalseGender());

    }

    makeFalseGender() {
        this.maleGenderActive = false;
        this.feMaleGenderActive = false;
        this.otherGenderActive = false;
    }
    onNotify() {
        this.empregisterForm.get('gender').setValue(this.getActiveGender);
        const empobject = this.empregisterForm.value;



        const deleteEmp = this.empCreation;

        this.empCreation = deleteEmp.filter((empObj) => empObj.empName !== empobject.empName);





        console.log(deleteEmp);


        // let empArray = [];
        //  for (let key in empobject) {
        //      empArray.push(Object.assign(empobject[key], {key}));
        //  }
        //  console.log( empArray);

        // let data = this.toArray(empobject);
        // console.log(data)

        //      let myData = Object.keys(empobject).map(key => {
        //     return empobject[key];
        // })
        // console.log(myData);

        // let empArray = [];
        //  for( let item in empobject){
        //      empArray.push(empobject[item]);
        //  }

        // console.log(empArray);

        // const getShortMessages = messages => messages
        // .filter(obj => obj.message)
        // .map(obj => obj.message);
        // let test = getShortMessages(this.empregisterForm.value);
        // const peopleArray = Object.keys(empobject).map(i => empobject[i])

        // let  empdata = empArray.filter((emp) => console.log(emp));



        // let empdata = this.empCreation.filter((emp) => {
        //   console.log(emp);
        //   emp.empName !== empobject.empName});
        // console.log(empdata);


        // console.log(peopleArray);


        // let empdata = this.empregisterForm.value;
        // empdata = empdata.filter((emp) => console.log(emp));
        // empdata = empdata.filter((emp) => emp.age !== emp.age);
        // console.log(empdata);
        // window.alert('You will be notified when the product goes on sale');
    }

    toArray(obj_obj) {
        return Object.keys(obj_obj).map(i => obj_obj[i]);
    }
    getRoleId(role) {
        console.log(role);
    }


    conversionEncrypt(encrypt) {
        try {
            return CryptoJS.AES.encrypt(JSON.stringify(encrypt), this.secret).toString();
        } catch (e) {
            console.log(e);
        }
    }
    conversionDecrypt(decrypt) {
        try {
            const bytes = CryptoJS.AES.decrypt(decrypt, this.secret);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return decrypt;
        } catch (e) {
            console.log(e);
        }
    }

}
