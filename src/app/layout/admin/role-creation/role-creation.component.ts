import { Component, OnInit, AfterViewInit, DoCheck, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, ValidatorFn, AbstractControl, NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ThrowStmt } from '@angular/compiler';
import * as CryptoJS from 'crypto-js';
import { AdminService } from '../admin.service';
import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';


declare var $: any;   // not required


@Component({
    selector: 'app-role-creation',
    templateUrl: './role-creation.component.html',
    styleUrls: ['./role-creation.component.scss']
})
export class RoleCreationComponent implements OnInit, AfterViewInit {

    private readonly notifier: NotifierService;
    roleForms: FormGroup;

    issubmitted = false;
    submitted = false;
    roleList: any;
    roleData: any = [];
    roleName = { roleName: '' };
    selectedFeatureName = [];
    featureNameError: Boolean = true;
    result = [];
    roleUpdate = true;
    roleId: any;
    updatedName = { roleName: '' };
    updatedFeature = [];
    search: any;
    userToken: any;
    secret = 'i_have_some_small_master_secret_live_pin';
    setOrgId: any;
    baseFeaturList: any;
    selectedPizzaGroup: AbstractControl;
    selectedFeature: string[] = [];
    selctedone = false;
    title = 'Modern Rolls | Role Creation';
    deleteRoleId: any;
    constructor(private _fb: FormBuilder,
        private adminService: AdminService,
        private notifierService: NotifierService,
        private spinner: NgxSpinnerService,
        private titleService: Title) {
        this.notifier = notifierService;
    }
    ngAfterViewInit() {

    }
    ngOnInit() {
        this.roleForms = this._fb.group({
            roleName: [null],
            featuresCollactionList: this._fb.array([])
        });
        this.titleService.setTitle(this.title);
        const getjwt = localStorage.getItem('jwttoken');
        this.userToken = this.conversionDecrypt(getjwt);
        this.setOrgId = JSON.parse(localStorage.getItem('orgId'));
        this.getbaseFeatureList();
        this.outsideCaptureEvent();
    }

    outsideCaptureEvent() {
        $("#roleCreation").on("hidden.bs.modal", () => {
            this.baseFeaturList.forEach(feature => {
                this.onChange(feature, false);
            });
            this.roleUpdate = true;
        });
        $("#UpdateCreation").on("hidden.bs.modal", () => {
            this.baseFeaturList.forEach(feature => {
                this.onUpdateChange(feature, false);
            });
            this.roleUpdate = true;
        });
    }

    onUpdateChange(value: string, checked) {
        if (checked) {
            this.updatedFeature.push(value);
        } else {
            console.log('else');
            const index = this.updatedFeature.indexOf(value);
            this.updatedFeature.splice(index, 1);
        }
    }

    getbaseFeatureList() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.spinner.show();
        this.adminService.getUpdatedRoleFromDB(this.setOrgId, headers).subscribe(data => {
            this.spinner.hide();
            data.roleData.forEach(element => {
                this.roleList = element.rolelist;
                // console.log('roleList#################################33');
                // console.log(this.roleList)
            })

            let getData: any;
            const baseFeatureLis = [];
            getData = data.roleData.forEach(elemet => {
                baseFeatureLis.push(elemet.baseFeatureList);
            });
            this.baseFeaturList = baseFeatureLis[0];
        },
            err => {
                this.spinner.hide();
                console.log(err);
                this.notifier.notify('error', 'Please Check ' + err.statusText);
            },
            // () => this.notifier.notify('success', 'Role List Is Done!!! ')

        );

    }



    patchValues() {
        this.baseFeaturList.forEach(feature => {
            this.onChange(feature, false);
            this.onUpdateChange(feature, false);
        });
    }
    onSubmit() {
        this.spinner.show();
        const role = {
            roleName: this.roleName.roleName,
            checkedFeatureList: this.selectedFeature
        };
        const obj = {
            orgId: this.setOrgId,
            role: role
        };
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        this.adminService.createRole(obj, headers).subscribe(data => {
            this.getbaseFeatureList();
            this.spinner.hide();
        },
            err => {
                console.log(err);
                this.spinner.hide();
                this.notifier.notify('error', 'Please Check ' + err.statusText);
            },
            //() => this.notifier.notify('success', 'You are awesome! You are successfully created Role!'),
        );
        $('#roleCreation').modal('hide').reset();

    }
    checkedFeature(fName) {
        console.log(fName);
    }



    isUpdated(value: string): boolean {
        return this.updatedFeature.indexOf(value) >= 0;
    }

    isSelected(value: string): boolean {
        return this.selectedFeature.indexOf(value) >= 0;
    }

    onChange(value: string, checked) {
        if (checked) {
            this.selectedFeature.push(value);
        } else {
            console.log('else');
            const index = this.selectedFeature.indexOf(value);
            this.selectedFeature.splice(index, 1);
        }
    }




    get firstName() {
        return this.roleForms.get('roleName');
    }

    get featuresArray() {
        return <FormArray>this.roleForms.get('featuresName');
    }

    getSelectedFeatureValue() {
        this.selectedFeatureName = [];
        this.featuresArray.controls.forEach((control, i) => {
            // console.log('control.value');
            // console.log(control.value);
            if (control.value) {
                // console.log(this.featuresNameList[i]);
                // this.selectedFeatureName.push(this.featuresNameList[i]);
            }
        });

        this.featureNameError = this.selectedFeatureName.length > 0 ? false : true;
    }


    onRoleSubmit() {
        const obj = {
            id: null,
            'roleName': '',
            'featuresName': []
        };
        const role = this.roleForms.value;
        const selectedFeature = this.selectedFeatureName;
        obj.roleName = role.roleName;
        obj.featuresName = selectedFeature;
        // obj.id = this.roleNameArray.length + 1;
        // this.roleNameArray.push(obj);
        console.log(this.roleForms.value);

        $('#roleCreation').modal('hide');

    }

    onRoleEdit(role) {
        // alert('dfdfdfdf')
        this.roleUpdate = false;
        this.roleId = role._id;
        console.log('role');
        console.log(role);
        // this.selectedFeature = role.checkedFeatureList;


        this.updatedName.roleName = role.roleName;
        //this.selectedFeature = role.checkedFeatureList;

        $('#UpdateCreation').modal('show');
    }
    getRoleIdOnClick(role) {
        this.deleteRoleId = role._id;
        // console.log(role);
        // console.log(this.deleteRoleId);
    }
    onRoleDelete() {
        this.spinner.show();
        const reqBody = this.deleteRoleId
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        console.log(reqBody);
        this.adminService.removeRoleInOrg(reqBody, headers).subscribe(removeRoleObj => {
            this.spinner.hide();
            this.getbaseFeatureList();
            $('#deleteModal').modal('hide');
        },
            err => {
                this.spinner.hide()
                console.log(err);
                $('#deleteModal').modal('hide');
                this.notifier.notify('error', 'Please Check ' + err.statusText);
            },
            // () => this.notifier.notify('success', 'Role List Is Done!!! ')
        );
        // this.roleNameArray = this.roleNameArray.filter(roleId => roleId.id !== role.id);

    }

    addFeatureEditControls(editFeature) {
        const arr = editFeature.map(item => {
            return this._fb.control(item);
        });
        return this._fb.array(arr);
    }


    onFormReset() {
        this.roleForms.reset();

    }

    getMatch(a, b) {
        const matches = [];
        for (let i = 0; i < a.length; i++) {
            for (let e = 0; e < b.length; e++) {
                // if (a[i] === b[e]) { matches.push(a[i]); }
                if (a[i] === b[e]) {

                }
            }
        }
        return matches;
    }

    createRoleHandler() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
    }


    conversionDecrypt(decrypt) {
        try {
            const bytes = CryptoJS.AES.decrypt(decrypt, this.secret);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return decrypt;
        } catch (e) {
            console.log(e);
        }
    }
    onUpdate() {
        this.spinner.show();
        console.log('value222222222222222');


        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        const roles = {
            roleName: this.updatedName.roleName,
            checkedFeatureList: this.updatedFeature,
            _id: this.roleId
        };

        // roles = JSON.parse(JSON.stringify(roles));
        // roles.checkedFeatureList = this.selectedFeature;
        const body = {
            orgId: this.setOrgId,
            roleObj: roles
        };
        // console.log(body);
        this.adminService.updateRoleInOrg(body, headers).subscribe(update => {
            this.spinner.hide();
            this.getbaseFeatureList();
        },
            err => {
                this.spinner.hide()
                console.log(err);
                this.notifier.notify('error', 'Please Check ' + err.statusText);
            },
            // () => this.notifier.notify('success', 'Role List Is Done!!! ')
        );

        $('#UpdateCreation').modal('hide');
    }

}
