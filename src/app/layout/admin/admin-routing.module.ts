import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { PocCreationComponent } from './poc-creation/poc-creation.component';
import { EmployeeCreationComponent } from './employee-creation/employee-creation.component';
import { RoleCreationComponent } from './role-creation/role-creation.component';

const routes: Routes = [
  {
    path: '', component: AdminComponent,
    children: [
      {
        path: 'POC Creation',
        component: PocCreationComponent
      },
      {
        path: 'Role Creation',
        component: RoleCreationComponent
      },
      {
        path: 'Employee Creation',
        component: EmployeeCreationComponent
      },
      {
        path: '',
        redirectTo: 'POC Creation'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
