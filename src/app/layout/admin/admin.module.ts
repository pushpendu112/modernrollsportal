import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';
import {DataTableModule} from 'primeng/datatable';
import { AccordionModule } from 'primeng/accordion';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
 import { NgxSpinnerModule } from 'ngx-spinner'

import { AdminComponent } from './admin.component';
import { PocCreationComponent } from './poc-creation/poc-creation.component';
import { EmployeeCreationComponent } from './employee-creation/employee-creation.component';
import { RoleCreationComponent } from './role-creation/role-creation.component';

const notifierDefaultOptions: NotifierOptions = {
  position: {
      horizontal: {
          position: 'right',
          distance: 12
      },
      vertical: {
          position: 'bottom',
          distance: 12,
          gap: 10
      }
  },
  theme: 'material',
  behaviour: {
      autoHide: 5000,
      onClick: false,
      onMouseover: 'pauseAutoHide',
      showDismissButton: true,
      stacking: 4
  },
  animations: {
      enabled: true,
      show: {
          preset: 'slide',
          speed: 300,
          easing: 'ease'
      },
      hide: {
          preset: 'fade',
          speed: 300,
          easing: 'ease',
          offset: 50
      },
      shift: {
          speed: 300,
          easing: 'ease'
      },
      overlap: 150
  }
};

@NgModule({
  declarations: [AdminComponent, PocCreationComponent, EmployeeCreationComponent, RoleCreationComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    DataTableModule,
    AccordionModule,
    NotifierModule.withConfig(notifierDefaultOptions),
    Ng2SearchPipeModule,
    NgxSpinnerModule
  ]
})
export class AdminModule { }
