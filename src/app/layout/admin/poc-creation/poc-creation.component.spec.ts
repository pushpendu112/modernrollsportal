import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PocCreationComponent } from './poc-creation.component';

describe('PocCreationComponent', () => {
  let component: PocCreationComponent;
  let fixture: ComponentFixture<PocCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PocCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PocCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
