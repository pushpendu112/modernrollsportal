import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { AppConstants} from '../../../base/appconstants'
import { Title } from '@angular/platform-browser';


import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { NotifierService } from 'angular-notifier';
import * as $AB from 'jquery';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
declare var jquery: any;   // not required
declare var $: any;   // not required
import * as CryptoJS from 'crypto-js';


@Component({
    selector: 'app-poc-creation',
    templateUrl: './poc-creation.component.html',
    styleUrls: ['./poc-creation.component.scss']
})
export class PocCreationComponent implements OnInit {
    private readonly notifier: NotifierService;
    private readonly spinnerCall: NgxSpinnerService;
    createPocForm: FormGroup;
    setOrgId: any;
    allPocList: any;
    userToken: any;
    newPoc = true;
    updatedPoc = false;
    pocValue: any;

    issubmitted = false;
    search: any;
    cols: any[];
    sortF: any;
    title = 'Modern Rolls | Poc Creation';

    constructor(private _fb: FormBuilder,
                private adminService: AdminService,
                notifierService: NotifierService,
                private router: Router,
                private spinner: NgxSpinnerService,
                private spinner2: NgxSpinnerService,
                private titleService: Title) {
                this.notifier = notifierService;
                this.spinnerCall = spinner;
                this.spinner.show();
            }


    ngOnInit() {
        this.setOrgId = JSON.parse(localStorage.getItem('orgId'));
        const getjwt = localStorage.getItem('jwttoken');
        this.userToken = this.conversionDecrypt(getjwt);
        // console.log('this.userToken',this.userToken);
        this.titleService.setTitle(this.title);
        this.createPocFoms();
        this.getAllPocsForOrg();
        this.outsideCaptureEvent();this.spinner.show();

    }

     outsideCaptureEvent(){
        $("#createPocForm").on("hidden.bs.modal", () =>{
            this.newPoc = true;
            this.updatedPoc = false;
        });
    }

    createPocFoms() {
        this.createPocForm = this._fb.group({
            orgId: [null],
            centerName: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
            doorNo: [null, Validators.required],
            streetName: [null, Validators.required],
            area:  [null],
            city:  [null],
            state:  [null],
            pinCode:  [null, Validators.required],
            location: this._fb.group({
                lat:  [null],
                lng: [null],
            })
        });
    }

    get f() {
        return this.createPocForm.controls;
      }

    submitPocHandler() {
        this.spinner.show();
        this.issubmitted = true;
        if (this.createPocForm.invalid) { 
            this.spinner.hide();
            this.notifier.notify('error', 'Something went wrong please try again! ')
            return;
        }
        this.createPocForm.get('orgId').setValue(this.setOrgId);
        $('#createPocForm').modal('hide');
        this.adminService.cteatePocService(this.createPocForm.value).subscribe(
            data => {
                this.spinner.hide();
                console.log(data);
                if (data.statusCode === 200) {
                }
                this.getAllPocsForOrg();
            },
            err => {
                this.spinner.hide();
                this.notifier.notify('error', 'Please Check ' + err.statusText);
            },
            // () => this.notifier.notify('success', 'New POC Created!!! ')
        );
        console.log(this.createPocForm.value);
        this.issubmitted  = false;
        // window.location.reload();

    }

    getAllPocsForOrg() {
        this.spinnerCall.show();
        this.adminService.getAllPocsForOrg(this.setOrgId).subscribe(data => {
            this.spinnerCall.hide();
            if (data.statusCode === 200) {
                this.allPocList = data.pocData;
                // console.log(JSON.stringify(this.allPocList));
                data.pocData.forEach(poc => {
                    const location = poc.location;
                    // this.getLatLngAddressFormat(location, poc);
                    try {
                        if(location.toString().length){
                            if(location.lat!==null && location.lng !==null){
                                this.adminService.getLatLngAddressFormat(location).subscribe(data => {
                                this.spinnerCall.hide();
                                    const results = data.results[0].formatted_address;
                                    poc.address = results;
                                    //console.log(results);
                                }, err=>{
                                    console.log(err);
                                    }
                                );
                            }
                        }
                    } catch (err) {
                        console.log('Error:' + err);
                    }
                });
            }
        },
            err => {
                this.spinnerCall.hide();
                console.log(err);
                this.notifier.notify('error', 'Please Check ' + err.statusText);
            },
            // () => this.notifier.notify('success', 'Getting All Poc List Is Done ')
        );
    }

    get keys(): Array<any> {
        return Object.keys(this.allPocList);
    }

    onclear() {
        this.createPocForm.reset();
    }
    onPocView(pocValue) {

        this.createPocForm.patchValue(pocValue);
        this.newPoc = false;
        this.updatedPoc = true;
        this.pocValue = pocValue;
        this.getZipCode(pocValue.pinCode);
        $('#createPocForm').modal('show');
    }
    onPocUpdate(pocValue) {
        const body = {
            _id: pocValue.Id,
            orgId: pocValue.orgId,
            centerName: pocValue.orgId,
            doorNo: pocValue.orgId,
            streetName: pocValue.orgId,
            area: pocValue.orgId,
            city: pocValue.orgId,
            pinCode: pocValue.orgId,
            location: pocValue.orgId,
        };
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });
        this.createPocForm.patchValue(pocValue);
        // this.adminService.updatePocForOrg(body,headers).subscribe(updatePoc => {

        // })
        this.getZipCode(pocValue.pinCode);
        $('#createPocForm').modal('show');
    }
    getZipCode(code) {

        // this.createPocForm.get('pinCode').valueChanges.subscribe((result) => {
        //     // const size = result;
        //     if (!result) {
        //         const size = result.toString().length;
        //         if(size === 6) {
        //             console.log(size);
        //         }
        //     }
        // });
        const pincode = code;
        if (pincode.length === 6) {
            this.adminService.getAddress(pincode).subscribe(data => {
                const geoCodeObj = data.results;
                geoCodeObj.forEach(element => {
                    const city = element.address_components;
                    city.forEach(element => {
                        const pinAddress = element;
                        const types = element.types;
                        types.forEach(element => {
                            if (element === 'administrative_area_level_1') {
                                this.createPocForm.get('state').patchValue(pinAddress.long_name);
                            }
                            if (element === 'administrative_area_level_2') {
                                this.createPocForm.get('city').patchValue(pinAddress.long_name);
                            }
                        });
                    });
                });
            });
        }
    }
    onUpdatedPoc(createPocForm) {
        console.log('createPocForm');
        console.log(createPocForm);
        // console.log('this.pocValue');
        // console.log(this.pocValue);
        const body = {
            _id: this.pocValue._id,
            orgId: this.pocValue.orgId,
            centerName: createPocForm.centerName,
            doorNo: createPocForm.doorNo,
            streetName: createPocForm.streetName,
            area: createPocForm.area,
            city: createPocForm.city,
            pinCode: createPocForm.pinCode,
            location: createPocForm.location,
        };
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        this.spinner.show();

        this.adminService.updatePocForOrg(body, headers).subscribe(updatePoc => {
            this.spinner.hide();
            console.log('updatePoc');
            console.log(updatePoc);
            this.getAllPocsForOrg();

        },
            err => {
                this.spinner.hide();
                this.notifier.notify('error', 'Please Check ' + err.statusText);
            },
            //() => this.notifier.notify('success', 'New POC Created!!! ')
         );
        this.getZipCode(this.pocValue.pinCode);
        $('#createPocForm').modal('hide');
    }

    onPocDelete(poc) {
        console.log(poc);
        this.pocValue = poc;
    }
    onPocDeleteConform() {
        const headers = new Headers(
            {
                'accept': 'application/json',
                'x-access-token': this.userToken
            });

        // console.log(this.pocValue._id);
        this.spinner.show();
        this.adminService.removePocForOrg(this.pocValue._id , headers).subscribe(data => {
            this.spinner.hide();
            // console.log(data);
            this.getAllPocsForOrg();
            $('#deleteModal').modal('hide');
        },
            err => {
                this.spinner.hide();
                this.notifier.notify('error', 'Please Check ' + err.statusText);
            },
            //() => this.notifier.notify('success', 'New POC Created!!! ')
         );
    }
    conversionDecrypt(decrypt) {
        try {
            const bytes = CryptoJS.AES.decrypt(decrypt, AppConstants.secret);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return decrypt;
        } catch (e) {
            console.log(e);
        }
    }

}
