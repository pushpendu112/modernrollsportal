import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class URLStringFormatter {

    format(...args: any[]) {
        let theString = args[0];
        for (let i = 1; i < args.length; i++) {
            const regEx = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
            theString = theString.replace(regEx, args[i]);
        }

        return theString;
    }
}
