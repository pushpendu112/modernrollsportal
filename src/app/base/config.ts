export class Config {

    private static  DEV = 0;
    private static LOCAL_UAT: number = Config.DEV + 1;
    private static AWS_QA: number = Config.LOCAL_UAT + 1;
    private static AWS_DEMO: number = Config.AWS_QA + 1;
    private static AWS_UAT: number = Config.AWS_DEMO + 1;
    private static LIVE: number = Config.AWS_UAT + 1;

    public static TEST_TYPE: number = Config.AWS_QA;
    public static VERSION = 'v1.0.0 (01 May 2019)';

}
