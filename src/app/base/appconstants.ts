export class AppConstants {
    public static BASE_URLS_MOVIES = 'http://www.omdbapi.com/?s=';
    public static BASE_URLS_MOVIES_KEY = '&apikey=a4b16c5e';
    public static google_geo_coding_api_key = '&key=AIzaSyCOvFwkVvSqfkOTHcMYGeyraw8DFxUK2R4';
    public static google_geo_coding_api_path = `https://maps.googleapis.com/maps/api/geocode/json?address=`;
    // public static google_geo_coding_api_path = `https://maps.googleapis.com/maps/api/geocode/json?`;

    public static secret = `i_have_some_small_master_secret_live_pin`;

    public static post_Web_URl = 'http://13.233.82.26:3001/';
    // public static EXCEL_DOWNLOAD_URL = 'http://15.206.145.4:4009/ReportGeneration/genericxls';
    public static EXCEL_DOWNLOAD_URL = 'https://www.excel.hawkeyeway.com/ReportGeneration/genericxls';
    // public static post_Web_URl = 'http://18.189.7.101:3001/';
    // public static post_Web_URl = 'http://localhost:3001/';
    public static post_get_passport_public_login = `${AppConstants.post_Web_URl}basicNode/services/passportpubliclogin`;
    public static post_Create_Patient = `${AppConstants.post_Web_URl}basicNode/services/createPatient`;
    public static post_getPatient_ByMobileNumber = `${AppConstants.post_Web_URl}basicNode/services/getPatientByMobileNumber/`;
    public static post_Create_PatientQueue = `${AppConstants.post_Web_URl}basicNode/services/createPatientQueue`;
    public static get_PatientQueueForPoc = `${AppConstants.post_Web_URl}basicNode/services/getPatientQueueForPoc/`;
    public static post_update_patient_queue = `${AppConstants.post_Web_URl}basicNode/services/updatePatientQueue`;
    // public static post_get_updated_role_fromdb = 'basicNode/services/getUpdatedRoleFromDB';

    public static post_passport_publlic_login = `${AppConstants.post_Web_URl}basicNode/services/passportpubliclogin`;
    public static post_create_poc = `${AppConstants.post_Web_URl}basicNode/services/createPoc`;
    public static post_save_users = `${AppConstants.post_Web_URl}basicNode/services/saveUsers`;
    public static post_update_employee = `${AppConstants.post_Web_URl}basicNode/services/updateEmployee`;
    public static post_create_role_in_org = `${AppConstants.post_Web_URl}basicNode/services/createRoleInOrg`;
    public static post_get_updated_role_fromDB = `${AppConstants.post_Web_URl}basicNode/services/getUpdatedRoleFromDB`;
    public static get_get_all_pocs_for_Org = `${AppConstants.post_Web_URl}basicNode/services/getAllPocsForOrg/`;
    public static post_get_all_employee = `${AppConstants.post_Web_URl}basicNode/services/getAllEmployee`;
    public static post_update_poc_forOrg = `${AppConstants.post_Web_URl}basicNode/services/updatePocForOrg`;
    public static get_remove_poc_for_org = `${AppConstants.post_Web_URl}basicNode/services/removePocForOrg/`;
    public static get_remove_employee = `${AppConstants.post_Web_URl}basicNode/services/removeEmployee/`;
    public static get_remove_role_inOrg = `${AppConstants.post_Web_URl}basicNode/services/removeRoleInOrg/`;
    public static post_update_role_inOrg = `${AppConstants.post_Web_URl}basicNode/services/updateRoleInOrg`;


    public static post_save_medical_history = `${AppConstants.post_Web_URl}basicNode/services/saveMedicalHistory`;
    public static post_update_medical_history = `${AppConstants.post_Web_URl}basicNode/services/updateMedicalHistory`;
    public static get_medical_history_by_patientId = `${AppConstants.post_Web_URl}basicNode/services/getMedicalHistoryByPatientId/`;
    public static post_create_transaction = `${AppConstants.post_Web_URl}basicNode/services/createTransaction`;
    public static get_transaction_for_queue = `${AppConstants.post_Web_URl}basicNode/services/getTransactionForQueue/`;

    public static post_update_patient = `${AppConstants.post_Web_URl}basicNode/services/updatePatient`;


    constructor() { }
}



