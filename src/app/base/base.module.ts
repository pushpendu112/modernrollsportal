import { NgModule, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalErrorHandler } from './error-handler/global-error-handler';
import { BillingReceiptComponent } from './billing-receipt/billing-receipt.component';

@NgModule({
  declarations: [BillingReceiptComponent],
  imports: [
    CommonModule
  ],
  providers: [
    { provide: ErrorHandler, useClass: GlobalErrorHandler }
  ],
  exports: [BillingReceiptComponent]
})
export class BaseModule { }
