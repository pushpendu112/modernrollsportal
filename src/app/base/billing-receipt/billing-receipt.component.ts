import { Component, OnInit } from '@angular/core';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';

interface Payment {
  id?: number;
  particular: string;
  quantity: number;
  amount: number;
}

const PAYMENT: Payment[] = [
  {
    particular: 'Register',
    quantity: 1,
    amount: 100
  },
  {
    particular: 'Register',
    quantity: 1,
    amount: 100
  },
  {
    particular: 'Doctor',
    quantity: 1,
    amount: 1000
  }
];
@Component({
  selector: 'app-billing-receipt',
  templateUrl: './billing-receipt.component.html',
  styleUrls: ['./billing-receipt.component.scss']
})
export class BillingReceiptComponent implements OnInit {
  log: any = 'assets/images/masterhomelogo_header.png';

  page = 1;
  pageSize = 4;
  collectionSize = PAYMENT.length;
  totalamount: any;
  gstValue: any;
  gstRemainder: any;
  modeofPayment = ['Cash', 'Credit Card', 'Check'];
  isPrint = true;


  get payment(): Payment[] {
    return PAYMENT
      .map((payment, i) => ({ id: i + 1, ...payment }))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  constructor() { }

  ngOnInit() {
    this.getTotalAmount();
    this.gstValueDetaction();
  }

  getTotalAmount() {
    let total = 0;
    for (let i = 0; i < this.payment.length; i++) {
      if (this.payment[i].amount) {
        total += this.payment[i].amount;
        this.totalamount = total.toFixed(2);
      }
    }
    console.log('Total', total);
    return total;
  }

  gstValueDetaction() {
    const gstdetection = 5;
    const gstAmount = this.totalamount * ((100 - gstdetection) / 100);
    this.gstValue = gstAmount.toFixed(2);
  }

  get gstRemainderCal() {
    return (this.totalamount - this.gstValue).toFixed(2);
  }

  public captureScreenpdf() {
    const data = document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      const imgWidth = 208;
      const pageHeight = 295;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      const heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png');
      const pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
      const position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
      pdf.save('MYPdf.pdf'); // Generated PDF
    });
  }
  print(): void {
    this.isPrint = false;
    setTimeout(() => {
      let printContents, popupWin;
      printContents = document.getElementById('print-section').innerHTML;
      popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
      popupWin.document.open();
      popupWin.document.write(`
        <html>
          <head>
            <title>Invice</title>
           <style>
           table.borderStyle{
                border: 1px solid black;
            }
            table.borderStyle thead tr th{
              border-bottom: 1px solid black;
            }
             table.borderStyle thead tr th:nth-child(1){
              border-right: 1px solid black;
            }

            table.borderStyle thead tr th:nth-child(2){
              border-right: 1px solid black;
            }

            table.borderStyle thead tr th:nth-child(3){
              border-right: 1px solid black;
            }
            table.borderStyle tbody tr th:nth-child(1){
              border-right: 1px solid black;
            }
            table.borderStyle tbody tr td:nth-child(2){
              border-right: 1px solid black;
                padding-left: 12%;
            }
            table.borderStyle tbody tr td:nth-child(3){
              border-right: 1px solid black;
              padding-left: 13%;
            }
            table.borderStyle tbody tr td:nth-child(4){
              padding-left: 8%;
            }
           </style>
          </head>
        <body onload="window.print();window.close()">${printContents}</body>
        </html>`
      );
      popupWin.document.close();
    }, 1000);

    setTimeout(() => { this.isPrint = true; }, 1000);

  }

  onPayment() {
  }

}
