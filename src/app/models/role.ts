export enum Role {
    Doctor = 'Doctor',
    Nurse = 'Nurse',
    Pharmacy = 'Pharmacy',
    Admin = 'Admin',
    SuperAdmin = 'SuperAdmin',
    LabInvestigator = 'LabInvestigator',
    JuniorDoctor = 'JuniorDoctor',
    Billing = 'Billing',
    POCreator ='POCreator'

  }
