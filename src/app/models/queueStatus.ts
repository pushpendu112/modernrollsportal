export enum QueueStatus {
    Rejected = '0',
    Planning = '1',
    RollCasting = '2',
    QualityCheck = '3',
    Machining = '4',
    Inspection = '5',
    GrovingCentreFinishing = '6',
    BearingShapperFinishing = '7',
    GrovingCentreFinal = '8',
    BearingShapperFinal = '9',
    FinalInspection = '10',
    TransportFinish = '11',
    TestCertificateFinish = '12',
    PackagingFinish = '13',
    FinalReport = '14'

}