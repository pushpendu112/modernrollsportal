import { Injectable, OnInit } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import {Injector} from '@angular/core';
import { RoleService } from '../services';
import { User, Role  } from '../../models';
@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor, OnInit {
    constructor( private getRoleService: RoleService,
                 private inj: Injector) {
    }
    // roleData: User[];
    ngOnInit() {

    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const roleData: User[] = [
            { id: 1, username: 'Nurse', password: 'Nurse', role: Role.Nurse },
            { id: 2, username: 'Doctor', password: 'Doctor',  role: Role.Doctor },
            { id: 3, username: 'Pharmacy', password: 'Pharmacy',  role: Role.Pharmacy },
            { id: 4, username: 'Admin', password: 'Admin',  role: Role.Admin },
            { id: 5, username: 'SuperAdmin', password: 'SuperAdmin',  role: Role.SuperAdmin },
            { id: 6, username: 'LabInvestigator', password: 'LabInvestigator',  role: Role.LabInvestigator },
            { id: 7, username: 'JuniorDoctor', password: 'JuniorDoctor',  role: Role.JuniorDoctor },
            { id: 8, username: 'Billing', password: 'Billing',  role: Role.Billing }
        ];
            // const rolelist = this.inj.get(RoleService);
            //  const roleData = rolelist.getRole()[0];
            // const roleData = roleDetails;
        //    this.getRoleService.getRole().subscribe(roles => {
        //     this.roleData = roles.roleList;
        const authHeader = request.headers.get('Authorization');
        const isLoggedIn = authHeader && authHeader.startsWith('Bearer fake-jwt-token');
        const roleString = isLoggedIn && authHeader.split('.')[1];
        const role = roleString ?  Role[roleString] : null;


        // wrap in delayed observable to simulate server api call
        return of(null).pipe(mergeMap(() => {

            // authenticate - public
            if (request.url.endsWith('/users/authenticate') && request.method === 'POST') {
                const user = roleData.find(x => x.username === request.body.username && x.password === request.body.password);
                if (!user) { return error('Username or password is incorrect Please Check it'); }
                return ok({
                    id: user.id,
                    username: user.username,
                    // firstName: user.firstName,
                    // lastName: user.lastName,
                     role: user.role,
                     token: `fake-jwt-token.${user.role}`
                });
            }

            // get user by id - admin or user (user can only access their own record)
              // get user by id - admin or user (user can only access their own record)
            // if (request.url.match(/\/users\/\d+$/) && request.method === 'GET') {
            //     if (!isLoggedIn) { return unauthorised(); }

            //     // get id from request url
            //     const urlParts = request.url.split('/');
            //     console.log('urlParts');
            //     console.log(urlParts);
            //     const id = parseInt(urlParts[urlParts.length - 1]);

            //     // only allow normal users access to their own record
            //     const currentUser = roleData.find(x => x.role === role);
            //     if (id !== currentUser.id && role !== 'Nurse') { return unauthorised(); }
            //     const user = roleData.find(x => x.id === id);
            //     return ok(user);
            // }

            // // get all users (admin only)
            // if (request.url.endsWith('/users') && request.method === 'GET') {
            //     if (role !== 'Nurse') { return unauthorised(); }
            //     return ok(roleData);
            // }

            // pass through any requests not handled above
            return next.handle(request);
        }))
     // call materialize and dematerialize to ensure delay even if
     // an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
        .pipe(materialize())
        .pipe(delay(500))
        .pipe(dematerialize());

        // private helper functions

        function ok(body) {
            return of(new HttpResponse({ status: 200, body }));
        }

        function unauthorised() {
            return throwError({ status: 401, error: { message: 'Unauthorised' } });
        }

        function error(message) {
            return throwError({ status: 400, error: { message } });
        }
    }
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};
