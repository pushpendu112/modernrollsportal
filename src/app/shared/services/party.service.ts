import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PartyDetails } from '../modals/partyDetails';
import { HttpService } from 'src/app/base/http.service';
import { AppConstants } from 'src/app/base/appconstants';


@Injectable({
    providedIn: 'root'
})
export class PartyService {
    private readonly BASE_URL = AppConstants.post_Web_URl;
    poDetailsId: string;
    poId: string;
    constructor(private httpservice: HttpService) {

    }

    createParty(reqBody, headers) {
        // return this.http.post(this.BASE_URL + '/create', reqBody);
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/createParty', reqBody, '', headers);
    }

    updateParty(reqBody, headers) {
        // return this.http.post(this.BASE_URL + '/create', reqBody);
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/updateParty', reqBody, '', headers);
    }

    getAllParty(headers) {
        return this.httpservice.httpGetObserevable(this.BASE_URL + 'basicNode/services/getAllParties', '' + headers)
    }

    createChemicalComposition(reqBody, headers) {
        // return this.http.post(this.BASE_URL + '/create', reqBody);
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/createChemComp', reqBody, '', headers);
    }

    getAllChemicalComposition(headers) {
        return this.httpservice.httpGetObserevable(this.BASE_URL + 'basicNode/services/getAllChemComp', '' + headers)
    }

    updateChemicalComposition(reqBody, headers) {
        // return this.http.post(this.BASE_URL + '/create', reqBody);
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/updateChemComp', reqBody, '', headers);
    }

    removeChemicalComposition(_id, headers) {
        return this.httpservice.httpGetObserevable(this.BASE_URL + `basicNode/services/removeChemComp/${_id}`, '' + headers)
    }

    removeParty(_id, headers) {
        return this.httpservice.httpGetObserevable(this.BASE_URL + `basicNode/services/removeParty/${_id}`, '' + headers)
    }

    createPODetails(reqBody, headers) {
        // return this.http.post(this.BASE_URL + '/create', reqBody);
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/createPODetails', reqBody, '', headers);
    }

    createPOProduct(reqBody, headers) {
        // return this.http.post(this.BASE_URL + '/create', reqBody);
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/createPOProduct', reqBody, '', headers);
    }

    getAllPoProducts(headers, _id) {
        return this.httpservice.httpGetObserevable(this.BASE_URL + `basicNode/services/getAllPOProduct/${_id}`, '' + headers)
    }

    removePoProduct(_id, headers) {
        return this.httpservice.httpGetObserevable(this.BASE_URL + `basicNode/services/removePOProduct/${_id}`, '' + headers)
    }

    UpdatePOProduct(reqBody, headers) {
        // return this.http.post(this.BASE_URL + '/create', reqBody);
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/updatePOProduct', reqBody, '', headers);
    }

    UpdatePODetails(reqBody, headers) {
        // return this.http.post(this.BASE_URL + '/create', reqBody);
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/updatePODetails', reqBody, '', headers);
    }

    getAllPoDetails(headers) {
        return this.httpservice.httpGetObserevable(this.BASE_URL + 'basicNode/services/getAllPODetails', '' + headers)
    }

    fetchAllPoProducts(headers) {
        return this.httpservice.httpGetObserevable(this.BASE_URL + 'basicNode/services/getAllPOProductForModernRolls', '' + headers)
    }

    getDispatchPOProductForModernRolls(URL, headers) {
        return this.httpservice.httpGetObserevable(this.BASE_URL + URL, '' + headers)
    }

    updateQueueStatus(reqBody, headers) {
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/updateQueueStatus', reqBody, '', headers);
    }

    getAllPOProductByQueueStatus(headers, status) {
        return this.httpservice.httpGetObserevable(this.BASE_URL + `basicNode/services/getAllPOProductByQueueStatus/${status}`, '' + headers)
    }

    getAllHoldProducts(headers) {
        return this.httpservice.httpGetObserevable(this.BASE_URL + `basicNode/services/getAllHoldProducts`, '' + headers)
    }

    searchPOProductByQueueStatus(reqBody, headers) {
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/searchPOProductByQueueStatus', reqBody, '', headers);
    }

    searchPOProductByQueueStatusForMasterTable(reqBody, headers) {
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/searchPOProductByQueueStatusForMasterTable', reqBody, '', headers);
    }

    fetchAllPoProductsForHistory(reqBody, headers) {
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/fetchAllPoProductsForHistory', reqBody, '', headers);
    }

    updateQueueData(reqBody, headers, id) {
        return this.httpservice.httpPostObservable(this.BASE_URL + `basicNode/services/updateQueueData/${id}`, reqBody, '', headers);
    }

    getChemCompByCCId(headers, ccId) {
        return this.httpservice.httpGetObserevable(this.BASE_URL + `basicNode/services/getChemCompByCCId/${ccId}`, '' + headers)
    }

    tranferDisplayId(reqBody, headers) {
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/tranferDisplayId', reqBody, '', headers);
    }

    pathRoll(reqBody, headers) {
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/pathRoll', reqBody, '', headers);
    }

    moveRollToProcess(reqBody, headers) {
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/moveRollToProcess', reqBody, '', headers);
    }

    holdAndReplanRoll(reqBody, headers) {
        return this.httpservice.httpPostObservable(this.BASE_URL + 'basicNode/services/holdAndReplanRoll', reqBody, '', headers);
    }

}