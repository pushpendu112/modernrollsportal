import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TabledataService {
  private roleUrl = '/assets/json/tabledata.json';
  constructor(private http: HttpClient) {
    this.getTableData().subscribe(data => {
      console.log(data);
    });
    this.getUserByPhoneNo('Victoria Cantrell').subscribe(data => {
      console.log('data of Name');
      console.log(data);
    });
  }

  /** GET rool from the server */
  getTableData(): Observable<any> {
    console.log(this.roleUrl);
    return this.http.get(this.roleUrl);
      // .pipe(
      //   // map((response: Response) => response),
      //   tap(_ => this.log('Calling DatableData')),
      //   catchError(this.handleError<any>('getRole', []))
      // );
  }

  getUserByPhoneNo(pname: string) {
  return this.http.get<any[]>(`${this.roleUrl}?patientName=${pname}`);
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  private log(message: string) {
    console.log(`TableData Services : ${message}`);
  }
}
