import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private roleUrl = '/assets/json/role.json';
  constructor(private http: HttpClient) {
    this.getRole().subscribe(data => {
            // console.log(data);
        });
   }

  /** GET rool from the server */
  getRole(): Observable<any> {
    return this.http.get(this.roleUrl)
      .pipe(
        // map((response: Response) => response),
        tap(_ => this.log('Calling Service')),
        catchError(this.handleError<any>('getRole', []))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  private log(message: string) {
    //console.log(`Role Services : ${message}`);
  }
}
