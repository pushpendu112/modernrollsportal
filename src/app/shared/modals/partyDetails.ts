export class PartyDetails {
    name: string;
    address: string;
    contactperson: string;
    contactmobile: string;
    contactemail: string;
    gstin: string;
}