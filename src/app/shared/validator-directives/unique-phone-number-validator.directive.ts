import { Directive } from '@angular/core';
import { AsyncValidator, AbstractControl, ValidationErrors, NG_ASYNC_VALIDATORS, AsyncValidatorFn } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { TabledataService } from '../services/tabledata.service';

export function uniquePhoneNumberValidator(tabledataService: TabledataService): AsyncValidatorFn {
  return (c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
    return tabledataService.getUserByPhoneNo(c.value).pipe(
      map(phonenumber => {
        return phonenumber && phonenumber.length > 0 ? { 'uniquePhoneNumber': true } : null;
      })
    );
  };
}

@Directive({
  selector: '[uniquePhoneNumber]',
  providers: [{ provide: NG_ASYNC_VALIDATORS, useExisting: UniquePhoneNumberValidatorDirective, multi: true }]
})
export class UniquePhoneNumberValidatorDirective implements AsyncValidator {

  constructor(private tabledataService: TabledataService) { }

  validate(c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return uniquePhoneNumberValidator(this.tabledataService)(c);
  }

}
