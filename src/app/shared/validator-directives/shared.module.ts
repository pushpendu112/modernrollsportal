import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UniquePhoneNumberValidatorDirective } from './unique-phone-number-validator.directive';

@NgModule({
  imports: [],
  declarations: [
    UniquePhoneNumberValidatorDirective
  ],
  exports: [
    CommonModule,
    UniquePhoneNumberValidatorDirective
  ]
})
export class SharedModule { }
