import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasePatientInfoComponent } from './base-patient-info.component';

describe('BasePatientInfoComponent', () => {
  let component: BasePatientInfoComponent;
  let fixture: ComponentFixture<BasePatientInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasePatientInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasePatientInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
