import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasePatientInfoComponent } from './base-patient-info.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WebcamModule } from 'ngx-webcam';



@NgModule({
  declarations: [BasePatientInfoComponent],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    WebcamModule,
    ReactiveFormsModule
  ],
  exports: [BasePatientInfoComponent]


})
export class BasePatientInfoModule {
  open: any;
}
