import { Component, ViewEncapsulation, OnInit, DoBootstrap } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TestDataService } from '../shared/services/test-data.service';
import { NgbCalendar, NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';

import { Subject } from 'rxjs';
import { WebcamImage } from 'ngx-webcam';
// import {Observable} from "rxjs/Observable";
import { Observable } from 'rxjs';
import { BasePatinetInfoService } from './base-patinet-info.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import * as CryptoJS from 'crypto-js';
declare var $: any;   // not required

import { tap } from 'rxjs/operators';
import { PharmacyQueueComponent } from '../layout/nurse/components/pharmacy-queue/pharmacy-queue.component';
import { Router } from '@angular/router';




// import { VgStreamingModule } from 'videogular2/streaming';

@Component({
  selector: 'app-base-patient-info',
  templateUrl: './base-patient-info.component.html',
  styleUrls: ['./base-patient-info.component.scss'],
  providers: [DatePipe]
})
export class BasePatientInfoComponent implements OnInit {
  private readonly notifier: NotifierService;
  image: any;
  imageUrl: any;

  age1: any;
  submitted = false;
  paymentvalue: any;
  paymentmodevalue: any;
  lpatientId: any;
  imagePhoto: boolean = false;
  patientGender: boolean = false;
  constructor(private modalService: NgbModal,
    private parentPassData: TestDataService,
    private calendar: NgbCalendar,
    private formBuilder: FormBuilder,
    private basePatinetInfoService: BasePatinetInfoService,
    private datePipe: DatePipe,
    private patientQueue: PharmacyQueueComponent,
    private router: Router,
    private spinner: NgxSpinnerService,
    private notifierService: NotifierService) {
    this.notifier = notifierService;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }
  // uploadedFiles: any[] = [];
  Secret = 'i_have_some_small_master_secret_live_pin';
  getjwttoken: any;
  userToken: any;
  existpatientId: any;

  radioDOB = true;
  isChecked = false;
  radioAge1 = false;
  radioDOB1 = false;
  isChecked1 = false;
  centerId: any;

  loader = false;

  imageicon = true;
  existimage = false;


  newphone = true;
  existphone = false;
  buttonDisabled = false;


  Selected = 'dob';
  dobSelected = false;
  ageSelected = false;



  date4: any;
  date5: any;
  month5: any;
  year5: any;
  newexistpatient: any;
  vinoth: any;

  existPatientObject: any;
  mobileNumber: any;
  patientDetails: any;
  gender: any;
  date: any;
  date1: any;
  month1: any;
  year1: any;
  date2: any;
  patientObject: any;
  patientForm: FormGroup;

  public seconds: number;
  private trigger: Subject<void> = new Subject<void>();
  public webcamImage: WebcamImage = null;
  decodedString: any;
  base64string: any;
  selectedLink2 = false;
  selectedLink3 = false;

  model: NgbDateStruct;
  closeResult: string;
  hideNave = true;
  pushRightClass: any;

  isPayNowDisabled: boolean;

  lastVisitedDate1: any;

  maleGenderActive = false;
  feMaleGenderActive = false;
  otherGenderActive = false;

  private selectedLink = 'DOB';
  getGenderValue: any;
  lorgId: any;
  lempId: any;

  PaymentType: any = [{ 'name': 'Cash', 'value': 0 }, { 'name': 'Online', 'value': 1 }];




  ngOnInit() {
    this.createPatientForm();

    this.getjwttoken = localStorage.getItem('jwttoken');
    this.userToken = this.decryptData(this.getjwttoken);
    this.centerId = JSON.parse(localStorage.getItem('centerId'));
    this.lorgId = JSON.parse(localStorage.getItem('orgId'));
    this.lempId = JSON.parse(localStorage.getItem('_id'));
    // this.lpatientId = JSON.parse(localStorage.getItem('patientId'));
    // console.log('##############################$44444444444444444444447777777777777777777', this.centerId);

  }
  createPatientForm() {
    this.patientForm = this.formBuilder.group({
      orgId: '',
      centerId: '',
      employeeId: '',
      mobileNumber: ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10)])],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      dob: new Date(),
      age: 0,
      emailId: ['', [Validators.required, Validators.email]],
      gender: '',
      imageUrl: '',
      registrationPaymentMode: 0,
      registrationFee: 0,
      height: '',
      weight: '',
      address: this.formBuilder.array([
        this.formBuilder.group({
          addressType: 0,
          address1: '',
          address2: '',
          zipCode: ''
        }),
        this.formBuilder.group({
          addressType: 0,
          address1: '',
          address2: '',
          zipCode: ''
        }),
        this.formBuilder.group({
          personName: '',
          personMobile: 1234567890,
          personEmailId: '',
          addressType: 0,
          address1: '',
          address2: '',
          zipCode: ''
        })
      ]),
      maritalLife: 0,
      maritalStatus: 0,
      diteType: 0,
      usedHomeopathic: 0,
      maleChildren: 0,
      femaleChildren: 0,
    });

  }

  changePaymentType($event) {
    console.log($event.target.value);
    // tslint:disable-next-line: no-unused-expression
    this.paymentmodevalue = parseInt($event.target.value);
    console.log('this.paymentmodevalue', this.paymentmodevalue);
    this.patientForm.get('registrationPaymentMode').setValue($event.target.value, {
      onlySelf: true
    });
  }
  get f() {
    return this.patientForm.controls;
  }

  onSubmit() {
    this.spinner.show();
    this.submitted = true;

    if (this.patientForm.invalid) {
      this.spinner.hide();
      return;
    }
    // this.modalService.dismissAll();
    // this.patientForm.reset();

    // console.log('agggggggggggggggggggeeeeeeeeee', this.age );
    if (this.model === undefined) {
      this.date2 = '';
    } else if (this.model === null) {
      this.date2 = '';
    } else {
      this.date = this.model;
      this.date1 = (this.date.day);
      this.month1 = this.date.month;
      this.year1 = this.date.year;
      this.date2 = new Date(this.month1 + '/' + this.date1 + '/' + this.year1).toISOString();
      // this.dateUTC = this.date2.toISOString();
    }
    // console.log('this.webcamImage@@@@@@@', this.webcamImage);

    // alert('Outside');
    if (this.webcamImage === null) {
      this.imagePhoto = true;
      this.spinner.hide();
    }
    console.log('this.getGenderValue@@@@@@@', this.getGenderValue);
    if (this.getGenderValue === undefined) {
      this.patientGender = true;
      this.spinner.hide();
    }
    // this.date = this.model;  this.getGenderValue
    // this.date1 = this.date.day;
    // this.month1 = this.date.month;
    // this.year1 = this.date.year;
    // this.date2 = new Date(this.month1 + '/' + this.date1 + '/' + this.year1);


    this.patientForm.get('orgId').setValue(this.lorgId);
    this.patientForm.get('employeeId').setValue(this.lempId);
    this.patientForm.get('centerId').setValue(this.centerId);
    this.patientForm.get('gender').setValue(this.getGenderValue);
    this.patientForm.get('imageUrl').setValue(this.webcamImage.imageAsDataUrl);
    this.patientForm.get('dob').setValue(this.date2);
    this.patientForm.get('registrationPaymentMode').setValue(this.paymentmodevalue);
    this.patientForm.get('registrationFee').setValue(parseFloat(this.patientForm.value.registrationFee));



    this.basePatinetInfoService.getpatientdetails(this.patientForm.value).subscribe(data => {
      // console.log(data);
      this.spinner.hide();
      this.patientObject = data;
      this.patientQueue.getExistingPatientQueuees();
      // console.log('this.patientQueue.getExistingPatientQueuees();' );
      // console.log('this.patientObject =======' + JSON.stringify(this.patientObject));
    },
      err => {
        this.spinner.hide();
        this.notifier.notify('error', 'Something went wrong please try again! ')
        console.log(err);
      }
    );
    // this.modalService.dismissAll();

    //  this.getExistingPatientQueuees();
    this.modalService.dismissAll();
    this.patientForm.reset();
    this.submitted = false;
  }


  createPatientQueue() {
    const PatientQueue = {
      patientId: this.existpatientId,
      orgId: JSON.parse(localStorage.getItem('orgId')),
      centerId: JSON.parse(localStorage.getItem('centerId')),
      presentQueueType: 0,
      durationList: []
    };

    this.basePatinetInfoService.createPatientQueue(PatientQueue).subscribe(data => {
      console.log('aaaaaaaaaaaaaaaaaaaaaa', data);
      this.patientQueue.getExistingPatientQueuees();
      // alert('iam in creating position');
      // console.log('sssssssssssssssssss' + JSON.stringify(this.patientQueueObject));
    });

    this.dismisModel();
    // this.patientForm.reset();
  }

  onclear() {
    this.patientForm.reset();
  }

  dismisModel() {
    this.modalService.dismissAll();
    this.patientForm.reset();
    this.imageicon = true;
    this.existimage = false;
    this.dobSelected = false;
    this.ageSelected = false;
    this.isChecked = false;
    this.isChecked1 = false;
    this.existphone = false;
    this.imageicon = true;
    this.webcamImage = null;
    this.newphone = true;
    this.selectedLink2 = false;
    this.buttonDisabled = false;
    this.patientForm.get('gender').setValue(this.genderSetActive(''));
  }

  // this.loginservice.getlogin(this.loginFormvalues).subscribe(data => {
  //   this.userObject = data;
  //   // console.log('userobject', this.userObject);
  //   console.log('this.userobject===' + JSON.stringify(this.userObject));



  open(content) {
    // For side bar closing;
    this.hideNave = false;
    this.parentPassData.notifyOther({ option: 'event-hideNave', value: this.hideNave });
    // For creation of Modal as a Slide;
    this.modalService.open(content, { windowClass: 'positionRight' }).result.then((result) => {

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      // For sidebar Opening;
      this.hideNave = true;
      this.parentPassData.notifyOther({ option: 'event-hideNave', value: this.hideNave });
    });
  }
  private getDismissReason(reason: any): string {

    this.selectedLink2 = false;
    this.dismisModel();

    return `with: ${reason}`;
  }
  isDisabled = (date: NgbDate, current: { month: number }) => date.month !== current.month;
  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6;



  onDobSelection(dateObj) {
    this.isChecked = true;
    this.dobSelected = true;
    this.ageSelected = false;
    this.model = dateObj;
  }
  onAgeSelection(value) {
    this.ageSelected = true;
    this.dobSelected = false;
    // this.isChecked = true;
  }

  genderActive(event) {
    const target = event.target || event.srcElement || event.currentTarget;
    const idAttr = target.attributes.id;
    const value = idAttr.nodeValue;
    if (value === 'Male') {
      this.maleGenderActive = true;
      this.feMaleGenderActive = false;
      this.otherGenderActive = false;
      this.getGenderValue = value;
    } else if (value === 'FeMale') {
      this.feMaleGenderActive = true;
      this.maleGenderActive = false;
      this.otherGenderActive = false;
      this.getGenderValue = value;
    } else if (value === 'Other') {
      this.otherGenderActive = true;
      this.maleGenderActive = false;
      this.feMaleGenderActive = false;
      this.otherGenderActive = true;
      this.getGenderValue = value;
    } else {
      this.maleGenderActive = false;
      this.feMaleGenderActive = false;
      this.otherGenderActive = false;
      this.getGenderValue = '';
    }
  }

  genderSetActive(value) {
    // alert(value);
    if (value === 'Male') {
      this.maleGenderActive = true;
      this.feMaleGenderActive = false;
      this.otherGenderActive = false;
    } else if (value === 'FeMale') {
      this.feMaleGenderActive = true;
      this.maleGenderActive = false;
      this.otherGenderActive = false;
    } else if (value === 'Other') {
      this.otherGenderActive = true;
      this.maleGenderActive = false;
      this.feMaleGenderActive = false;
      this.otherGenderActive = true;
    } else {
      this.maleGenderActive = false;
      this.feMaleGenderActive = false;
      this.otherGenderActive = false;
    }
  }
  opencontent() {
    this.selectedLink2 = !this.selectedLink2;
    {
      this.selectedLink3 = true;
      setTimeout(() => {
        this.selectedLink3 = false;
      }, 3000);
    }
  }

  public triggerSnapshot(): void {
    this.seconds = 3;
    setTimeout(() => {
      this.seconds = 2;
      setTimeout(() => {
        this.seconds = 1;
        setTimeout(() => {
          this.trigger.next();
          this.seconds = null;

        }, 0);
      }, 0);
    }, 0);
  }


  public handleImage(webcamImage: WebcamImage): void {
    console.info('received webcam image', webcamImage);
    this.webcamImage = webcamImage;
    console.log('ssssssssssssssssssssssssssss:', webcamImage.imageAsBase64);
  }

  onKeyup($event) {

    this.mobileNumber = $event.target.value;
    if (this.mobileNumber.length === 10) {
      this.loader = true;
      const headers = new Headers(
        {
          'accept': 'application/json',
          'x-access-token': this.userToken
        });
      const reqbody = this.mobileNumber;
      // console.log(this.patientDetails);
      this.basePatinetInfoService.getExistigPatientDetails(reqbody, headers).subscribe(data => {
        this.existPatientObject = JSON.parse(JSON.stringify(data));
        console.log(data);
        // console.log('qqqqqqqqqqqqqqqqqq', this.existPatientObject.patientData.age);
        if (this.existPatientObject.patientData.length === 0) {
          this.loader = false;
        }

        if (this.existPatientObject.patientData.length !== 0) {
          this.loader = false;
          this.existphone = true;
          this.newphone = false;
          this.buttonDisabled = true;

          this.existPatientObject.patientData.forEach(element => {
            const age = element.age;
            this.lastVisitedDate1 = element.lastVisitedDate;
            if (age === 0) {
              this.radioAge1 = true;
              this.patientForm.get('gender').setValue(this.genderSetActive(element.gender));
            } else {
              this.radioAge1 = false;
            }
            const dateofbirth = element.dob;
            if (dateofbirth === 0) {
              this.patientForm.get('age').setValue(this.onAgeSelection(element.age));
              this.radioDOB1 = true;
            }

            this.existpatientId = element._id;
            console.log('iddddddddddddddddddddd', this.existpatientId);
          });
        }

        console.log(`##############this.existPatientObject.patientData`);
        console.log(JSON.stringify(this.existPatientObject.patientData));
        // console.log('aaaaaaaaaaaaaaaaaaaaa', data.patientData.age);

        this.newexistpatient = JSON.stringify(this.existPatientObject.patientData);

        this.existPatientObject.patientData.forEach(element => {
          if (element.firstName) {
            this.patientForm.patchValue({
              firstName: element.firstName,
              lastName: element.lastName,
              emailId: element.emailId,
              imageUrl: element.imageUrl,
              gender: element.gender,
              dob: element.dob,
              age: element.age
            });


            this.date4 = new Date(element.dob);
            this.year5 = this.datePipe.transform(new Date(element.dob), 'yyyy');
            this.month5 = this.datePipe.transform(new Date(element.dob), 'MM');
            this.date5 = this.datePipe.transform(new Date(element.dob), 'dd');

            console.log(this.year5);
            console.log(this.month5);
            console.log(this.date5);

            const dateObj = {
              year: parseInt(this.year5),
              month: parseInt(this.month5),
              day: parseInt(this.date5)
            };
            console.log(dateObj);

            this.patientForm.get('dob').setValue(this.onDobSelection(dateObj));
            this.imageUrl = element.imageUrl;

            if (this.imageUrl) {
              this.imageicon = false;
              this.existimage = true;
            }
            // console.log(this.image);
          }
        });
      });
    }
    this.toErasePrePopulateData();
  }

  toErasePrePopulateData() {
    if (this.mobileNumber.length !== 10) {
      this.imageicon = true;
      this.existimage = false;
      this.dobSelected = false;
      this.ageSelected = false;
      this.isChecked = false;
      this.isChecked1 = false;
      this.radioAge1 = false;
      this.radioDOB1 = false;
      this.existphone = false;
      this.newphone = true;
      this.buttonDisabled = false;
      this.patientForm.get('gender').setValue(this.genderSetActive(''));
      this.patientForm.get('firstName').reset();
      this.patientForm.get('lastName').reset();
      this.patientForm.get('emailId').reset();
    }
  }
  decryptData(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, this.Secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }
  onPayment() {
    this.router.navigate(['/dashboard/billingreceipt/billingReceipt']);
    // $('#exampleModal').modal('hide');

  }
}
