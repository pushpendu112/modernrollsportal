import { Injectable } from '@angular/core';
import { HttpService } from '../base/http.service';
import { AppConstants } from '../base/appconstants';

@Injectable({
    providedIn: 'root'
})
export class BasePatinetInfoService {
    // subscribe(arg0: (data: any) => void) {
    //   throw new Error('Method not implemented.');
    // }

    constructor(private httpservice: HttpService) { }


    getpatientdetails(patientformvalues: any) {
        const reqbody = {
            orgId: patientformvalues.orgId,
            centerId: patientformvalues.centerId,
            employeeId: patientformvalues.employeeId,
            mobileNumber: patientformvalues.mobileNumber,
            firstName: patientformvalues.firstName,
            lastName: patientformvalues.lastName,
            dob: patientformvalues.dob,
            age: patientformvalues.age,
            emailId: patientformvalues.emailId,
            gender: patientformvalues.gender,
            imageUrl: patientformvalues.imageUrl,
            registrationPaymentMode: patientformvalues.registrationPaymentMode,
            registrationFee: patientformvalues.registrationFee,
            height: patientformvalues.height,
            weight: patientformvalues.weight,
            address: patientformvalues.address,
            maritalLife: patientformvalues.maritalLife,
            diteType: patientformvalues.diteType,
            usedHomeopathic: patientformvalues.usedHomeopathic,
            maleChildren: patientformvalues.maleChildren,
            femaleChildren: patientformvalues.femaleChildren
        };

        return this.httpservice.httpPostObservable(AppConstants.post_Create_Patient, reqbody, '');
    }

    getExistigPatientDetails(reqbody, headers) {
        return this.httpservice.httpGetObserevable(AppConstants.post_getPatient_ByMobileNumber + reqbody, '', headers);
    }

    createPatientQueue(patinetDetails: any) {
        const reqbody = {
            patientId: patinetDetails.patientId,
            orgId: patinetDetails.orgId,
            centerId: patinetDetails.centerId,
            presentQueueType: patinetDetails.presentQueueType,
            durationList: patinetDetails.durationList
        };
        return this.httpservice.httpPostObservable(AppConstants.post_Create_PatientQueue, reqbody, '');
      }
    }
