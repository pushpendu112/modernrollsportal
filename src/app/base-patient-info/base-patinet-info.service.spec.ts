import { TestBed } from '@angular/core/testing';

import { BasePatinetInfoService } from './base-patinet-info.service';

describe('BasePatinetInfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BasePatinetInfoService = TestBed.get(BasePatinetInfoService);
    expect(service).toBeTruthy();
  });
});
