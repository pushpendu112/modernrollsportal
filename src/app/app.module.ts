import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LanguageTranslationModule } from './shared/modules/language-translation/language-translation.module';
import { DataTableModule } from 'primeng/datatable';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxViewerModule } from 'ngx-viewer';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { AccordionModule } from 'primeng/accordion';     // accordion and accordion tab
import { MenuItem } from 'primeng/api';
// import { BaseModule } from './base/base.module';
import { fakeBackendProvider } from './shared/guard/fake-backend';
import { JwtInterceptor } from './shared/guard/jwt.interceptor';
// import { ErrorInterceptor } from './shared/guard/error.interceptor';

const notifierDefaultOptions: NotifierOptions = {
    position: {
        horizontal: {
            position: 'right',
            distance: 12
        },
        vertical: {
            position: 'bottom',
            distance: 12,
            gap: 10
        }
    },
    theme: 'material',
    behaviour: {
        autoHide: 5000,
        onClick: false,
        onMouseover: 'pauseAutoHide',
        showDismissButton: true,
        stacking: 4
    },
    animations: {
        enabled: true,
        show: {
            preset: 'slide',
            speed: 300,
            easing: 'ease'
        },
        hide: {
            preset: 'fade',
            speed: 300,
            easing: 'ease',
            offset: 50
        },
        shift: {
            speed: 300,
            easing: 'ease'
        },
        overlap: 150
    }
};

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        LanguageTranslationModule,
        AppRoutingModule,
        DataTableModule,
        AccordionModule,
        NgxSpinnerModule,
        NgxViewerModule,
        // BaseModule,
        NotifierModule.withConfig(
            notifierDefaultOptions
        )
    ],
    exports: [
        BrowserAnimationsModule,
        DataTableModule,
        AccordionModule,
    ],
    declarations: [AppComponent],
    providers: [
        // AuthGuard,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        // { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

        // provider used to create fake backend
        fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
